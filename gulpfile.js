'use strict';

//require node modules
var gulp = require('gulp');
var del = require('del');
var runSequence = require('run-sequence');

//gulp-plugin
var $ = require('gulp-load-plugins')();

/**
 * config
 */
var _site_path = '';

var _config = {
  src_path:        './_src' + _site_path,
  dest_path:       './dest' + _site_path,
  archive_path:    './archive'  //created zip archive file directory
};

/**
 * sass
 */
gulp.task('sass', function () {
  var path = _config.src_path + '/resources/_scss';
  var dest_path = _config.src_path + '/resources/css';
  var files = [
    path + '/**/*.scss'
  ];
  return gulp.src(files)
    .pipe($.sass({
      outputStyle: 'compressed'  //set compile mode (nested|expanded|compact|compressed)
    })
    .on('error', $.sass.logError))
    .pipe(gulp.dest(dest_path));
});

/**
 * auto prefixer
 */
gulp.task('autoprefixer', function () {
  var path = _config.src_path + '/resources/css';
  var files = [
    path + '/**/*.css'
  ];
  return gulp.src(files)
    .pipe($.autoprefixer({
      browsers: ['last 2 versions']  //set target browser.
    }))
    .pipe(gulp.dest(path));
});

/**
 * js lint
 */
gulp.task('lint', function() {
  var path = _config.src_path + '/resources/js';
  var files = [
    path + '/script.js'
  ];
  return gulp.src(files)
    .pipe($.jshint('.jshintrc'))
    .pipe($.jshint())
    .pipe($.jshint.reporter());
});

/**
 * concat
 */
gulp.task('concat', function() {
  var path = _config.src_path + '/resources/js/vendor';
  var files = [
    path + '/_libs/jquery-1.11.3.min.js',
    path + '/_libs/js.cookie.js',
    path + '/_libs/underscore-min.js',
    path + '/_libs/moment/moment.min.js',
    path + '/_libs/moment/lang/ja.js',
    path + '/_libs/jquery.slick.min.js',
    path + '/_libs/jquery.magnific-popup.js'
  ];
  return gulp.src(files)
    .pipe($.concat('libs.js'))
    .pipe(gulp.dest(path));
});

/**
 * uglify
 */
gulp.task('uglify', function() {
  var path = _config.src_path + '/resources/js';
  var files = [
    path + '/libs.js'
  ];
  return gulp.src(files)
    .pipe($.uglify())
    .pipe(gulp.dest(path));
});

/**
 * icon font
 */
gulp.task('svgmin', function () {
  return gulp.src(['./_src/resources/_fonts/*.svg'])
    .pipe($.svgmin())
    .pipe(gulp.dest('./_src/resources/_fonts'));
});

gulp.task('iconfont', function () {

  return gulp.src(['./_src/resources/_fonts/*.svg'])
    .pipe($.iconfontCss({
      fontName: 'iconfont',
      path: './_src/resources/_scss/setting/template/_iconfont.scss',  // scssテンプレート
      targetPath: '../_scss/setting/_iconfont.scss',  // scssを生成
      fontPath: "../fonts/"  // scssで使うフォントのパス
    }))
    .pipe($.iconfont({
      fontName: 'iconfont',  //set fonts file name.
      normalize:true,
      appendUnicode: true,  // recommended option
      formats: ['ttf', 'eot', 'woff', 'woff2', 'svg']
      //appendCodepoints: true
    }))
    .pipe(gulp.dest('./_src/resources/fonts'));
});

/**
 * css tasks
 */
gulp.task('css', function (cb) {
  runSequence(
    'sass',
    'autoprefixer',
    cb
  );
});

/**
 * js tasks
 */
gulp.task('js', function (cb) {
  runSequence(
    'lint',
    'concat',
    //'uglify',
    cb
  );
});

/**
 * font tasks
 */
gulp.task('font', function (cb) {
  runSequence(
    'svgmin',
    'iconfont',
    cb
  );
});

/**
 * monitoring files for task
 * $ gulp watch
 */
gulp.task('watch', function() {
  gulp.watch([_config.src_path + '/resources/_scss/**/*.scss'], ['css']);
  gulp.watch([_config.src_path + '/resources/js/*.js'], ['js']);
});
gulp.task('default', ['watch']);

/**
 * build release image
 * $ gulp release
 */
// clean release image
gulp.task('release:clean', function() {
  return del(['dest']);
});

// create release directory & copy all files
gulp.task('release:copy', function () {
  return gulp.src([_config.src_path + '/**/**'], {base: _config.src_path})
    .pipe(gulp.dest(_config.dest_path));
});

// clean some directory & files
gulp.task('release:clean_files', function () {
  return del([
    _config.dest_path + '/robots.txt',
    _config.dest_path + '/resources/_scss',
    _config.dest_path + '/resources/_fonts'
  ]);
});

// optimize image files
gulp.task('release:imagemin', function(){
  var imageminOptions = {
    optimizationLevel: 7
  };

  return gulp.src(_config.dest_path + '/**/*.+(jpg|jpeg|png|gif|svg)', {base: _config.dest_path })
    .pipe($.imagemin(imageminOptions))
    .pipe(gulp.dest(_config.dest_path));
});

// create zip archive file
gulp.task('release:zip', function () {
  var date = new Date();
  var filename = '_archive_' + date.getFullYear() + (date.getMonth() + 1) + date.getDate()+ '_' + date.getHours() + date.getMinutes() + '.zip';

  return gulp.src(_config.dest_path + '/**')
    .pipe($.zip(filename))
    .pipe(gulp.dest(_config.archive_path));
});

// release tasks
gulp.task('release', function (cb) {
  return runSequence(
    'release:clean',
    'release:copy',
    'release:clean_files',
    //'release:imagemin',
    'release:zip',
    cb
  );
});
