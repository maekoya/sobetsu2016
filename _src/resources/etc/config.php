<?php
class General
{
  public $root;        // document root path
  public $base;        // based directory path
  public $site_name;   // site name
  public $title;       // page title
  public $description; // page description

  function __construct(){
    $this->root = $_SERVER["DOCUMENT_ROOT"];
    $this->base = '';
    $this->sitename = '壮瞥町の行政情報サイト';
    $this->title = 'タイトルが入ります';
    $this->description = 'ディスクリプションが入ります';
  }

  /**
   * Simple function on htmlspecialcharacter.
   * @param  {string}
   * @return {string}
   */
  public function h($str){
    return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
  }

  /**
   * Get current date.
   * @return {string}
   */
  public function getTime(){
    return date('m月d日');
  }
}

$page = new General();
