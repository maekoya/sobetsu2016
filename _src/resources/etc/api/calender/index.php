<?php
class jsonManager
{
  private $url;
  private $param;

  function __construct(){
    if(isset($_GET['cat'])) {
      $this->param = $_GET['cat'];
    }
    $this->url = './json/events.json';

    $this->getJSON($this->url, $this->param);
  }

  /**
   *
   * @param {string}
   * @return {array}
   */
  private function getJSON($source_url, $param){
    $url = sprintf($source_url);
    $json = file_get_contents($url);

    header('Content-Type: application/json; charset=utf-8');
    echo $json;
  }
}

$json = new jsonManager();
