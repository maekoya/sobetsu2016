<?php
/**
 * 1時間に一回だけDBに保存 && DBから取得する処理を追加しなければならない。
 * See API document 'http://www.worldweatheronline.com/api/docs/local-city-town-weather-api.aspx'.
 * http://www.worldweatheronline.com/feed/wwoConditionCodes.txt
 */
class WeatherManager
{
  private $api_key;
  private $local;

  function __construct(){
    $this->api_key = '3669531ea3be561485071c57efd00';
    $this->local = 'sobetsu';

    $this->outputJSON(
      $this->getWeatherDataByLocal($this->api_key, $this->local)
    );
  }

  /**
   * Get weather data by local.
   *
   * @param {string} $api_key
   * @param {string} $local
   * @return {array}
   */
  private function getWeatherDataByLocal($api_key, $local){
    $url = sprintf('http://api.worldweatheronline.com/free/v2/weather.ashx?key=%s&q=%s&date=today&num_of_days=1&tp=1&format=json&showlocaltime=yes&fx=no&lang=ja', $api_key, $local);

    $json = file_get_contents($url);
    $json = json_decode($json);
    $current_condition = $json->data->current_condition[0];

    $time = intval(date('H'));
    $time_group = ($time >= 6 && $time < 17) ? 'day' : 'night';

    $array = array(
      'code' => $current_condition->weatherCode,
      'description' => $current_condition->lang_ja[0]->value,
      'timeGroup' => $time_group,
    );
    return $array;
  }

  /**
   * Out put JSON.
   *
   * @param {array}
   */
  private function outputJSON($array){
    $json =  json_encode($array);
    header('Content-Type: application/json; charset=utf-8');
    echo $json;
  }
}

$weather = new WeatherManager();
