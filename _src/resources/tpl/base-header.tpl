
  <noscript>
    <div class="p-head-alert">
      <p>JavaScriptが無効な設定のため一部機能が動作しません。動作させるためにはJavaScriptを有効な設定にしてください。</p>
    </div>
  </noscript>
  <!--[if lt IE 8]>
    <div class="p-head-alert">
      <p>当ウェブサイトは、Microsoft Internet Explorer 8 以上での閲覧を対象としています。快適に閲覧するためには、ご使用のウェブブラウザのバージョンを更新してください。</p>
    </div>
  <![endif]-->

  <header id="base-header">
    <div class="p-header">
      <div class="p-header__inner">
        <div class="p-header__top-inner">
          <p class="p-header__top-inner__logo"><a href="<?php echo $page->base; ?>/"><img src="<?php echo $page->base; ?>/resources/img/common/logo-1.png" width="300" height="44" alt="壮瞥町の行政情報サイト"></a></p>
          <div class="p-header__top-inner__menu-btn js-show-navigation"><span></span><span></span><span></span></div>
        </div><!-- /.p-header-top-inner -->

        <div class="p-header__controlls">
          <nav class="p-header__nav">
            <ul class="p-header__nav__list js-show-under-nav">
              <li class="p-header__nav__list__item"><a href="<?php echo $page->base; ?>/">トップ</a></li>
              <li class="p-header__nav__list__item"><a href="<?php echo $page->base; ?>/about/" data-nav-target="about">まちの紹介</a></li>
              <li class="p-header__nav__list__item"><a href="<?php echo $page->base; ?>/kurashi/" data-nav-target="kurashi">暮らし</a></li>
              <li class="p-header__nav__list__item"><a href="<?php echo $page->base; ?>/chosei/" data-nav-target="chosei">行政・産業情報</a></li>
              <li class="p-header__nav__list__item"><a href="<?php echo $page->base; ?>/kokyosisetsu.php">公共施設</a></li>
              <li class="p-header__nav__list__item"><a href="<?php echo $page->base; ?>/anzen/" data-nav-target="anzen">防災・安全情報</a></li>
            </ul>

            <div class="p-header__navUnder">
              <div class="p-header__navUnder__item" data-under-nav="about">
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/about/mayor.php">町長室</a></p>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/about/kazan-kyosei.php">火山との共生</a></p>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/about/geopark.php">洞爺湖有珠山ジオパーク</a></p>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/about/saisei-enerugi.php">再生可能エネルギー</a></p>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/about/yukigassen.php">雪合戦</a></p>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/about/kitanoumi.php">北の湖</a></p>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/about/kosodate.php">子育て支援・高齢者福祉・長寿</a></p>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/about/kemibyaru.php">ケミヤルヴィ交流</a></p>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/about/noshokorenke.php">農商工連携</a></p>
              </div><!-- /.p-header__navUnder__item -->
              <div class="p-header__navUnder__item" data-under-nav="kurashi">
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/kurashi/joho/">暮らしの情報</a></p>
                <ul class="p-header__navUnder__item__list">
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/gomi.php">ごみ・リサイクル</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/shinyoshori.php">し尿処理・浄化槽</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/gaichukujo.php">ペット・害虫駆除</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/kotsukikan.php">交通機関・EV充電</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/jutaku.php">持家・空き家・民間住宅（住宅制度）</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/mainanba.php">社会保障・税番号（マイナンバー）</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/toroku.php">住民登録・戸籍・印鑑登録・パスポート</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/tsusin.php">情報通信</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/suido.php">水道・下水道・温泉水</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/zeikin.php">税金</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/koeijutaku.php">町営住宅（公営住宅）</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/nenkin-kokuho.php">年金・国民健康保険・後期高齢者医療制度</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/akiyabanku.php">空き家情報（空き家バンク）</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/hikkoshi.php">引越し手続き</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/joho/okuyami.php">おくやみ</a></li>
                </ul>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/kurashi/kyoiku/">教育・生涯学習</a></p>
                <ul class="p-header__navUnder__item__list">
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/kyoiku/shogai.php">生涯学習</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/kyoiku/gakko.php">学校教育</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/kyoiku/gyosei.php">教育行政</a></li>
                </ul>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/kurashi/kenko/">健康・福祉</a></p>
                <ul class="p-header__navUnder__item__list">
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/kenko/kodomo-hoken.php">お母さんと子どもの保健</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/kenko/iryokikan.php">医療機関</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/kenko/kaigo-fukushi.php">介護保険・高齢者福祉</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/kenko/kosodatesien.php">子育て支援</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/kenko/shakaifukushi.php">社会福祉</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/kenko/shogaifukushi.php">障がい福祉</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/kurashi/kenko/kenkodukuri.php">大人の保健・健康づくり</a></li>
                </ul>
              </div><!-- /.p-header__navUnder__item -->
              <div class="p-header__navUnder__item" data-under-nav="chosei">
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/chosei/gyosei/">行政情報</a></p>
                <ul class="p-header__navUnder__item__list">
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/gyosei/kantosobetsu.php">関東そうべつ会</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/gyosei/kouiki.php">広域行政</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/gyosei/kohoakaibu.php">広報アーカイブ</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/gyosei/zaiseiunei.php">行財政運営</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/gyosei/machidukuri.php">住民参画・まちづくり</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/gyosei/seisaku.php">政策・計画</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/gyosei/senkyo.php">選挙</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/gyosei/machinoshokai.php">町のプロフィール</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="#">町長室</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/gyosei/soshiki.php">役場案内・組織機構</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/gyosei/kobai.php">インターネット公売</a></li>
                </ul>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/chosei/sangyo/">産業情報</a></p>
                <ul class="p-header__navUnder__item__list">
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/sangyo/kanko.php">観光</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/sangyo/shokogyo.php">商工業・企業誘致</a></li>
                  <li class="p-header__navUnder__item__listItem"><a href="<?php echo $page->base; ?>/chosei/sangyo/nogyo.php">農業</a></li>
                </ul>
              </div><!-- /.p-header__navUnder__item -->
              <div class="p-header__navUnder__item" data-under-nav="anzen">
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/anzen/kotsukisei.php">交通規制</a></p>
                <p class="p-header__navUnder__item__heading"><a href="<?php echo $page->base; ?>/anzen/bosai.php">防災</a></p>
              </div><!-- /.p-header__navUnder__item -->
            </div><!-- /.p-header__navUnder -->
          </nav><!-- /.p-header__nav -->

          <div class="p-header__controlls__inner">
            <div class="p-header__small-btns">
              <ul>
                <li><a href="#">イベントカレンダー</a></li>
                <li><a href="#">例規集</a></li>
                <li><a href="#">議会情報</a></li>
              </ul>
            </div>
            <div class="p-header__search">
              <form action="https://www.town.sobetsu.lg.jp/search" id="cse-search-box">
                <input type="hidden" name="cx" value="002721248338057750523:7iny3cpstu4" />
                <input type="hidden" name="cof" value="FORID:9" />
                <input type="hidden" name="ie" value="UTF-8" />
                <input type="text" name="q" class="c-form-text" />
                <button class="p-header__search__btn" type="submit"></button>
              </form>
            </div>
            <div class="p-header__font-btns">
              <p class="p-header__font-btns__heading">文字サイズ</p>
              <ul class="p-header__font-btns__list">
                <li class="p-header__font-btns__list__item __is-active js-font-btn" data-size="basic">標準</li>
                <li class="p-header__font-btns__list__item js-font-btn" data-size="zoom">拡大</li>
              </ul>
            </div>
            <div class="p-header__link">
              <ul>
                <li><a href="http://www.sobetsu-kanko.com">観光情報サイト</a></li>
                <li><a href="<?php echo $page->base; ?>/iju/">移住情報サイト</a></li>
              </ul>
            </div>
            <div class="p-header__contact">
              <p class="p-header__contact__text__tel">0142-66-2121</p>
              <p class="p-header__contact__text__limit">（平日 08:45～17:30）</p>
              <p class="p-header__contact__text__email"><a href="<?php echo $page->base; ?>/contact/" class="__email"><span>お問い合わせ</span></a></p>
            </div>
            <div class="p-header__sns-btns">
              <ul class="p-header__sns-btns__list">
                <li class="p-header__sns-btns__list__item"><a href="#" target="_blank" rel="nofollow" class="__facebook"><span>Facebook</span></a></li>
              </ul>
            </div>
          </div>
        </div><!-- /.p-header__controlls -->
      </div><!-- /.p-header__inner -->

      <div class="p-header__sideNav">
        <ul class="p-header__sideNav__list">
          <li class="p-header__sideNav__listItem"><a href="http://www.sobetsu-kanko.com" class="__sightseeing">観光情報</a></li>
          <li class="p-header__sideNav__listItem"><a href="<?php echo $page->base; ?>/iju/" class="__immigration">移住情報</a></li>
        </ul>
      </div><!-- /.p-header__sideNav -->

    </div><!-- /.p-header -->
  </header><!-- /#base-header -->

