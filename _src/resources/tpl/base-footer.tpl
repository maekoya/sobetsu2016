
  <footer id="base-footer">
    <div class="p-footer">
      <p class="p-footer__logo"><a href="/"><img src="<?php echo $page->base; ?>/resources/img/common/logo-2.png" width="120" height="45" alt="壮瞥町"></a></p>
      <div class="p-footer__contact">
        <p class="p-footer__contact__address">〒052-0101 北海道有珠郡壮瞥町字滝之町287番地7</p>
        <p class="p-footer__contact__number">
          <span class="p-footer__number__tel">TEL : (0142)66-2121</span>
          <span class="p-footer__number__fax">FAX : (0142)66-7001</span>
          <span class="p-footer__number__access"><a href="/akusesu.html">アクセスはこちら</a></span>
        </p>
      </div>
      <ul class="p-footer__nav-list">
        <li class="p-footer__nav-list__item"><a href="/contact/">お問い合わせ</a></li>
        <li class="p-footer__nav-list__item"><a href="/kojinjoho.php">プライバシーポリシー</a></li>
        <li class="p-footer__nav-list__item"><a href="/sitemap.php">サイトマップ</a></li>
      </ul>
      <p class="p-footer__copyright">©<?php echo $page->h(date('Y')); ?> SOBETSU TOWN. All rights reserved.</p>

    </div>
  </footer><!-- /#base-footer -->
