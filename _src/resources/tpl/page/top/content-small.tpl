
    <div class="p-container">
      <div class="top-transmit-sobetsu">
        <p class="top-transmit-sobetsu__heading"><img src="<?php echo $page->base; ?>/resources/img/page/top/top-transmit-sobetsu__heading.png" width="214" height="24" alt="ハッシンそうべつ"></p>
        <p class="top-transmit-sobetsu__btn"><a href="/hasshinsobetsu/">一覧を見る</a></p>
        <div class="top-transmit-sobetsu__item">
          <div class="top-transmit-sobetsu__item__image"><img src="https://placehold.jp/120x120.png" width="120" height="120" alt=""></div>
          <p class="top-transmit-sobetsu__item__heading">ここにテキストが入ります。</p>
          <p class="top-transmit-sobetsu__item__text">ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
        </div>
      </div>
      <div class="top-entries-recently">
        <div class="top-entries-recently__header">
          <div class="top-entries-recently__header__heading"><span class="__tag">新着</span>町からのお知らせ</div>
          <div class="top-entries-recently__header__more-btn"><a href="/sinchaku/">一覧を見る</a></div>
        </div>
        <ul class="top-entries-recently__list">
          <li class="top-entries-recently__list__item">
            <div class="__data">
              <span class="__data__tag"><a href="/sinchaku/" class="__is-admin">行政</a></span>
              <time class="__data__date" datetime="2016-00-00" pubdate>2016.04.03</time>
            </div>
            <div class="__detail">
              <a class="__detail__title" href="/sinchaku/">そうべつ議会だよりNo.00（議会事務局）そうべつ議会だよりNo.00（議会事務局）そうべつ議会だよりNo.00（議会事務局）そうべつ議会だよりNo.00（議会事務局）</a>
            </div>
          </li>
          <li class="top-entries-recently__list__item">
            <div class="__data">
              <span class="__data__tag"><a href="/sinchaku/" class="__is-admin">行政</a></span>
              <time class="__data__date" datetime="2016-00-00" pubdate>2016.04.03</time>
            </div>
            <div class="__detail">
              <a class="__detail__title" href="/sinchaku/">そうべつ議会だよりNo.00（議会事務局）そうべつ議会だよりNo.00（議会事務局）そうべつ議会だよりNo.00（議会事務局）そうべつ議会だよりNo.00（議会事務局）</a>
            </div>
          </li>
        </ul>
      </div>
    </div><!-- /.p-container -->

    <div class="p-container__fluid">
      <div class="top-life-window">
        <div class="top-heading-delta">暮らしの<br>窓口</div>
        <div class="top-life-window__inner">
          <ul class="top-life-window__inner__list">
            <li class="top-life-window__inner__list__item"><a href="/yoshiki.html" class="__is-form">様式ダウンロード</a></li>
            <li class="top-life-window__inner__list__item"><a href="/hikkoshi.html" class="__is-certificate">戸籍・証明</a></li>
            <li class="top-life-window__inner__list__item"><a href="/okuyami.html" class="__is-house">住まい・引越し</a></li>
            <li class="top-life-window__inner__list__item"><a href="#" class="__is-trash">ゴミ・リサイクル</a></li>
            <li class="top-life-window__inner__list__item"><a href="#" class="__is-marriage">結婚・離婚</a></li>
            <li class="top-life-window__inner__list__item"><a href="/nichin.html" class="__is-childcare">妊娠・子育</a></li>
            <li class="top-life-window__inner__list__item"><a href="#" class="__is-learning">教育・生涯学習</a></li>
            <li class="top-life-window__inner__list__item"><a href="#" class="__is-welfare">年金・保険・高齢者福祉</a></li>
            <li class="top-life-window__inner__list__item"><a href="#" class="__is-condolence">おくやみ</a></li>
          </ul>
        </div>
      </div>
    </div><!-- /.p-container -->

    <div class="p-container">
      <div class="top-banners-3">
        <ul class="top-banners-3__list">
          <li class="top-banners-3__list__item__left"><a href="/furusato.html"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-support-1.jpg" width="550" height="200" alt="壮瞥町ならではの支援体制"></a></li>
          <li class="top-banners-3__list__item__right"><a href="/shien-ichiran.html"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-tax-1.jpg" width="430" height="200" alt="ふるさと納税"></a></li>
        </ul>
      </div>
    </div><!-- /.p-container -->

    <div class="p-container">
      <div class="top-side-nav">
        <ul class="top-side-nav__list">
          <li class="top-side-nav__list__item"><a href="/ibento.html" class="__is-calender">イベントカレンダー</a></li>
          <li class="top-side-nav__list__item"><a href="#" class="__is-publish">広報そうべつ</a></li>
          <li class="top-side-nav__list__item"><a href="#" class="__is-office">町長室</a></li>
          <li class="top-side-nav__list__item"><a href="/gikai/" class="__is-congress">議会情報</a></li>
          <li class="top-side-nav__list__item"><a href="#" class="__is-system">組織案内</a></li>
          <li class="top-side-nav__list__item"><a href="http://houmu.h-chosonkai.gr.jp/~reikidb/" class="__is-rule" target="_blank" rel="nofollow">例規集</a></li>
          <li class="top-side-nav__list__item"><a href="/akusesu.html" class="__is-access">アクセス／交通機関</a></li>
        </ul>
      </div>
    </div><!-- /.p-container -->



