<!DOCTYPE html>
<!--[if IE 8]>         <html lang="ja" class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
<head>
<meta charset="UTF-8">
<title><?php echo $page->h($page->title); ?></title>
<meta name="description" content="<?php echo $page->h($page->description); ?>">
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=1">
<meta name="format-detection" content="telephone=no,address=no,email=no">
<meta name="author" content="SOBETSU TOWN">
<meta name="copyright" content="©SOBETSU TOWN. All rights reserved.">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=Edge, chrome=1">
<meta http-equiv="imagetoolbar" content="no">
<![endif]-->

<!-- OGP -->
<meta property="og:type" content="article">
<meta property="og:title" content="<?php echo $page->h($page->title); ?>">
<meta property="og:description" content="<?php echo $page->h($page->description); ?>">
<meta property="og:image" content="https://xxxxxx">
<meta property="og:url" content="https://xxxxxx">
<meta property="og:site_name" content="<?php echo $page->h($page->sitename); ?>" />
<meta property="twitter:title" content="<?php echo $page->h($page->title); ?>">
<meta property="twitter:description" content="<?php echo $page->h($page->description); ?>">
<meta property="twitter:image" content="https://xxxxxx">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@xxxxxx">

<!-- Favicon -->
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $page->base; ?>/resources/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $page->base; ?>/resources/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $page->base; ?>/resources/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $page->base; ?>/resources/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $page->base; ?>/resources/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $page->base; ?>/resources/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $page->base; ?>/resources/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $page->base; ?>/resources/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $page->base; ?>/resources/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="<?php echo $page->base; ?>/resources/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo $page->base; ?>/resources/favicons/favicon-194x194.png" sizes="194x194">
<link rel="icon" type="image/png" href="<?php echo $page->base; ?>/resources/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="<?php echo $page->base; ?>/resources/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="<?php echo $page->base; ?>/resources/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?php echo $page->base; ?>/resources/favicons/manifest.json">
<link rel="mask-icon" href="<?php echo $page->base; ?>/resources/favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="apple-mobile-web-app-title" content="壮瞥町">
<meta name="application-name" content="壮瞥町">
<meta name="msapplication-TileColor" content="#2b5797">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">

<!-- Feed -->
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="xxxxxx">
<link rel="alternate" type="text/xml" title="RSS .92" href="xxxxxx">
<link rel="alternate" type="application/atom+xml" title="Atom 1.0" href="xxxxxx">

<!-- StyleSheet -->
<link rel="stylesheet" media="screen,print" href="<?php echo $page->base; ?>/resources/css/common.css">

<!-- Javascript -->
<!--[if lt IE 9]>
<script src="<?php echo $page->base; ?>/resources/js/vendor/html5shiv-printshiv.js"></script>
<![endif]-->
