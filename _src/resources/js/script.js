'use strict';
window.Script = window.Script || {};

/**
 * Variables.
 */
Script.Vars = {
  debug: false
};

/**
 * Common functions.
 */
Script.Common = {
  init: function() {
    if(Script.Vars.debug){
      this.setThePostErrorMsgHandler();
    }
    this.importTheSocialPlugins();
    this.setTheChangeFontSize();
    this.addCurrentCategoryClassToBody();
    this.setHandlerToShowTheUnderNavigation();
    this.setHandlerToShowTheNavigation();
    this.setHandlerToTheSmoothScroll();
  },

  /**
   * Set the Event that 'show the under navigation' to DOM element.
   */
  setHandlerToShowTheUnderNavigation: function() {
    if(Script.Utils.checkDevice.Mobile || Script.Utils.checkDevice.Tablet){
      return false;
    }

    var $trigger = $('.js-show-under-nav').find('a');
    var $receiving = $('[data-under-nav]');
    var $avaivable_scope = $('.p-header__controlls');
    var trigger_class_name = '__is-active';

    var removeClass = function(){
      $trigger.removeClass(trigger_class_name);
      $receiving.removeClass(trigger_class_name);
    };

    var addClass = function($elem, target){
      $elem.addClass(trigger_class_name);
      $('[data-under-nav="' + target +'"]').addClass(trigger_class_name);
    };

    $avaivable_scope.on('mouseleave', function(){
      removeClass();
    });

    $trigger.on('mouseenter', function(e){
      var target = $(this).data('nav-target');
      e.preventDefault();
      removeClass();

      if(!target){
        return false;
      }
      addClass($(this), target);
    });
  },

  /**
   * Set the show navigation Event Handler to DOM element.
   */
  setHandlerToShowTheNavigation: function() {
    var $trigger = $('.js-show-navigation');
    var eventListener = Script.Utils.checkDevice.Mobile || Script.Utils.checkDevice.Tablet ? 'touchend' : 'click';

    $trigger.on(eventListener, function(e) {
      e.preventDefault();
      $(this).toggleClass('__is-active');
      return Script.Utils.showNavigation();
    });
  },

  /**
   *
   */
  addCurrentCategoryClassToBody: function() {
    var className;
    var className_prefix = 'page-';
    var dir = (location.protocol + '//' + location.host + location.pathname).split('/');
    dir = dir[3];

    if(dir.match(/.html/)) {
      className = className_prefix + 'root';
    } else {
      className = className_prefix + dir;
    }

    if(!dir) {
      className = className_prefix + 'home';
    }

    $('#base-page').addClass(className);
  },

  /**
   * Font size change functions.
   */
  setTheChangeFontSize: function() {
    if(Script.Utils.checkDevice.Mobile || Script.Utils.checkDevice.Tablet){
      return false;
    }

    var font_size_per = 100;       // Font size percent.
    var font_size_rate = 8;        // Rate of increase in font size.
    var cookie_name = 'fonrsize';  // Cookie name.
    var cookie_expires = 7;        // Cookie Expires day.
    var $trigger = $('.js-font-btn');
    var $receiver = $('#base-page');
    var trigger_class_name = '__is-active';

    var getCookie = function(){
      return parseInt(Cookies.get(cookie_name), 10);
    };

    var setCookie = function(value){
      Cookies.set(cookie_name, value, { expires: cookie_expires, path: '/'});
    };

    var changeFontsize = function(font_size_per){
      $receiver.css('font-size', font_size_per + '%');
      setCookie(font_size_per);
    };

    if(getCookie()){
      font_size_per = getCookie();
      changeFontsize(font_size_per);

      if(font_size_per !== 100){
        $trigger.toggleClass(trigger_class_name);
      }
    }

    $trigger.on('click', function(e){
      e.preventDefault();

      if($(this).data('size') === 'zoom'){
        font_size_per += font_size_rate;
      } else {
        font_size_per = 100;
      }
      changeFontsize(font_size_per);
      $trigger.removeClass(trigger_class_name);
      $(this).addClass(trigger_class_name);
    });
  },

  /**
   * Set the SmoothScroll Event Handler to DOM element.
   */
  setHandlerToTheSmoothScroll: function() {
    var $elem = $('a[href^="#"], a[href^="' + location.pathname + '#"]');

    $elem.on('click', function(e) {
      var hash = this.hash;
      e.preventDefault();
      return Script.Utils.smoothScroll(hash);
    });
  },

  /**
   * Import social pulugins by ajax.
   */
  importTheSocialPlugins: function() {
    (function(w, d, s) {
      function go() {
        var js, fjs = d.getElementsByTagName(s)[0],
        load = function(url, id) {
          if (d.getElementById(id)) {
            return;
          }
          js = d.createElement(s);
          js.src = url;
          js.id = id;
          fjs.parentNode.insertBefore(js, fjs);
        };
        load('//www.google.com/cse/brand?form=cse-search-box&amp;lang=ja', 'gsearchjs');  // google custom search
        load('//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.5', 'facebook-jssdk');  // facebook
        //load('https://apis.google.com/js/plusone.js', 'gplus1js');  // google pluse
        //load('//platform.twitter.com/widgets.js', 'tweetjs');  // twitter
        //load('//b.st-hatena.com/js/bookmark_button.js', 'hatenajs');  // hatena
      }
      if (w.addEventListener) {
        w.addEventListener('load', go, false);
      } else if (w.attachEvent) {
        w.attachEvent('onload', go);
      }
    }(window, document, 'script'));
  },

  /**
   * Set the Handler to POST javascript error to server.
   * If you are 'try-catch', not caught by the 'onerror'.
   */
  setThePostErrorMsgHandler: function() {
    window.onerror = function(errorMsg, fileName, lineNumber) {
      var errorInfo = {
        'errorMsg'   : errorMsg,           // error message
        'fileName'   : fileName,           // error file name
        'lineNumber' : lineNumber,         // error line
        'urlDispPage': location.href,      // error URL
        'userAgent'  : navigator.userAgent // user agent
      };

      // POST to Server.
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '受け取り先');
      xhr.setRequestHeader('Content-Type', 'applicatoin/json;charset=UTF-8');
      xhr.send(JSON.stringify(errorInfo));
    };
  }
};

/**
 * Utilities functions.
 */
Script.Utils = {
  /**
   * Check whether mobile or tablet.
   *
   * Mobile Devices
   * - iPhone
   * - iPod
   * - Windows OS smartphone
   * - Android OS smartphone
   * - Firefox OS smartphone
   * - BlackBerry OS smartphone
   *
   * Tablet Devices
   * - iPad
   * - Kindle
   * - Android OS tablet
   * - Windows OS tablet
   * - Firefox OS tablet
   * - BlackBerry OS tablet
   *
   * @return {object} Tablet:boolean, Mobile:boolean
   */
  checkDevice: (function(u){
    var tablet = (u.indexOf('windows') && u.indexOf('touch')) || u.indexOf('ipad') || (u.indexOf('android') && u.indexOf('mobile')) || (u.indexOf('firefox') && u.indexOf('tablet')) || u.indexOf('kindle') || u.indexOf('silk') || u.indexOf('playbook');
    var mobile = (u.indexOf('windows') && u.indexOf('phone')) || u.indexOf('iphone') || u.indexOf('ipod') || (u.indexOf('android') && u.indexOf('mobile')) || (u.indexOf('firefox') && u.indexOf('mobile')) || u.indexOf('blackberry');

    tablet = tablet > 0 ? true : false;
    mobile = mobile > 0 ? true : false;

    return {
      Tablet:tablet,
      Mobile:mobile
    };
  })(window.navigator.userAgent.toLowerCase()),

  /**
   * Show navigation.
   */
  showNavigation: function() {
    var $target = $('#base-page');
    $target.toggleClass('is-show-navigation-active');
  },

  /**
   * Scroll to DOM element.
   * @param {string}  element id
   */
  smoothScroll: function(hash) {
    var target = $(hash).offset().top;

    if(hash === '#'){
      return false;
    }

    $('body, html').stop().animate({
      scrollTop: target >= 10 ? target - 0 : target
    }, 600, function() {
      $(this).unbind('mousewheel DOMMouseScroll');
    }).bind('mousewheel DOMMouseScroll', function() {
      $(this).queue([]).stop();
      $(this).unbind('mousewheel DOMMouseScroll');
    });
  },

  /**
   * Create Google Maps
   * @param {string} embed element id
   * @param {number} latitude
   * @param {number} longitude
   * @param {string} title
   * @param {number} zoomLevel
   */
  createMap: function(embedId, lat, lng, title, zoomLevel) {
    var mapParams = {
      'embedId': embedId,
      'lat': lat,
      'lng': lng,
      'zoomLevel': zoomLevel
    };
    var latlng = new google.maps.LatLng(mapParams.lat, mapParams.lng);
    var myOptions = {
      zoom: mapParams.zoomLevel,
      center: latlng,
      panControl: true,
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL
      },
      scrollwheel: false,
      draggable: false,
      navigationControl: true,
      mapTypeControl: false,
      streetViewControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById(mapParams.embedId), myOptions);
    /*var icon = {
      url:'/resources/img/common/icon-googlemaps.2x.png',
      scaledSize:{width: 130, height: 169}
    };*/
    var markerOptions = new google.maps.Marker({
      position: latlng,
      map: map,
      //icon: icon,
      title: mapParams.title
    });
  },

  /**
   * Get Current Date
   * @return {object}
   */
  getCurrentDate: function() {
    var date = new Date();
    //var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    //var week = date.getDay();
    //var weekNames = ['日', '月', '火', '水', '木', '金', '土'];
    //week = weekNames[week];

    // Zero padding
    month = month < 10 ? ('0' + month) : month;
    day = day < 10 ? ('0' + day) : day;

    return {
      month : month,
      day : day
    };
  }
};

/**
 * Top page functions.
 */
Script.Top = {
  init: function (){
    this.setCurrentDate();
    this.setCurrentWeather();
    this.getFeedByJSON('iju');
    this.getFeedByJSON('kanko');
  },

  /**
   * Set Current date to DOM element.
   */
  setCurrentDate: function (){
    var date = Script.Utils.getCurrentDate();
    var str = date.month + '月' + date.day + '日';
    $('.js-weather-date').text(str);
  },

  /**
   * Set Current weather to DOM element.
   */
  setCurrentWeather: function() {
    var url = '//www.town.sobetsu.lg.jp/resources/etc/api/weather/index.php';
    var jqxhr = $.ajax({
      type: 'GET',
      url: url,
      dataType: 'json',
      cache: false
    })
    .done(function(data) {
      //console.log('success');
    })
    .fail(function() {
      //console.log('error');
    })
    .always(function(data) {
      //console.log('complete');
      setWeather(data.code, data.description, data.timeGroup);
    });

    var setWeather = function(code, description, time_group){
      var $elem_text = $('.js-weather-desc');
      var $elem_image = $('.js-weather-image');

      if(!code || !description || !time_group){
        description = '天気情報の取得に失敗しました。';
        $elem_text.html(description);
        return false;
      }

      $elem_text.html(description);
      $elem_image.html('<img src="/resources/img/weather/' + time_group + '/' + code +'.png" alt="'+ description +'">');
    };
  },

  /**
   *
   */
  setResultPopulation: function (current_num, previous_num, curent_element_class, previous_element_class){
    var result_num = calc(current_num, previous_num);
    if(result_num >= 0){
      result_num = '+' + separate(result_num);
    } else {
      result_num = separate(result_num);
    }

    $(curent_element_class).find('.__js-num').text(separate(current_num));
    $(previous_element_class).find('.__js-num').text('前月比 ' + result_num);

    function calc(current_num, previous_num){
      return current_num - previous_num;
    }
    function separate(num){
      return String(num).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    }
  },

  /**
   *
   */
  getFeedByJSON: function(target) {
    var url = '//www.town.sobetsu.lg.jp/api/feed/index.php?site=' + target;
    var jqxhr = $.ajax({
      type: 'GET',
      url: url,
      dataType: 'json',
      cache: false
    })
    .done(function(data) {
      //console.log('success');
    })
    .fail(function() {
      //console.log('error');
    })
    .always(function(data) {
      //console.log('complete');
      createElem(data);
    });

    var createElem = function(obj) {
      var templete = '';

      templete += '<% _.each(data, function(item) { %>';
      templete += '<li class="top-entries-other__item__list__item">';
      templete += '<a href="<%= item.url %>" target="_blank">';
      templete += '<div class="__image"><img src="<%= item.thumb_url %>" width="130" height="100" alt=""></div>';
      templete += '<time class="__date" datetime="<%= item.date %>" pubdate><%= item.date %></time>';
      templete += '<span class="__title"><%= item.title %></span>';
      templete += '</a>';
      templete += '</li>';
      templete += '<% }); %>';

      var compiled = _.template(templete);
      $('.js-feed[data-feed="' + target +'"]').html(compiled(obj));
    };
  }
};

/**
 * Akiyabank page functions.
 */
Script.Bank = {
  init: function (){
    this.setGallery();
  },

  /**
   * Set
   */
  setMap: function (markerData){
    var embed_id = 'js-map';  //embed map id
    var map;
    var marker = [];
    var markerLatLng;
    var infoWindow = [];
    var markerBounds = new google.maps.LatLngBounds();
    /*var markerData = [
      {
        name: '',
        id: '',
        lat: 0,
        lng: 0,
        icon: 'xx.png'
      }
    ];*/

    function initMap() {
      // Create Map
      var mapLatLng = new google.maps.LatLng({lat: markerData[0].lat, lng: markerData[0].lng});
      map = new google.maps.Map(document.getElementById(embed_id), {
        center: mapLatLng,
        zoom: 15
      });

      // Create Marker
      for (var i = 0; i < markerData.length; i++) {
        markerLatLng = new google.maps.LatLng({lat: markerData[i].lat, lng: markerData[i].lng});
        marker[i] = new google.maps.Marker({
          position: markerLatLng,
          map: map/*,
          icon: {
            url: markerData[i]['icon']
          }*/
        });
        markerBounds.extend(markerLatLng);

        infoWindow[i] = new google.maps.InfoWindow({
          content: '<div class="">' + markerData[i].name + '</div>'
        });

        markerEvent(i);
      }

      map.fitBounds(markerBounds);
    }

    function markerEvent(i) {
      marker[i].addListener('mouseover', function() {
        infoWindow[i].open(map, marker[i]);
      });
      marker[i].addListener('mouseout', function() {
        infoWindow[i].close(map, marker[i]);
      });
      marker[i].addListener('click', function() {
        Script.Utils.smoothScroll('#' + markerData[i].id);
      });
    }

    initMap();
  },

  /**
   * Set
   */
  setGallery: function (){
    var change_point_on_width = 662;
    var slide_on_state = false;
    var $slide_list = $('.js-gallery');
    var $window = $(window);

    var getWinWidth = function(){
      var winW = $window.width();
      chkGalleryByWinWidth(winW);
    };

    var chkGalleryByWinWidth = function(winW){
      if(winW <= change_point_on_width){
        if(!slide_on_state){
          slide_on_state = true;
          goSlick();
        }
        setLightbox(false);
      }

      if(winW > change_point_on_width){
        if(slide_on_state){
          slide_on_state = false;
          $slide_list.slick('unslick');
        }
        setLightbox(true);
      }
    };

    var goSlick = function(){
      $slide_list.each(function(){
        if($(this).find('li').length <= 1){
          return true;
        }
        $(this).slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: '0px',
          arrows: false,
          autoplay: false,
          autoplaySpeed: 5000,
          speed: 300,
          accessibility: false,
          pauseOnHover: false,
          adaptiveHeight: true
        });
      });
    };

    var setLightbox = function(boolean){
      var active_class = 'js-lightbox';
      var $trigger = $slide_list.find('li').find('a');

      if(!boolean){
        $trigger.removeClass(active_class);
      } else {
        $trigger.addClass(active_class);
      }

      $trigger.on('click', function(e){
        e.preventDefault();
        if(!$(this).hasClass(active_class)){
          return false;
        }

        $.magnificPopup.open({
          items: {
            src: $(this).attr('href')
          },
          type: 'image'
        }, 0);
      });
    };

    $window.on('resize', getWinWidth);
    getWinWidth();
  }
};


Script.Common.init();
