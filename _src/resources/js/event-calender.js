'use strict';
/**
 * Calender functions.
 */
(function(){

  var url = '/api/calender/index.php';
  var jqxhr = $.ajax({
    type: 'GET',
    url: url,
    dataType: 'json',
    cache: false
  })
  .done(function(data) {
    //console.log('success');
  })
  .fail(function() {
    //console.log('error');
  })
  .always(function(data) {
    //console.log('complete');
    calenderInit(data);
  });

  function calenderInit(array){

    // Call this from the developer console and you can control both instances
    var calendars = {};

    // Here's some magic to make sure the dates are happening this month.
    var thisMonth = moment().format('YYYY-MM');

    // Events to load into calendar
    var eventArray = array;

    /*
    // if local
    eventArray = [
      {
        date: thisMonth + '-1',
        endDate: thisMonth + '-10',
        title: 'テストイベント',
        url: 'aaaa',
        description: '説明文が入ります。説明文が入ります。',
        category: 'town',
        category_ja: '町民のスケジュール'
      },{
        date: thisMonth + '-1',
        endDate: null,
        title: 'テストイベント2',
        url: 'bbbb',
        description: '説明文が入ります。説明文が入ります。',
        category: 'festival',
        category_ja: 'まちのイベント・お祭り・体験'
      }, {
        date: thisMonth + '-15',
        endDate: null,
        title: '仮イベント',
        description: '説明文が入ります。説明文が入ります。',
        category: 'festival',
        category_ja: 'まちのイベント・お祭り・体験'
      }, {
        date: thisMonth + '-27',
        endDate: null,
        title: 'イベントです',
        description: '説明文が入ります。説明文が入ります。',
        category: 'around',
        category_ja: '周辺のイベント'
      }
    ];*/

    eventArray = _.filter(eventArray, function(array){
      if(array['category'] === 'town' || array['category'] === 'festival'){
        return true;
      }
    });

    //日付降順でソート
    eventArray = _.chain(eventArray).sortBy(function(item) {return item.date; }).value();

    var myCalendar = $('#p-clndr').clndr({
      template: $('#clndr-template').html(),
      events: eventArray,
      daysOfTheWeek: ['日', '月', '火', '水', '木', '金', '土'],
      adjacentDaysChangeMonth: true,
      forceSixRows: true,
      multiDayEvents: {
        endDate: 'endDate',
        startDate: 'date',
        singleDay: 'date'
      },
      clickEvents: {
        click: function(target) {
          if (target.events.length) {

            $('#p-clndr .day').removeClass('is-active');
            target.element.classList.add('is-active');

            showEventlist({
              date: moment(target.date).format('LL'),
              events: target.events,
              alert: ''
            });
          }
        },
        onMonthChange: function (month) {
          $('#p-clndr .day').removeClass('is-active');
          if (myCalendar.eventsThisInterval.length) {
            showEventlist({
              date: moment(myCalendar.month).format('YYYY年MMMM'),
              events: myCalendar.eventsThisInterval,
              alert: ''
            });
          } else {
            showEventlist({
              date: moment(myCalendar.month).format('YYYY年MMMM'),
              events: myCalendar.eventsThisInterval,
              alert: '開催予定のイベントはありません。'
            });
          }
        }
      },
      doneRendering: function () {
        $('.js-clndr-sort-month').on('click', function(){
          $('#p-clndr .day').removeClass('is-active');
          if (myCalendar.eventsThisInterval.length) {
            showEventlist({
              date: moment(myCalendar.month).format('YYYY年MMMM'),
              events: myCalendar.eventsThisInterval,
              alert: ''
            });
          } else {
            showEventlist({
              date: moment(myCalendar.month).format('YYYY年MMMM'),
              events: myCalendar.eventsThisInterval,
              alert: '開催予定のイベントはありません。'
            });
          }
        });
      }
    });


    var sortByCategories = function(){
      var $trigger = $('.js-clndr-sort-button');

      $trigger.on('click', function(e){
        var newEvents;
        var params = [];

        e.preventDefault();
        $(this).toggleClass('__is-active');

        $trigger.each(function(){
          if($(this).hasClass('__is-active')){
            params.push($(this).data('category'));
          }
        });

        newEvents = _.filter(eventArray, function(array){
          for(var n = 0; n < params.length; n++){
            if(array['category'] === params[n]){
              return true;
              break;
            }
          }
        });

        myCalendar.setEvents(newEvents);
        if (myCalendar.eventsThisInterval.length) {
          showEventlist({
            date: moment(myCalendar.month).format('YYYY年MMMM'),
            events: myCalendar.eventsThisInterval,
            alert: ''
          });
        } else {
          showEventlist({
            date: moment(myCalendar.month).format('YYYY年MMMM'),
            events: myCalendar.eventsThisInterval,
            alert: '開催予定のイベントはありません。'
          });
        }
      });
    };


    var template = _.template($('#event-template').text());
    var showEventlist = function(array){
      var showEvents = array;
      $('#p-entries-event').html(template(showEvents));
    };

    var init = function(){
      sortByCategories();
      if (myCalendar.eventsThisInterval.length) {
        showEventlist({
          date: moment(myCalendar.month).format('YYYY年MMMM'),
          events: myCalendar.eventsThisInterval,
          alert: ''
        });
      } else {
        showEventlist({
          date: moment(myCalendar.month).format('YYYY年MMMM'),
          events: myCalendar.eventsThisInterval,
          alert: '開催予定のイベントはありません。'
        });
      }
    }
    init();
  }

}());
