<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">イベントカレンダー</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $page->base; ?>/event/" itemprop="url"><span itemprop="title">イベントカレンダー</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">壮瞥町観光祭り</span></li>
    </ul>


    <div class="p-container__full">
      <article class="p-entry-event">
        <div class="p-entry-event__header">
          <h1 class="p-entry-event__header__heading">壮瞥町観光祭り</h1>
          <div class="p-entry-event__header__data">
            <ul class="p-entry-event__header__data__tag-list">
              <li class="p-entry-event__header__data__tag-list__item"><a href="#" class="__is-">まちのイベント・お祭り・体験</a></li>
            </ul>
            <div class="p-entry-event__header__data__date">開催期間 : 2016年3月1日～2016年3月10日</div>
          </div>
        </div><!-- /.p-entry-event__header -->

        <div class="p-entry-event__image js-gallery-wrap">
          <ul class="p-entry-event__image__gellery js-gallery">
            <li><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt=""></li>
            <li><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt=""></li>
            <li><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt=""></li>
          </ul>
          <ul class="p-entry-event__image__thumbs js-gallery-thumbs">
            <li><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="100" alt=""></li>
            <li><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="100" alt=""></li>
            <li><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="100" alt=""></li>
          </ul>
        </div>

        <div class="p-container__auto-margin-paragraph">
          <p>イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。</p>
          <p>イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。イベント概要が入ります。</p>
        </div>

        <div class="p-container__auto-margin-paragraph">
          <div class="u-grid__row">
            <div class="u-grid__col-6">
              <table class="c-table-1 p-entry-event__table">
                <tr>
                  <th class="__strong">イベント名</th>
                  <td>壮瞥町観光祭り</td>
                </tr>
                <tr>
                  <th class="__strong">開催期間・時間</th>
                  <td>2016年3月1日～2016年3月10日<br>10時～16時</td>
                </tr>
                <tr>
                  <th class="__strong">開催場所</th>
                  <td>壮瞥町</td>
                </tr>
                <tr>
                  <th class="__strong">会場</th>
                  <td>北海道有珠郡壮瞥町字滝之町287番地7</td>
                </tr>
                <tr>
                  <th class="__strong">料金</th>
                  <td>無料</td>
                </tr>
                <tr>
                  <th class="__strong">その他</th>
                  <td>備考テキストが入ります。備考テキストが入ります。備考テキストが入ります。</td>
                </tr>
                <tr>
                  <th class="__strong">お問合わせ先</th>
                  <td>0142-66-2121 そうべつ観光協会</td>
                </tr>
              </table>
            </div>
            <div class="u-grid__col-6">
              <div class="p-entry-event__map">
                <div id="__map"></div>
                <p class="u-text__right"><a href="#" class="c-link">GoogleMapsで見る</a></p>
              </div>
            </div>
          </div>
        </div>

        <div class="p-entry-event__footer">
          <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="300" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
        </div>
      </article><!-- /.p-entry-event -->
      <p class="u-text__center"><a href="#" class="c-btn__middle">イベントカレンダーへ戻る</a></p>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
<script>
Script.Utils.createMap('__map', 42.567708, 140.969465, 'イベントタイトルが入ります', 13);

(function(){
  var $slide_list = $('.js-gallery');
  var $slide_contlloler = $(".js-gallery-thumbs").find('li');

  if($slide_list.find('li').length <= 1){
    return false;
  }
  $('.js-gallery-wrap').addClass('__is-slide');

  $slide_list.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '0px',
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 300,
    accessibility: false,
    pauseOnHover: false,
    adaptiveHeight: true
  });

  $slide_list.on('afterChange', function(event, slick, currentSlide, nextSlide){
    $slide_contlloler.removeClass('__is-active');
    $slide_contlloler.eq(currentSlide).addClass('__is-active');
  });

  $slide_contlloler.on('click', function(e) {
    e.preventDefault();
    $slide_list.slick('slickGoTo', $(this).index());
  });

  $slide_contlloler.eq(0).addClass('__is-active');
}());
</script>
</body>
</html>
