<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>


<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">イベントカレンダー</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $page->base; ?>/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">イベントカレンダー</span></li>
    </ul>

    <div class="p-container">
      <div id="base-content__half">
        <div class="p-calender">
          <div class="p-calender__categories">
            <div class="p-calender__category js-clndr-sort-button __is-active" data-category="town">町民のスケジュール</div>
            <div class="p-calender__category js-clndr-sort-button __is-active" data-category="festival">まちのイベント・お祭り・体験</div>
            <div class="p-calender__category js-clndr-sort-button __is-active" data-category="around">周辺のイベント</div>
          </div>
          <div id="p-clndr">
            <script id="clndr-template" type="text/template">
              <div class="controls">
                <div class="clndr-previous-button">前月</div>
                <div class="month js-clndr-sort-month"><%= year %>年<%= month %></div>
                <div class="clndr-next-button">次月</div>
              </div>
              <div class="days-container">
                <div class="days">
                  <div class="headers">
                    <% _.each(daysOfTheWeek, function(day) { %><div class="day-header"><%= day %></div><% }); %>
                  </div>
                  <% _.each(days, function(day) {
                    %><div class="<%= day.classes %> <%= _.uniq(_.pluck(day.events, 'category')).join(' ') %>" id="<%= day.id %>" data-date="<%= day.date %>">
                    <div class="day-date"><%= day.day %></div>
                    <div class="day-tag __town">町</div>
                    <div class="day-tag __festival">祭</div>
                    <div class="day-tag __around">周</div>
                  </div><% }); %>
                </div>
              </div>
            </script>
          </div><!-- /#p-clndr -->
        </div><!-- /.p-calender -->
      </div><!-- /#base-content -->

      <div id="base-content__half">
        <div class="p-entries-event" id="p-entries-event">
          <script id="event-template" type="text/template">
            <p class="p-entries-event__heading"><%= date %>に開催されるイベント一覧</p>
            <ul class="p-entries-event__list">
              <% _.each(events, function(event) { %>
                <li class="p-entries-event__listItem <%= event.category %>">
                  <a href="<%= event.url %>">
                    <div class="p-entries-event__listItem__image"><img src="<%= event.thumb_url %>" width="100" height="100" alt=""></div>
                    <div class="p-entries-event__listItem__detail">
                      <p class="__heading"><%= event.title %></p>
                      <span class="__tag"><%= event.category_ja %></span>
                      <p class="__date"><%= moment(event.date).format('LL') %>
                      <% if(event.endDate) { %>
                       ～ <%= moment(event.endDate).format('LL') %>
                      <% } %>
                      </p>
                      <p class="__text"><%= event.description %></p>
                    </div>
                  </a>
                </li>
              <% }); %>
            </ul>
            <p class="p-entries-event__alert"><%= alert %></p>
          </script>
        </div><!-- /.p-entries-event -->
      </div><!-- /#base-content -->

    </div><!-- /.p-container -->
  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>

<!-- Append -->
<script src="<?php echo $page->base; ?>/resources/js/vendor/clndr.js"></script>
<script src="<?php echo $page->base; ?>/resources/js/event-calender.js"></script>
</body>
</html>
