<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>

<!-- Append -->
<link rel="stylesheet" media="screen,print" href="<?php echo $page->base; ?>/resources/css/page-top.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">
    <div class="p-container__fluid">
      <div class="top-hero">
        <h1 class="top-hero-text"><img src="<?php echo $page->base; ?>/resources/img/page/top/top-hero-text-1.png" width="339" height="171" alt="ようこそ壮瞥町"></h1>
      </div>
    </div><!-- /.p-container -->

    <div class="u-display__large">
      <?php include($page->root.'/resources/tpl/page/top/content-large.tpl'); ?>
    </div>
    <div class="u-display__small">
      <?php include($page->root.'/resources/tpl/page/top/content-small.tpl'); ?>
    </div>

    <div class="p-container">
      <div class="top-pref-infomations">
        <div class="top-pref-infomations__item">
          <div class="top-pref-infomations__item__disaster">
            <table class="__table">
              <thead>
                <tr>
                  <th colspan="2">現在の災害・防災・気象情報</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><a href="http://www.river.go.jp/nrpc0303gDisp.do?mode=&areaCode=81&wtAreaCode=2110&itemKindCode=901&timeAxis=60" target="_blank" rel="nofollow">川の防災情報<br>（北海道道南胆振西部）</a></td>
                  <td><a href="http://www.bousai-hokkaido.jp/BousaiPublic/html/iburi/top.html" target="_blank" rel="nofollow">北海道防災情報<br>（胆振）</a></td>
                </tr>
                <tr>
                  <td><a href="http://info-road.hdb.hkd.mlit.go.jp/RoadInfo/index.htm" target="_blank" rel="nofollow">道路情報<br>（北海道開発局）</a></td>
                  <td><a href="http://vivaweb2.bosai.go.jp/viva/v_rsam_usus.htm" target="_blank" rel="nofollow">火山活動連続観測<br>（有珠山）</a></td>
                </tr>
                <tr>
                  <td colspan="2"><a href="http://weather.yahoo.co.jp/weather/jp/1d/2100/1575.html" target="_blank" rel="nofollow">壮瞥町の天気予報</a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="top-pref-infomations__item">
          <div class="top-pref-infomations__item__weather">
            <div class="__image js-weather-image"></div>
            <div class="__detail">
              <div class="__detail__date js-weather-date"><?php echo $page->h($page->getTime()); ?></div>
              <div class="__detail__weather js-weather-desc">&nbsp;</div>
            </div>
          </div>
        </div>
        <div class="top-pref-infomations__item">
          <div class="top-pref-infomations__item__population">
            <p class="__heading"><span>まちの<br>人口</span></p>
            <table class="__table">
              <tr>
                <th>総人口</th>
                <td>
                  <span class="__current_month js-population-all-current"></span>
                  <span class="__previous_month js-population-all-previous"></span>
                </td>
              </tr>
              <tr>
                <th>男性</th>
                <td>
                  <span class="__current_month js-population-male-current">2,681人</span>
                  <span class="__previous_month js-population-male-previous">前月比+100人</span>
                </td>
              </tr>
              <tr>
                <th>女性</th>
                <td>
                  <span class="__current_month js-population-female-current">2,681人</span>
                  <span class="__previous_month js-population-female-previous">前月比+100人</span>
                </td>
              </tr>
              <tr>
                <th>世帯数</th>
                <td>
                  <span class="__current_month js-population-house-current">2,681人</span>
                  <span class="__previous_month js-population-house-previous">前月比+100人</span>
                </td>
              </tr>
            </table>
            <p class="__annotation">※外国人を含めた集計です ※0000年00月00日現在</p>
          </div>
        </div>
      </div>
    </div><!-- /.p-container -->

    <div class="p-container">
      <div class="top-entries-other">
        <div class="top-entries-other__item">
          <div class="top-entries-other__item__header">
              <div class="top-entries-other__item__header__heading __is-immigration">移住のお知らせ</div>
              <div class="top-entries-other__item__header__more-btn"><a href="#">一覧を見る</a></div>
            </div>
          <ul class="top-entries-other__item__list">
            <li class="top-entries-other__item__list__item">
              <a href="#">
                <div class="__image"><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-4.png" width="130" height="100" alt=""></div>
                <time class="__date" datetime="2016-00-00" pubdate>2016.3.20</time>
                <span class="__title">そうべつ「おためし移住」施設。利用ご希望の方に宿泊施設を割引料金にてご提供します。</span>
              </a>
            </li>
            <li class="top-entries-other__item__list__item">
              <a href="#">
                <div class="__image"><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-4.png" width="130" height="100" alt=""></div>
                <time class="__date" datetime="2016-00-00" pubdate>2015.2.25</time>
                <span class="__title">「おすすめ物件情報」を更新しました。詳しくはこちらから！</span>
              </a>
            </li>
            <li class="top-entries-other__item__list__item">
              <a href="#">
                <div class="__image"><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-4.png" width="130" height="100" alt=""></div>
                <time class="__date" datetime="2016-00-00" pubdate>2016.3.20</time>
                <span class="__title">そうべつ「おためし移住」施設。利用ご希望の方に宿泊施設を割引料金にてご提供します。</span>
              </a>
            </li>
          </ul>
        </div>
        <div class="top-entries-other__item">
          <div class="top-entries-other__item__header">
              <div class="top-entries-other__item__header__heading __is-sightseeing">観光のお知らせ</div>
              <div class="top-entries-other__item__header__more-btn"><a href="#">一覧を見る</a></div>
            </div>
          <ul class="top-entries-other__item__list">
            <li class="top-entries-other__item__list__item">
              <a href="#">
                <div class="__image"><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-4.png" width="130" height="100" alt=""></div>
                <time class="__date" datetime="2016-00-00" pubdate>2016.3.20</time>
                <span class="__title">ジオパーク「フードパスメニュー」登場。町内8店舗でジオの恵みを使ったフードパスメニューを提供。</span>
              </a>
            </li>
            <li class="top-entries-other__item__list__item">
              <a href="#">
                <div class="__image"><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-4.png" width="130" height="100" alt=""></div>
                <time class="__date" datetime="2016-00-00" pubdate>2016.2.25</time>
                <span class="__title">第28回 昭和新山国際雪合戦 今年も激戦。開催の模様をUPしました！</span>
              </a>
            </li>
            <li class="top-entries-other__item__list__item">
              <a href="#">
                <div class="__image"><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-4.png" width="130" height="100" alt=""></div>
                <time class="__date" datetime="2016-00-00" pubdate>2016.3.20</time>
                <span class="__title">ジオパーク「フードパスメニュー」登場。町内8店舗でジオの恵みを使ったフードパスメニューを提供。</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div><!-- /.p-container -->

    <div class="p-container__fluid">
      <div class="top-banners">
        <div class="top-heading-delta">バナー一覧</div>
        <div class="top-banners__inner">
          <ul class="top-banners__inner__list">
            <li class="top-banners__inner__listItem"><a href="http://sobetsu-fp.co.jp/" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-1.png" width="285" height="70" alt="そうべつ農産物直売所サムズ"></a></li>
            <li class="top-banners__inner__listItem"><a href="http://okutoya.com/" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-2.png" width="285" height="70" alt="奥洞爺温泉郷"></a></li>
            <li class="top-banners__inner__listItem"><a href="http://www.yukigassen.jp/" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-3.png" width="285" height="70" alt="昭和新山国際雪合戦"></a></li>
            <li class="top-banners__inner__listItem"><a href="http://www.toya-usu-geopark.org/" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-4.png" width="285" height="70" alt="洞爺湖有珠山ジオパーク"></a></li>
            <li class="top-banners__inner__listItem"><a href="http://sobetsu-kankou.server-shared.com/" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-5.png" width="285" height="70" alt="そうべつ観光協会"></a></li>
            <li class="top-banners__inner__listItem"><a href="http://www.sobetsu.net/" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-6.png" width="285" height="70" alt="壮瞥町商工会"></a></li>
            <li class="top-banners__inner__listItem"><a href="http://www.sobetsu-onsen.net/" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-7.png" width="285" height="70" alt="壮瞥町公共日帰り温泉"></a></li>
            <li class="top-banners__inner__listItem"><a href="http://www.kudamonomura.com/" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-8.png" width="285" height="70" alt="そうべつくだもの村"></a></li>
            <li class="top-banners__inner__listItem"><a href="http://orofure.web.fc2.com/" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-9.png" width="285" height="70" alt="オロフレスキー場"></a></li>
            <li class="top-banners__inner__listItem"><a href="http://www.sumo-museum.net/" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-10.png" width="285" height="70" alt="北の湖記念館"></a></li>
            <li class="top-banners__inner__listItem"><a href="https://www.facebook.com/nittan.hokkaido" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-11.png" width="285" height="70" alt="北海道新幹線×nittan地域戦略会議"></a></li>
            <li class="top-banners__inner__listItem"><a href="/link.php" target="_blank" rel="nofollow"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-other-12.png" width="285" height="70" alt="その他リンク"></a></li>
          </ul>
        </div>
      </div>
    </div><!-- /.p-container -->

    <div class="p-container">
      <div class="top-banners-2">
        <ul class="top-banners-2__list">
          <li class="top-banners-2__listItem"><a href="/iju/" target="_blank"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-site-immigration-1.jpg" width="300" height="90" alt="移住情報へ"><span class="__is-immigration">移住情報へ</span></a></li>
          <li class="top-banners-2__listItem"><a href="http://www.sobetsu-kanko.com" target="_blank"><img src="<?php echo $page->base; ?>/resources/img/page/top/banner-site-sightseeing-1.jpg" width="300" height="90" alt="観光情報へ"><span class="__is-sightseeing">観光情報へ</span></a></li>
        </ul>
      </div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
<script>
Script.Top.init();
Script.Top.setResultPopulation(900, 1000, '.js-population-all-current', '.js-population-all-previous');
</script>
</body>
</html>
