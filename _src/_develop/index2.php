<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">テキストが入ります。</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" itemprop="url"><span itemprop="title">まちの紹介</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">まちA</span></li>
    </ul>

    <div class="p-container">
      <div id="base-content__main">
        <div class="p-container__auto-margin-paragraph">
          <article class="p-entry">
            <div class="p-entry__header">
              <h1 class="p-entry__header__heading">壮瞥公園梅の見ごろ予報</h1>
              <div class="p-entry__header__data">
                <ul class="p-entry__header__data__tag-list">
                  <li class="p-entry__header__data__tag-list__item"><a href="#" class="__is-job">しごと</a></li>
                </ul>
                <time class="p-entry__header__data__date" datetime="2016-00-00" pubdate>2016.00.00</time>
              </div>
            </div>
            <div class="p-entry__body">
              <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。<a href="#">テキスト入ります。</a>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
              <p>テキスト入りま入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。<span class="u-text__orange">テキスト入ります。</span>テキスト入ります。テキスト入ります。</p>
              <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
              <p class="c-heading-2 u-mb__0 u-text__blue">壮瞥公園梅の見ごろ予報</p>
              <p class="u-mt__small">テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
            </div>
            <div class="p-entry__footer">
              <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="300" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
            </div>
          </article><!-- /.p-entry -->

          <div class="p-entries-relation">
            <div class="p-entries-relation__header">
              <div class="p-entries-relation__header__heading">関連記事</div>
              <div class="p-entries-relation__header__more-btn"><a href="#">もっと見る</a></div>
            </div>
            <ul class="p-entries-relation__list">
              <li class="p-entries-relation__list__item">
                <a class="__title" href="#">タイトル入ります。</a>
                <span class="__tag"><a href="#" class="__is-job">しごと</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2016.00.00</time>
              </li>
              <li class="p-entries-relation__list__item">
                <a class="__title" href="#">タイトル入ります。タイトル入ります。タイトル入ります。</a>
                <span class="__tag"><a href="#" class="__is-life">食・くらし</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2016.00.00</time>
              </li>
              <li class="p-entries-relation__list__item">
                <a class="__title" href="#">タイトル入ります。</a>
                <span class="__tag"><a href="#" class="__is-nature">自然</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2016.00.00</time>
              </li>
              <li class="p-entries-relation__list__item">
                <a class="__title" href="#">タイトル入ります。タイトル入ります。</a>
                <span class="__tag"><a href="#" class="__is-job">しごと</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2016.00.00</time>
              </li>
              <li class="p-entries-relation__list__item">
                <a class="__title" href="#">タイトル入ります。</a>
                <span class="__tag"><a href="#" class="__is-nature">自然</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2016.00.00</time>
              </li>
            </ul>
          </div><!-- /.p-entries-relation -->

          <div class="p-entries-around">
            <div class="p-entries-around__item__prev">
              <a href="#">
                <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-3.png" width="" height="" alt="">
                <span>けんたろう(苺)、本日出荷!</span>
              </a>
            </div>
            <div class="p-entries-around__item__next">
              <a href="#">
                <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-3.png" width="" height="" alt="">
                <span>大地の春は、行者にんにく</span>
              </a>
            </div>
          </div><!-- /.p-entries-around -->

          <article class="p-entry">
            <div class="p-entry__header">
              <h1 class="p-entry__header__heading-2">壮瞥公園梅の見ごろ予報</h1>
              <div class="p-entry__header__data">
                <ul class="p-entry__header__data__tag-list">
                  <li class="p-entry__header__data__tag-list__item"><a href="#">しごと</a></li>
                </ul>
                <time class="p-entry__header__data__date" datetime="2016-00-00" pubdate>2016.00.00</time>
              </div>
            </div>
            <div class="p-entry__body">
              <div class="u-grid__row">
                <div class="u-grid__col-12">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
              </div>

              <div class="u-grid__row">
                <div class="u-grid__col-6">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
                <div class="u-grid__col-6">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
              </div>

              <div class="u-grid__row">
                <div class="u-grid__col-4">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
                <div class="u-grid__col-4">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
                <div class="u-grid__col-4">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
              </div>

              <div class="u-grid__row">
                <div class="u-grid__col-2">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。</p>
                </div>
                <div class="u-grid__col-2">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。</p>
                </div>
                <div class="u-grid__col-2">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。</p>
                </div>
                <div class="u-grid__col-2">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。</p>
                </div>
                <div class="u-grid__col-2">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。</p>
                </div>
                <div class="u-grid__col-2">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                  <p>テキスト入ります。テキスト入ります。</p>
                </div>
              </div>

              <div class="u-grid__row">
                <div class="u-grid__col-6">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                </div>
                <div class="u-grid__col-6">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
              </div>

              <div class="u-grid__row">
                <div class="u-grid__col-6">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
                <div class="u-grid__col-6">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                </div>
              </div>

              <div class="u-grid__row">
                <div class="u-grid__col-4">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                </div>
                <div class="u-grid__col-8">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
              </div>

              <div class="u-grid__row">
                <div class="u-grid__col-8">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
                <div class="u-grid__col-4">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                </div>
              </div>

              <div class="u-grid__row">
                <div class="u-grid__col-3">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                </div>
                <div class="u-grid__col-3">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
                <div class="u-grid__col-3">
                  <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
                </div>
                <div class="u-grid__col-3">
                  <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
                </div>
              </div>
            </div>
            <div class="p-entry__footer">
              <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="300" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
            </div>
          </article><!-- /.p-entry -->
        </div><!-- /.p-container -->
      </div><!-- /#base-content__main -->

      <div id="base-content__side">
        <div class="p-entries-side">
          <div class="p-entries-side__item">
            <p class="p-entries-side__item__heading">カテゴリー</p>
            <ul class="p-entries-side__item__list">
              <li><a href="#">イベント(8)</a></li>
              <li><a href="#">見所発見(6)</a></li>
              <li><a href="#">旬の物(5)</a></li>
              <li><a href="#">旬の人(4)</a></li>
              <li><a href="#">その他(3)</a></li>
            </ul>
          </div>
          <div class="p-entries-side__item">
            <p class="p-entries-side__item__heading">人気記事TOP5</p>
            <ul class="p-entries-side__item__list__thum">
              <li><a href="#"><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-2.png" width="60" height="60" alt=""><span>壮瞥公園 梅の見ごろ予報</span></a></li>
              <li><a href="#"><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-2.png" width="60" height="60" alt=""><span>仲洞爺キャンプ場&来夢人の家</span></a></li>
              <li><a href="#"><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-2.png" width="60" height="60" alt=""><span>そうべつくだもの村</span></a></li>
              <li><a href="#"><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-2.png" width="60" height="60" alt=""><span>壮瞥公園 梅の見ごろ予報</span></a></li>
              <li><a href="#"><img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-2.png" width="60" height="60" alt=""><span>仲洞爺キャンプ場&来夢人の家</span></a></li>
            </ul>
          </div>
        </div>
      </div><!-- /#base-content__side -->

    </div><!-- /.p-container -->
  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
