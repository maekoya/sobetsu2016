<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" itemprop="url"><span itemprop="title">まちの紹介</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">まちA</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">見出し（大）</h1>
      <h2 class="c-heading-2">見出し（中）</h2>
      <h2 class="c-heading-2__mincho u-text__blue">見出し（中）</h2>
      <h3 class="c-heading-3">見出し（小）</h3>
      <h3 class="c-heading-3__mincho u-text__blue">見出し（小）</h3>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-1">テキストが入ります。テキストが入ります。</h3>
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
          <h3 class="c-heading-2 u-text__blue">テキストが入ります。テキストが入ります。</h3>
          <h3 class="c-heading-3">テキストが入ります。テキストが入ります。</h3>
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
          <h3 class="c-heading-3">見出し（小）</h3>
          <p><strong>テキストテキストテキスト</strong></p>
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-6">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-4">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-4">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-2">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-2">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-2">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-2">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-2">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-2">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
          <p>テキスト入ります。テキスト入ります。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
        </div>
        <div class="u-grid__col-6">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-6">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
        </div>
        <div class="u-grid__col-8">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-8">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-4">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-3">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
        </div>
        <div class="u-grid__col-3">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-3">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
        </div>
        <div class="u-grid__col-3">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-3">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
        </div>
        <div class="u-grid__col-3">
          <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
        </div>
        <div class="u-grid__col-6">
          <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
        </div>
      </div>

      <h3 class="c-heading-3">見出し（小）</h3>
      <table class="c-table-1">
        <tr>
          <th class="__strong">テキスト</th>
          <th class="__strong">テキスト</th>
          <th class="__strong">テキスト</th>
          <th class="__strong">テキスト</th>
        </tr>
        <tr>
          <td>テキスト</td>
          <td>テキスト</td>
          <td>テキスト</td>
          <td>テキスト</td>
        </tr>
        <tr>
          <td>テキスト</td>
          <td>テキスト</td>
          <td>テキスト</td>
          <td>テキスト</td>
        </tr>
      </table>

      <table class="c-table-1">
        <tr>
          <th class="__strong">テキスト</th>
          <td>テキスト</td>
          <td>テキスト</td>
          <td>テキスト</td>
        </tr>
        <tr>
          <th class="__strong">テキスト</th>
          <td>テキスト</td>
          <td>テキスト</td>
          <td>テキスト</td>
        </tr>
        <tr>
          <th class="__strong">テキスト</th>
          <td>テキスト</td>
          <td>テキスト</td>
          <td>テキスト</td>
        </tr>
      </table>

      <table class="c-table-1">
        <tr>
          <th class="__strong">テキスト</th>
          <th class="__strong">テキスト</th>
          <th class="__strong">テキスト</th>
          <th class="__strong">テキスト</th>
        </tr>
        <tr>
          <td class="__weak">テキスト</td>
          <td>テキスト</td>
          <td>テキスト</td>
          <td>テキスト</td>
        </tr>
        <tr>
          <td class="__weak">テキスト</td>
          <td>テキスト</td>
          <td>テキスト</td>
          <td>テキスト</td>
        </tr>
      </table>

      <ol class="c-list__num">
        <li>リスト1</li>
        <li>リスト2</li>
        <li>リスト3</li>
        <li>リスト4</li>
        <li>リスト5</li>
        <li>リスト6</li>
        <li>リスト7</li>
        <li>リスト8</li>
        <li>リスト9</li>
        <li>リスト10</li>
      </ol>

      <ul class="c-list">
        <li>リスト1</li>
        <li>リスト2</li>
        <li>リスト3</li>
        <li>リスト4</li>
        <li>リスト5</li>
        <li>リスト6</li>
      </ul>

      <p><a href="#" class="c-link">リンク</a></p>
      <p><a href="#" class="c-link__blank">外部リンク</a></p>
      <p><a href="#" class="c-link__pdf">PDFリンク</a></p>
      <p><a href="#" class="c-link__xslx">XSLXリンク</a></p>

      <p class="c-annotation">ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
      <p class="c-annotation__no">ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>

      <div class="p-form">
        <div class="p-form-step">
          <ol class="p-form-step-list">
            <li class="p-form-step-list-item is-active">
              <span class="p-form-step-item-heading">STEP<span class="__num">1</span></span>
              <span class="p-form-step-item-label">お問い合わせ<br>入力</span>
            </li>
            <li class="p-form-step-list-item">
              <span class="p-form-step-item-heading">STEP<span class="__num">2</span></span>
              <span class="p-form-step-item-label">お問い合わせ<br>内容確認</span>
            </li>
            <li class="p-form-step-list-item">
              <span class="p-form-step-item-heading">STEP<span class="__num">3</span></span>
              <span class="p-form-step-item-label">お問い合わせ<br>完了</span>
            </li>
          </ol>
        </div>
        <table class="p-form-table">
          <tr>
            <th class="__must">お名前</th>
            <td><input class="c-form__text" type="text"></td>
          </tr>
          <tr>
            <th>ふりがな</th>
            <td><input class="c-form__text" type="text"></td>
          </tr>
          <tr>
            <th>性別</th>
            <td>
              <label class="c-form__radio"><input type="radio">男性</label>
              <label class="c-form__radio"><input type="radio">女性</label>
            </td>
          </tr>
          <tr>
            <th>年齢</th>
            <td><input class="c-form__text __small" type="text">　歳</td>
          </tr>
          <tr>
            <th>ご職業</th>
            <td><input class="c-form__text __middle" type="text"></td>
          </tr>
          <tr>
            <th class="__must">Email</th>
            <td><input class="c-form__text __middle" type="email">　半角</td>
          </tr>
          <tr>
            <th>携帯メールアドレス</th>
            <td><input class="c-form__text __middle" type="email">　半角</td>
          </tr>
          <tr>
            <th>郵便番号</th>
            <td>〒　<input class="c-form__text __xsmall" type="text">　-　<input class="c-form__text __xsmall" type="text"></td>
          </tr>
          <tr>
            <th>ご住所</th>
            <td><input class="c-form__text" type="text"></td>
          </tr>
          <tr>
            <th class="__must">電話番号（携帯可）</th>
            <td><input class="c-form__text __middle" type="text"></td>
          </tr>
          <tr>
            <th>セレクトボックス</th>
            <td>
              <select class="c-form__select">
                <option value="">選択項目</option>
                <option value="">選択項目</option>
                <option value="">選択項目</option>
              </select>
            </td>
          </tr>
          <tr>
            <th>チェックボックス</th>
            <td>
              <label class="c-form__checkbox"><input type="checkbox">選択項目</label>
              <label class="c-form__checkbox"><input type="checkbox">選択項目</label>
              <label class="c-form__checkbox"><input type="checkbox">選択項目</label>
              <label class="c-form__checkbox"><input type="checkbox">選択項目</label>
            </td>
          </tr>
          <tr>
            <th class="__must">お問い合わせ内容は<br>こちらへどうぞ</th>
            <td><textarea class="c-form__text" rows="8"></textarea></td>
          </tr>
        </table>
      </div>

      <p><a href="#" class="c-btn">詳しく見る</a><a href="#" class="c-btn __blue">詳しく見る</a></p>
      <p class="u-text__right"><a href="#" class="c-btn__middle">詳しく詳しく見る</a><a href="#" class="c-btn__middle __blue">詳しく詳しく見る</a></p>
      <p class="u-text__center"><a href="#" class="c-btn__large">重要ボタン</a><a href="#" class="c-btn__large __blue">重要ボタン</a></p>


      <div class="p-qa">
        <dl class="p-qa__item">
          <dt class="p-qa__item__q">
            <span class="p-qa__item__q__num">Q1</span>
            <span class="p-qa__item__q__label">質問が入ります。質問が入ります。質問が入ります。質問が入ります。質問が入ります。質問が入ります。質問が入ります。質問が入ります。質問が入ります。質問が入ります。質問が入ります。質問が入ります。質問が入ります。質問が入ります。質問が入ります。</span>
          </dt>
          <dd class="p-qa__item__a">テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</dd>
        </dl>
        <dl class="p-qa__item">
          <dt class="p-qa__item__q">
            <span class="p-qa__item__q__num">Q1</span>
            <span class="p-qa__item__q__label">質問が入ります。</span>
          </dt>
          <dd class="p-qa__item__a">テキスト入ります。テキスト入ります。</dd>
        </dl>
      </div>

      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
