<?php
// Include general settings.
require($_SERVER["DOCUMENT_ROOT"].'/sobetsu/resources/etc/config.php');

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">壮瞥町ならではの支援体制一覧</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">壮瞥町ならではの支援体制一覧</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">

        <div class="u-grid__row">
        <div class="u-grid__col-12">
        
            <h2 class="c-heading-1">そうべつ町ならではの支援体制一覧</h2>
            <img src="images/shien/shientaisei.gif" width="" height="" alt="">
            
            <section>
            	<h3 class="c-heading-2">子ども支援</h3>
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w15" rowspan="7">子ども</th>
                  <td>0-15歳（中学卒業）までの医療費無料化</td>
                </tr>
                <tr>
                  <td>インフルエンザ予防接種費用を全額補助</td>
                </tr>
                <tr>
                  <td>おたふくかぜ・みずぼうそう予防接種費用の半額補助</td>
                </tr>
                <tr>
                  <td>キッズスポーツクラブ（子ども向け運動教室）の開催</td>
                </tr>
                <tr>
                  <td>子育て支援センターの運営、育児サークル開催</td>
                </tr>
                <tr>
                  <td>チャイルドシート等の無償貸し出し</td>
                </tr>
                <tr>
                  <td>ブックスタート事業（絵本などのプレゼント）</td>
                </tr>
                <tr>
                  <th class="__strong u-w15" rowspan="3">小学生</th>
                  <td>放課後児童クラブの運営（６年生まで受入）</td>
                </tr>
                <tr>
                  <td>町単独での外国語指導助手の配置</td>
                </tr>
                <tr>
                  <td>キッズスポーツクラブ（子ども向け運動教室）の開催</td>
                </tr>
                <tr>
                  <th class="__strong u-w15">中学生</th>
                  <td>中学２年生全員を公費負担でフィンランドへ派遣</td>
                </tr>
                <tr>
                  <th class="__strong u-w15">高校生</th>
                  <td>通学定期代への補助（半額）</td>
                </tr>
              </table>
            </section>
        
            <section>
            	<h3 class="c-heading-2">働き盛り支援</h3>
                <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w15" rowspan="7">住まい</th>
                      <td>持ち家住宅取得奨励金（新築最大100万円・中古50万円）</td>
                    </tr>
                    <tr>
                      <td>住宅リフォームをされる方へ工事費に合わせて6-10万円の商品券交付</td>
                    </tr>
                    <tr>
                      <td>空き家を売却･賃貸するための整理･改修費の2/3を助成（上限30万円）</td>
                    </tr>
                    <tr>
                      <td>民間賃貸住宅を建設される方に１戸あたり上限100万円を助成</td>
                    </tr>
                    <tr>
                      <td>水洗便所改造等に必要な資金を無利子貸付</td>
                    </tr>
                    <tr>
                      <td>合併処理浄化槽設置費の一部を補助</td>
                    </tr>
                    <tr>
                      <td>温泉水を浴用・暖房用に低額で供給（蟠渓地区のみ）</td>
                    </tr>
                    <tr>
                      <th class="__strong u-w15" rowspan="2">医療</th>
                      <td>20歳以上の町民全員を対象とした健診を年2回開催（国保・後期は無料）</td>
                    </tr>
                    <tr>
                      <td>40歳以上の方を対象とした脳ドック検診を実施（一部本人負担有り）</td>
                    </tr>
                    <tr>
                      <th class="__strong u-w15" rowspan="5">仕事</th>
                      <td>新たに起業する方に補助金を交付（対象経費の1/2以内、上限100万円）</td>
                    </tr>
                    <tr>
                      <td>特産品開発に取り組む方に補助金を交付（対象経費の1/2以内、上限100万円）</td>
                    </tr>
                    <tr>
                      <td>農商工連携に取り組む方に補助金を交付（対象経費の4/5以内、上限200万円）</td>
                    </tr>
                    <tr>
                      <td>事業所の改修等を行う事業者に補助金を交付（対象経費の1/2以内、上限200万円）</td>
                    </tr>
                    <tr>
                      <td>新規就農者･農業後継者に最大200万円を助成</td>
                    </tr>
                    <tr>
                      <th class="__strong u-w15">その他</th>
                      <td>母子家庭等の方への特定利用券交付（町営温泉が130円で利用可）</td>
                    </tr>
              </table>
            </section>

            <section>
            	<h3 class="c-heading-2">高齢者支援</h3>
                <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w15" rowspan="8">支援内容</th>
                      <td>ドアtoドアのデマンド交通を運行（片道町内100円・町外通院500円）</td>
                    </tr>
                    <tr>
                      <td>65歳以上の方への利用証交付（町営温泉が130円で利用可）</td>
                    </tr>
                    <tr>
                      <td>70歳以上の方は、町内での路線バス運賃を無料化</td>
                    </tr>
                    <tr>
                      <td>携帯電話型の緊急通報装置を年額3000円で貸与</td>
                    </tr>
                    <tr>
                      <td>高齢者等の世帯を対象とした各種サービスを実施(※いずれも利用条件有)
                      	<ul class=" c-list">
                        	<li>配食（１食300円で夕食を配達）</li>
                            <li>除雪（無料）</li>
                            <li>入浴送迎（年間1,000円）</li>
                            <li>友愛訪問（安否確認のためボランティアがヤクルトを持って訪問（無料）</li>
                            <li>出支援サービス（デイサービス）　※有料</li>
                            <li>訪問支援サービス（ホームヘルプ）　※有料</li>
                            <li>短期入所サービス（ショートステイ）　※有料</li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td>長寿祝金のお渡し（満88・99・100歳）</td>
                    </tr>
                    <tr>
                      <td>家族介護用品の支給（1ヶ月8,000円まで）　※利用条件あり</td>
                    </tr>
                    <tr>
                      <td>住宅改修拡大措置事業（工事費の半額助成、80万円まで）　※利用条件あり</td>
                    </tr>
              </table>
            </section>

            </div>
         </div>



      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
