<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">まちの紹介</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">まちの紹介</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">子育て支援・高齢者福祉・長寿</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">手厚い子育て支援・充実した高齢者福祉とご長寿のまち</h1>
      
      <h2 class="c-heading-2">（1）手厚い子育て支援</h2>
      <p>壮瞥町は過疎化、少子高齢化が進んでいますが、少ないからこその手厚いサービスが提供できるよう取り組んでいます。</p>

      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kosodate/img01.jpg" width="" height="" alt="子どもセンター">
        </div>
        <div class="u-grid__col-6">
          <p>平成22年には、子育て支援の総合施設「そうべつ子どもセンター」を建設し、保育所（短時間・長時間）、児童館、児童クラブ（小学校6年生まで入所年限を引き上げ）、子育て支援センターを一つの施設にまとめ、子どもたちが新しい施設で快適な時間をすごす環境を整備し、同時に保護者の皆さんの利便性を高めています。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kosodate/img02.jpg" width="" height="" alt="子育て応援住宅">
        </div>
        <div class="u-grid__col-6">
          <p>平成25年には、「子育てしやすい賃貸住宅の施設、機能とはどういうものか？」をテーマに、現役で子育てをされている町内のママさんたちとのワークショップを重ね、子育てしやすさに配慮したメゾネットタイプ（※）の「ママと考えた子育て応援住宅コティ」を考案し、平成26年度から建設を開始しており、入居された子育て世代のみなさんからご好評をいただいています。</p>
          <p class="c-annotation">メゾネットタイプとは、住戸内が2階層（以上）に分かれているもの（複層住戸）を指します。簡単に言えば「マンション、アパートで住戸が2つの階にまたがっているもの」ということです。賃貸アパートでよく見られる「ロフト」とは異なり、マンションやアパートでありながら、2階建ての一戸建て住宅のような構造を取り入れたものです。小さな子供がいる家庭では騒音の発生を気にして「マンションの1階を選ぶ」というケースもありますが、メゾネットの上階に子供部屋を設ければその心配もありません。</p>          
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kosodate/img03.jpg" width="" height="" alt="子育て支援サービス">
        </div>
        <div class="u-grid__col-6">
          <p>施設面以外でも、ボランティアを交えた子育て世代向けの各種サービス・事業のほか、平成25年からは中学生以下のお子様の医療費無料化制度を導入しているほか、高校への通学定期運賃代の半額助成制度、インフルエンザやおたふくかぜなどの法定外予防接種へのまち独自助成、妊産婦健診助成の拡充、持家住宅取得に対する助成制度、チャイルドシート等の無料貸出制度などを導入しているほか、保育料、国保税、上下水道料などの公共料金も国の基準や他の市町村と比較して全般的に低く設定し、子育て世代の経済的負担の軽減に努めています。</p>
        </div>
      </div>

      
     
      <h2 class="c-heading-2">（2）充実した高齢者福祉とご長寿のまち</h2>
      <p>壮瞥町は、北海道としては少雪・温暖な気候で、豊かな自然環境、町内各地にある温泉、果物・米・野菜など多種多様で安全安心な農産品などにも恵まれ、女性の平均寿命が88.4歳と北海道内1位、全国2位（平成22年国勢調査）と全国屈指のご長寿のまちです。</p>


      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kosodate/img04.jpg" width="" height="" alt="健診">
        </div>
        <div class="u-grid__col-6">
          <p>昭和53年に北海道の事業として実施された「寒冷地における高血圧疫学調査研究」が、研究期間終了後も札幌医科大学の全面的な協力により今もなお「生活習慣病健診」として継続されており、町民には「夏の健診」として定着し、その受診率は非常に高く、町民の健康意識の高揚につながっています。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kosodate/img05.jpg" width="" height="" alt="コミュニティタクシー">
        </div>
        <div class="u-grid__col-6">
          <p>それ以外にも、町内の豊富な温泉を町民のみなさんに楽しんでもらうため、65歳以上の方は町営温泉施設のお風呂が1回130円で入浴できる制度や、町内ならどこでも1回100円、近隣の伊達市や洞爺湖町までの通院なら1回500円で利用できるドアツードア型の乗合タクシー「コミュニティタクシー」も運行しているほか、充実した高齢者福祉サービスを提供しています。</p>
          <p><a href="/kurashi/kenko/kaigo-fukushi.html" class="c-link">高齢者福祉ページみる</a></p>
        </div>
      </div>
      
      
      

      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
