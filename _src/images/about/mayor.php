<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">まちの紹介</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">まちの紹介</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">町長室</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">町長室</h1>

      <h2 class="c-heading-2">町長挨拶</h2>
      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/mayor.jpg" width="" height="" alt="壮瞥町町長">
        </div>
        <div class="u-grid__col-6">
          <p>壮瞥町は、世界レベルの大地の公園として世界ジオパークネットワークに登録された、美しい洞爺湖、雄大な有珠山、国の特別天然記念物に指定されている昭和新山、火山の恵みである温泉など、
          豊富な天然資源に恵まれています。また、北海道内でも比較的温暖な気候を活かしたりんごやぶどうをはじめとする果樹生産、高級菜豆、米、地熱を利用した野菜の栽培など多種多様な農作物を生産
          する「農業と観光のまち」です。</p>
          <p>また、昭和の大横綱・北の湖の生誕地であるとともに、冬季の呼び物として人気のあるスポーツ雪合戦を開発し、国内外に普及させるなど町民一丸となった取り組みは、
          まさに壮瞥町の誇れる財産です。</p>
          <p>高齢化社会、人口減少、地方財政の厳しい現状などの課題に加え、20世紀で4回の噴火活動を繰り返した有珠山をかかえておりますが、限られた財源の中で火山と共生し、
          町民が安心して暮らせる環境づくり、資源を活かした力強い産業の振興を進めるためには、町民と行政がお互いに連携、協力し合う、協働のまちづくりを進めることが必要不可欠です。<br>
          小さくても「『自然・ひと・まち』が響き輝くそうべつ」として、官民一体となって壮瞥町の未来を創ってまいります。</p>
		 <p class="c-heading-3__mincho u-text__right">壮瞥町長　佐藤秀敏</p>            
        </div>
      </div>

      <h2 class="c-heading-2">経歴</h2>
      <table class="c-table-1">
        <tr>
          <th class="__strong u-text__left" width="20%">昭和23年</th>
          <td class="u-text__left">壮瞥町出身</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">昭和36年</th>
          <td class="u-text__left">仲洞爺小学校卒業</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">昭和39年</th>
          <td class="u-text__left">仲洞爺中学校卒業</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">昭和42年</th>
          <td class="u-text__left">伊達高校卒業後、家業の農業に従事</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">昭和43年</th>
          <td class="u-text__left">壮瞥町消防団入団</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">平成 5年</th>
          <td class="u-text__left">壮瞥町農業委員会委員</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">平成 7年</th>
          <td class="u-text__left">西胆振消防組合壮瞥消防団第5分団分団長</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">平成13年</th>
          <td class="u-text__left">壮瞥町社会福祉協議会理事</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">平成15年</th>
          <td class="u-text__left">壮瞥町議会議員初当選</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">平成19年</th>
          <td class="u-text__left">壮瞥町議会議員(2期目) 壮瞥町議会総務常任委員長</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">平成20年</th>
          <td class="u-text__left">壮瞥町農業委員会会長</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">平成23年</th>
          <td class="u-text__left">壮瞥町長選に初当選</td>
        </tr>
        <tr>
          <th class="__strong u-text__left">平成27年</th>
          <td class="u-text__left">二期目当選</td>
        </tr>
      </table>
      
      <section class="u-grid__row">
      	<h3 class="c-heading-2">町政執行方針・予算編成方針</h3>
        <ul class="c-list">
         <li><a href="/pdf/yosanhenseihoushin_h27.pdf" class="c-link__pdf">平成27年度予算編成方針DL/PDF</a></li>
         <li><a href="/pdf/chouseisikkouhoushin_h25.pdf" class="c-link__pdf">平成26年度町政執行方針DL/PDF</a></li>
         <li><a href="/pdf/chouseisikkouhoushin_h25.pdf" class="c-link__pdf">平成25年度町政執行方針DL/PDF</a></li>
        </ul>
      </section>

      <section class="u-grid__row">
      	<h3 class="c-heading-2">町長交際費</h3>
        <p>町長交際費は、町政の推進に必要な交際をするために支出するもので、町長交際費支出基準にもとづき支出しております。</p>
        <h4 class="c-heading-3">交際費支出状況一覧</h4>
        <ul class="c-list">
         <li><a href="/pdf/kousaihi_h26.pdf" class="c-link__pdf">平成26年度分DL/PDF</a></li>
         <li><a href="/pdf/kousaihi_h25.pdf" class="c-link__pdf">平成25年度分DL/PDF</a></li>
         <li><a href="/pdf/kousaihi_h24.pdf" class="c-link__pdf">平成24年度分DL/PDF</a></li>
        </ul>
      </section>


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
