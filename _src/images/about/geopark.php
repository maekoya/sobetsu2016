<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">まちの紹介</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">まちの紹介</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">洞爺湖有珠山ジオパーク</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">国内初の世界ジオパーク認定</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/geopark/img01.gif" width="280" height="396" alt="洞爺有珠ジオパーク">
        </div>
        <div class="u-grid__col-8">
          <p>ジオパークとは、科学的に価値の高い地形・地質・火山などの地質遺産が複数ある自然公園です。</p>
          <p>保護だけでなく、研究・教育にも活用され、さらに観光の対象とする「ジオツーリズム」を通じて、持続可能な地域経済や地域文化の発展を目指す取り組みです。</p>
          <p>2004年には『世界ジオパークネットワーク（GGN）』がユネスコの支援により設立され「世界ジオパーク」がスタートしました。<br>2015年11月には「国際地質科学ジオパーク計画（International Geoscience and Geoparks Program：IGGP）」として、ユネスコの正式事業になりました。現在、世界33カ国120地域（2015年9月現在）のジオパークがGGNに加盟しています。<br>平成21年、洞爺湖有珠山ジオパークは、中国・泰安市で開催されたGGN事務局会議で、新潟県糸魚川・長崎県島原半島とともに国内初の世界ジオパークに認定され、世界レベルの自然公園として、その価値と取り組みが認められました。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/geopark/img02.jpg" width="280" height="186" alt="審査風景">
        </div>
        <div class="u-grid__col-4">
          <img src="images/geopark/img03.jpg" width="280" height="186" alt="通知が来た時の反応">
        </div>
        <div class="u-grid__col-4">
          <img src="images/geopark/img04.jpg" width="280" height="186" alt="国内初の世界ジオパークに認定">
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/geopark/img05.jpg" width="280" height="425" alt="空から撮影された昭和新山">
        </div>
        <div class="u-grid__col-8">
          <p>｢洞爺湖有珠山ジオパーク｣は”変動する大地の共生”をテーマとし、エリアは壮瞥町のほか、伊達市、洞爺湖町、豊浦町の全域と留寿都村、真狩村の一部を含み面積は約1,180平方キロもあります。</p>
          <p>約11万年前の巨大な火砕流を出す噴火でカルデラの窪地ができ、そこに水が溜まった洞爺湖とその周囲に広がる火砕流台地、5万年前頃に噴火を繰り返してできた中島、約2万年前から噴火が始まった有珠山の生成、そして、近世以降9回の顕著な火山活動と人間の歴史を数多くの地質や地形、遺跡、災害遺構、防災施設などを通して知ることが最大の特徴です。</p>
          <p>また、この地域では、地球活動のダイナミズムや被災史ばかりではなく、洞爺カルデラをつくった火砕流噴火により形成された洞爺火砕流台地で収穫される農産物の豊かな実りや、7～8000年前の有珠山の山体崩壊による岩屑なだれの流下でつくられた岩礁の多い有珠湾近海で獲れる多くの魚介類の恵みを人々が享受してきた歴史があり、現在、北海道有数の観光地として発展してきた洞爺湖温泉の必須の要件でもある温泉が湧き出たのも1910年の火山活動の恩恵のひとつとして、それらのすべてがジオパークを構成する資源となっています。</p>
          <p><a href="http://www.toya-usu-geopark.org/" class="c-link__blank" target="_blank">洞爺湖有珠山ジオパーク</a></p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/geopark/img06.jpg" width="280" height="186" alt="1977火山遺構公園">
        </div>
        <div class="u-grid__col-4">
          <img src="images/geopark/img07.jpg" width="280" height="186" alt="鉄橋遺構公園">
        </div>
        <div class="u-grid__col-4">
          <img src="images/geopark/img08.jpg" width="280" height="186" alt="ガイドの取組">
        </div>
      </div>

      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
