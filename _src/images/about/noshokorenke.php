<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">まちの紹介</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">まちの紹介</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">農商工連携</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">農商工連携の取組</h1>
      
      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/noshoko/img01.jpg" width="435" height="300" alt="物産展" class="u-mb__0">
        </div>
        <div class="u-grid__col-6">
          <img src="images/noshoko/img02.jpg" width="435" height="300" alt="かぼちゃのしっとりケーキ" class="u-mb__0">
        </div>
      </div>
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <p>壮瞥町では近年、この農商工連携の取り組みが加速しています。
    壮瞥町商工会会員などで構成する委員会を中心に、2013年の秋からは町内産のこだわりの「ダークホースかぼちゃ」を使ったスイーツを、町内の旅館、飲食店などで提供を開始しました。ダークホースかぼちゃとは、2005年11月に登録された比較的新しい西洋かぼちゃの品種です。 特長は、表面の皮の色が濃黒緑色でハート型をしている外観と、 甘味が強く強粉質で、栗の様にホクホクしているその食味です。なんとその甘さは、メロンより甘い糖度以上と言われています。</p>
    	</div>
      </div>



      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/noshoko/img03.jpg" width="435" height="600" alt="シードル">
        </div>
        <div class="u-grid__col-6">
          <p>また、2015年からは町内の果樹農家や商工、観光業者ら約20名で実行委員会を組織し、特産のリンゴを使ったシードル（りんご酒）と炭酸ジュース造りに本格的に乗り出しています。道内や青森の先進地視察を重ね、研究を続けてきました。</p>
          <p>原料のリンゴは、町内の果樹農家21軒から18キロ入りリンゴ箱で263ケースを購入。品種はふじ、北斗、王林、紅玉、ジョナゴールドなどで、複数の品種を使うことで壮瞥リンゴが持つ、香りや食味を最大限生かした製品に仕上がりました。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/noshoko/img04.jpg" width="435" height="250" alt="グルメマルシェ">
        </div>
        <div class="u-grid__col-6">
          <p>壮瞥町ではこれらの動きに呼応するように、「壮瞥町農商工連携推進委員会」を設置、農商工連携基本戦略を策定し、関係者の協議の場の拡大、テイクアウト商品開発、関係イベントの開催、補助制度の整備などを行い、壮瞥町の新たな特産品開発を進めています。</p>
        </div>
      </div>


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
