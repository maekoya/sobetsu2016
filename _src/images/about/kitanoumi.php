<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">まちの紹介</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">まちの紹介</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">北の湖</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">名誉町民　北の湖親方</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/kitanoumi/img01.jpg" width="435" height="391" alt="昭和の大横綱・北の湖親方">
        </div>
        <div class="u-grid__col-8">
          <p>昭和の大横綱・北の湖親方（本名小畑敏満）は、昭和28年、壮瞥町で父・小畑勇三、母テルコの４男として誕生しました。壮瞥中学校1年在学中の昭和41年、弱冠13歳で三保ヶ関部屋に入門、当時の身長は173センチ、体重100キロ、その後厳しい稽古に耐え、刻苦精励、生来の素質を大きく開花させました。17歳11ヶ月で十両に昇進、18歳7ヶ月で幕内、19歳7ヶ月で小結、20歳7ヶ月で大関と、<br>当時の大相撲界では異例の記録的なスピード出世を果たしました。</p>
          <p>昭和49年、21歳2ヶ月の若い横綱の誕生である第55代横綱に推挙されると、優勝回数24回(うち全勝優勝7回)、通算勝利951勝、幕内勝利804勝、連続二桁勝利37場所、横綱勝利670勝、年間最多勝利回数7回、横綱在位63場所など数々の記録を打ち立てました。先輩横綱である輪島とは、ほとんどの場所でこの両者が優勝を争う「輪湖時代」と呼ばれる一時代を築きました。</p>
          <p>昭和60年1月場所、両国国技館のこけら落としとなったこの場所で現役引退し、一代年寄「北の湖」を襲名、「北の湖部屋」を開設しました。
長年にわたる現役時代中、1年に90回、テレビ・ラジオを通じて、「北海道有珠郡壮瞥町出身」のアナウンスが全国に流れたことは、私たち町民や町を離れた人々にとってどんなに勇気と誇りを与えてくれたかわかりません。</p>            
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/kitanoumi/img02.jpg" width="435" height="421" alt="横綱北の湖銅像">
        </div>
        <div class="u-grid__col-8">
          <p>また、北の湖親方は現役時代から常に故郷のことを思いやる方でした。<br>昭和52年有珠山噴火の際には、壮瞥中学校グランドで「北の湖横綱出世相撲」を開催し被災者を激励、平成12年の有珠山噴火の際は、東京都において有珠山噴火復興観光キャンペーンに参加し、力強いエールを送ってくれました。</p>
          <p>壮瞥町では平成3年に「壮瞥町郷土史料館・横綱北の湖記念館」を建設、同館は国技館を模して造られ、施設内には資料展示室(北の湖関係資料約500点)をはじめ、24枚の優勝額やミニ桟敷、やぐら、実物大の土俵に北の湖の土俵入りの模型などが展示されています。<br>平成4年には、その功績と栄誉をたたえ名誉町民を贈呈、郷土後援会も設立され、引退後も交流が続きました。平成20年には記念館前に「横綱北の湖銅像」が完成し除幕されました。</p>            
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/kitanoumi/img03.jpg" width="435" height="186" alt="第9代理事長に就任">
        </div>
        <div class="u-grid__col-8">
          <p>平成14年2月1日、北の湖親方は日本相撲協会の第9代理事長に就任しました。<br>戦後世代としては初の理事長で、低迷する相撲人気の回復を最優先課題として「土俵の充実」を力説し、その改革への実行力が期待されました。しかし、平成19年頃より、現役力士の大麻問題、不祥事などが発生し、平成20年に引責辞任しましたが、平成24年、北の湖親方は第12代の理事長に選出され、再び混迷する日本相撲協会の舵取りを委ねなれました。「残りの人生のすべてを懸ける覚悟です」と悲壮な決意を述べた北の湖親方でしたが、平成27年11月、九州場所が開かれているさなかに緊急入院し、治療を受けていましたが、11月20日、帰らぬ人となりました。<br>享年62歳、あまりにも早すぎる別れでした。</p>
          <p><a href="http://www.sumo-museum.net/" class="c-link__blank" target="_blank">横綱北の湖記念館</a></p>     
        </div>
      </div>


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
