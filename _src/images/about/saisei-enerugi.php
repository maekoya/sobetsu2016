<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">まちの紹介</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">まちの紹介</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">再生可能エネルギー</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h1 class="c-heading-1">再生可能エネルギーのまち</h1>

          <p>農業経営の安定化が課題となっていた壮瞥町の幸内地区で、昭和51年、国のエネルギー政策の転換も背景に、全国初となる地熱水の野菜栽培利用事業に着手しました。様々な調査、実験、検収、実習を乗り越えて、昭和55年、ついに地熱水を利用した促成栽培など効率性の高い施設園芸を導入し、農業経営の改善を図るとともに、年間を通した生鮮野菜の安定供給と省エネルギー化を推進する事業が開始されました。</p>          
          <img src="images/saisei/img01.jpg" width=900"" height="450" alt="地熱団地">          
          <p>昭和55年~58年にかけて48棟の施設を建設して3つの団地を形成、2.5km先の泉源から動力ポンプでくみ上げ、温泉管を通って高低差を活用した自然流下により運ばれた地熱水は、施設内に這わされたチューブ管を通ることで、放熱により地温と室温を上昇させています。</p>
       </div>
      </div>
          
      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/saisei/img02.jpg" width="435" height="257" alt="地熱ハウス">
        </div>
        <div class="u-grid__col-6">
          <p>さらに排湯地熱水をさらに下流にまで流し、病院・老人福祉施設、町営温泉施設で入浴用に、中学校では熱交換し暖房にも活用するなど、自然の恵みである地熱水を余すことなく利用しています。</p>
          <p>しかし、建設当時は、水の給水の仕組みはありましたが、温泉水は高温であるがゆえに管内の圧力が変化し、施設に届く前に止まってしまうトラブルが頻発、その際に作物を守るためジェットヒーターなどで温度管理する作業が昼夜を問わず発生しました。<br>また、土地の改良にも3年近い年数を要するなど、事業が軌道に乗るまで苦労の連続でしたが、その後は全国初の事業を見ようと視察団が殺到、平成元年には19か国の駐日大使がこの施設を見学し、国際的にも関心が高い施設であることがわかりました。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/saisei/img04.jpg" width="" height="" alt="地熱資源調査">
        </div>
        <div class="u-grid__col-4">
          <p>当初は様々な作物が生産されていましたが、近年はトマトに特化し、北国では異例の2-7月に収穫、出荷するという栽培法が確立し、「オロフレトマト」という名称で町の風物詩として定着し、壮瞥町内ではこれ以外にも各地で温泉が湧出しており、主に浴用に利用されていますが、活用率の低かった蟠渓温泉地区では平成26年の地熱資源開発調査を行ったところ、<br>地下深部に地熱貯留層が推定され、今後の地熱発電事業や農業などの2次利用への活用を視野に入れ、現在も調査が続いています。</p>
          <p>国では平成42年までに再生可能エネルギーの比率を20%以上にまで高める目標を掲げていますが、壮瞥町の再生可能エネルギー自給率は389%で、全国17位、北海道内3位と高い順位に位置しています。</p>
        </div>
        <div class="u-grid__col-4">
			<img src="images/saisei/img03.jpg" width="435" height="257" alt="オロフレトマト">
        </div>
      </div>



      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
