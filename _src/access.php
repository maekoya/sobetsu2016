<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="css/access.css">
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
google.maps.event.addDomListener(window, 'load', function() {
	var mapdiv = document.getElementById('map');
	var myOptions = {
	zoom: 12,
	center: new google.maps.LatLng(42.575194,140.956743),
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	draggable: false,
	scaleControl: true,
	scrollwheel: false
	};
	var map = new google.maps.Map(mapdiv, myOptions);
});
</script>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">アクセス/交通機関</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">アクセス/交通機関</span></li>
    </ul>

    <div class="p-container__full__auto-paragraph map_bg">
    	<div class="map_area">
        	<div class="text_box">
            	<h2><img src="images/access/map_text.png" width="" height="" alt="壮瞥町へようこそ"></h2>
                <dl>
                	<dt>壮瞥町役場</dt>
                    <dd>〒052-0101<br>北海道有珠郡壮瞥町字滝之町287番地7<br>
                    TEL：<a href="0142662121">（0142）66-2121</a><br>
                    FAX：（0142）66-7001<br>
                    <p><a href="" class="c-btn__middle __blue">Google Mapで見る</a></p>
                    </dd>
                </dl>
            </div>
            <p class="img_box"><img src="images/access/map.png" width="" height="" alt=""></p>
        </div>
    </div>
    


    
    <div class="p-container__full__auto-margin-paragraph">
	
        <h2 class="c-heading-1">壮瞥町への交通<br class="u-display__small">アクセス</h2>
  
        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="title_bg"><strong>札幌</strong>から</h3>
              <img src="images/access/access_sapporo.gif" width="" height="" alt="札幌から壮瞥町へ行き方">
              <p class=" c-link u-text__right"><a href="http://donanbus.co.jp/kougai/" target="_blank">道南バスの時刻表はこちら <span class="text_url">http://donanbus.co.jp/kougai/</span></a></p>
             </section>
         </div>

        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="title_bg"><strong>新千歳空港</strong>から</h3>
              <img src="images/access/access_shinchitose.gif" width="" height="" alt="新千歳空港から壮瞥町へ行き方">
              <p class=" c-link u-text__right"><a href="http://donanbus.co.jp/kougai/" target="_blank">道南バスの時刻表はこちら <span class="text_url">http://donanbus.co.jp/kougai/</span></a></p>
             </section>
         </div>

        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="title_bg"><strong>苫小牧</strong>から</h3>
              <img src="images/access/access_tomakomai.gif" width="" height="" alt="苫小牧から壮瞥町へ行き方">
              <p class=" c-link u-text__right"><a href="http://donanbus.co.jp/kougai/" target="_blank">道南バスの時刻表はこちら <span class="text_url">http://donanbus.co.jp/kougai/</span></a></p>
             </section>
         </div>

        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="title_bg"><strong>室蘭</strong>から</h3>
              <img src="images/access/access_muroran.gif" width="" height="" alt="室蘭から壮瞥町へ行き方">
              <p class=" c-link u-text__right"><a href="http://donanbus.co.jp/kougai/" target="_blank">道南バスの時刻表はこちら <span class="text_url">http://donanbus.co.jp/kougai/</span></a></p>
             </section>
         </div>

        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="title_bg"><strong>函館</strong>から</h3>
              <img src="images/access/access_hakodate.gif" width="" height="" alt="函館から壮瞥町へ行き方">
              <p class=" c-link u-text__right"><a href="http://donanbus.co.jp/kougai/" target="_blank">道南バスの時刻表はこちら <span class="text_url">http://donanbus.co.jp/kougai/</span></a></p>
             </section>
         </div>
        
         
         <div class="u-grid__row">
             <section class="u-grid__col-12">
              <h2 class="title_bg u-block__center">高速道路利用の場合は</h2>
             <ul class="box_block">
                <li><strong>道央自動車道伊達IC</strong>から約<span>20</span>分</li>
                <li><strong>道央自動車道虻田洞爺湖IC</strong>から約<span>25</span>分</li>
              </ul>         
             </section>
         </div>
    </div><!-- /.p-container -->


    <div class="p-container__full__auto-paragraph blue_bg">
    	<h2 class="c-heading-2__mincho"><span>交通のお問い合わせ先</span></h2>
        <ul class="traffic_info">
        	<li><a href="tel:0142-23-2148"><p><span class="name">JR伊達紋別駅</span><br><span class="ico_tel">0142-23-2148</span></p></a></li>
            <li><a href="tel:0142-75-2277"><p><span class="name">道南ハイヤー</span><br><span class="ico_tel">0142-75-2277</span></p></a></li>
            <li><a href="tel:0142-75-2277"><p><span class="name">道南バス洞爺湖温泉ターミナル</span><br><span class="ico_tel">0142-75-3117</span></p></a></li>
            <li><a href="tel:0142-66-2366"><p><span class="name">毛利ハイヤー</span><br><span class="ico_tel">0142-66-2366</span></p></a></li>
        </ul>
    </div>

    <div class="p-container__full__auto-paragraph gmap">
    	<h2 class="c-heading-2__mincho u-text__blue">MAP</h2>
        <div id="map"></div>
    </div>


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
