<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">まちの紹介</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">まちの紹介</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">ケミヤルヴィ交流</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">中学生全員をフィンランドへ派遣<br>（ケミヤルヴィ市との交流）</h1>
      
      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kemibyaru/img01.jpg" width="435" height="245" alt="フィンランド調印式" class="u-mb__0">
        </div>
        <div class="u-grid__col-6">
	        <img src="images/kemibyaru/img02.jpg" width="435" height="245" alt="ケミ雪合戦" class="u-mb__0">
        </div>
      </div>
      
       <div class="u-grid__row">
        <div class="u-grid__col-12">
      <p>平成5年、壮瞥町は人口規模・産業・地理的条件が比較的類似している、フィンランドのラップランド州にあるケミヤルヴィ市（以下「ケ市」と表記）との友好都市調印を行い、その交流は現在もなお続いています。
同年6月、ケ市で開催された国際木彫シンポジウムに、北海道在住の彫刻家を派遣するとともに6名の壮瞥町訪問団がケ市を訪れました。翌年2月には、ケ市から2名が友好親善のため当町を訪れ、「昭和新山国際雪合戦」を視察し、木彫シンポジウムへの彫刻家の派遣、雪合戦視察のためのケ市からの来町は翌年も続き、1995年、海外初の雪合戦大会がケ市で開催され、この運営と技術指導のため、町民スタッフ5名が派遣され、その後も雪合戦を通じた総合交流が継続されています。</p>
        </div>
     </div>


      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kemibyaru/img03.jpg" width="435" height="326" alt="フィンランド">
        </div>
        <div class="u-grid__col-6">
          <p>平成7年には中学生フィンランド国派遣海外研修事業が始まりました。<br>町内の中学生全員を全額公費で海外研修させるこの方法は全国的にも珍しい事例ですが、小さな町だからできる事業といえます。</p>
          <p>当初は中学3年生が対象でしたが、高校受験や修学旅行との重複を避けるため、平成10年からは中学2年生が対象になっています。これまでの21回にわたる参加者合計は735名、また、ケ市からも壮瞥町に12回209名の学生が訪れています（平成28年2月現在）。<br>フィンランド研修には語学学習など事前研修への参加、行程に現地での3泊4日のホームステイをいれること、事後の報告書の作成という3つの約束事があります。国際化時代の到来を踏まえて、感受性豊かな中学生はこの研修を通じて多くのことを体験し、相互の子どもたちの知的好奇心を刺激し、心を育てています。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kemibyaru/img04.jpg" width="435" height="400" alt="ケ市来町">
        </div>
        <div class="u-grid__col-6">
          <p>ケ市では日本語を会の名称に使った「友達クラブ」が、ホームステイの受け入れや交流事業を行う交流推進団体として組織され、壮瞥町でも交流事業に参加した人や中学生の世帯を中心に一般住民に参加を呼びかけ、町民による国際交流推進団体「キートス（フィンランド語で「ありがとう」という感謝の気持ちを表す言葉）クラブ」が設立されています。<br>ケ市の学生訪問団が当町を訪れる際にはキートス・クラブ会員が、当町から派遣する際には友達クラブがホームステイ受け入れ等を行い、交流と友好的な関係を構築する上で欠かすことのできない存在となっています。</p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kemibyaru/img05.jpg" width="435" height="245" alt="来夢人の家">
        </div>
        <div class="u-grid__col-6">
          <p>平成10年には経済面での交流を深めようと、建設予定だった温泉施設を付帯した仲洞爺キャンプ場センターハウスをフィンランドログ材を利用して建てることになりました。<br>建設に利用されたログ材はケ市の北50kmにあるサブコスキー産のラップランド松で、全部つなぎ合わせると5.2kmにも及ぶ材料をトラック、シベリア鉄道、ロシアのウラジオストクからは船で韓国の釜山へ、そこから苫小牧港へ輸送され、実に110日間の長旅を経て到着した逸材で、約半年の建設期間を経て無事平成11（1999）年に完成し、現在も活用されています。</p>
        </div>
      </div>
      
      
      

      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
