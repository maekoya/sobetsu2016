<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">まちの紹介</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">まちの紹介</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">火山と共生するまち</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">火山と共生するまち</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <img src="images/kazan/img01.jpg" width="900" height="450" alt="ダイナミックな自然景観">
          <p>壮瞥町にある有珠山は約2万年前に生成された新しい火山で、1663 年以降2000年までに9回、特に20世紀には4度噴火しています。ひとたび噴火すれば、大きな被害をもたらし、時には生命の危険にすらさらされます。しかし、その一方で火山は、ダイナミックな自然景観や温泉などの地熱資源、農耕に適した肥沃な大地など様々な恵みをもたらし、壮瞥町の住民生活や産業を支えてきました。私たち壮瞥町は、火山の存在を正面から受け入れ、理解し、適切に防減災対策を講ずることで住民や観光客の安全性を確保し、同時にその恵みを享受する、そんな「火山と共生するまち」として様々な取り組みをしています。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/kazan/img02.jpg" width="280" height="400" alt="火災災害">
        </div>
        <div class="u-grid__col-8">
          <p>かつてこの地域では防災論議イコール危険情報・マイナスイメージという拒絶の風土がありました。観光資源が火山に由来するという認識も薄く、温泉と景観の陰に「噴火・災害」が潜在することを軽視していました。</p>
          <p>1977年有珠山噴火の前夜、壮瞥町は火山性地震が発生している中で昭和新山爆発再現花火大会を実施し、山麓には強烈な揺れを感じながらも５万人が花火を楽しんでいました。幸いにして噴火は翌朝午前9時頃という観光客の少ない時間に起こり、初動における犠牲者は出さずに済みました。</p>
          <p>しかし、一歩間違えば大惨事を招いたことは疑う余地もありません。この噴火の４年前、科学者が北海道の依頼により有珠山を調査し、噴火に備える必要性を報告していたにもかかわらず、その情報は周辺地域で共有されず、全く無防備な状態で噴火を迎えたのです。<br>このときの反省が、壮瞥町が平時の啓発事業を開始する出発点（原点）となりました。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kazan/img03.jpg" width="435" height="246" alt="国際火山ワークショップ開催">
        </div>
        <div class="u-grid__col-6">
          <p>1993年から95年にかけて、昭和新山生成50周年記念事業を地域の有志の手によって開催することになり、その中心事業として「国際火山ワークショップ（火山会議)」 が開催されました。そして当時としては画期的な防災マップが発刊され、全戸に配布されました。<br>さらに1997年には地域防災計画の見直しと避難所サイン（看板）整備にあわせ、避難経路、避難場所を明示した壮瞥町独自の防災マップ「災害に備えて」を発刊、全戸配付を行うとともに、毎月の町広報誌に火山防災一口メモを継続的に掲載するなど、行政情報の提供を強化し、住民の生活の一部として根付くよう努力を積み重ねてきました。</p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kazan/img04.jpg" width="435" height="481" alt="昭和新山">
        </div>
        <div class="u-grid__col-6">
          <p>直近の2000年噴火では人的な被害は全くありませんでした。<br>この背景には、有珠山の麓に北海道大学有珠火山観測所があり研究者が常駐していたこと、自然環境や火山防災に造詣の深い地域リーダー三松正夫記念館長・三松三朗氏の存在が大きいと言えます。</p>
          <p>また、有珠山周辺では、1983年から「子ども郷土史講座（壮瞥町教委主催）」や、「昭和新山・有珠山登山会」等、活火山の懐（フィールド）に出かけ、 自然の恵みと一時期の災害について有識者からコメントをもらい「地域の災害環境を学ぶ」事業が継続してきました。</p>
          <p>このような事業を通して、有珠山に関する情報の共有が図られ、専門家と行政、住民間での信頼関係も構築されていたため、迅速な措置に呼応して住民が事前避難の行動をとったのです。</p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/kazan/img05.jpg" width="435" height="246" alt="小中高生を対象とした防災キャンプ事業">
        </div>
        <div class="u-grid__col-6">
          <p>2000年噴火以降も、火山防災を念頭においた社会資本整備、自治体間や民間企業と防災協定締結、フォーラムなどの普及啓発や防災訓練、小中高生等を対象とした防災キャンプ事業など、
          次期噴火を見据え、その備えを続けているのです。</p>
        </div>
      </div>
	

      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
