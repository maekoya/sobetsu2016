<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">まちの紹介</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">まちの紹介</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">雪合戦</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">昭和新山国際雪合戦</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <img src="images/yukigassen/img01.jpg" width="900" height="450" alt="開会式">
          <p>「雪合戦」―雪国の子どもたちのこの遊びが、北の大地で冬のニュースポーツに進化しました。毎年2月に、全国の予選を勝ち抜いた精鋭150チームが、『スポーツ雪合戦』発祥の地、大地の息吹のような噴煙たなびく壮瞥町昭和新山のふもとに集い、熱いバトルを繰り広げます。</p>
        </div>
      </div>

	<div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/yukigassen/img02.jpg" width="435" height="218" alt="スポーツ雪合戦" class="u-mb__0">
        </div>
        <div class="u-grid__col-6">
          <img src="images/yukigassen/img03.jpg" width="435" height="218" alt="専用ヘルメットや雪球製造器などの用具開発にも着手" class="u-mb__0">
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <p>雪合戦は、1チーム9名（出場は7名）の選手と１人の監督により編成され、強い精神力と瞬時の状況判断力、そして戦術を生み出す知力が求められる『スポーツ雪合戦』、筋書きのない戦いに会場は大いに盛り上がります。 壮瞥町は、北海道内屈指の観光地ですが、冬の観光客誘致による地域活性化が町の大きな課題でした。昭和62年、町内の若者グループが立ち上がり、活性化のアイディアを考えましたが決定打は出ません。そんな時に、生まれて初めて見る雪に感動し、子どものように無邪気に雪を丸めて投げ合う東南アジアからの旅行者たちの姿を見た彼らは、「雪国に住む者は雪の神秘さ、雪遊びの楽しさを忘れていた」ことに気づき、昔誰もが遊んだ雪合戦を『スポーツ雪合戦』として再生させようと考えたのです。</p>
        </div>
      </div>
	
    <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/yukigassen/img04.jpg" width="435" height="218" alt="国際連合">
        </div>
        <div class="u-grid__col-6">
          <p>「その後、幾多の壁を乗り越え、世界初のスポーツ雪合戦のルールを作ります。さらに、専用ヘルメットや雪球製造器などの用具開発にも着手し、ついに平成元年、第1回の「昭和新山国際雪合戦」が開催されました。このスポーツの普及はめざましく、北は北海道から南は九州・大分まで、全国各地で大会が行われています。また、平成7年にフィンランドで海外初の大会が行われると、その後、ヨーロッパ、北米、オーストラリアにも広がり、平成25年には海外11か国で構成する国際雪合戦連合も設立され、壮瞥町生まれの国際ルールに基づいた「Yukigassen」大会が開催されるなど、『スポーツ雪合戦』のグローバル化は着々と進んでいます。</p>
        </div>
      </div>      
      
      <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/yukigassen/img05.jpg" width="280" height="186" alt="ボランティアが運営" class="u-mb__0">
        </div>
        <div class="u-grid__col-4">
          <img src="images/yukigassen/img06.jpg" width="280" height="186" alt="町民を中心とするボランティア"  class="u-mb__0">
        </div>
        <div class="u-grid__col-4">
          <img src="images/yukigassen/img07.jpg" width="280" height="186" alt="ボランティアの豚汁" class="u-mb__0">
        </div>        
      </div>
      

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <p>このように発展を続ける雪合戦ですが、大会は今もなお町民を中心とする多くのボランティアが運営しています。例えば、お年寄りの方は「会場は寒いから行けないけど、豚汁の野菜切りはできる」と公民館の調理室で手伝い、商工会青年部は重機やトラックを持ち込みコートづくり、若いスタッフは会場を設営し、試合の審判を務めるなど、町内各地区、各世代、各業種の町民が、雪合戦の協働作業を通じ、交流を深めています。</p>
		  </div>
      </div>
      
      
      <div class="u-grid__row">
        <div class="u-grid__col-6">
          <img src="images/yukigassen/img08.jpg" width="435" height="250" alt="海外の方も参加">
        </div>
        <div class="u-grid__col-6">
          <p>私たちの夢、それは、この雪合戦が世界各地で行なわれるようになっても、昭和新山は「雪合戦の聖地」であり、数ある大会の中でも昭和新山国際雪合戦こそが世界最高峰の大会であり、「雪合戦のウィンブルドン」であり続けたい。北国の小さな町の大きな挑戦はこれからも続きます。</p>
          <p><a href="http://www.yukigassen.jp/" class="c-link__blank" target="_blank">昭和新山国際雪合戦実行委員会</a></p>
        </div>
      </div>


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
