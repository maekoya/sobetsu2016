<?php
// Include general settings.
//require($_SERVER['CONFIG_PATH']);

require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="css/about.css">
<!--[if lt IE 9]>
<script src="js/jquery-1.11.3.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="js/jquery-2.1.4.min.js"></script>
<script>
    $(function(){
        $("p.main_btn_link").on("click", function() {
            $(this).closest(".title_area").next(".content_area").slideToggle();
        });
    });
    $(function(){
        $(".close").on("click", function() {
            $(this).closest(".content_area").slideToggle();
        });
    });
</script>
<script>
if( window.matchMedia('(max-width:679px)').matches ){
 $(function setPagetop() {
	var pagetop = $('.c-pagetop'),

		show_flg = false;

	$(window).on('scroll', function() {
		var st = $(this).scrollTop(),
			wh = $(this).height();
		var under = $( 'body' ).height() - ( st + $(window).height() ) ;

		if(st > wh/4 && !show_flg) {
			pagetop.stop().animate({opacity: 1}, 200);
			show_flg = true;
		} else if(st < wh/4 && show_flg || under < 10) {
			pagetop.stop().animate({opacity: 0}, 200);
			show_flg = false;
		}
	})
});
}
</script>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container" class="about_page">

    <div class="p-container__full__auto-paragraph" id="about01">
    	<div class="title_area about01">
        	<div class="p-container__full__auto-margin-paragraph">
                <h1><img src="images/index/main01_txt.png" width="" height="" alt="勇壮な活火山と優美な洞爺湖。心もわきたつ壮瞥町。" /></h1>
                <p class="lead">ダイナミックにして美しい景観は、<br class="u-display__small">地球が誕生した<br class="u-display__large">約46億年前に閉じ込められた<br class="u-display__small">熱が原動力となって生み出されました。<br>
                洞爺湖、有珠山、そして奇跡の山・昭和新山は、<br class="u-display__small">この地の宝物。<br>幾多の噴火によって生まれた活火山は、
                <br class="u-display__small">こんこんと湧き出る温泉や<br>
                肥沃な大地がもたらす食の恵みなどの恩恵を授け、<br>壮瞥町の暮らしと文化、産業を支えています。</p>
                <p class="main_btn_link">「町の概要」はこちらから</p>
                <ul class="local_nav">
                    <li><a href="#about02">私が、町長です。</a></li>
                    <li><a href="#about03">火山と共生するまち</a></li>
                    <li><a href="#about04">活気あるスポーツのまち</a></li>
                    <li><a href="#about05">名誉町民 北の湖親方</a></li>
                    <li><a href="#about06">子育て支援・高齢者福祉</a></li>
                </ul>
            </div>
        </div>
        <div class="content_area p-container__full__auto-margin-paragraph">
        	<dl>
            	<dt class="">町名の由来</dt>
                <dd>アイヌ語で「滝の川」を意味する「ソーペツ」より転化し「壮瞥」となりました。</dd>
            </dl>
        	<dl class="rekishi">
            	<dt>歴&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;史</dt>
                <dd>
                	<p>壮瞥町は、明治12年、岩手県人の移住により開拓の鍬が入れられ、郷土の建設が始まりました。<br>
                    明治32年8月、西紋鼈・長流・有珠の3村から分離独立して、戸数346戸、人口1,304人（昭和54年発行の壮瞥町史より）をもって壮瞥村戸長役場が設置されました。<br>
                    大正4年4月、2級町村制が施行され、徳舜別村（現伊達市大滝区）を分村しました。昭和14年4月、1級町村制施行、昭和37年1月、町村施行となり現在に至っています。</p>
                    <div class="col2">
                    	<img src="images/index/about1_img01.jpg" width="" height="" alt="" class="fl" />
                        <p class="zoom u-text__blue font_l u-mb__0"><b>町政施行50周年記念「壮瞥町史」</b></p>
                        <p class="zoom u-mt__small">前町史発刊（昭和54年）以降の本町の歴史がまとめられています。<br>
                        A4版約500ページ、付属DVD（114分）付、1冊5,000円<br>
                        町ではこの「壮瞥町史」を販売していますので、ご希望の方は下記までご連絡ください。<br>
                        壮瞥町役場企画調整課 TEL <a href="tel:0142662121">0142（66）2121</a></p>
                    </div>
                </dd>
            </dl>
        	<dl class="ichi">
            	<dt class="w18">地勢・位置</dt>
                <dd>
                	町は北海道の南西部に位置し、東はオロフレ山系を境とし伊達市大滝区、白老町に接し、南部は<br>
                    登別市と伊達市に、また西は洞爺湖をはさんで洞爺湖町に接しています。<br>
                    町の中央を東から西へ長流川が貫流し、本町はこの流域の平坦地と、その周辺の丘陵地に大きく<br>
                    分けることが出来ます。<br>
                    東経／141度4分23秒～140度49分 <br class="u-display__small">北緯／42度30分12秒～42度38分56秒<br>面積／205．94平方km
                </dd>
            </dl>
        	<dl class="kisyo">
            	<dt class="w18">気&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;象</dt>
                <dd>
                	内浦湾をはるかに望む壮瞥町は、北海道では珍しい温暖な地に属し、<br class="u-display__large">道内でも快適な地といえます。<br>
                    気温は年平均7.8℃で、冬の積雪は平坦地で30cm前後と、
                    道内では比較的<br class="u-display__large">少ないのが特徴です。
                </dd>
            </dl>
        	<dl class="tokei">
            	<dt class="w22">町勢要覧・統計書</dt>
                <dd>
                	町勢要覧・統計書はこちらからダウンロードできます。<a href="" class="c-link__pdf">町勢要覧</a>
                </dd>
            </dl>
        	<dl class="chosyo">
            	<dt class="w18">町&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;章</dt>
                <dd>
                	全体の形象は、壮瞥の「そ」を<br class="u-display__small">図形化したもので、<br class="u-display__small">上部は翼を広げて躍進を示し、<br>
                    さらに有珠山と昭和新山の山頂を<br class="u-display__small">表現しています。<br>
                    下部は、洞爺湖を象り、<br class="u-display__small">円満にして明朗闊達を意味しています。<br>
                    (昭和33年6月30日制定)
                </dd>
                <dd><img src="images/index/about1_img03.jpg" width="" height="" alt="" class="" /></dd>
            </dl>
        	<dl class=" choka">
            	<dt>町花</dt>
                <dd class="c-heading-3 font_w_normal">むくげ</dd><dd class="img"><img src="images/index/about1_img04.jpg" width="" height="" alt="" class="" /></dd>
            	<dt class="choboku">町木</dt>
                <dd class="c-heading-3 font_w_normal">もみじ、いちい</dd><dd class="img"><img src="images/index/about1_img05.jpg" width="" height="" alt="" class="" /></dd>
            </dl>
        	<dl class="bg_box chouminkensyo">
            	<dt class="w16">壮瞥町民憲章</dt>
                <dd>1．壮瞥町は、湖と火山と温泉の町です。自然を愛し、恵まれた環境を大切にします。<br>
                1．壮瞥町は、農業と観光の町です。資源を活用し、豊かな町づくりに励みます。<br>
                1．壮瞥町は、文化と平和の町です。心をゆたかにし、互いに信頼し合います。<br>
                1．壮瞥町は、健康づくりの町です。スポーツに親しみ、心と体をきたえます<br>
                1．壮瞥町は、未来に開かれた町です。より高い目標に向かい、希望をもって進みます。</dd>
                <dd><img src="images/index/about1_img06.jpg" width="" height="" alt="" /></dd>
            </dl>
        	<dl class="toshi">
            	<dt class="w16">友好都市</dt>
                <dd>
                フィンランド国ケミヤルヴィ市（平成5年調印）は、<br class="u-display__large">ラップランド北部と北極圏の高原地帯の中央に位置します。<br>
                1700年代より交易の中心地として栄え、<br class="u-display__large">ケミ川の豊富な水資源により発展。<br>
                総面積の1割強が河川・湖沼で占められており、<br class="u-display__large">市全体が豊かな自然に囲まれています。
                <p class="u-mt__small u-mb__0"><a href="http://www.kemijarvi.fi/" target="_blank" class="c-link"><span class="color_bl">http://www.kemijarvi.fi/</span></a></p>
                </dd>
                <dd>
	                <img src="images/index/about1_img07.jpg" width="" height="" alt=""/>
                </dd>
            </dl>
            
            <p class="c-link"><a href="">壮瞥町の観光情報サイト <span class="color_bl">http://www.sobetsu-kanko.com</span></a></p>
            
            <p class="close"><img src="images/index/close.gif" width="" height="" alt="" /></p>
        </div>
    </div>
    <!-- /.about01 -->


	<!-- .about02 -->
    <div class="p-container__full__auto-paragraph" id="about02">
    	<div class="title_area about02">
        	<div class="p-container__full__auto-margin-paragraph">
            	<div class="lead_left">
                    <h2><img src="images/index/main02_txt.png" width="" height="" alt="壮瞥一筋の男" /></h2>
                    <p class="lead">“つなぎ”に袖を通すのは、町長就任以来なのだとか。<br class="u-display__large">
                    18歳で家業を継ぎ、62歳で現職に就くまで土に触れ続け、<br class="u-display__large">
                    農業振興に尽力した人だけあって、命を見つめる目はまっすぐ。<br>
                    それは、スーツ姿の町長であっても同じこと。<br class="u-display__large">
                    町に適した“土”を耕し、“水”で潤し、<br class="u-display__small">一人ひとりに“光”をあてる。<br>
                    圃場で培った信念は、壮瞥町民への想いに通じています。</p>
                </div>
                <p class="main_btn_link">「私が、町長です。」はこちらから</p>
            </div>
        </div>
        <div class="content_area p-container__full__auto-margin-paragraph">
            <div class="u-clearboth">
                    <div class="fl">
                        <dl>
                            <dt>子どもの頃は、洞爺湖で水泳、<br class="u-display__small">ヒメマス釣り。<br>冬は自宅の畑でスキー三昧。</dt>
                            <dd>町長は、生まれも育ちも壮瞥町、洞爺湖東湖畔の仲洞爺育ち。<br class="u-display__large">
                            幼い頃は、カルデラ湖では日本第3位の大きさを誇る洞爺湖で<br class="u-display__large">
                            水泳をしたり（今は遊泳禁止ですが）、ヒメマスを釣ったり。<br>
                            冬になれば傾斜地だった自宅の畑をゲレンデに見立て、スキー三昧。<br>
                            まるで支笏洞爺湖国立公園の大自然を、庭のように遊んだのでした。
                            <img src="images/index/about2_img01.jpg" width="" height="" alt="" />                
                            </dd>
                        </dl>
                        <dl>
                            <dt>壮瞥の地で、「一“所”懸命」。<br>家では孫に甘いおじいちゃん。</dt>
                            <dd>町長には、趣味がありません。<br>
                            それもそのはず、農家の三代目として家業を継いだ最盛期には、<br class="u-display__large">水田・畑作・畜産と幅広く、特に豚舎で<br class="u-display__large">
                            生き物を相手にしている頃は、お盆や正月休みも無かったのです。<br class="u-display__large">けれど、ひたむきに農業と向き合ったからこそ得るものもありました。
                            その経験値は多方面に生かされ、壮瞥町農業委員会会長や、町議2期を勤め上げ、2011年から町長に。<br>
                            そんな多忙な町長ですが、家に帰れば優しいおじいちゃん。今は、孫が寝るまで一緒に遊ぶことが、何よりも癒される時間なのだとか。<br>
                            ちなみに座右の銘は「一“所”懸命」。<br>
                            どこまでも壮瞥ひと筋、なのであります。       
                            </dd>
                        </dl>
                    </div>
                    <div class="fr">
                        <dl>
                            <dt>忘れられない「昭和52年」。<br>母校の閉校、そして火祭りの夜…</dt>
                            <dd>町長は、四国・徳島から北海道に入植した開拓三代目として家業を継ぐべく、若くして農業に従事しました。<br>
                            その傍ら町の消防団に所属し、さらに明治時代に入植した人々から継承される「仲洞爺・獅子舞保存会」の一員でもありました。<br>
                            あれは確か20代最後の年。母校だった小中学校が閉校し、一抹の寂しさを抱えながら迎えた、夏の「昭和新山火祭り」。<br class="u-display__large">
                            ステージで仲間と獅子舞踊りを披露していると、地震が発生！<br>その翌日の1977(昭和52)年8月7日、有珠山が噴火したのです。           
                            </dd>
                        </dl>
                        <dl>
                            <dt>町長の横顔</dt>
                            <dd>周囲に町長の人柄を聞くと、<br class="u-display__large">
                            「温厚」「誠実」という声がかえってきます。趣味もないし、お酒も飲めないけれど、みんなと一緒に居るのが大好きな町長。<br>
                            ちなみにカラオケも好き。<br>だから、アルコールが入っていなくても会合がお開きになるまでみんなと盛り上がることが出来ちゃうお茶目な一面も。<br class="u-display__large">
                            人とのつながり、絆、和やかな雰囲気や安心感を大切にする町長。<br>
                            それは、どんな時も一人ひとりの声に耳を傾ける、<br>町長としての姿勢にも通じています。
                            <img src="images/index/about2_img02.jpg" width="" height="" alt="" /> 
                            </dd>
                        </dl>
                    </div>
                </div>
            
            <p><a href="" class="c-link">「町長室」を見る</a></p>
            <p class="close"><img src="images/index/close.gif" width="" height="" alt="" /></p>
        </div>
    </div>
    <!-- /.about02 -->


	<!-- .about03 -->
    <div class="p-container__full__auto-paragraph" id="about03">
    	<div class="title_area about03">
        	<div class="p-container__full__auto-margin-paragraph">
            	<div class="lead_left">
                    <h2><img src="images/index/main03_txt.png" width="" height="" alt="火山と共生するまち" /></h2>
                    <p class="lead">火山のふもとで、暮らしをたくましく築き上げてきた壮瞥町。<br>
                    それは、生きている大地と人々の強さが響き合ってきた<br class="u-display__large">
                    歴史でもあります。<br class="u-display__small">私たちは防災・減災の知恵を培いながら、<br>
                    一方で、壮大で美しい自然景観や多彩な資源を享受。<br class="u-display__large">
                    “火山と共生するまち”として、<br class="u-display__small">独自の取り組みを展開しています。</p>
                </div>
               <p class="main_btn_link">「火山と共生するまち」はこちらから</p>
            </div>
        </div>
        <div class="content_area p-container__full__auto-margin-paragraph">
        	<ul class="col4">
            	<li>
                	<img src="images/index/about3_img01.jpg" width="" height="" alt="" />
                	<p class="txt_s u-mb__0">災害に強いまちづくり</p>
                	<h3 class="txt_l u-mt__small">火山と共生する<br>まち</h3>
                    <p>有珠山は20世紀に4回もの噴火を繰り返し、私たちに“共生”のメッセージを発しました。しかしその一方で、火山は美しい自然景観や温泉、
                    地熱資源などの恵みをもたらしています。私たちは、その恩恵に感謝するとともに、適切な防災・減災対策を講ずることで、“火山と共生するまち”として
                    平時から防災・安全管理の強化と普及啓発に取り組んでいます。</p>
                    <a href="" class="more">もっと情報を見る</a>
                </li>
            	<li>
                	<img src="images/index/about3_img02.jpg" width="" height="" alt="" />
                	<p class="txt_s u-mb__0">地球が息づく壮大な“自然公園”</p>
                	<h3 class="txt_l u-mt__small">国内初の<br>世界ジオパーク認定</h3>
                    <p>伊達市、豊浦町、壮瞥町、洞爺湖町で推進する「洞爺湖有珠山ジオパーク」は、2009年に日本初の世界ジオパークに登録されました。
                    洞爺カルデラや有珠山などに代表される地質遺産や自然遺産などの豊富な見どころを、フットパス(散策路)やガイドツアーなどによる体験学習を通じて、
                    楽しみながら学ぶことができます。</p>
                    <a href="" class="more">もっと情報を見る</a>
                </li>
            	<li>
                	<img src="images/index/about3_img03.jpg" width="" height="" alt="" />
                	<p class="txt_s u-mb__0">多彩な農産物と地熱利用</p>
                	<h3 class="txt_l u-mt__small">再生エネルギーの<br>まち</h3>
                    <p>火山がもたらす肥沃な大地を活用した果樹・野菜・稲作、畜産品など、壮瞥町の農産物はバラエティに富んでいます。
                    なかでも、地熱を活用した施設栽培の「オロフレトマト」が北国では異例の2～7月に収穫できることで人気を集めるなど、近年は高付加価値商品の開発や、
                    再生可能エネルギーの高自給率を受け、地熱発電事業を視野に入れた研究も進んでいます。</p>
                    <a href="" class="more">もっと情報を見る</a>
                </li>
            	<li>
                	<img src="images/index/about3_img04.jpg" width="" height="" alt="" />
                	<p class="txt_s u-mb__0">垣根を越えた連携で特産品を開発！</p>
                	<h3 class="txt_l u-mt__small">農商工連携<br>プロジェクト</h3>
                    <p>町の資源を生かした特産品の開発も本格始動しています。2013年からは壮瞥産「ダークホースかぼちゃ」を使ったスイーツを、町内の旅館や飲食店で提供。
                    また、2015年からは果樹農家、商工、観光業者が連携し、特産のリンゴを活用したシードル（リンゴ酒）作りに乗り出しています。</p>
                    <a href="" class="more">もっと情報を見る</a>
                </li>
            </ul>
            <p class="close"><img src="images/index/close.gif" width="" height="" alt="" /></p>
        </div>
    </div>
    <!-- /.about03 -->

	<!-- .about04 -->
    <div class="p-container__full__auto-paragraph" id="about04">
    	<div class="title_area about04">
        	<div class="p-container__full__auto-margin-paragraph">
            	<div class="lead_left">
                    <h2><img src="images/index/main04_txt.png" width="" height="" alt="活気あるスポーツのまち" /></h2>
                    <p class="lead">やっかいものの雪を活用し、北国の冬を元気にしようと<br>
                    町民が考案した冬のニュースポーツ「雪合戦」は、<br>
                    いまや壮瞥発の「yukigassen」として世界共通語になっています。<br>
                    スポーツを通して、健康と活力のある生活に貢献できるよう、<br>
                    町では、優秀な指導者との連携や行政の枠組みを超えた推進体制で、<br>
                    スポーツを核とした魅力あるまちづくりに取り組んでいます。</p>
                </div>
                    <p class="main_btn_link">「活気あるスポーツのまち」はこちらから</p>
            </div>
        </div>
        <div class="content_area p-container__full__auto-margin-paragraph">
             <div class="u-clearboth">
                <section class="u-grid__col-6">
                  <img src="images/index/about4_img01.jpg" width="" height="" alt="">
                  <h3 class=" txt_l"><span class=" txt_s">“スポーツ雪合戦”発祥の地！</span>昭和新山国際雪合戦</h3>
                  <p>1987年、町内の若者グループが立ち上がり、忘れかけていた雪遊びの楽しさをここ壮瞥町で『スポーツ雪合戦』に昇華させました。
                  以来、毎年2月、予選を勝ち抜いた国内外150チームが噴煙たなびく昭和新山のふもとに集い、熱いバトルを繰り広げます。</p>
                   <a href="" class="more tr">もっと情報を見る</a>
                </section>
                <section class="u-grid__col-6">
                  <img src="images/index/about4_img02.jpg" width="" height="" alt="">
                  <h3 class=" txt_l"><span class=" txt_s">剣道・柔道・バドミントン </span>全国レベルで活躍！</h3>
                  <p>町内には、剣道やバドミントンなどにおいて全国レベルで活躍した実績を持つ指導者がおり、現在、スポーツ少年団として剣道2団体、バドミントン1団体があり、
                  全国レベルで活躍。近年は、総合型地域スポーツクラブを設立するなど、スポーツ環境のさらなる充実を図っています。</p>
                   <a href="" class="more tr">もっと情報を見る</a>
                </section>
              </div>        
            <p class="close"><img src="images/index/close.gif" width="" height="" alt="" /></p>
        </div>
    </div>
    <!-- /.about04 -->

	<!-- .about05 -->
    <div class="p-container__full__auto-paragraph" id="about05">
    	<div class="title_area about05">
        	<div class="p-container__full__auto-margin-paragraph">
            	<div class="lead_left">
                    <h2><img src="images/index/main05_txt.png" width="" height="" alt="名誉町民 北の湖親方" /></h2>
                    <p class="lead">「憎らしいほど強い」。<br>
                    数々の記録を打ち立てた昭和の大横綱、北の湖。<br class="u-display__large">
                    1953(昭28)年に壮瞥町で産声をあげ、<br class="u-display__small">弱冠13歳で初土俵を踏み、<br>
                    史上最年少の21歳2ヶ月で横綱に昇進。<br>
                    巨体ながらも迅速な動きと多彩な技で、先輩横綱・輪島とともに<br class="u-display__large">
                    「輪湖時代」と呼ばれる一時代を築きました。<br>
                    テレビ・ラジオから「北海道有珠郡壮瞥町出身」の<br class="u-display__large">
                    アナウンスが全国に流れ続けたことは、町民そして町を離れた人々に<br>
                    勇気と誇りを与え、その存在は、今も私たちの胸を熱くします。</p>
                </div>
                    <p class="main_btn_link">「誉町民 北の湖親方」はこちらから</p>
            </div>
        </div>
        <div class="content_area p-container__full__auto-margin-paragraph">
        	<p class="mincho u-text__center font_l">2015年11月20日、<br class="u-display__small">北の湖親方が逝去されました。<br class="u-display__small">壮瞥町の名を全国に知らしめ、<br>
            二度の有珠山噴火の際にも町の復興に大変<br class="u-display__small">ご支援をいただきました。<br>
            生前のご功績に対し、敬意と感謝の念を表すとともに心よりご冥福をお祈り申し上げます。</p>
             <div class="u-clearboth u-mt__40">
                <div class="u-grid__col-8">
                  <img src="images/index/about5_img01.jpg" width="" height="" alt="">
                </div>
                <div class="u-grid__col-4">
                  <p>壮瞥町は、前日本相撲協会理事長であり、昭和の大横綱・北の湖の生誕の地。<br>
                  町ではその偉業をたたえ、1992年に「名誉町民章」を贈呈、町内には「横綱 北の湖記念館」があります。</p>
                  <a href="" class="more block">もっと情報を見る</a>
                  <p><a href="" class="c-link">横綱 北の湖記念館 <br class="u-display__small">http://www.sumo-museum.net</a></p>
                </div>
              </div>
            <p class="close"><img src="images/index/close.gif" width="" height="" alt="" /></p>
        </div>
    </div>
    <!-- /.about05 -->


	<!-- .about06 -->
    <div class="p-container__full__auto-paragraph" id="about06">
    	<div class="title_area about06">
        	<div class="p-container__full__auto-margin-paragraph">
            	<div class="lead_left">
                    <h2><img src="images/index/main06_txt.png" width="" height="" alt="子育て支援と高齢者福祉" /></h2>
                    <p class="lead">少子高齢化がすすむ壮瞥町ですが、<br>
                    だからこそ手厚い支援が提供できる町といえます。<br class="u-display__large">
                    町では、子どもの医療費無料化、子育て支援住宅、さらに中学生の<br class="u-display__large">
                    国際交流など、子育て世代を応援する独自の取り組みも多様。<br class="u-display__large">
                    また、住み慣れた町で健康を維持しながら安心して暮らし続けられる<br>
                    まちづくり、生きがいと介護予防を推進するまちづくりを目指し、<br>
                    ご高齢者の支援・福祉の充実をすすめています。</p>
                </div>
                    <p class="main_btn_link">「子育て支援と高齢者福祉」はこちらから</p>
            </div>
        </div>
        <div class="content_area p-container__full__auto-margin-paragraph">
        	<ul class="col3">
            	<li>
                	<img src="images/index/about6_img01.jpg" width="" height="" alt="" />
                	<p class="txt_s u-mb__0">ママといっしょに考えます</p>
                	<h3 class="txt_l u-mt__small">手厚い子育て支援</h3>
                    <p>子育て世代を応援するため、子どもの医療費無料化や予防接種助成、通学定期運賃の補助などを実施しています。
                    また、ママたちとワークショップを重ねて考案した「ママと考えた子育て支援住宅」や、保育所、子育て支援センター、児童クラブなどの機能を一つにまとめた
                    「そうべつ子どもセンター」など独自に取り組んでいます。</p>
                    <a href="" class="more tr">もっと情報を見る</a>
                </li>
            	<li>
                	<img src="images/index/about6_img02.jpg" width="" height="" alt="" />
                	<p class="txt_s u-mb__0">中学生全員をフィンランドへ派遣！</p>
                	<h3 class="txt_l u-mt__small">ケミヤルヴィ市との交流</h3>
                    <p>豊かな森と湖があり、北欧のイメージと重なる壮瞥町は1993年、フィンランド北東部のケミヤルヴィ市と友好都市提携を結びました。
                    以来、国際感覚の養成を目的として町内の中学生のフィンランド国派遣事業(海外研修)がスタート。
                    さらに、ゲミヤルヴィ市学生訪問団のホームステイを受け入れるなど、さまざまな交流を重ねています。</p>
                    <a href="" class="more tr">もっと情報を見る</a>
                </li>
            	<li>
                	<img src="images/index/about6_img03.jpg" width="" height="" alt="" />
                	<p class="txt_s u-mb__0">全国屈指のご長寿のまち！</p>
                	<h3 class="txt_l u-mt__small">充実した高齢者福祉</h3>
                    <p>北海道では比較的少雪・温暖で、温泉や食に恵まれた壮瞥町は、第二の人生を豊かに過ごせる環境です。平成22年の国勢調査では、
                    女性の平均寿命は道内1位(全国2位)になるほどのご長寿のまち。福祉施設やサービスの充実はもちろん、生涯学習や社会参加まで、高齢者の総合的な支援をすすめています。</p>
                    <a href="" class="more tr">もっと情報を見る</a>
                </li>
            </ul>
            <p class="close"><img src="images/index/close.gif" width="" height="" alt="" /></p>
        </div>
    </div>
    <!-- /.about06 -->
    








    <div class="p-container__full__auto-margin-paragraph">
      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->
    
    

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
