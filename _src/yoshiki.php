<?php
// Include general settings.
require($_SERVER["DOCUMENT_ROOT"].'/sobetsu/resources/etc/config.php');

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">様式ダウンロード</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">様式ダウンロード</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">


        <div class="u-grid__row">
	      <h2 class="c-heading-1">暮らし情報</h2>
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">住民登録・戸籍・印鑑登録・パスポート</h3>
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">戸籍証明請求書</a></li>
                    <li><a href="" class="c-link__pdf">住民票郵送交付申請書</a></li>
                    <li><a href="" class="c-link__pdf">委任状様式</a></li>
                 </ul>
            </section>

            <section class="u-grid__col-12">
              <h3 class="c-heading-2">おくやみ</h3>
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">墓地使用申請書</a></li>
                 </ul>
            </section>

            <section class="u-grid__col-12">
              <h3 class="c-heading-2">持ち家・空き家・民間住宅</h3>
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">持ち家住宅取得奨励事業利用申込書</a></li>
                    <li><a href="" class="c-link__pdf">空き家整理改修事業利用申込書</a></li>
                    <li><a href="" class="c-link__pdf">民間賃貸住宅建設助成事業認定申請書</a></li>
                    <li><a href="" class="c-link__pdf">不動産情報提供事業登録申込書（買主・借主）</a></li>
                    <li><a href="" class="c-link__pdf">不動産情報提供事業登録申込書（売主・貸主）</a></li>
                    <li><a href="" class="c-link__pdf">不動産情報登録申請書（事業者）</a></li>
                 </ul>
            </section>
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">税金（国保税含む）</h3>
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">固定資産税評価証明書等交付請求書</a></li>
                    <li><a href="" class="c-link__pdf">委任状</a></li>
                    <li><a href="" class="c-link__pdf">納税証明書交付申請書</a></li>
                 </ul>
            </section>
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">水道・下水道・温泉水</h3>
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">給水開始届</a></li>
                    <li><a href="" class="c-link__pdf">給水中止、休止、廃止届</a></li>
                    <li><a href="" class="c-link__pdf">貸付申請書（水洗便所改造等資金貸付制度/集落排水）</a></li>
                    <li><a href="" class="c-link__pdf">貸付申請書（水洗便所改造等資金貸付制度/管理型浄化槽）</a></li>
                    <li><a href="" class="c-link__pdf">温泉供給申請書</a></li>
                 </ul>
            </section>
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">し尿処理・浄化槽</h3>
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">壮瞥町浄化槽整備事業補助金交付申請書</a></li>
                    <li><a href="" class="c-link__pdf">浄化槽工事チェックリスト</a></li>
                 </ul>
            </section>
        </div>


        <div class="u-grid__row">
	      <h2 class="c-heading-1">健康・福祉</h2>
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">子育て支援</h3>
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">保育所入所申込書</a></li>
                    <li><a href="" class="c-link__pdf">就労証明書</a></li>
                    <li><a href="" class="c-link__pdf">就労申告書</a></li>
                    <li><a href="" class="c-link__pdf">求職活動申立書</a></li>
                    <li><a href="" class="c-link__pdf">そうべつ児童クラブ申込書</a></li>
                    <li><a href="" class="c-link__pdf">くぼない児童クラブ申込書</a></li>
                    <li><a href="" class="c-link__pdf">子どもの医療費助成制度受給者証交付申請書</a></li>
                    <li><a href="" class="c-link__pdf">通学定期補助交付申請書</a></li>
                 </ul>
            </section>
        </div>

        <div class="u-grid__row">
	      <h2 class="c-heading-1">ふるさと納税</h2>
            <section class="u-grid__col-12">
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">寄付金申込書</a></li>
                 </ul>
            </section>
        </div>

        <div class="u-grid__row">
	      <h2 class="c-heading-1">行政</h2>
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">職員採用</h3>
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">受験申込書</a></li>
                 </ul>
            </section>
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">インターネット公売</h3>
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">公売保証金納付申込書、還付請求書兼口座振替依頼書</a></li>
                    <li><a href="" class="c-link__pdf">保管依頼書</a></li>
                    <li><a href="" class="c-link__pdf">送付依頼書</a></li>
                    <li><a href="" class="c-link__pdf">所有権移転登記請求書</a></li>
                    <li><a href="" class="c-link__pdf">委任状</a></li>
                    <li><a href="" class="c-link__pdf">委任状（共同入札用）</a></li>
                    <li><a href="" class="c-link__pdf">共有合意書</a></li>
                    <li><a href="" class="c-link__pdf">共同入札者持分内訳書</a></li>
                 </ul>
            </section>
        </div>


        <div class="u-grid__row">
	      <h2 class="c-heading-1">産業</h2>
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">農林業</h3>
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">就農認定者認定申請書</a></li>
                 </ul>
            </section>
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">商工業・企業誘致</h3>
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">起業化計画書</a></li>
                    <li><a href="" class="c-link__pdf">特産品開発助成事業計画書</a></li>
                    <li><a href="" class="c-link__pdf">特産品開発収支予算書</a></li>
                    <li><a href="" class="c-link__pdf">農商工連携認定申請書</a></li>
                    <li><a href="" class="c-link__pdf">農商工連携事業計画書</a></li>
                    <li><a href="" class="c-link__pdf">農商工連携収支予算書</a></li>
                    <li><a href="" class="c-link__pdf">助成対象事業者指定申請書</a></li>
                    <li><a href="" class="c-link__pdf">施設新設(増設)事業計画書</a></li>
                 </ul>
            </section>
        </div>

        <div class="u-grid__row">
	      <h2 class="c-heading-1">公共施設</h2>
            <section class="u-grid__col-12">
                 <ul class="c-list">
                 	<li><a href="" class="c-link__pdf">洞爺湖園地船揚施設使用申込書</a></li>
                 </ul>
            </section>
        </div>

        





      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
