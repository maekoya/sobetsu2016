<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">防災・安全情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">防災・安全情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">防災</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">防　災</h1>
      
      <section class="u-grid__row">      
          <div class="u-grid__col-12">
	          <h2 class="c-heading-2">指定避難所一覧</h2>
              <table class="c-table-1">
                <tr>
                  <th class="__strong">地 区</th>
                  <th class="__strong">名 称</th>
                  <th class="__strong">所在地</th>
                  <th class="__strong">収容人員（人）</th>
                  <th class="__strong">不適 災害</th>
                </tr>
                <tr>
                  <td class="__weak" rowspan="8">滝之町</td>
                  <td>壮瞥小学校体育館</td>
                  <td>滝之町435</td>
                  <td>240</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>壮瞥中学校体育館</td>
                  <td>滝之町420-5</td>
                  <td>240</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>壮瞥高等学校体育館</td>
                  <td>滝之町235-13</td>
                  <td>210</td>
                  <td>噴火・土砂災害</td>
                </tr>
                <tr>
                  <td>壮瞥町町民会館</td>
                  <td>滝之町245</td>
                  <td>26</td>
                  <td>噴火</td>
                </tr>
                <tr>
                  <td>壮瞥町遊学館</td>
                  <td>滝之町242</td>
                  <td>134</td>
                  <td>噴火・土砂災害</td>
                </tr>
                <tr>
                  <td>ゆーあいの家</td>
                  <td>滝之町290-44</td>
                  <td>21</td>
                  <td>噴火</td>
                </tr>
                <tr>
                  <td>壮瞥町保健センター</td>
                  <td>滝之町284-2</td>
                  <td>53</td>
                  <td>噴火</td>
                </tr>
                <tr>
                  <td>そうべつ子どもセンター</td>
                  <td>滝之町432-9</td>
                  <td>194</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td class="__weak" rowspan="4">久保内</td>
                  <td>久保内小学校体育館</td>
                  <td>南久保内142-4</td>
                  <td>171</td>
                  <td>土砂災害</td>
                </tr>
                <tr>
                  <td>久保内中学校体育館</td>
                  <td>南久保内142-38</td>
                  <td>203</td>
                  <td>土砂災害</td>
                </tr>
                <tr>
                  <td>壮瞥町青少年会館</td>
                  <td>南久保内14-22</td>
                  <td>144</td>
                  <td>土砂災害</td>
                </tr>
                <tr>
                  <td>壮瞥町農村環境改善センター</td>
                  <td>南久保内145-8</td>
                  <td>164</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td class="__weak">蟠渓</td>
                  <td>蟠渓ふれあいセンター</td>
                  <td>蟠渓26-1</td>
                  <td>74</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td class="__weak">弁景</td>
                  <td>オロフレほっとピアザ</td>
                  <td>弁景204-7</td>
                  <td>20</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td class="__weak">立香</td>
                  <td>立香ふれあいセンター</td>
                  <td>立香142</td>
                  <td>92</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td class="__weak" rowspan="2">仲洞爺</td>
                  <td>仲洞爺公民館</td>
                  <td>仲洞爺22-5</td>
                  <td>24</td>
                  <td>土砂災害</td>
                </tr>
                <tr>
                  <td>来夢人の家</td>
                  <td>仲洞爺30-10</td>
                  <td>13</td>
                  <td>&nbsp;</td>
                </tr>
              </table>
            </div>
      </section>
      

      <div class="u-grid__row">
        <div class="u-grid__col-12">
	      <p style="background: #666; height: 300px;">google mapがはいります。</p>                  
        </div>
     </div>


      <section class="u-grid__row">
		 <div class="u-grid__col-12">        
        	<h2 class="c-heading-2">火山災害</h2>
         </div>
        <div class="u-grid__col-4">
          <img src="images/img01.jpg" width="280" height="400" alt="火山災害">
        </div>
        <div class="u-grid__col-8">
          壮瞥町は火山と共存する町だということを忘れてはいけません。ここでは火山災害に対する備えと、緊急時の対応についてご説明します。お問い合わせは 総務課<a href="tel:66-2121">（66-2121）</a>まで<br>
          <ul>
           <li class="" style=" margin-top: 30px;">
           	<h3 class="c-heading-3 u-mb__0 u-mt__0">壮瞥町有珠山噴火防災マップ（平成25年4月作成）</h3>
           ア）<a href="pdf/kikenkuiki_yosozu.pdf" class="c-link__pdf">危険区域予測図　ダウンロードPDF</a><br>
           イ）<a href="pdf/kokoroe.pdf" class="c-link__pdf">災害に備えて・平時の心得・避難時の心得 ダウンロードPDF</a>
           </li>
           <li class="" style=" margin-top: 30px;">
           	<h3 class="c-heading-3 u-mb__0 u-mt__0">有珠山の前兆地震がはじまった！まず何をすればいいの？</h3>
            落ち着いて各自避難できる準備をし、行政・関係機関の情報に注意をしましょう。
            行政・関係機関の指示を待たずに危険を感じたら役場に連絡の上指定避難場所に早めに避難して、その後の経過を見守りましょう。
            また、その際には家には家族の安否を確認しやすいよう避難先のメモを残しておきましょう。
           </li>
           <li class="" style=" margin-top: 30px;">
           	<h3 class="c-heading-3 u-mb__0 u-mt__0">避難はどのようにするの？</h3>
            災害発生時、町内の公共施設に避難所を設定します（発生する災害の種類により、安全が確保できる施設を町が指定します）。
            災害の発生が予測される場合、発生した場合には防災無線、広報車等により情報を提供します。
           </li>
          </ul>
        </div>
      </section>

      <section class="u-grid__row">
        <div class="u-grid__col-12">
	        <h2 class="c-heading-2 u-mb__0">土砂災害</h2>
        <dl>
         <dt class="c-heading-3">(1) 壮瞥町土砂災害危険箇所図</dt>
         <dd>壮瞥町土砂災害危険箇所図は、土砂災害発生のおそれのある箇所と、避難勧告等の発令時に町が指定する避難所を示した地図です。<br>いざというときに備えて、みなさんの住んでいる周辺の土砂くずれなどが発生しそうな場所を確認しておきましょう。<br>また、地図に着色されている土砂災害危険箇所以外のところでも、雨の降り方により、被害が発生することがありますので注意してください。<br>
         	<a href="pdf/kikenkasyo_map.pdf" class="c-link__pdf">土砂災害危険箇所マップ ダウンロードPDF</a>
         </dd>
        </dl>

        <dl>
         <dt class="c-heading-3">(2) 壮瞥町土砂災害警戒区域の指定状況</dt>
         <dd>現在の壮瞥町内の土砂災害警戒区域等の指定状況についてお知らせします。
          <ol class="u-mt__small">
           <li>1. 土砂災害警戒区域　白水川（蟠渓）、立香右の沢川（立香）</li>
           <li>2. 土砂災害警戒区域及び土砂災害特別警戒区域　壮瞥立香（立香）、壮瞥神社川（滝之町）詳しくはハザードマップをご覧ください。</li>
          </ol>
          <ul class="u-mt__small">
         	<li><a href="pdf/hazardmap_takinocho.pdf" class="c-link__pdf">土砂災害ハザードマップ_滝之町 ダウンロードPDF</a></li>
            <li><a href="pdf/hazardmap_tastuka.pdf" class="c-link__pdf">土砂災害ハザードマップ_立香 ダウンロードPDF</a></li>
            <li><a href="pdf/hazardmap_bankei.pdf" class="c-link__pdf">土砂災害ハザードマップ_蟠渓 ダウンロードPDF</a></li>
            <li><a href="pdf/hazardmap_ura.pdf" class="c-link__pdf">土砂災害ハザードマップ_裏面 ダウンロードPDF</a></li>
          </ul>
         </dd>
        </dl>

        <dl>
         <dt class="c-heading-3">(3) 土砂災害に関連する情報提供（北海道）</dt>
         <dd>北海道では、ホームページや携帯電話の登録サービスなどで土砂災害に関連する情報を提供していますので、ぜひご活用ください。<br>
         	<a href="dosyasaigai_kanren.pdf" class="c-link__pdf">土砂災害に関連する情報 ダウンロードPDF</a>
         </dd>
        </dl>
       </div>
      </section>


      <section class="u-grid__row">
		<div class="u-grid__col-12">        
        	<h2 class="c-heading-2">コミュニティFM　wi-radio</h2>
        </div>
        
        <div class="u-grid__col-4">
          <img src="images/img02.gif" width="280" height="375" alt="wi-radioロゴ">
        </div>
        <div class="u-grid__col-8">
          <p>伊達市・豊浦町・壮瞥町・洞爺湖町を放送エリアとするwo-radioが平成27年4月開局しました。<br>普段は地域の情報を取得する手段として、災害時は災害情報や避難情報を得るための手段として、<br>多くの皆さんに活用していただけるように放送を行っています。</p>
          <ul>
           <li>
           	<h3 class="c-heading-3 u-mb__0">wi-radioはどうすれば聴くことができるの？</h3>
            ラジオ受信機の周波数をFM77.6MHzにあわせると聴くことができます。
           </li>
           <li style=" margin-top: 20px;">
           	<h3 class="c-heading-3 u-mb__0">放送曜日・時間は？</h3>
            24時間365日放送しています。<br>壮瞥町からの情報も毎週火曜日の17:48と木曜日の7:55にそれぞれ放送します。
           </li>
           <li style=" margin-top: 20px;">
           	<h3 class="c-heading-3 u-mb__0">放送内容は？</h3>
            Wi-radioの番組は室蘭市のコミュニティFM放送局であるFMびゅーと共同で制作するので、FMびゅーと同じ放送が流れます。そのため、１市３町の情報だけでなく、
            室蘭市や登別市の情報も放送されます。音楽番組や行政からのお知らせ、トーク番組など、毎週様々な番組を放送します。
           </li>
           <li style=" margin-top: 20px;">
           	<h3 class="c-heading-3 u-mb__0">災害時はwi-radioを聴いてください。</h3>
            災害発生時や警報発令時などは、随時災害情報を放送します。皆さんにとって身近な情報を放送しますので、ぜひお聴きください。
            <p><a href="https://www.facebook.com/cfm2015sbpc/" class="c-link__blank" target="_blank">1市3町CFM放送局「wi-radio」ワイラジオ</a></p>
           </li>

          </ul>
        </div>
      </section>

      <section class="u-grid__row">
      <div class="u-grid__col-12">
        <h2 class="c-heading-2">防災情報リンク</h2>
        <div class="u-grid__row">
         <dl class="u-grid__col-4">  	
            <dt class="c-heading-3 u-mb__0">気象庁</dt>
            <dd>
            <ul class="c-list u-mt__0">
             <li><a href="http://www.jma.go.jp/jma/menu/menuflash.html" class="c-link__blank" target="_blank">防災情報</a></li>
             <li><a href="http://www.jma.go.jp/jma/kishou/know/tokubetsu-keiho/index.html" class="c-link__blank" target="_blank">特別警報</a></li>
             <li><a href="http://www.jma.go.jp/jma/kishou/books/cb_saigai_dvd/index.html" class="c-link__blank" target="_blank">防災啓発ビデオ</a></li>
            </ul>
            </dd>
           </dl>
         <dl class="u-grid__col-4">  	
            <dt class="c-heading-3 u-mb__0">北海道土砂災害警戒情報システム</dt>
            <dd>
            <ul class="c-list u-mt__0">
             <li><a href="http://www.njwa.jp/hokkaido-sabou/" class="c-link__blank" target="_blank">北海道土砂災害警戒情報システム</a></li>
            </ul>
            </dd>
           </dl>
         <dl class="u-grid__col-4">  	
            <dt class="c-heading-3 u-mb__0">北海道防災情報</dt>
            <dd>
            <ul class="c-list u-mt__0">
             <li><a href="http://www.bousai-hokkaido.jp/" class="c-link__blank" target="_blank">北海道防災情報</a></li>
            </ul>
            </dd>
           </dl>
          </div>
          
         <div class="u-grid__row">            
             <dl class="u-grid__col-4">  	
                <dt class="c-heading-3 u-mb__0">国土交通省</dt>
                <dd>
                <ul class="c-list u-mt__0">
                 <li><a href="http://www.river.go.jp/81.html" class="c-link__blank" target="_blank">川の防災情報（北海道版）</a></li>
                 <li><a href="http://www.river.go.jp/nrpc0303gDisp.do?mode=&areaCode=81&wtAreaCode=2110&itemKindCode=901&timeAxis=60" class="c-link__blank" target="_blank">壮瞥町周辺の雨量/河川水位</a></li>
                </ul>
                </dd>
               </dl>
             <dl class="u-grid__col-4">  	
                <dt class="c-heading-3 u-mb__0">北海道大学有珠火山観測所</dt>
                <dd>
                <ul class="c-list u-mt__0">
                 <li><a href="http://uvo4.sci.hokudai.ac.jp/~www/" class="c-link__blank" target="_blank">北海道大学有珠火山観測所</a></li>
                </ul>
                </dd>
               </dl>
             <dl class="u-grid__col-4">  	
                <dt class="c-heading-3 u-mb__0">西胆振消防組合</dt>
                <dd>
                <ul class="c-list u-mt__0">
                 <li><a href="http://nfd119.sakura.ne.jp/" class="c-link__blank" target="_blank">西胆振消防組合</a></li>
                </ul>
                </dd>
               </dl>
          </div>
      </div>
	      </section>

      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
