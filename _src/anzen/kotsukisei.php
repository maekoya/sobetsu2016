<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">防災・安全情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">防災・安全情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">交通規制</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">交通規制</h1>
      
      <section class="u-grid__row">
          <h2 class="c-heading-2">町道の交通規制</h2>
          <p>現在、下記の区間が通行止めになっています。ご注意ください。</p>
          <table class="c-table-1">
            <tr>
              <th class="__strong" width="20%">路線名</th>
              <td class="u-text__left">町道関内蟠渓線及び町道幸内上幸内線</td>
            </tr>
            <tr>
              <th class="__strong">規制内容</th>
              <td class="u-text__left">通行止め</td>
            </tr>
            <tr>
              <th class="__strong">概要</th>
              <td class="u-text__left">地すべりの恐れ</td>
            </tr>
            <tr>
              <th class="__strong">規制区間</th>
              <td class="u-text__left">・町道関内蟠渓線（総延長　1.3㎞）　壮瞥町字幸内176番地1地先～壮瞥町字幸内239番地13地先<br>
              ・町道幸内上幸内線（町道関内蟠渓線への侵入不可）壮瞥町字幸内193番地3地先（町道関内蟠渓線交差点）</td>
            </tr>
            <tr>
              <th class="__strong">規制日時</th>
              <td class="u-text__left">平成25年5月10日14時00分から（規制解除日時未定）</td>
            </tr>
            <tr>
              <th class="__strong">迂回路</th>
              <td class="u-text__left">国道453号 道道2号（洞爺湖登別線）</td>
            </tr>
            <tr>
              <th class="__strong">位置図</th>
              <td class="u-text__left"><a href="pdf/" class="c-link__pdf">通行規制箇所図DL/PDF</a></td>
            </tr>
          </table>
      </section>

      <section class="u-grid__row">
          <h2 class="c-heading-2">国道・道道の交通規制</h2>
          <p>こちらでご確認ください。</p>
          <a href="http://info-road.hdb.hkd.mlit.go.jp/RoadInfo/index.htm" class="c-link__blank" target="_blank">北海道地区道路情報</a>
      </section>

      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
