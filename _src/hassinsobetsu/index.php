<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">ハッシンそうべつ</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $page->base; ?>/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $page->base; ?>/hassinsobetsu/" itemprop="url"><span itemprop="title">ハッシンそうべつ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">2016年3月の記事一覧</span></li>
    </ul>

    <div class="p-container">
      <div id="base-content__main">
        <div class="p-container">

          <article class="p-entry-hassin">
            <div class="p-entry-hassin__detail">
              <div class="p-entry-hassin__header">
                <time class="p-entry-hassin__header__date" datetime="2016-00-00" pubdate>2016年03月21日</time>
                <h2 class="p-entry-hassin__header__heading">壮瞥公園梅の見ごろ予報</h2>
              </div>
              <div class="p-entry-hassin__thumb">
                <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-10.jpg" width="200" height="200" alt="">
              </div>
              <div class="p-entry__body">
                <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
              </div>
            </div>
          </article><!-- /.p-entry-hassin -->

          <article class="p-entry-hassin">
            <div class="p-entry-hassin__detail">
              <div class="p-entry-hassin__header">
                <time class="p-entry-hassin__header__date" datetime="2016-00-00" pubdate>2016年03月21日</time>
                <h2 class="p-entry-hassin__header__heading">壮瞥公園梅の見ごろ予報</h2>
              </div>
              <div class="p-entry-hassin__thumb">
                <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-10.jpg" width="200" height="200" alt="">
              </div>
              <div class="p-entry__body">
                <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
              </div>
            </div>
          </article><!-- /.p-entry-hassin -->

          <article class="p-entry-hassin">
            <div class="p-entry-hassin__detail">
              <div class="p-entry-hassin__header">
                <time class="p-entry-hassin__header__date" datetime="2016-00-00" pubdate>2016年03月21日</time>
                <h2 class="p-entry-hassin__header__heading">壮瞥公園梅の見ごろ予報</h2>
              </div>
              <div class="p-entry-hassin__thumb">
                <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-10.jpg" width="200" height="200" alt="">
              </div>
              <div class="p-entry__body">
                <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
              </div>
            </div>
          </article><!-- /.p-entry-hassin -->

          <article class="p-entry-hassin">
            <div class="p-entry-hassin__detail">
              <div class="p-entry-hassin__header">
                <time class="p-entry-hassin__header__date" datetime="2016-00-00" pubdate>2016年03月21日</time>
                <h2 class="p-entry-hassin__header__heading">壮瞥公園梅の見ごろ予報</h2>
              </div>
              <div class="p-entry-hassin__thumb">
                <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-10.jpg" width="200" height="200" alt="">
              </div>
              <div class="p-entry__body">
                <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
              </div>
            </div>
          </article><!-- /.p-entry-hassin -->

          <article class="p-entry-hassin">
            <div class="p-entry-hassin__detail">
              <div class="p-entry-hassin__header">
                <time class="p-entry-hassin__header__date" datetime="2016-00-00" pubdate>2016年03月21日</time>
                <h2 class="p-entry-hassin__header__heading">壮瞥公園梅の見ごろ予報</h2>
              </div>
              <div class="p-entry-hassin__thumb">
                <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-10.jpg" width="200" height="200" alt="">
              </div>
              <div class="p-entry__body">
                <p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
              </div>
            </div>
          </article><!-- /.p-entry-hassin -->
        </div><!-- /.p-container -->
      </div><!-- /#base-content__main -->

      <div id="base-content__side">
        <div class="p-entries-side">
          <div class="p-entries-side__item">
            <p class="p-entries-side__item__heading">月別アーカイブ</p>
            <ul class="p-entries-side__item__list__monthly">
              <li>
                <dl>
                  <dt>2016年</dt>
                  <dd><a href="#">01月</a></dd>
                  <dd><a href="#">02月</a></dd>
                  <dd><a href="#">03月</a></dd>
                  <dd><a href="#">04月</a></dd>
                  <dd><a href="#">05月</a></dd>
                  <dd><a href="#">06月</a></dd>
                  <dd><a href="#">07月</a></dd>
                  <dd><a href="#">08月</a></dd>
                  <dd><a href="#">09月</a></dd>
                  <dd><a href="#">10月</a></dd>
                  <dd><a href="#">11月</a></dd>
                  <dd><a href="#">12月</a></dd>
                </dl>
              </li>
              <li>
                <dl>
                  <dt>2015年</dt>
                  <dd><a href="#">01月</a></dd>
                  <dd><a href="#">02月</a></dd>
                  <dd><a href="#">03月</a></dd>
                  <dd><a href="#">04月</a></dd>
                  <dd><a href="#">05月</a></dd>
                  <dd><a href="#">06月</a></dd>
                  <dd><a href="#">07月</a></dd>
                  <dd><a href="#">08月</a></dd>
                  <dd><a href="#">09月</a></dd>
                  <dd><a href="#">10月</a></dd>
                  <dd><a href="#">11月</a></dd>
                  <dd><a href="#">12月</a></dd>
                </dl>
              </li>
              <li>
                <dl>
                  <dt>2014年</dt>
                  <dd><a href="#">01月</a></dd>
                  <dd><a href="#">02月</a></dd>
                  <dd><a href="#">03月</a></dd>
                  <dd><a href="#">04月</a></dd>
                  <dd><a href="#">05月</a></dd>
                  <dd><a href="#">06月</a></dd>
                  <dd><a href="#">07月</a></dd>
                  <dd><a href="#">08月</a></dd>
                  <dd><a href="#">09月</a></dd>
                  <dd><a href="#">10月</a></dd>
                  <dd><a href="#">11月</a></dd>
                  <dd><a href="#">12月</a></dd>
                </dl>
              </li>
            </ul>
          </div>
        </div>
      </div><!-- /#base-content__side -->

    </div><!-- /.p-container -->
    <div class="c-pagetop"><a href="#base-page">TOP</a></div>
  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
