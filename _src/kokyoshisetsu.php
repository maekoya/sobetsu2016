<?php
// Include general settings.
require($_SERVER["DOCUMENT_ROOT"].'/sobetsu/resources/etc/config.php');

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">公共施設</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">公共施設</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">

      <h2 class="c-heading-1">行政・社会教育施設</h2>

        <div class="u-grid__row" id="sisetsu01">
        <div class="u-grid__col-12">	
          <h3 class="c-heading-2">(1) 壮瞥町役場（本庁舎）</h3>
        </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img01_1.jpg" width="" height="" alt="壮瞥町役場">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字滝之町287番地7</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2121</td>
                </tr>
                <tr>
                  <th class="__strong">開庁時間</th>
                  <td>平日8:45-17:30</td>
                </tr>
                <tr>
                  <th class="__strong">閉庁日</th>
                  <td>土日･祝日、12/31-1/5</td>
                </tr>
                <tr>
                  <th class="__strong">詳細･参考情報</th>
                  <td><p><a href="chosei/gyosei/soshiki.php" class="c-link">行政組織</a></p></td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row" id="sisetsu01">
            <div class="u-grid__col-12">	
              <h3 class="c-heading-2">(2) 地域交流センター山美湖（教育委員会）</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img01_2_a.jpg" width="" height="" alt="地域交流センター山美湖">
              <img src="images/kokyosisetsu/img01_2_b.jpg" width="" height="" alt="山美湖ホール">
              <img src="images/kokyosisetsu/img01_2_c.jpg" width="" height="" alt="図書室">
            </div>
            <div class="u-grid__col-8">
              <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字滝之町287番地7</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2131</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間</th>
                  <td>月曜日 9:00-17:00<br>火～金曜日 9:00-21:00<br>土日･祝日 13:00-21:00</td>
                </tr>
                <tr>
                  <th class="__strong">休館日</th>
                  <td>12/31-1/5、祝祭日にあたる月曜日</td>
                </tr>
                <tr>
                  <th class="__strong">利用料金(1時間あたり)</th>
                  <td>入館のみ、図書室利用は無料です<br>
                  	<p class="u-mt__small u-mb__0"><strong>各施設利用の場合</strong></p>
                    <ul class="c-list u-mb__0 u-mt__0">
                        <li>多目的ホール（ステージ含）1,500円</li>
                        <li>多目的ホール1,000円</li>
                        <li>和室（楽屋1）、小会議室（楽屋2）、<br>
                        　研修室1、研修室3  300円</li>
                        <li>研修室2  200円</li>
                    </ul>
                    <p class="u-mb__0">
                    ※町外個人･団体はそれぞれ10%加算<br>
                    ※舞台設備等を利用の場合、別途加算<br>
                    ※営利目的利用の場合は別料金制
                    </p>
                 </td>
                </tr>
                <tr>
                  <th class="__strong">備考</th>
                  <td>教育委員会開庁時間は本庁舎と同じです（平日8:45-17:30）</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row u-grid__col-12">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(3) 壮瞥町保健センター（住民福祉課健康づくり係・歯科診療所）</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img01_3.jpg" width="" height="" alt="保健センター">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字滝之町284番地2</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2340</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間</th>
                  <td>平日8:45-17:30<br>土日･祝日、12/31-1/5</td>
                </tr>
                <tr>
                  <th class="__strong">併設機関</th>
                  <td>歯科診療所<br>
                  	<a href="http://www.662511.net" class="c-link__blank" target="_blank">地域包括支援センター社会福祉協議会</a>
                  </td>
                </tr>
                <tr>
                  <th class="__strong">備考</th>
                  <td>
                  	<p><a href="kurashi/kenko/iryokikan.html" class="c-link">歯科診療所営業日時はこちら</a></p>
                  </td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(4) 道の駅そうべつ情報館ｉ（商工観光課・西胆振消防組合壮瞥支署）</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img01_4.jpg" width="" height="" alt="情報館i">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字滝之町384番1</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-4200</td>
                </tr>
                <tr>
                  <th class="__strong">開館時間</th>
                  <td>4/1-11/15　9:00-17:30<br>11/16-3/31　9:00-17:00</td>
                </tr>
                <tr>
                  <th class="__strong">休館日</th>
                  <td>12/31-1/5<br>11/16-3/31の火曜日（祝日除く）
                  </td>
                </tr>
                <tr>
                  <th class="__strong">利用料金<br>(1時間あたり)</th>
                  <td>
                  	入館料は無料です
                    <p class="u-bold u-mt__small u-mb__0">研修室利用の場合</p>
                    <ul class=" u-mt__0">
                    	<li>第1研修室<br>夏期300円/冬期400円</li>
                        <li>第2研修室<br>夏期200円/冬期300円</li>
                    </ul>
                  </td>
                </tr>
                <tr>
                  <th class="__strong">詳細･参考情報</th>
                  <td class="u-w70">
                  	<a href="http://sobetsu-fp.co.jp/" class="c-link__blank" target="_blank">農産物直売所そうべつサムズ</a><br>
                    <a href="http://www.sobetsu-kanko.com" class="c-link__blank" target="_blank">そうべつ観光協会</a>
                  </td>
                </tr>
                <tr>
                  <th class="__strong">併設機関</th>
                  <td>消防支署･観光協会･農産物直売所</td>
                </tr>
                <tr>
                  <th class="__strong">備考</th>
                  <td>商工観光課開庁時間は<a href="#sisetsu01">本庁舎</a>と同じです</td>
                </tr>
              </table>
            </div>
         </div>
		
        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(5) そうべつ子どもセンター（保育施設）</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img01_5.jpg" width="" height="" alt="子どもセンター">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字滝之町432番地9</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2452</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間</th>
                  <td><a href="kurashi/kenko/kosodatesien.html" class="c-link">施設により異なりますのでこちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">休館日</th>
                  <td>日･祝日、12月31～1月6日
                  </td>
                </tr>
                <tr>
                  <th class="__strong">併設機関</th>
                  <td>認定こども園そうべつ保育所、子育て支援センター、児童館、児童クラブ</td>
                </tr>
              </table>
            </div>
         </div>
		
        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(6) 壮瞥町青少年会館（久保内出張所）</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img01_6.jpg" width="" height="" alt="青少年会館">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字南久保内14番地の22</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-65-2201</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間</th>
                  <td>日・祝日9:30-21:00　その他13:00-21:00</td>
                </tr>
                <tr>
                  <th class="__strong">休館日</th>
                  <td>毎週月曜日（祝日と重複する場合は翌日）<br>12/31-1/5、その他教育委員会が定める日</td>
                </tr>
                <tr>
                  <th class="__strong">利用料金(1時間あたり)</th>
                  <td>体育場<br>夏期600円/冬期1,300円<br>会議室<br>夏期150円/冬期300円</td>
                </tr>
                <tr>
                  <th class="__strong">併設機関等</th>
                  <td><p>役場久保内内出張所<br>開庁時間は<a href="#sisetsu01">本庁舎</a>と同じです</p>
                  	<p>くぼない児童クラブ<br><a href="/kurashi/kenko/kosodatesien.html" class="c-link">利用についてはこちらでご確認ください</a></p>
                  </td>
                </tr>
              </table>
            </div>
         </div>
		
        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(7) 壮瞥町町民会館</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img01_7.jpg" width="" height="" alt="町民会館">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字滝之町245番地</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2131（教育委員会）</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間</th>
                  <td>月曜日 9:00-17:00火～金曜日 9:00-21:00<br>土日･祝日 10:00-21:00</td>
                </tr>
                <tr>
                  <th class="__strong">休館日</th>
                  <td>12/31-1/5、祝祭日にあたる月曜日</td>
                </tr>
                <tr>
                  <th class="__strong">利用料金(1時間あたり)</th>
                  <td>
                  	<ul class="c-list u-mb__0 u-mt__0">
                    	<li>調理室<br>夏期200円/冬期310円</li>
                        <li>和室<br>夏期130円/冬期280円</li>
                        <li>研修室<br>夏期120円/冬期230円</li>
                        <li>会議室<br>夏期150円/冬期300円</li>
                    </ul>
                 </td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(8) 壮瞥町遊学館</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img01_8.jpg" width="" height="" alt="遊学館">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字滝之町242番地1</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2131（教育委員会）</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間</th>
                  <td>13:00-21:00</td>
                </tr>
                <tr>
                  <th class="__strong">休館日</th>
                  <td>土日･祝日、12/31-1/5</td>
                </tr>
                <tr>
                  <th class="__strong">利用料金<br>(1時間あたり)</th>
                  <td>
                  	<ul class="c-list u-mb__0 u-mt__0">
                    	<li>和室･講習室<br>夏期130円/冬期210円</li>
                        <li>遊戯室･体育館<br>夏期200円/冬期350円</li>
                    </ul>
                 </td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
          <div class="u-grid__col-12">
              <h2 class="c-heading-1">集会・研修・体育施設</h2>
          </div>
        </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(1) 壮瞥町農村環境改善センター</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img02_1.jpg" width="" height="" alt="農村環境改善センター">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字南久保内145番地8</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2201（堀口水道施設管理部）</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・料金・<br>休館等</th>
                  <td><a href="http://www.sobetsu-onsen.net/" class="c-link__blank" target="_blank">こちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">施設内容</th>
                  <td>農業後継者室・研修室・農産加工実習室・多目的ホール</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(2) 立香ふれあいセンター</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img02_2.jpg" width="" height="" alt="立香ふれあいセンター">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字立香142番地</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2201（堀口水道施設管理部）</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・料金・<br>休館等</th>
                  <td><a href="http://www.sobetsu-onsen.net" class="c-link__blank" target="_blank">こちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">施設内容</th>
                  <td>和室・研修室・調理室・シャワー・多目的ホール</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(3) 壮瞥町研修センター</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img02_3.jpg" width="" height="" alt="研修センター">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字壮瞥温泉78番地</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2201（堀口水道施設管理部）</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・料金・<br>休館等</th>
                  <td><a href="http://www.sobetsu-onsen.net" class="c-link__blank" target="_blank">こちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">施設内容</th>
                  <td>研修室</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(4) オロフレスキー場・オロフレほっとピアザ</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img02_4.jpg" width="" height="" alt="オロフレほっとピアザ">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字弁景204番地5</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-65-2323（オロフレリゾート）</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・料金・<br>休館等</th>
                  <td><a href="http://orofure.web.fc2.com" class="c-link__blank" target="_blank">こちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">施設内容</th>
                  <td>
                  	<ul>
                    	<li>スキー場<br>ペアリフト１基・スキー用品レンタル</li>
                        <li>オロフレほっとピアザ<br>多目的ホール・研修室・食堂・売店</li>
                        <li>キャンプ場</li>
                    </ul>
                  </td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(5) 壮瞥町パークゴルフ場</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img02_5.jpg" width="" height="" alt="パークゴルフ場">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字滝之町290番地</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2201（堀口水道施設管理部）</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・料金・<br>休館等</th>
                  <td><a href="http://www.sobetsu-onsen.net" class="c-link__blank" target="_blank">こちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">施設内容</th>
                  <td>パークゴルフ場（18ホール）、ゴルフ用具レンタル</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(6) 総合グラウンド</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img02_6.jpg" width="" height="" alt="総合グラウンド">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字滝之町234番地6</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2131（教育委員会）</td>
                </tr>
                <tr>
                  <th class="__strong">使用期間</th>
                  <td>5/1-10/31</td>
                </tr>
                <tr>
                  <th class="__strong">利用料金等</th>
                  <td>1時間あたり500円</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
                <h2 class="c-heading-1">町営温泉・観光・文化施設</h2>
            </div>
        </div>

        <div class="u-grid__row">
	     <div class="u-grid__col-12">
          <h3 class="c-heading-2">(1) ゆーあいの家</h3>
         </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img03_1.jpg" width="" height="" alt="ゆーあいの家">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字滝之町290番地44</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2310</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・料金・<br>休館等</th>
                  <td><a href="http://www.sobetsu-onsen.net" class="c-link__blank" target="_blank">こちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">施設内容</th>
                  <td>温泉入浴施設、貸室</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(2) 来夢人の家・仲洞爺キャンプ場</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img03_2_a.jpg" width="" height="" alt="来夢人の家">
              <img src="images/kokyosisetsu/img03_2_b.jpg" width="" height="" alt="仲洞爺キャンプ場">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字仲洞30番地11</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-7022</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・料金・<br>休館等</th>
                  <td><a href="http://www.sobetsu-onsen.net" class="c-link__blank" target="_blank">こちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">施設内容</th>
                  <td>
                      <p class="u-mb__0 u-mt__0">来夢人の家</p>
                        <ul class="c-list u-mb__0 u-mt__0">
                            <li>温泉入浴施設</li>
                        </ul>
                      <p class="u-mb__0">キャンプ場</p>
                      <ul class="c-list u-mb__0 u-mt__0">
                      	<li>水道、炊事場、水洗トイレ、売店</li>
                        <li>キャンプ場（第1･第2）</li>
                        <li>キャンピングカー、トレーラーハウススペース</li>
                      </ul>
                  </td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(3) 久保内ふれあいセンター</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img03_3.jpg" width="" height="" alt="久保内ふれあいセンター">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字南久保内151番地3</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-65-2010</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・<br>料金・休館等</th>
                  <td><a href="http://www.sobetsu-onsen.net" class="c-link__blank" target="_blank">こちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">施設内容</th>
                  <td>温泉入浴施設</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(4) 蟠渓ふれあいセンター</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img03_4.jpg" width="" height="" alt="蟠渓(ばんけい)ふれあいセンター">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字蟠渓26番地1</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-65-2004</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・料金・<br>休館等</th>
                  <td><a href="http://www.sobetsu-onsen.net" class="c-link__blank" target="_blank">こちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">施設内容</th>
                  <td>温泉入浴施設、和室、多目的ホール</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(5) 森と木の里センター</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img03_5.jpg" width="" height="" alt="森と木の里センター">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字東湖畔3番地の1</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2201（堀口水道施設管理部）</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・料金・<br>休館等</th>
                  <td><a href="http://www.sobetsu-onsen.net" class="c-link__blank" target="_blank">こちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">施設内容</th>
                  <td>炊事舎（バーベキューコーナー）、木工室、研修室、和室、テント床、バンガロー、あずま屋、交流広場</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(6) 壮瞥町郷土史料館・横綱北の湖記念館</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img03_6.jpg" width="" height="" alt="横綱北の湖記念館">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字滝之町294番地2</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-2201（堀口水道施設管理部）</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・料金・<br>休館等</th>
                  <td><a href="http://www.sobetsu-onsen.net" class="c-link__blank" target="_blank">こちらでご確認ください</a></td>
                </tr>
                <tr>
                  <th class="__strong">施設内容</th>
                  <td>展示など</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-2">(7) 洞爺湖園地船揚施設</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kokyosisetsu/img03_6.jpg" width="" height="" alt="洞爺湖園地船揚施設">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">所在地</th>
                  <td class="u-w70">壮瞥町字壮瞥温泉70番地1地先</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td>0142-66-4200（商工観光課）</td>
                </tr>
                <tr>
                  <th class="__strong">利用時間･期間・料金・<br>休館等</th>
                  <td>2016年度のスケジュール作成中</td>
                </tr>
                <tr>
                  <th class="__strong">利用料金</th>
                  <td>1艇1日あたり1,500円</td>
                </tr>
                <tr>
                  <th class="__strong">手続き等</th>
                  <td>利用には事前登録が必要です<br>「使用申込書」は「様式ダウンロード」から<br>ダウンロードしてください。
                 <p><a href="" class="c-link" target="_blank">「様式ダウンロード」</a></p>
                  【必要何書類】<br>・船舶検査証書<br>・船舶検査手帳<br>・船舶所有者の海技免状の写しを添付</td>
                </tr>
              </table>
            </div>
         </div>



      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
