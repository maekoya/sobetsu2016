<?php
class jsonManager
{
  private $url;

  function __construct(){
    $this->url = './json/events.json';

    $this->getJSON($this->url);
  }

  /**
   *
   * @param {string}
   * @return {array}
   */
  private function getJSON($source_url){
    $url = sprintf($source_url);
    $json = file_get_contents($url);

    header('Content-Type: application/json');
    header('charset=utf-8');
    header('Access-Control-Allow-Origin: *');
    echo $json;
  }
}

$json = new jsonManager();
