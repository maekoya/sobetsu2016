<?php
ini_set( 'display_errors', 1 );

class jsonManager
{
  private $url;
  private $target;

  function __construct(){
    $this->target = $_GET['site'];
    $this->getJSON($this->target);
  }

  /**
   *
   * @param {string}
   * @return {array}
   */
  private function getJSON($target){
    if(!$target){
      header('Content-Type: application/json;charset=utf-8');
      echo 'set param';
      return false;
    }

    $url = array(
      'gyosei' => 'https://www.town.sobetsu.lg.jp/shinchaku/feed/rss.json',
      'iju'    => 'https://www.town.sobetsu.lg.jp/iju/kyo/feed/rss.json',
      'kanko'  => 'https://sobetsu-kanko.com/wp-content/themes/SOBETSU_KANKOU/shun/feed-json.php'
    );
    $url = sprintf($url[$target]);

    $context = stream_context_create(array(
      'http' => array('ignore_errors' => true)
    ));
    $json = file_get_contents($url, false, $context);

    header('Content-Type: application/json;charset=utf-8');
    header('Access-Control-Allow-Origin: *');
    echo $json;
  }
}

$json = new jsonManager();
