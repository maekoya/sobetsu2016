<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">議会情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">議会情報</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">議 会</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">次回議会のご案内</h2>
          <div class="u-box_border">
              <ul class=" c-list p-list__mb10">
                <li>日 程<br>
                平成年月日</li>
                <li>議 案<br>
                テキストテキストテキストテキスト</li>
                <li>
                一般質問<br>
                テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                </li>
              </ul>
          </div>
          <p>議会の傍聴に来ませんか！議会事務局では定例会開会前に当該定例会の内容を記載したチラシを新聞折り込みにより配布しています。<br>
          <span class="c-annotation">定例会は3・6・9・12月にあります。</span></p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■インターネット議会中継</h2>
          <section>
          	<h3 class="c-heading-3">ライブ中継</h3>
            <p>ライブ中継は、現在、議会が開かれているときにご覧になることができます。</p>
            <p class="u-mb__0 u-text__red">〈注意事項〉</p>
            <ul class=" c-list u-mt__0">
            	<li>この議会中継は、議会の公式記録ではありません。</li>
                <li>配信する議会中継は、定例会、臨時会、予算審査特別委員会、決算審査特別委員会です。</li>
                <li>映像をご覧になるには、「Windows Media Player（無償）」が必要です。</li>
                <li>ご利用の環境次第では、うまく表示されない場合があります。</li>
            </ul>
            <a href="http://stream.town.sobetsu.lg.jp/AssemblyLive" class="c-link__blank" target="_blank">ライブ中継を見る</a>
          </section>

          <section>
          	<h3 class="c-heading-3">録画中継</h3>
            <p>録画中継は、過去の議会映像をご覧になることができます。配信期間は、当該会議の終了後から次の定例会までとなります。</p>
            <p class="u-mb__0 u-text__red">〈注意事項〉</p>
            <ul class=" c-list u-mt__0">
            	<li>この議会中継は、議会の公式記録ではありません。</li>
                <li>配信する議会中継は、定例会、臨時会、予算審査特別委員会、決算審査特別委員会です。</li>
                <li>映像をご覧になるには、「Windows Media Player（無償）」が必要です。</li>
                <li>ご利用いただく環境次第では、うまく表示されない場合があります。</li>
            </ul>

            <p class=" u-bold u-mb__small">平成27年第4回定例会</p>
            <ul class=" c-list u-mt__0">
            	<li><a href="http://stream.town.sobetsu.lg.jp/AssemblyArchive/Archive_20151210_001.asf" target="_blank" class=" c-link__blank">12月10日午前</a></li>
                <li><a href="http://stream.town.sobetsu.lg.jp/AssemblyArchive/Archive_20151210_002.as" target="_blank" class=" c-link__blank">12月10日午後</a></li>
                <li><a href="http://stream.town.sobetsu.lg.jp/AssemblyArchive/Archive_20151211_001.asf" target="_blank" class=" c-link__blank">12月11日午前</a></li>
                <li><a href="http://stream.town.sobetsu.lg.jp/AssemblyArchive/Archive_20151211_002.asf" target="_blank" class=" c-link__blank">12月11日午後</a></li>
            </ul>
          </section>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■議会の構成、特別委員会の設置状況</h2>
          <p>壮瞥町議会では、開かれた議会と、議会活動の活性化を図るための取り組みを行っています。<br>
          議員活動は、地域や住民の考え、要望を的確に把握すること、また、議会の活動や議員の考え方が地域に理解されることから始まります。<br>
          議会では年4回議会広報を発行し、地域と議会、住民と議員の「橋渡し役」になるため、活動しています。議会についてのご意見、ご質問は議会事務局までお寄せください。</p>
          
          <section>
            <h3 class="c-heading-3">(1) 議会の構成（平成27年5月8日現在）</h3>
            <table class="c-table-1">
               <tr>
                 <th class="__strong">職 名</th>
                 <th class="__strong">氏 名</th>
                 <th class="__strong u-w10">年 齢</th>
                 <th class="__strong u-w10">在職期間</th>
                 <th class="__strong u-w15">所属委員会</th>
                 <th class="__strong">役 職</th>
               </tr>
               <tr>
                 <td class="__weak">議長</td>
                 <td>松本 勉</td>
                 <td>58</td>
                 <td>6期</td>
                 <td>総務・経済</td>
                 <td>&nbsp;</td>
               </tr>
               <tr>
                 <td class="__weak">副議長</td>
                 <td>長内 伸一</td>
                 <td>57</td>
                 <td>6期</td>
                 <td>経済</td>
                 <td>&nbsp;</td>
               </tr>
               <tr>
                 <td class="__weak">常任委員長</td>
                 <td>佐藤 忞</td>
                 <td>77</td>
                 <td>2期</td>
                 <td>総務</td>
                 <td>議会運営委員<br>西いぶり広域連合議会議員</td>
               </tr>
               <tr>
                 <td class="__weak">副委員長</td>
                 <td>髙井 一英</td>
                 <td>60</td>
                 <td>2期</td>
                 <td>総務</td>
                 <td>&nbsp;</td>
               </tr>
               <tr>
                 <td class="__weak">常任委員長</td>
                 <td>真鍋 盛男</td>
                 <td>60</td>
                 <td>2期</td>
                 <td>経済</td>
                 <td>議会運営委員</td>
               </tr>
               <tr>
                 <td class="__weak">副委員長</td>
                 <td>毛利 爾</td>
                 <td>61</td>
                 <td>1期</td>
                 <td>経済</td>
                 <td>西胆振消防組合議会議員</td>
               </tr>
               <tr>
                 <td class="__weak">議運委員長</td>
                 <td>森 太郎</td>
                 <td>65</td>
                 <td>3期</td>
                 <td>総務</td>
                 <td>西いぶり広域連合議会議員</td>
               </tr>
               <tr>
                 <td class="__weak">副委員長</td>
                 <td>加藤 正志</td>
                 <td>60</td>
                 <td>5期</td>
                 <td>経済</td>
                 <td>西胆振消防組合議会議員</td>
               </tr>
               <tr>
                 <td class="__weak">議員</td>
                 <td>菊地 敏法</td>
                 <td>49</td>
                 <td>4期</td>
                 <td>総務</td>
                 <td>監査委員</td>
               </tr>
              </table>
          </section>

          <section>
            <h3 class="c-heading-3">(2) 特別委員会の設置状況（平成27年6月12日）</h3>
            <table class="c-table-1">
               <tr>
                 <th class="__strong">委員会名</th>
                 <th class="__strong">定数</th>
                 <th class="__strong">設置月日</th>
                 <th class="__strong">委員長名</th>
                 <th class="__strong">副委員長</th>
               </tr>
               <tr>
                 <td>議会広報特別委員会</td>
                 <td>6</td>
                 <td>平成27年6月12日</td>
                 <td>髙井 一英</td>
                 <td>真鍋 盛男</td>
               </tr>
              </table>
          </section>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■そうべつ議会だより</h2>
          <p>ご覧いただくためには、Acrobat Readerが必要です。Adobe社が無料で配布しておりますのでお持ちでない方は、下記ビューワソフトのダウンロードから、ダウンロードしてください。</p>
          <a href="https://get.adobe.com/jp/reader/" target="_blank" class="c-link__blank">Adobe Acrobat Readerをダウンロードする</a>
          
          <section>
            <h3 class="c-heading-3">ダウンロード</h3>
            <p>【最新版】<br><a href="" class="c-link__pdf" target="_blank">議会だよりNo.59 (2015年11月発行)</a></p>
          </section>
		
        <section>
            <h3 class="c-heading-3">過去のそうべつ議会だよりはこちらから</h3>
            <div class="u-grid__col-4">
             <h4>平成27年</h4>
             <ul>
             	<li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.59 (2015年11月発行)</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.58 (2015年8月発行)</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.57 (2015年4月発行)</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.56 (2015年2月発行)</a></li>
             </ul>
            </div>
            <div class="u-grid__col-4">
             <h4>平成26年</h4>
             <ul>
             	<li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.55 (2014年11月発行)</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.54 (2014年8月発行)</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.53 (2014年5月発行)</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.52 (2014年2月発行)</a></li>
             </ul>
            </div>
            <div class="u-grid__col-4">
             <h4>平成25年</h4>
             <ul>
             	<li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.51 (2013年11月発行)</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.50 (2013年8月発行)</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.49 (2013年5月発行)</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">議会だよりNo.48 (2013年2月発行)</a></li>
             </ul>
            </div>
          </section>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■議会会議録</h2>          
        <section>
            <div class="u-grid__col-4">
             <h4>平成27年定例会</h4>
             <ul>
             	<li><a href="" class="c-link__pdf" target="_blank">第4回（作成中）</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">第3回</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">第2回</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">第1回</a></li>
             </ul>
            </div>
            <div class="u-grid__col-4">
             <h4>平成27年臨時会</h4>
             <ul>
             	<li><a href="" class="c-link__pdf" target="_blank">第4回</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">第3回</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">第2回</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">第1回</a></li>
             </ul>
            </div>
            <div class="u-grid__col-4">
             <h4>平成27年審査特別委員会</h4>
             <ul>
             	<li><a href="" class="c-link__pdf" target="_blank">平成27年予算審査特別委員会</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">平成27年決算審査特別委員会</a></li>
             </ul>
            </div>
          </section>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■議決結果</h2>          
            <div class="u-grid__col-4">
             <ul>
             	<li><a href="" class="c-link__pdf" target="_blank">平成27年定例会　第1回 ～ 第4回</a></li>
                <li><a href="" class="c-link__pdf" target="_blank">平成27年臨時会　第1回 ～ 第4回</a></li>
             </ul>
            </div>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <p><span class="c-heading-3">【お問い合わせ】</span><br>議会事務局　TEL: <a href="tel:0142-66-2121">0142-66-2121</a> / MAIL: <a href="mailto:gikai@town.sobetsu.lg.jp">gikai@town.sobetsu.lg.jp</a></p>   
        </div>
      </div>






      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
