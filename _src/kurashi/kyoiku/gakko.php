<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">教育・生涯学習</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">教育・生涯学習</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">学校教育</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">

      <h2 class="c-heading-1">学校教育</h2>
      

        <div class="u-grid__row">
        <div class="u-grid__col-12">
         <h2 class="c-heading-2">■町立学校</h2>	
         <h3 class="c-heading-3">(1) 壮瞥小学校</h3>
        </div>
            <div class="u-grid__col-4">
              <img src="images/gakko/img01.jpg" width="" height="" alt="壮瞥小学校">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">住所</th>
                  <td class="u-w70">壮瞥町字滝之町435番地</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td><a href="tel:0142-66-2368">0142-66-2368</a></td>
                </tr>
                <tr>
                  <th class="__strong">通学区域</th>
                  <td>字滝之町・字洞爺湖温泉・字壮瞥温泉・字昭和新山・字仲洞爺・字東湖畔・字立香</td>
                </tr>
                <tr>
                  <th class="__strong">WEB SITE</th>
                  <td>&nbsp;</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">	
              <h3 class="c-heading-3">(2) 久保内小学校</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/gakko/img02.jpg" width="" height="" alt="久保内小学校">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">住所</th>
                  <td class="u-w70">壮瞥町字南久保内142</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td><a href="tel:0142-65-2300">0142-65-2300</a></td>
                </tr>
                <tr>
                  <th class="__strong">通学区域</th>
                  <td>字久保内・字南久保内・字上久保内・字弁景・字幸内、字蟠渓</td>
                </tr>
                <tr>
                  <th class="__strong">WEB SITE</th>
                  <td>&nbsp;</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row u-grid__col-12">
            <div class="u-grid__col-12">
              <h3 class="c-heading-3">(3) 壮瞥中学校</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/gakko/img03.jpg" width="" height="" alt="壮瞥中学校">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">住所</th>
                  <td class="u-w70">壮瞥町滝之町420</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td><a href="tel:0142-66-2367">0142-66-2367</a></td>
                </tr>
                <tr>
                  <th class="__strong">通学区域</th>
                  <td>字滝之町・字洞爺湖温泉・字壮瞥温泉・字昭和新山・字仲洞爺・字東湖畔・字立香</td>
                </tr>
                <tr>
                  <th class="__strong">WEB SITE</th>
                  <td>&nbsp;</td>
                </tr>
              </table>
            </div>
         </div>

        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-3">(4) 久保内中学校</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/gakko/img04.jpg" width="" height="" alt="久保内中学校">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">住所</th>
                  <td class="u-w70">壮瞥町字南久保内142</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td><a href="tel:0142-65-2210">0142-65-2210</a></td>
                </tr>
                <tr>
                  <th class="__strong">通学区域</th>
                  <td>字久保内・字南久保内・字上久保内・字弁景・字幸内、字蟠渓</td>
                </tr>
                <tr>
                  <th class="__strong">WEB SITE</th>
                  <td><a href="http://www.sobetsu.jp/kuchu/" class="c-link__blank">http://www.sobetsu.jp/kuchu/</a></td>
                </tr>
              </table>
            </div>
         </div>
		
        <div class="u-grid__row">
            <div class="u-grid__col-12">
              <h3 class="c-heading-3">(5) 壮瞥高校</h3>
            </div>
            <div class="u-grid__col-4">
              <img src="images/gakko/img05.jpg" width="" height="" alt="壮瞥高校">
            </div>
            <div class="u-grid__col-8">
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">住所</th>
                  <td class="u-w70">壮瞥町字滝之町235-13</td>
                </tr>
                <tr>
                  <th class="__strong">電話番号</th>
                  <td><a href="tel:0142-66-2456">0142-66-2456</a></td>
                </tr>
                <tr>
                  <th class="__strong">学科</th>
                  <td>地域農業科</td>
                </tr>
                <tr>
                  <th class="__strong">WEB SITE</th>
                  <td><a href="http://www.sobetsu.jp/soukou/" class="c-link__blank">http://www.sobetsu.jp/soukou/</a></td>
                </tr>
              </table>
            </div>
         </div>
		
        <div class="u-grid__row">
          <div class="u-grid__col-12">
           <h2 class="c-heading-2">■教育相談</h2>
           <p>児童・生徒に関するさまざまな不安や悩みの相談を受け付けています。
           就学、不登校、いじめ、集団不適応問題などでお困りのことがありましたら、お気軽にご相談ください。</p>
                <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">対象者</th>
                  <td class="u-w70">壮瞥町内在住<br>幼児・児童・生徒とその保護者</td>
                </tr>
                <tr>
                  <th class="__strong">相談日時</th>
                  <td>平日（月曜日から金曜日・祝日以外）午前9時から午後5時まで<br>
                  ※事前にご連絡ください。メールでの連絡も受け付けます。<br>
                  ※翌年度の就学に関する就学相談の際は、できるだけ就学の前年9月頃までに、1度ご相談ください。</td>
                </tr>
                <tr>
                  <th class="__strong">相談の際の持ち物</th>
                  <td>通常は何も必要ありませんが、就学相談の際には、発達等に関わる医療機関等からの資料などがあればご持参ください。</td>
                </tr>
              </table>
          </div>
         </div>


        <div class="u-grid__row">
          <div class="u-grid__col-12">
           <h2 class="c-heading-2">■学力学習状況調査</h2>
           <p>全国的な義務教育の機会均等と水準向上のため、児童生徒の学力・学習状況を把握、分析することにより、教育委員会・学校が、
           全国的な状況との関係において自らの教育の結果を検証し改善を図るとともに、児童生徒ひとり一人の学習改善や学習意欲につなげることを目的に実施しています。
           文部科学省は、全国の小学校6年生と中学校3年生を対象に学力学習状況調査を実施しております。壮瞥町では、今年度も義務教育校４校全てが参加しています。</p>
            <p class="c-heading-3 u-mb__0">ダウンロード</p>
            <ul class="c-list u-mt__0">
                <li><a href="" class="c-link__pdf">平成27年度調査結果DL/PDF</a></li>
                <li><a href="" class="c-link__pdf">平成26年度調査結果DL/PDF</a></li>
                <li><a href="" class="c-link__pdf">平成25年度調査結果DL/PDF</a></li>
            </ul>
          </div>
         </div>


        <div class="u-grid__row">
          <div class="u-grid__col-12">
           <h2 class="c-heading-2">■小中学校教科用図書に係る採択結果等</h2>
          </div>
            <div class="u-grid__col-6">
             <h3 class="c-heading-3">平成28年度から使用する中学校教科用図書に係る採択結果等について</h3>
                <ul class="c-list">
                    <li><a href="" class="c-link__pdf">採択結果及び理由DL/PDF</a></li>
                    <li><a href="" class="c-link__pdf">議事録DL/PDF</a></li>
                </ul>
                <p>教科用図書第10採択地区教育委員会協議会の採択結果及び理由は<br>
                <a href="http://www.town.toyako.hokkaido.jp/01_guide/guide-syousai/11_kyouiku/tyuugaku_kyoukayoutosyo.jsp" class="c-link__blank">こちら</a>をご覧ください。</p>
            </div>
            <div class="u-grid__col-6">
             <h3 class="c-heading-3">平成27年度から使用する小学校教科用図書に係る採択結果等について</h3>
                <ul class="c-list">
                    <li><a href="" class="c-link__pdf">採択結果及び理由DL/PDF</a></li>
                    <li><a href="" class="c-link__pdf">議事録DL/PDF</a></li>
                </ul>
                <p>教科用図書第10採択地区教育委員会協議会の採択結果及び理由は<br>
                <a href="http://www.town.toyako.hokkaido.jp/01_guide/guide-syousai/11_kyouiku/H27_kyoukayoutosyo.jsp" class="c-link__blank">こちら</a>をご覧ください。</p>
            </div>
         </div>


        <div class="u-grid__row">
          <div class="u-grid__col-12">
           <h2 class="c-heading-2">■壮瞥町立学校通学区域規則について</h2>
           <p>壮瞥町立小学校・中学校の通学区域については、「壮瞥町立学校通学区域規則」により定められています。</p>
            <p class="c-heading-3 u-mb__0">ダウンロード</p>
            <ul class="c-list u-mt__0">
                <li><a href="" class="c-link__pdf">壮瞥町立学校通学区域規則DL/PDF</a></li>
            </ul>
          </div>
         </div>


        <div class="u-grid__row">
          <div class="u-grid__col-12">
           <h2 class="c-heading-2">■就学指定校の変更について</h2>
           <p>教育委員会では、あらかじめ各学校ごとに通学区域を定めており、就学予定者等の住所地によりその就学すべき学校を指定していますが、<br>
           下記のような場合は、保護者の申立により指定された学校以外の学校に通学することができる場合もありますので、ご相談ください。</p>
           <dl>
           	<dt>(1) 身体的配慮に関する理由</dt>
            <dd>
            	<span class=" u-circle__blue">心身に障害がある児童で、特別支援学級がある学校へ通学する場合</span><br>
                <span class=" u-circle__blue">心身等の事情により、校区の学校への通学が困難な場合</span>
            </dd>           
           </dl>
           <dl>
           	<dt>(2) いじめ、不登校等教育的配慮に関する理由</dt>
            <dd>
            	<span class=" u-circle__blue">いじめ、不登校等の状態にある児童生徒が、いじめの回避又は不登校の回復を目的とする場合</span><br>
                <span class=" u-circle__blue">いじめ、その他の理由により、非行に巻き込まれる怖れがある場合</span>
            </dd> 
           </dl>
           <dl>
           	<dt>(3) 家庭の事情に関する理由</dt>
            <dd>
            	<span class=" u-circle__blue">両親の離婚により転居する場合</span><br>
                <span class=" u-circle__blue">小学校において両親が勤務する地域への通学の場合</span><br>
                <span class=" u-circle__blue">保護者の病気が原因で、親戚に預けられたことによる場合</span>
            </dd> 
           </dl>
           <dl>
           	<dt>(4) 転居に関する理由</dt>
            <dd>
            	<span class=" u-circle__blue">学年途中である児童生徒が、他の校区への転居をした場合</span><br>
                <span class=" u-circle__blue">住宅の新築、取得のため、1年以内に転居が確定している場合</span><br>
                <span class=" u-circle__blue">増改築のため仮の住宅に住所変更した場合</span>
            </dd>           
           </dl>
           <dl>
           	<dt>(4) その他</dt>
            <dd>
            	<span class=" u-circle__blue">その他教育委員会が特にやむを得ないと認めた場合</span>
            </dd> 
           </dl>
           <p class="c-annotation__no">※指定校変更の承認は、事実確認のうえ、通学に支障がないと認められた場合に限ります。<br>
           ※なお、申請理由により、関係書類の提出をお願いする場合がありますので、詳細については生涯学習課学校教育係までご相談ください。<br>
           ※壮瞥町立小学校・中学校の通学区域については、壮瞥町立学校通学区域規則をご覧下さい</p>
            <p class="c-heading-3 u-mb__0">ダウンロード</p>
            <ul class="c-list u-mt__0">
                <li><a href="" class="c-link__pdf">壮瞥町立学校通学区域規則DL/PDF</a></li>
            </ul>
          </div>
         </div>

        <div class="u-grid__row">
          <div class="u-grid__col-12">
           <h2 class="c-heading-2">■そうべつ学校評価マニュアル</h2>
           <p>壮瞥町教育委員会では、平成23年度に文部科学省委託事業「学校評価・情報提供の充実・改善等に向けた実践研究」の指定を受け、<br>
           「学校改善の実効性を高めるための学校評価の在り方」を研究課題として調査研究に取り組んでまいりました。<br>
           この調査研究では、本町のような小さな自治体（学校）でもできる効果的で持続可能な学校評価システムを構築し、それを容易に活用できるよう、「そうべつ学校評価マニュアル」を研究成果としてまとめましたので公表いたします。すでに評価システムを構築されている自治体（学校）も多いことと存じますが、「そうべつ学校評価マニュアル」をご一読いただき、ご参考にしていただきたく存じます。</p>
            <p class="c-heading-3 u-mb__0">ダウンロード</p>
            <ul class="c-list u-mt__0">
                <li><a href="" class="c-link__pdf">そうべつ学校評価マニュアルDL/PDF</a></li>
            </ul>
          </div>
         </div>

        <div class="u-grid__row">
          <div class="u-grid__col-12">
           <h2 class="c-heading-2">■学校施設整備計画の事後評価</h2>
           <p>壮瞥町では、平成21年度に「安全・安心な学校づくり交付金」を受けて、壮瞥町立壮瞥小学校及び壮瞥中学校の体育館で地震補強工事を行いました。「安全・安心な学校づくり交付金」交付要綱第8の1に基づき、施設整備計画における事後評価の結果を公表いたします。</p>
            <p class="c-heading-3 u-mb__0">ダウンロード</p>
            <ul class="c-list u-mt__0">
                <li><a href="" class="c-link__pdf">壮瞥町立学校施設整備計画の事後評価DL/PDF</a></li>
            </ul>
          </div>
         </div>

        <div class="u-grid__row">
          <div class="u-grid__col-12">
           <h2 class="c-heading-2">■町立小中学校耐震診断の結果</h2>
           <p>「地震防災対策特別措置法」が、平成20年6月18日に改正され、地方公共団体の設置する小中学校等の校舎等について、耐震診断の実施と公表が義務付けられました。町内の小中学校施設耐震診断の結果をお知らせします。<br>
           <span class="c-annotation__no">※昭和56年6月以降の新耐震基準で建築されたものについては、耐震基準を満たしているため、耐震診断は行っておりません。</span></p>
            <p class="c-heading-3 u-mb__0">ダウンロード</p>
            <ul class="c-list u-mt__0">
                <li><a href="" class="c-link__pdf">小中学校耐震診断の結果DL/PDF</a></li>
            </ul>
          </div>
         </div>









      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
