<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">教育・生涯学習</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">教育・生涯学習</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">生涯学習</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">

      <h2 class="c-heading-1">生涯学習</h2>
      

        <div class="u-grid__row">
            <div class="u-grid__col-12">
             <h2 class="c-heading-2">■イベントカレンダー</h2>	
             <p>壮瞥町では様々な生涯学習事業を行っています。スケジュールはイベントカレンダーや広報そうべつ・おしらせそうべつでご確認ください。</p>
             <p><a href="" class="c-link">「イベントカレンダー」を見る</a></p>
             <p>今月の生涯学習情報「山美湖」・「やまびこ図書室」は「広報そうべつ」をご覧ください。<br>
             <a href="" class="c-link">「広報そうべつ」を見る</a></p>         
            </div>         
       </div>
       
        <div class="u-grid__row">
            <div class="u-grid__col-12">	
              <h3 class="c-heading-3">(1) 乳幼児・家庭教育</h3>
            </div>
        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">親子ふれあい事業</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img01_a.jpg" alt="ベビーマッサージ講座">
            </div>
            <div class="u-grid__col-8">
            	<p>親子のふれあいを通し、親子での生活や、遊びの経験を豊かにする体験的な学習機会の充実を図ることを目的として手遊び体遊び、
                ベビーマッサージ講座などの事業を行っています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>町内在住の未就学児童及び保護者</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>年2回程度</td>
               	  </tr>
                </table>
            </div>
         </section>
         
        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">親力つむぎ事業～壮瞥まんきつDAY</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img01_b.jpg" alt="親力つむぎ事業">
            </div>
            <div class="u-grid__col-8">
            	<p>家庭の教育力向上を図り子どもの健やかな成長を促すことを目的として、親子体験プログラム、教育講演会、子どもプログラムなどの事業を行っています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>町内小学生及び保護者、一般住民</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>年2回程度</td>
               	  </tr>
                </table>

            </div>
         </section>
         
        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">図書室ブックスタートコーナーの充実（図書室お話会）</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img01_c.jpg" alt="ブックスタート">
            </div>
            <div class="u-grid__col-8">
            	<p>絵本を通して乳幼児と保護者が触れあうことにより、より良い親子関係の構築を手助けすることを目的として絵本や紙芝居の読み聞かせを行います。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>町内在住の未就学児童及び保護者</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>毎月1回</td>
               	  </tr>
                </table>
            </div>
         </section>
       </div>


        <div class="u-grid__row">
            <div class="u-grid__col-12">	
              <h3 class="c-heading-3">(2) 青少年教育</h3>
            </div>
        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">子ども郷土史講座</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img02_a.jpg" alt="郷土史講座">
            </div>
            <div class="u-grid__col-8">
            	<p>自分たちの郷土壮瞥町の自然の様子や歴史を学習することにより郷土についての理解を深め、関心を高めさせることを目的として、昭和新山登山などの事業を行っています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>町内在住小学3年生から6年生まで</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>年4回程度</td>
               	  </tr>
                </table>
            </div>
         </section>
         
        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">中学生フィンランド国派遣（海外研修）事業</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img02_b.jpg" alt="フィンランド">
            </div>
            <div class="u-grid__col-8">
            	<p>フィンランド国ケミヤルヴィ市友好都市宣言に基づき、21世紀を担う子どもたちの見聞を広げ国際的な視野と感覚を持って国際化の時代に対応できる人材を育てることを
                目的として、全額公費負担で派遣しています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>町内在住の中学2年生</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>年1回</td>
               	  </tr>
                </table>
            </div>
         </section>
         
        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">壮瞥町防災キャンプ事業</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img02_c.jpg" alt="防災キャンプ">
            </div>
            <div class="u-grid__col-8">
            	<p>壮瞥町が有珠山噴火や自然災害に見舞われたことを想定し、一人ひとり、「自分の身を守るためにはどうしたらよいのか」「他の人のために何か出来ることはないのか」などを考える機会とし、避難の際に必要なこと、避難所生活の心得、災害時の判断力や、生きる力互いに助け合う心、思いやりの心を育むことを目的として災害時の困難な状況での避難所生活を体験します。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>町内小中学生児童生徒から一般町民</td>
                  </tr>
                  <tr>
                    <th class="__weak">事業報告書</th>
                    <td><a href="" class="c-link__pdf">H24報告書　DL/PDF</a></td>
               	  </tr>
                </table>
            </div>
         </section>

        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">子ども朝活事業</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img02_d.jpg" alt="子ども朝活">
            </div>
            <div class="u-grid__col-8">
            	<p>児童の運動や外遊びの習慣化、体力・運動機能の向上を図り、同時に地域住民による学校への協力体制構築を目的として、
                夏休み・冬休みのサポート学習に合わせて生活習慣を整えるなどの取り組みを行っています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>町内小学4年生～6年生</td>
                  </tr>
                </table>
            </div>
         </section>
         
         <p class="u-grid__col-12">その他　毎年1月、町内小中学生を対象とした新春書き初め大会を行っています。</p>
       </div>


        <div class="u-grid__row">
            <div class="u-grid__col-12">	
              <h3 class="c-heading-3">(3) 成人教育</h3>
            </div>
        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">夜空を見る集い</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img03_a.jpg" alt="夜空を見る集い">
            </div>
            <div class="u-grid__col-8">
            	<p>夜空に輝く星座を観望し、宇宙の世界に対する興味を喚起し、知識と理解を深めるため、天体観望や講義、工作などを行っています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>壮瞥町一般住民（町外の方も受入可）</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>年6回程度</td>
               	  </tr>
                </table>
            </div>
         </section>
                  
         <p class="u-grid__col-12"><span class=" u-bold">その他</span><br>毎年1月、壮瞥町在住又は出身者の新成人を対象とした成人式を行っています。</p>
       </div>


        <div class="u-grid__row">
            <div class="u-grid__col-12">	
              <h3 class="c-heading-3">(4) 高齢者教育</h3>
            </div>
        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">壮瞥町山美湖大学</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img04_a.jpg" alt="山美湖大学">
            </div>
            <div class="u-grid__col-8">
            	<p>生きがいのある充実した生活ができるように、社会の変化に対応した社会的能力を養うとともに、健康体力の維持増進や趣味教養の向上を図るため、
                軽スポーツ、趣味、講話、体験学習、施設見学、見学旅行などを行っています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>町内在住の60歳以上の方で事前登録された方</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>月1回程度</td>
               	  </tr>
                </table>
            </div>
         </section>
       </div>


        <div class="u-grid__row">
            <div class="u-grid__col-12">	
              <h3 class="c-heading-3">(5) 芸術文化の振興</h3>
              <p>壮瞥町地域交流センター運営ボランティア実行委員会イベント</p>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img05_a.jpg" alt="">
            </div>
            <div class="u-grid__col-8">
            壮瞥町地域交流センター運営ボランティア実行委員会が中心となり、優れた芸術文化事業の企画立案及び情報収集を図り、広く町民に情報を発信し、芸術文化の鑑賞機会を提供しています。
            </div>

        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">芸術鑑賞ツアー</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img05_b.jpg" alt="芸術鑑賞ツアー">
            </div>
            <div class="u-grid__col-8">
            	<p>町外で開催の著名な舞台芸術を鑑賞することにより、多くの町民に生の芸術や高いレベルの芸術文化に触れてもらう機会を増やし、文化振興の意識高揚を図ることを目的として、美術館の展示博覧会、コンサート、クラシック音楽等の鑑賞ツアーを行っています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>一般町民</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>年4～5回程度</td>
               	  </tr>
                </table>
            </div>
         </section>

        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">壮瞥町文化祭</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img05_c.jpg" alt="文化祭">
            </div>
            <div class="u-grid__col-8">
            	<p>文化団体、各教育機関をはじめ、自ら芸術文化活動に深い関心がある町民すべてが、日頃活動している成果を発表する文化祭を開催しています（ステージ発表・展示発表）。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>文化関係団体、各学校児童生徒、一般町民</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>年1回</td>
               	  </tr>
                </table>
            </div>
         </section>
       </div>


        <div class="u-grid__row">
            <div class="u-grid__col-12">	
              <h3 class="c-heading-3">(6) 読書活動の推進</h3>
            </div>
        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">地域交流センター移動図書</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img06_a.jpg" alt="移動図書">
            </div>
            <div class="u-grid__col-8">
            	<p>子どもたちに広く図書に親しんでもらうことを目的として、各学校及び保育所へ地域交流センター図書室の蔵書を貸し出ししています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>町内児童生徒</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>通年、夏休み、冬休み</td>
               	  </tr>
                </table>
            </div>
         </section>

        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">壮瞥町図書フェスティバル</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img06_b.jpg" alt="図書フェスティバル">
            </div>
            <div class="u-grid__col-8">
            	<p>図書室を町民に広く周知し、親子が一緒に読書を楽しむコミュニケーションの場を提供するため、読み聞かせ、展示、遊びのコーナー、雑誌リサイクル市などを行います。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>幼児～一般住民</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>年1回</td>
               	  </tr>
                </table>
            </div>
         </section>

        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">読書推進事業</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img06_c.jpg" alt="読書推進事業">
            </div>
            <div class="u-grid__col-8">
            	<p>地域の子どもたちを中心に、普段触れることのない生の人形劇や映画を招聘し、また、一般住民向けに読書の楽しさが理解していただけるような講師を招聘し講演会等を開催しています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>幼児～一般住民</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>年数回</td>
               	  </tr>
                </table>
            </div>
         </section>

        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">図書ボランティアによるお話会・図書分室朗読会</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img06_d.jpg" alt="図書ボランティアお話し会">
            </div>
            <div class="u-grid__col-8">
            	<p>読書の推進や図書に親しむことを目的に、子どもたちや親のニーズに合わせて図書室キッズルームにて「お話し会」を実施、図書室分室では一般住民を対象に「おとなの朗読会」を実施しています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>幼児～一般住民</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>月1回程度</td>
               	  </tr>
                </table>
                <p>壮瞥町の読書活動推進計画はこちらからご覧ください。<br><a href="" class="c-link">「政策・計画」を見る</a></p>
            </div>
         </section>
      </div>

      
        <div class="u-grid__row">
            <div class="u-grid__col-12">	
              <h3 class="c-heading-3">(7) スポーツの振興</h3>
            </div>
        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">キッズスポーツクラブ</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img07_a.jpg" alt="キッズスポーツクラブ">
            </div>
            <div class="u-grid__col-8">
            	<p>幼少年期よりスポーツ（運動）に親しむことによって、体を動かすことの楽しさやコミュニケーション能力、協調性を養い、スポーツに対する興味関心を高めさせることを目的として、室内・屋外運動、水泳・スキー教室などを行います。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>幼児（年中･年長）、小学1・2年生</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>各期7-8回（4期）程度</td>
               	  </tr>
                </table>
            </div>
         </section>

        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">町民歩けあるけ運動</h4>
            </div>
            <div class="u-grid__col-4">
              <img src="images/shogai/img07_b.jpg" alt="町民歩けあるけ運動">
            </div>
            <div class="u-grid__col-8">
            	<p>町民の健康維持と体力向上を推進し自分の体力に挑戦し、親子等での参加を推進し、景色や自然と触れあう機会を設けています。</p>
                <table class="c-table-1">
                  <tr>
                  	<th class="__weak u-w30">対象</th>
                    <td>小学校3年生以上</td>
                  </tr>
                  <tr>
                    <th class="__weak">回数</th>
                    <td>年1回</td>
               	  </tr>
                </table>
            </div>
         </section>

        <section class="u-clearboth">
            <div class="u-grid__col-12">	
              <h4 class="c-heading-4">その他</h4>
            </div>
            <div class="u-grid__col-12">
            毎年、町内在住の小学生以上を対象としたスイミングスクールやスキー・スノーボードスクール、町内在住の中学生以上又は町内に勤務する者による町民親善
            ミニバレーボール大会も行っています。
            </div>
         </section>
      </div>
      
      <div class="u-grid__row">
          <div class="u-grid__col-12">
             <h2 class="c-heading-2">■地遊クラブ "ジョイ"</h2>
            <div class="u-grid__col-4">
              <img src="images/shogai/img08.jpg" alt="joy体育教室">
            </div>
            <div class="u-grid__col-8">
           地遊クラブ”ジョイ”は壮瞥町に拠点を置く総合型地域スポーツクラブです。
           ジュニアスポーツクラブ、らくらくスポーツクラブ、登山界、各種スポーツ体験会など、学区にとらわれず、子どもからお年寄りまで幅広い世代を対象としたスポーツ事業を行っています。
           <p><a href="www.facebook.com/JiyuClubJoy" class="c-link__blank">www.facebook.com/JiyuClubJoy</a></p>
            </div>             
          </div>         
       </div>



      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
