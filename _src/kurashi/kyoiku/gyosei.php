<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">教育・生涯学習</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">教育・生涯学習</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">教育行政</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">教育行政</h1>

      <div class="u-grid__col-12">
        <p>教育委員会では、学校施設や学校運営に関する業務や広く町民が学ぶ社会教育、社会体育事業を実施しています。</p>

        <p class="c-heading-3 u-mb__0">教育行政執行方針</p>
        <ul class="c-list u-mt__0">
        	<li><a href="" class="c-link__pdf">平成27年度教育行政執行方針DL/PDF</a></li>
            <li><a href="" class="c-link__pdf">平成26年度教育行政執行方針DL/PDF</a></li>
            <li><a href="" class="c-link__pdf">平成25年度教育行政執行方針DL/PDF</a></li>
        </ul>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        
        <section>        
          <h2 class="c-heading-2">■教育委員会点検・評価報告書</h2>
          <p>地方教育行政の組織及び運営に関する法律の一部改正（平成１９年法律第９７号）により平成２０年度から各教育委員会は、毎年、
          その教育行政事務の管理及び執行状況について自己点検及び評価を行い、その結果に関する報告書を議会に提出するとともに公表することとなりました。
          そこで、壮瞥町教育委員会では、主な施策や事業を対象として内部評価を行うとともに、点検・評価の客観性を確保するために外部評価を実施し、報告書にまとめています。</p>
            <p class="c-heading-3 u-mb__0">ダウンロード</p>
            <ul class="c-list u-mt__0">
                <li><a href="" class="c-link__pdf">平成27年度（平成26年度対象）DL/PDF</a></li>
                <li><a href="" class="c-link__pdf">平成26年度（平成25年度対象）DL/PDF</a></li>
                <li><a href="" class="c-link__pdf">平成25年度（平成24年度対象）DL/PDF</a></li>
            </ul>
         </section>

        <section>        
          <h2 class="c-heading-2">■教育行政相談員</h2>
          <p>壮瞥町教育委員会では、地域の方からの教育行政に関する意見や要望などに的確に対応するため、相談の窓口となる教育行政相談員を指定いたしました。
          教育行政に関するご意見・ご要望などがございましたら、お気軽にご相談ください。</p>
            <p><span class="c-heading-3">【相談窓口】</span><br>〒052-0101<br>
            北海道有珠郡壮瞥町字滝之町287番地7<br>
            壮瞥町教育委員会（壮瞥町地域交流センター山美湖内）<br>
            TEL <a href="tel:0142-66-2131">0142-66-2131</a> / FAX　0142-66-2132<br>
            教育行政相談員　生涯学習課長まで
            </p>
         </section>

        <section>        
          <h2 class="c-heading-2">■総合教育会議・壮瞥町教育大綱</h2>
          <p>壮瞥町・壮瞥町教育委員会では、平成27年4月1日施行「地方教育行政の組織及び運営に関する法律の一部を改正する法律」に基づき、
          首長・教育委員会で構成する「総合教育会議」を設置し、本町の教育の目標や施策の根本的な方針である「壮瞥町教育大綱」を次のとおり策定しました。
          壮瞥町教育大綱はこちらでご覧ください。</p>
          <a href="" class="c-link">「政策・計画」を見る</a>
         </section>
        </div>
      </div>




      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
