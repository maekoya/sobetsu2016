<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">税金</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">税金</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■町民税</h2>
            <p>町民税は、住みよい町にするために必要な費用を、地域社会の構成員である町民のみなさんに前年の収入等に応じて負担していただくもので、個人に課税される個人の町民税、
            法人に課税される法人の町民税があります。</p>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1)	個人町民税</h3>
                    <p>個人の町民税は、一般的に市町村民税と都道府県民税を併せて住民税とよんでおり、町が道民税と共に徴収することになっています。</p>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">納める人</th>
                          <td>・1月1日現在で町内に住んでいる人<br>
                          ・1月1日現在で町内に住んでいないが、事務所、事業所又は家屋敷のある人</td>
                        </tr>
                        <tr>
                          <th class="__strong">納める金額</th>
                          <td>・均等割額･･･5,000円(町民税 3,500円、道民税 1,500円)<br>
                          <span class="c-annotation__no">※東日本大震災の復興および防災のための財源として平成26年度から10年間、均等割額が引き上げられます（
                          町民税・道民税それぞれ500円）。</span><br>
                          ・所得割額･･･課税対象所得金額(前年中の所得金額-所得控除額)×10％（町民税6％・道民税4％）－税額控除<br>
                          <span class="c-annotation__no">※退職所得、山林所得、土地等の譲渡所得などについては、通常他の所得と区分して課税されます。</span></td>
                        </tr>
                        <tr>
                          <th class="__strong">非課税となる人</th>
                          <td>
                          <p class="u-mt__0 u-mb__small">
                          <span class=" u-bold">＜均等割・所得割が非課税＞</span><br>
                          ・ 生活保護法による生活扶助を受けている人<br>
                          ・ 障害者、未成年者、寡婦又は寡夫で前年の合計所得金額が125万円以下であった人
                          </p>
                          <p class=" u-mt__0  u-mb__small">
                          <span class=" u-bold">＜均等割が非課税＞</span><br>
                          ・ 前年の合計所得金額が28万円×(控除対象配偶者+扶養親族の数+1)+16万8千円以下の人<br>
                          <span class="c-annotation__no">※ただし、控除対象配偶者及び扶養親族がいない人については、28万円以下の人</span>
                          </p>
                          <p class="u-mb__0 u-mt__0">
                          <span class=" u-bold">＜所得割が非課税＞</span><br>
                          ・ 前年の総所得金額が35万円×(控除対象配偶者+扶養親族の数+1)+32万円以下の人<br>
                          <span class="c-annotation__no">※ただし、控除象配偶者及び扶養親族がいない人については、35万円以下の人</span>
                          </p>
                          </td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2)	個人町民税の納付方法</h3>
                    <p>・指定した事業所にお勤めの人は、事業主がみなさんに代わって毎月の給料から均等に引き去りをして納付しています。<br>
                    また、公的年金受給の人は、基本的に年金からの引き去りにより納付していいただきます（特別徴収）。<br>
                    ・特別徴収以外の人は、6月、8月、10月、12月の年4回の納期限までに納めていただきます（普通徴収）。<br>
                    ・普通徴収の納付書は、平成20年度課税分から全国のコンビニエンスストアで納められようなりました。<br>
                    <span class="c-annotation__no">※ただし、1枚の納付額が30万円を超える納付書やバーコードが印字されていない納付書、バーコード部分が汚れている納付書などは、
                    コンビニでお取り扱いできません。金融機関または役場窓口でお支払ください。</span></p>
                </li>
                
            	<li>
                	<h3 class="c-heading-3 ">(3)	法人町民税</h3>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">納める人</th>
                          <td>町内に事業所、保養所、寮などを持っている法人など</td>
                        </tr>
                        <tr>
                          <th class="__strong">納める金額</th>
                          <td>・均等割額 ･･･ 資本金、従業員数により9段階の税率が適用されます。<br>
                          ・法人税割 ･･･ 課税標準となる法人税額 × 税率12.1%<br>
                          <a href="" class="c-link__pdf">法人町民税率早見表DL/PDF</a></td>
                        </tr>
                        <tr>
                          <th class="__strong">申告納税</th>
                          <td>事業年度の終了の日から原則として2か月以内に町に申告します。</td>
                        </tr>
                      </table>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■固定資産税</h2>
              <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w20">納める人</th>
                  <td>毎年1月1日現在、町内に土地、家屋、償却資産（事業に供する資産）を持っている人<br>
                  <span class="c-annotation__no">※登記簿や固定資産課税台帳に登録されている所有者を「納税義務者」として課税しますので、相続や売買で実際に所有者が変わっていても、
                  登記簿などの名義変更手続きが1月1日現在で完了していなければ、旧所有者のまま課税されます。</span></td>
                </tr>
                <tr>
                  <th class="__strong">納める金額</th>
                  <td>固定資産課税台帳に登録された価格をもとに算定された「課税標準額」に1.4%の税率をかけて算出した額<br>
                  <span class="c-annotation__no">※固定資産税の算定基礎となる固定資産の価格は、総務大臣の示す「固定資産評価基準」により、
                  3年に一度（償却資産は毎年度）改定し、価格を決定します。</span><br>
                  <p class="u-mt__small"><span class=" u-bold">【免税点】</span><br>
                  土地、家屋、償却資産それぞれの課税標準額の合計が、次の金額に満たない場合は課税されません。<br>（納税通知書も送付しておりません）<br>                  
                      <table class="c-table-1 u-mt__0 u-mb__0">
                        <tr>
                          <th class="__weak">土地</th>
                          <td>30万円</td>
                        </tr>
                        <tr>
                          <th class="__weak">家屋</th>
                          <td>20万円</td>
                        </tr>
                        <tr>
                          <th class="__weak">償却資産</th>
                          <td>150万円</td>
                        </tr>
                      </table>
                    </p>
                  </td>
                </tr>
                <tr>
                  <th class="__strong">その他</th>
                  <td>固定資産課税台帳は4月1日から6月30日（第1期納期限）まで縦覧できます。<br>
                  この期間は、納税者本人の土地や家屋の価格と他の土地や家屋の価格との比較ができます。<br>（この期間以外は有料です）<br>
                  毎年6月上旬に郵送する納税通知書にも詳しい内容が記載されています。</td>
                </tr>
                <tr>
                  <th class="__strong">手続き</th>
                  <td><span class=" u-bold">【固定資産評価証明書等の郵送請求】</span><br>
                  遠方にお住まいの方や日中都合が合わず窓口に来られない方は、郵便で請求することができます。
                  <div>
                  ① 請求に必要な書類（必須）<br>
                  <ul class="">
                  	<li>・「交付請求書」は「様式ダウンロード」ページからダウンロードしてください。
                    	<p class=" u-mt__small u-mb__small"><a href="" class="c-link">「様式ダウンロード」に行く</a></p>
                    </li>
                  	<li>・請求者の本人確認書類の写し（運転免許証などの写し）</li>
                  	<li>・手数料（必要金額分の定額小為替証書をゆうちょ銀行にてお求めください。なお、証書には何も書かずに同封ください。）</li>
                  	<li>・返信用封筒（返信先住所氏名を記載の上、切手を貼付して同封ください。）</li>
                  </ul>
                  </div>
                  
                  <p>②	請求に必要な書類（必要のある方のみ）<br>
                  ・「委任状」は「様式ダウンロード」ページからダウンロードしてください。<br>
                  <span class="c-annotation__no">※同居親族の方以外が請求する場合は、委任状の提出が必要です。</span><br>
                  <a href="" class="c-link">「様式ダウンロード」に行く</a><br>
                  ・相続関係が確認出来るもの（相続の場合は、戸籍・除籍謄本など、関係が確認出来る書類が必要です。）
                  </p>
                  <p><span class=" u-bold">【未登記建物の所有権移転】</span><br>
                  相続や贈与で家や物置等の建物を譲り受けたとき、その建物が未登記（法務局で登記をされていない）である場合は、所有権移転届出書を提出する必要があります。
                  届出は、役場税務財政課までお願いします。<br>
                  ①届出に必要なもの<br>
                  ・印鑑<br>
                  ・所有権を移転することを証する書類（遺産分割協議書など）<br>
                  <span class="c-annotation__no">※登記されている建物は、所有権移転登記を法務局で行ってください。</span></p>
                  
                  <p><span class=" u-bold">【未登記建物の取り壊し】</span><br>
                  未登記建物（法務局で登記をされていない建物）を取り壊したら、届出が必要です。届出は、役場税務財政課までお願いします。
                  届出を忘れてしまうと固定資産税がかかってしまうことがありますので、お忘れにならないようお願い致します。<br>
                  ①届出に必要なもの<br>
                  ・印鑑
                  </td>
                </tr>
              </table>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■軽自動車税</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1)	対象</h3>
                    <p>個人の町民税は、一般的に市町村民税と都道府県民税を併せて住民税とよんでおり、町が道民税と共に徴収することになっています。</p>
                      <table class="c-table-1">
                        <tr>
                          <th class="__strong u-w20">納める人</th>
                          <td>4月1日現在、原動機付自転車、<br>125ccを超える2輪車、<br>小型特殊自動車(トラクタｰやホイールローダーなど)、<br>軽自動車をお持ちの人</td>
                        </tr>
                        <tr>
                          <th class="__strong">基準日</th>
                          <td>毎年4月1日を基準日として課税されます。</td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2)	税率</h3>
                    <p>地方税制改正により、軽自動車税の税率が平成28年度から次のように変更になります。</p>
                    <p class=" u-bold u-mb__small u-mt__0">【原動機付自転車や１２５cc以上のバイク、小型特殊自動車など】</p>
                      <table class="c-table-1 u-mt__0">
                        <tr>
                          <th class="__strong" colspan="2" rowspan="2">種別</th>
                          <th class="__strong" colspan="2">税　率（年税額）</th>
                        </tr>
                        <tr>
                          <td>平成27年度まで</td>
                          <td>平成28年度から</td>
                        </tr>
                        <tr>
                          <td class="__weak" rowspan="4">原動機付自転車</td>
                          <td>50cc以下</td>
                          <td>1,000円</td>
                          <td>2,000円</td>
                        </tr>
                        <tr>
                          <td>50ccを超え90cc以下</td>
                          <td>1,200円</td>
                          <td>2,000円</td>
                        </tr>
                        <tr>
                          <td>90ccを超え125cc以下</td>
                          <td>1,600円</td>
                          <td>2,400円</td>
                        </tr>
                        <tr>
                          <td>ミニカー<br>(3輪以上で20ccを超えるもの)</td>
                          <td>2,500円</td>
                          <td>3,700円</td>
                        </tr>
                        <tr>
                          <td class="__weak">2輪の軽自動車</td>
                          <td>総排気量が125ccを超え<br>250cc以下</td>
                          <td>2,400円</td>
                          <td>3,600円</td>
                        </tr>
                        <tr>
                          <td class="__weak">雪上車</td>
                          <td>専ら雪上を走行するもの</td>
                          <td>2,400円</td>
                          <td>3,000円</td>
                        </tr>
                        <tr>
                          <td class="__weak">2輪の小型自動車</td>
                          <td>250ccを超えるもの</td>
                          <td>4,000円</td>
                          <td>6,000円</td>
                        </tr>
                        <tr>
                          <td class="__weak" rowspan="2">小型特殊自動車</td>
                          <td>農耕作業用のもの</td>
                          <td>1,600円</td>
                          <td>2,400円</td>
                        </tr>
                        <tr>
                          <td>その他のもの</td>
                          <td>4,700円</td>
                          <td>5,900円</td>
                        </tr>
                      </table>

                    <p class=" u-mb__small"><span class=" u-bold">【4輪以上及び3輪の軽自動車】</span><br>
                    平成27年4月1日以降に新規登録した車両は平成28年度から②の新税率が適用されます。</p>
                      <table class="c-table-1 u-mt__0 u-mb__small">
                        <tr>
                          <th class="__strong" colspan="4" rowspan="2">種別</th>
                          <th class="__strong" colspan="3">税率（年税額）</th>
                        </tr>
                        <tr>
                          <td>①旧税率<br>(平成27年3月31日以前に新規検査済)</td>
                          <td>②新税率<br>(平成27年4月1日以降に新規検査)</td>
                          <td>③重課税率<br>(13年経過車両)<span class="c-annotation__no">※注</span></td>
                        </tr>
                        <tr>
                          <td class="__weak" rowspan="5">軽自動車</td>
                          <td colspan="3">3輪</td>
                          <td>3,100円</td>
                          <td>3,900円</td>
                          <td>4,600円</td>
                        </tr>
                        <tr>
                          <td rowspan="4">4輪以上</td>
                          <td rowspan="2">乗用</td>
                          <td>営業用</td>
                          <td>5,500円</td>
                          <td>6,900円</td>
                          <td>8,200円</td>
                        </tr>
                        <tr>
                          <td>自家用</td>
                          <td>7,200円</td>
                          <td>10,800円</td>
                          <td>12,900円</td>
                        </tr>
                        <tr>
                          <td rowspan="2">貨物用</td>
                          <td>営業用</td>
                          <td>3,000円</td>
                          <td>3,800円</td>
                          <td>4,500円</td>
                        </tr>
                        <tr>
                          <td>自家用</td>
                          <td>4,000円</td>
                          <td>5,000円</td>
                          <td>6,000円</td>
                        </tr>
                      </table>
                      <p class="c-annotation__no u-mt__0">（※注）動力源または内燃機関の燃料が電気・天然ガス・メタノール・混合メタノール・ガソリン電力併用の軽自動車並びに被けん引車を
                      除きます。</p>
                      
                     <p class=" u-mb__small"><span class=" u-bold">【グリーン化特例（軽課）】</span><br>
                    平成28年度課税時に、3輪以上の軽自動車で排出ガス性能と燃費性能の優れた環境負荷の少ないものについて、グリーン化特例（軽課）が適用されます。
                    この特例は、平成27年4月1日から平成28年3月31日までに最初の新規検査を受けた3輪及び4輪の軽自動車（新車に限ります）で、
                    次の基準を満たす車両について、平成28年度分の軽自動車税に限り適用されます。</p>
                      <table class="c-table-1 u-mt__0 u-mb__small">
                        <tr>
                          <th class="__strong" colspan="4" rowspan="2">種別</th>
                          <th class="__strong" colspan="3">税率（年税額）</th>
                        </tr>
                        <tr>
                          <td>(ア) 75%軽減</td>
                          <td>(イ) 50%軽減</td>
                          <td>(ウ) 25%軽減</td>
                        </tr>
                        <tr>
                          <td class="__weak" rowspan="5">軽自動車</td>
                          <td colspan="3">3輪</td>
                          <td>1,000円</td>
                          <td>2,000円</td>
                          <td>3,000円</td>
                        </tr>
                        <tr>
                          <td rowspan="4">4輪</td>
                          <td rowspan="2">乗用</td>
                          <td>営業用</td>
                          <td>1,800円</td>
                          <td>3,500円</td>
                          <td>5,200円</td>
                        </tr>
                        <tr>
                          <td>自家用</td>
                          <td>2,700円</td>
                          <td>5,400円</td>
                          <td>8,100円</td>
                        </tr>
                        <tr>
                          <td rowspan="2">貨物用</td>
                          <td>営業用</td>
                          <td>1,000円</td>
                          <td>1,900円</td>
                          <td>2,900円</td>
                        </tr>
                        <tr>
                          <td>自家用</td>
                          <td>1,300円</td>
                          <td>2,500円</td>
                          <td>3,800円</td>
                        </tr>
                      </table>
                      <ul class="c-list p-list__mb10">
                      	<li>(ア) 75%軽減<br>電気自動車、燃料電池自動車、天然ガス車(平成21年排ガス規制NOx10%以上低減)</li>
                        <li>(イ) 50%軽減<br>乗用：平成17年排ガス規制75%低減(☆☆☆☆)かつ平成32年度燃費基準+20%達成<br>
                        貨物：平成17年排ガス規制75%低減(☆☆☆☆)かつ平成27年度燃費基準+35%達成</li>
                        <li>(ウ) 25%軽減<br>乗用：平成17年排ガス規制75%低減(☆☆☆☆)かつ平成32年度燃費基準<br>
                        貨物：平成17年排ガス規制75%低減(☆☆☆☆)かつ平成27年度燃費基準+15%達成</li>                                            
                      </ul>
                      <p class="c-annotation__no">※(イ)、(ウ)についてはガソリン車（ハイブリッドを含む）に限ります。<br>
                      ※各燃費基準の達成状況は、自動車検査証の備考欄に記載されています。</p>
                </li>                
            	<li>
                	<h3 class="c-heading-3">(3)	納付について</h3>
                    <ul class="c-list p-list__mb10">
                    	<li>軽自動車税の納付書は、平成20年度課税分から全国のコンビニエンスストアで納められようなっています。</li>
                        <li>ただし、バーコードが印字されていない納付書、バーコード部分が汚れている納付書などは、コンビニでお取り扱いできません。
                        金融機関または役場窓口でお支払ください。</li>
                        <li>前年度まで年度内に納付している場合、当初発送された軽自動車税の納付書で納めると、領収証書が納税証明書になりますので、車検の際にご利用できます。</li>
                    </ul>
                </li>
            	<li>
                	<h3 class="c-heading-3">(4)	税金の減免について</h3>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w30">減免申請が出来る方</th>
                          <td>身体等に障がいのある方のために使用する自動車で、一定の要件に当てはまるものは、申請により軽自動車税の減免を受けることができます。<br>
                          （既に自動車税等の減免を受けている場合は該当になりません。1台のみの適用となります。）</td>
                        </tr>
                        <tr>
                          <th class="__strong">減免手続きに必要なもの</th>
                          <td>①軽自動車税納税通知書（お支払いにならないようご注意ください）<br>
                          ②交付を受けている手帳<br>
                          ③運転免許証<br>
                          ④印鑑（シャチハタ以外の認印）
                          </td>
                        </tr>
                        <tr>
                          <th class="__strong">減免手続きの期限</th>
                          <td>納期限の７日前まで</td>
                        </tr>
                      </table>
                </li>
            </ol>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■国民健康保険税</h2>
            <ul class=" p-list__mt20">
            	<li class=" u-circle__blue"><span class=" u-bold">税額の算出方法</span><br>
                国保税は、加入者の所得に応じて計算される「所得割」、加入者の固定資産税に応じて計算される「資産割」、加入者数に応じて計算される「均等割」、
                世帯ごとの「平等割」の合計で計算されます。</li>
            	<li class=" u-circle__blue"><span class=" u-bold">国保税の税率（平成28年度）</span><br>
                国保税として納めていただくのは、医療給付費分と後期高齢者支援金分、介護納付金分それぞれの所得割・資産割・均等割・平等割を合計した額です。
                平成28年度の国保税の税率は、下表のようになっています。なお、介護納付金分は、40歳から64歳までの加入者にかかります。
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong">区分</th>
                      <th class="__strong">医療給付費分</th>
                      <th class="__strong">後期高齢者支援金等分</th>
                      <th class="__strong">介護納付金分</th>
                    </tr>
                    <tr>
                      <td class="__weak">1．所得割<br>（平成27年中の総所得－基礎控除33万円）×税率</td>
                      <td>4.91%</td>
                      <td>3.00%</td>
                      <td>1.70%</td>
                    </tr>
                    <tr>
                      <td class="__weak">2. 資産割<br>（平成28年度土地・家屋に係る固定資産税額）×税率</td>
                      <td>23.50%</td>
                      <td>15.00%</td>
                      <td>12.00%</td>
                    </tr>
                    <tr>
                      <td class="__weak">3. 均等割<br>（加入者1人あたり）</td>
                      <td>13,500円</td>
                      <td>11,000円</td>
                      <td>11,000円</td>
                    </tr>
                    <tr>
                      <td class="__weak">4. 平等割<br>（1世帯あたり）</td>
                      <td>27,000円</td>
                      <td>9,500円</td>
                      <td>7,000円</td>
                    </tr>
                    <tr>
                      <td class="__weak">5. 課税限度額<br>（※この金額以上は課税されません。）</td>
                      <td>52万円</td>
                      <td>17万円</td>
                      <td>16万円</td>
                    </tr>
                  </table>
                  <p>医療給付費分・・・国保加入者の医療給付にあてるための課税額です。<br>
                  後期高齢者支援金等分・・・75歳以上の方が加入する後期高齢者医療制度等の運営費用にあてるための課税額です。<br>
                  介護納付金分・・・40歳から64歳まで（介護保険第2号被保険者）の方の介護保険制度の運営費用にあてるための課税額です。
                  </p>
                </li>
                <li class=" u-circle__blue"><span class=" u-bold">国保税の軽減</span><br>
                所得が一定額よりも少ない世帯に対して、国保税を軽減する制度があります。国保税のうち均等割額と平等割額を軽減するもので、
                医療給付分・後期高齢者支援金等分・介護納付金分それぞれに適用されます。全て世帯の所得合計額で判定を行いますので、必ず確定申告か住民税の申告をお願いします。
                軽減の判定基準につきましては、下表のようになっております。
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong u-w20">軽減割合</th>
                      <td>基準となる所得金額<br>（擬制世帯主を含む世帯主、被保険者及び特定同一世帯所属者の所得の合計額）</td>
                    </tr>
                    <tr>
                      <th class="__strong">7割軽減</th>
                      <td>世帯の所得の合計額が33万円以下</td>
                    </tr>
                    <tr>
                      <th class="__strong">5割軽減</th>
                      <td>世帯の所得の合計額が {33万円＋（26万5千円×被保険者及び特定同一世帯所属者の数）} 以下</td>
                    </tr>
                    <tr>
                      <th class="__strong">2割軽減</th>
                      <td>世帯の所得の合計額が {33万円＋（48万円×被保険者及び特定同一世帯所属者の数）} 以下</td>
                    </tr>
                  </table>
                  <p class="c-annotation__no">※特定同一世帯所属者とは、一定の年齢到達により国保から後期高齢者医療制度の被保険者となった方で、継続して同一の世帯に属している方を言います。</p>                
                </li>
            </ul>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■入湯税</h2>
            <p>入湯税は、環境衛生、泉源の保護管理、消防施設や観光振興に要する費用に充てられ、目的税と呼ばれています。<br>
            壮瞥町では、環境衛生施設の整備・維持管理や観光の振興、観光施設の整備などに入湯税を充当しています。</p>
              <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w20">納める人</th>
                  <td>温泉に入浴した時に課税される税金です。温泉を利用した施設の管理者が、施設に宿泊したり入浴した人から、利用料と一緒に徴収しています(特別徴収)。</td>
                </tr>
                <tr>
                  <th class="__strong">納める金額</th>
                  <td>・日帰り入浴は100円／人<br>
                  ・宿泊150円／人<br>
                  ・修学旅行の学生生徒75円／人<br>
                  ・ユース・ホステルの会員(日本ユースホステル登録旅館)100円／人<br>
                  ・湯治のための7日以上の滞在は1日100円／人
                  </td>
                </tr>
                <tr>
                  <th class="__strong">課税免除</th>
                  <td>・年齢12歳未満の者<br>
                  ・共同浴場又は一般公衆浴場に入湯する者<br>
                  ・修学旅行の中学校の生徒</td>
                </tr>
              </table>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■納期・納付方法</h2>
            <ul>
            	<li>(1) 主な税金の納期月は次のようになっています。
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong u-w20">区分</th>
                      <th class="__strong">1期</th>
                      <th class="__strong">2期</th>
                      <th class="__strong">3期</th>
                      <th class="__strong">4期</th>
                      <th class="__strong">随1期</th>
                      <th class="__strong">随2期</th>
                      <th class="__strong">随3期</th>
                      <th class="__strong">随4期</th>
                    </tr>
                    <tr>
                      <td class="__weak">町民税</td>
                      <td>6月</td>
                      <td>8月</td>
                      <td>10月</td>
                      <td>12月</td>
                      <td>1月</td>
                      <td>2月</td>
                      <td>3月</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="__weak">固定資産税</td>
                      <td>6月</td>
                      <td>8月</td>
                      <td>10月</td>
                      <td>12月</td>
                      <td>1月</td>
                      <td>2月</td>
                      <td>3月</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="__weak">軽自動車税</td>
                      <td>6月</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="__weak">国民健康保険税</td>
                      <td>6月</td>
                      <td>8月</td>
                      <td>10月</td>
                      <td>12月</td>
                      <td>1月</td>
                      <td>2月</td>
                      <td>3月</td>
                      <td>4月</td>
                    </tr>
                  </table>
                  <p>・口座振替をご利用の方は、上記納期月の末日(12月は25日)に指定口座から振り替えます。<br> (※月の末日等が土日祝日の場合は、次の日以降の最初の平日になります)<br>
                  ・口座振替をご利用の方は、納期限の前に残高の確認をお願いします。<br>
                  ・納期限後20日を経過する日までに納付が確認できないときは、督促状を送付させていただきます。</p>
              </li>
              <li>(2) 口座振替<br>
              町税は、役場出納窓口のほか納付書に記載された指定の金融機関で納めることができます。
              また、下記の金融機関であれば、預金口座から自動振替をすることもできます。
              この制度を利用しますと税金を窓口に持参する必要がなく、納め忘れもなくなります。<br>納税は、便利で確実な口座振替をぜひご利用ください。
              <table class="c-table-1">
                <tr>
                  <th class="__strong u-w30">口座振替が可能な金融機関</th>
                  <td>伊達信用金庫、とうや湖農業協同組合、ゆうちょ銀行、北海道銀行、北洋銀行</td>
                </tr>
              </table>
              </li>
            </ul>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■納税証明書</h2>
              <table class="c-table-1">
                <tr>
                  <th class="__strong u-w30">手続きに必要なもの</th>
                  <td>印鑑、手数料（原則１税目につき200円）、身分証明書</td>
                </tr>
                <tr>
                  <th class="__strong">請求書の様式</th>
                  <td>「交付申請書」は「様式ダウンロード」ページからダウンロードしてください。
                  <p class=" u-mt__small  u-mb__0"><a href="" class=" c-link">「様式ダウンロード」はこちら</a></p>
                  </td>
                </tr>
              </table>
              <p><span class=" u-bold">郵送による各種税務証明書の申請について</span><br>
              納税証明書のほか、課税証明書、所得証明書などの各種税務関係証明書の郵送による請求には、手数料分の定額小為替、返送先を記入し切手を貼付した返信用封筒、身分証明書の写し、
              必要な証明書の名称を記載したメモなどを同封して税務財政課宛に申請してください。<br>
              手数料は、証明書１通につき200円ですが、児童手当用所得証明書、児童扶養手当用所得証明書は無料です。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■納税相談</h2>
            <p>やむをえない特別な事情があって、納期限内に納税できないような場合には、事前にご相談ください。
            支払いの方法(分割払いの金額や方法、納付期限のことなど)や口座振替の手続きなど相談に応じます。</p>
            <p>【お問い合わせ】税務財政課 TEL <a href="tel:0142-66-2121">0142-66-2121</a></p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■国税・道税</h2>
            <section>
                <h3 class="c-heading-4">(1) 国税・道税</h3>
                <p><a href="http://www.nta.go.jp/" class="c-link__blank">国税庁</a></p>
                <p><a href="http://www.pref.hokkaido.lg.jp/sm/zim/" class="c-link__blank">北海道総務部税務課</a></p>
            </section>
            <section>
                <h3 class="c-heading-4">(2) インタｰネットからの申告</h3>
                <p><a href="http://www.e-tax.nta.go.jp/" class="c-link__blank">e-Tax(イｰタックス):国税電子申告・納税システム</a></p>
                <p><a href="http://www.eltax.jp/" class="c-link__blank">eLTAX(エルタックス):地方税ポｰタルシステム</a></p>
            </section>
            <p class="c-annotation__no">※利用方法等、詳細につきましては、各ホｰムペｰジをご覧ください。</p>
            <p>【お問い合わせ】税務財政課 TEL <a href="tel:0142-66-2121">0142-66-2121</a></p>
        </div>
      </div>
      
      


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
