<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">おくやみ</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">おくやみ</h1>
      
      <ul>
      	<li><a href="">火葬・墓地について</a></li>
        <li><a href="">死亡届について</a></li>
      </ul>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■死亡届</h2>
            <p>家族等が亡くなられたときは、戸籍法に基づき「死亡届」を届出することとなっています。「死亡届」についてはこちらをご覧ください<br>
            <a href="" class="c-link">「住民登録・戸籍・印鑑登録・パスポート」を見る</a>
            </p>
        </div>
      </div>
      
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■火葬・墓地</h2>
            <h3 class="c-heading-3">火葬</h3>
            <ol>
            	<li>
                	<h4 class="c-heading-4">(1) 火葬場</h4>
                    <p>壮瞥町では町内に火葬場を管理・運営しています。
                    火葬場では、亡くなられた方の火葬を行っているほか、疾病などで切断された肢体などや出産時のあと産類の火葬業務を行っています。
                    火葬を行うためには事前予約が必要です。</p>
                      <table class="c-table-1">
                        <tr>
                          <td class="__strong u-w30">所在地</td>
                          <td>壮瞥町字滝之町446番地5</td>
                        </tr>
                        <tr>
                          <td class="__strong">電話</td>
                          <td>役場経済環境課環境衛生係 <a href="tel:0142-66-2121">0142-66-2121</a></td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h4 class="c-heading-4">(2) 火葬の手続き・使用料</h4>
                    <p>亡くなられた方の火葬の予約は、死亡届の受付と併せて町の担当が受け付けます。<br>
                    「死亡届」の詳しい内容はこちらをご覧ください。<br>
                    <a href="" class="c-link">住民登録・戸籍・印鑑登録・パスポート</a></p>
                    
                    <p>火葬場の使用料は下記のとおりです</p>
                      <table class="c-table-1">
                        <tr>
                          <th class="__strong" colspan="2" rowspan="2">区分</th>
                          <th class="__strong" colspan="2">使用料</th>
                        </tr>
                        <tr>
                          <td class=" c-txt__left">死亡者が死亡時に本町に住所を有していたとき又は使用者が本町に住所を有しているとき</td>
                          <td>左記以外のとき</td>
                        </tr>
                        <tr>
                          <td class="__weak">13歳以上</td>
                          <td>1体につき</td>
                          <td>15,000円</td>
                          <td>30,000円</td>
                        </tr>
                        <tr>
                          <td class="__weak">13歳未満</td>
                          <td>1体につき</td>
                          <td>7,500円</td>
                          <td>15,000円</td>
                        </tr>
                        <tr>
                          <td class="__weak">13歳以上</td>
                          <td>1体につき</td>
                          <td>15,000円</td>
                          <td>30,000円</td>
                        </tr>
                        <tr>
                          <td class="__weak">死産児</td>
                          <td>1体につき</td>
                          <td>7,500円</td>
                          <td>15,000円</td>
                        </tr>
                        <tr>
                          <td class="__weak">改葬・身体の一部</td>
                          <td>1件につき</td>
                          <td>7,500円</td>
                          <td>15,000円</td>
                        </tr>
                      </table>
                </li>
            </ol>

            <h3 class="c-heading-3">墓地</h3>
            <ol>
            	<li>
                	<h4 class="c-heading-4">(1) 墓地</h4>
                    <p>壮瞥町では、町内に下記の共同墓地を管理・運営しています。墓地にお墓を建立するときは、壮瞥町の使用許可が必要で、許可を受けるには次の条件が必要です。</p>
                    <ul class="c-list__num">
                    	<li>町内に住所があること</li>
                    	<li>使用許可を受けてから3年以内に墓碑（墓標）を建立すること</li>
                    </ul>
                      <table class="c-table-1">
                        <tr>
                          <th class="__strong u-w30">名称</th>
                          <th class="__strong">位置</th>
                        </tr>
                        <tr>
                          <td class="__weak">滝之町共同墓地</td>
                          <td>壮瞥町字滝之町445番地2、446番地1、446番地3</td>
                        </tr>
                        <tr>
                          <td class="__weak">久保内共同墓地</td>
                          <td>壮瞥町字上久保内27番地</td>
                        </tr>
                        <tr>
                          <td class="__weak">立香共同墓地</td>
                          <td>壮瞥町字立香159番地</td>
                        </tr>
                        <tr>
                          <td class="__weak">仲洞爺共同墓地</td>
                          <td>壮瞥町字仲洞爺41番地</td>
                        </tr>
                        <tr>
                          <td class="__weak">蟠渓共同墓地</td>
                          <td>壮瞥町字蟠渓79番地2</td>
                        </tr>
                        <tr>
                          <td class="__weak">弁景共同墓地</td>
                          <td>壮瞥町字弁景184番地</td>
                        </tr>
                        <tr>
                          <td class="__weak">黄渓共同墓地</td>
                          <td>壮瞥町字弁景252番地3、252番地24</td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h4 class="c-heading-4">(2) 墓地の手続き・使用料</h4>
                    <p>墓地にお墓を建立するときは、町から墓地の使用許可を受けなければなりません。</p>
                    
                    <p>手続きに必要なもの<br>
                    「墓地使用申請書」は「様式ダウンロード」ページからダウンロードください。<br>
                    <a href="" class="c-link">「様式ダウンロード」を見る</a></p>
                    
                      <table class="c-table-1">
                        <tr>
                          <th class="__strong">墓地名</th>
                          <th class="__strong">面積</th>
                          <th class="__strong">使用料</th>
                        </tr>
                        <tr>
                          <td>滝之町共同墓地</td>
                          <td>1区画　4.0平方メートル</td>
                          <td>445番地2の共同墓地1区画40,000円</td>
                        </tr>
                      </table>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■死亡したときの保険等の手続き</h2>
            <p>国民健康保険や後期高齢者医療、介護保険の加入者が亡くなったときには、それぞれに喪失届や葬祭費の手続きなどがあります。<br>
            死亡届と同日で行う必要はありませんが、忘れずにお手続きください。
            </p>
            <section>
            	<h3 class="c-heading-3">必要なもの</h3>
                <ul class="c-list">
                	<li>亡くなられた方の保険証類（健康保険証、身体障害者手帳や療育手帳など）</li>
                    <li>手続きされる方の印鑑</li>
                    <li>葬儀執行人（喪主）の方の口座がわかるもの（通帳など）</li>
                    <li>相続人の方の口座がわかるもの（通帳など）</li>
                    <li>会葬ハガキや葬儀の領収証など</li>
                </ul>
            </section>
            <section>
            	<h3 class="c-heading-3">手続き先</h3>
                <p>壮瞥町役場 住民福祉課<br>税金や水道の手続きがある場合には、その都度ご案内いたします。</p>
            </section>
            <section>
            	<h3 class="c-heading-3">年金の手続き</h3>
                <p>年金の手続きは、亡くなられた方が加入していた年金の種類によって手続き先、必要書類などが違います。<br>詳しくは住民福祉課または年金事務所へご確認ください。</p>
            </section>
            <p>【お問い合わせ】<br>壮瞥町役場　住民福祉課　TEL <a href="tel:0142-66-2121">0142-66-2121</a></p>
        </div>
      </div>



      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
