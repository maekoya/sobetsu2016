<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">ごみ・リサイクル</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">ごみ・リサイクル</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<p class=" u-circle__blue">壮瞥町のごみの出し方平成28年度版はこちらをご覧ください <a href="" class="c-link__pdf">DL/PDF</a></p>

        	<h2 class="c-heading-2">■家庭から出るごみの分け方</h2>
            <section>
            	<h3 class="c-heading-3">(1) 燃やせるごみ</h3>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong">プラスチック製品発砲スチロール</th>
                      <td>洗剤容器・ポリ容器・家電製品を梱包している発泡スチロールなど</td>
                    </tr>
                    <tr>
                      <th class="__strong">卵パック豆腐などのパック</th>
                      <td>卵パック・豆腐パック・野菜・果物のパックなど</td>
                    </tr>
                    <tr>
                      <th class="__strong">ポリ袋・トレーなどの食品容器</th>
                      <td>白いトレーは洗って改修しているお店に</td>
                    </tr>
                    <tr>
                      <th class="__strong">食用あぶら</th>
                      <td>紙や布などにしみこませて又は凝固剤で固めて</td>
                    </tr>
                    <tr>
                      <th class="__strong">ゴム製品</th>
                      <td>長ぐつ・手袋など</td>
                    </tr>
                    <tr>
                      <th class="__strong">紙くず・紙オムツなど</th>
                      <td>汚物はトイレに捨てて</td>
                    </tr>
                    <tr>
                      <th class="__strong">衣類・毛布類・座布団</th>
                      <td>50cm以内に切って</td>
                    </tr>
                    <tr>
                      <th class="__strong">皮革製品</th>
                      <td>くつ・かばんなど</td>
                    </tr>
                    <tr>
                      <th class="__strong">ビデオテープ・CDなど</th>
                      <td>カセットテープ・ビデオテープ・CDなど</td>
                    </tr>
                    <tr>
                      <th class="__strong">木の枝</th>
                      <td>50cm程度にまとめて<br>（指定袋に入らない時はゴミ処理券を貼ってください）</td>
                    </tr>
                    <tr>
                      <th class="__strong">その他</th>
                      <td>乾燥剤・防虫剤など（スプレー式を除く除湿剤は水を除いて）</td>
                    </tr>
                  </table>
            </section>

            <section>
            	<h3 class="c-heading-3">(2) 資源ごみ</h3>
                <p>排出のルールを守っていただいた資源ごみは無料で回収いたしますが、守れないものは資源ごみとして回収できませんのでご注意ください。</p>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong">ストックヤードへ排出するもの</th>
                      <td>
                      	<ul>
                        	<li>・空缶類（※1）、ペットボトル（※2）</li>
                            <li>・カレットビン（無色・茶色・その他に色分け）（※3）</li>
                            <li>・リターナブルびん（中をすすいで）</li>
                            <li>・新聞、チラシ、ダンボール、雑誌（※4）</li>
                            <li>・布類（古着・古布など／木綿類に限る）（※5）</li>
                            <li>・飲料用の紙パック（※6）</li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <th class="__strong">エコポストへ排出するもの</th>
                      <td>生ごみ（厨芥ごみ）（※7）、<br>落葉、刈草（土を除いて）</td>
                    </tr>
                    <tr>
                      <th class="__strong">回収ボックスへ排出するもの</th>
                      <td>小型の電気製品（※8）<br>（ドライヤー、トースター、ジューサー、ミキサー、ビデオカメラ、換気扇、コーヒーメーカー、小型扇風機など）</td>
                    </tr>
                  </table>
                  <dl>
                  	<dt class=" u-bold">※1空缶類</dt>
                    <dd>ひどく汚れた缶、一斗缶程度の大型缶、油の缶、スプレー缶（中を使い切り穴をあけて）等は燃やせないゴミになります。</dd>
                    <dt class=" u-bold u-mt__small">※2ペットボトル</dt>
                    <dd>工作などでテープや接着剤を付けたボトルや、空容器に農薬や薬剤、有害な化学物質を入れて使用していたもの等は燃やせないゴミになります。</dd>
                    <dt class=" u-bold u-mt__small">※3カレットビン</dt>
                    <dd>化粧品のびん、電球・蛍光灯等、耐熱ガラスや板ガラス、コップ等の食器類、油のびん等は燃やせないゴミになります。</dd>
                    <dt class=" u-bold u-mt__small">※4新聞、チラシ、ダンボール、雑誌</dt>
                    <dd>新聞・雑誌・ダンボールに分け、持ちやすい大きさにまとめ、種類ごとにひもで束ねてください。</dd>
                    <dt class=" u-bold u-mt__small">※5布類</dt>
                    <dd>布団、座布団、カーペットなどは回収しません。</dd>
                    <dt class=" u-bold u-mt__small">※6飲料用の紙パック</dt>
                    <dd>軽く洗い、開いて乾かし、ひもで束ねてください。</dd>
                    <dt class=" u-bold u-mt__small">※7生ごみ</dt>
                    <dd>水分を切り、収集日当日（朝8時まで）にゴミステーションに設置したエコポストへ出してください
                    （エコポストに排出する際は袋等に入れずにそのままお出しください）。<br>ビンまたはビンのふた、プラスチックまたはビニール製品、せともの・ガラスコップ等の破片、
                    たばこの吸い殻、アルミ箔等の異物が混入しているエコポストは回収できない場合があります。エコポストをご利用する方は、異物が入らないように十分に注意をして
                    生ごみを排出してください。</dd>
                    <dt class=" u-bold u-mt__small">※8小型廃家電</dt>
                    <dd>・地域交流センター山美湖・久保内出張所に設置している回収ボックスの投入口に直接入れてください。<br>
                    ・家電4品目（テレビ、冷蔵庫・冷凍庫、洗濯機・衣類乾燥機、エアコン）は対象外です。<br>
                    ・ブラウン管を内蔵する製品は対象外です。<br>
                    ・電池、バッテリー、蛍光管、電球類、CDなどのディスク類は対象外です。<br>
                    ・説明書や袋類、箱などは回収ボックスに入れないでください。<br>
                    ・携帯電話などの個人情報はあらかじめ消去してから出してください。</dd>
                  </dl>
           	</section>

            <section>
            	<h3 class="c-heading-3">(3) 燃やせないごみ</h3>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong">小型金属製品</th>
                      <td>腕時計、おき時計、なべ・やかん、<br>家庭用ペンキ缶など（中身を使い切って）</td>
                    </tr>
                    <tr>
                      <th class="__strong">蛍光灯</th>
                      <td>取り替えた箱に入れて、または、紙などで包んで「危険」と書いて</td>
                    </tr>
                    <tr>
                      <th class="__strong">スプレー式容器</th>
                      <td>整髪料、殺虫剤、卓上ガスボンベ、使い捨てガスライターなど
                      （中身を使い切ってから穴をあけること。火のそばでやらないこと）</td>
                    </tr>
                    <tr>
                      <th class="__strong">ガラス・せともの</th>
                      <td>食器・花びん・化粧品の容器など（割れ物は紙などで包んで「危険」と書いて）</td>
                    </tr>
                  </table>
            </section>

            <section>
            	<h3 class="c-heading-3">(4) 袋に入らない大型ごみ</h3>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong">スポーツ・レジャー用品</th>
                      <td>ゴルフセット、スキーセット、ルームランナー、自転車など</td>
                    </tr>
                    <tr>
                      <th class="__strong">寝具</th>
                      <td>布団（1枚ずつひもでしばって）、マットレス、ベッドなど</td>
                    </tr>
                    <tr>
                      <th class="__strong">家電製品</th>
                      <td>ステレオ、掃除機など</td>
                    </tr>
                    <tr>
                      <th class="__strong">家具</th>
                      <td>ソファー、いす、たんすなど<br>（ガラスなどは紙に包み「危険」又は「ガラス」と表示して）</td>
                    </tr>
                    <tr>
                      <th class="__strong">その他</th>
                      <td>煙突、ガス台、ストーブなど（灯油・電池を除いて）</td>
                    </tr>
                  </table>
            </section>

            <section>
            	<h3 class="c-heading-3">(5) 受け入れないもの</h3>
                  <table class="c-table-1">
                    <tr>
                      <td>ドラム缶、ホームタンク、オートバイ、ピアノ、浴槽、コンクリート、石、ブロック、LPガスボンベ、消火器、<br>
                      タイヤ、バッテリー、薬品の入っている容器、冷蔵庫（冷凍庫含む）、洗濯機、テレビ、エアコン、衣類乾燥機、パソコン、など<br>
                      ※販売者・専門業者又は許可業者に相談してください。</td>
                    </tr>
                  </table>
            </section>

            <section>
            	<h3 class="c-heading-3">(6) 乾電池の分別</h3>
                <p>資源ごみストックヤードに置いた入れ物に入れてください。</p>
            </section>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">

        	<h2 class="c-heading-2">■家庭のごみの出し方</h2>
            <section>
            	<h3 class="c-heading-3">(1) ごみ収集カレンダー</h3>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong">収集地区</th>
                      <th class="__strong">滝3、滝4、ほくと団地、上久保内、久保内、南久保内、蟠渓、幸内、弁景、壮瞥温泉、滝之上</th>
                      <th class="__strong">仲洞爺、東湖畔、立香、滝之町（滝3、滝4、ほくと団地を除く）</th>
                    </tr>
                    <tr>
                      <td class="__weak">燃やせるごみ</td>
                      <td>毎週月曜日・木曜日</td>
                      <td>毎週火曜日・金曜日</td>
                    </tr>
                    <tr>
                      <td class="__weak">燃やせないごみ・大型ごみ</td>
                      <td colspan="2">毎月第４土曜日</td>
                    </tr>
                    <tr>
                      <td class="__weak">生ごみ</td>
                      <td>毎週月曜日・木曜日</td>
                      <td>毎週火曜日・金曜日</td>
                    </tr>
                  </table>
                  <ul class="c-list">
                  	<li>平成28年度ごみ収集カレンダーはこちらからダウンロードできます <a href="" class="c-link__pdf">DL/PDF（作成中）</a></li>
                    <li>ごみの分け方に従い、ごみを分別して指定袋（有料）に入れ、ごみがこぼれないように縛り、収集日の午前8時までに指定されたゴミステーションに出してください。</li>
                    <li>大型ごみは１品ごとにごみ処理券を貼って、大型ごみの回収日にゴミステーションに出してください。</li>
                    <li>ゴミステーション付近に車両が止めてあると、ごみの収集ができない場合があるのでご注意ください。</li>
                    <li>都合により収集日が変更になる場合があります。事前に広報等でお知らせいたしますのであらかじめご了承ください。</li>                  
                  </ul>
            </section>

            <section>
            	<h3 class="c-heading-3">(2) 指定袋・ごみ処理券</h3>
                <p>ごみをゴミステーションに捨てる場合は指定袋又はごみ処理券を購入してください。</p>
                <h4 class="c-heading-4">ア）指定袋</h4>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong">40リットル</th>
                      <td>10枚入800円（1枚あたり80円）</td>
                      <td rowspan="3">燃やせるごみ（白）<br>燃やせないごみ（緑）<br>いずれも同額</td>
                    </tr>
                    <tr>
                      <th class="__strong">20リットル</th>
                      <td>10枚入400円（1枚あたり40円）</td>
                    </tr>
                    <tr>
                      <th class="__strong">10リットル</th>
                      <td>10枚入200円（1枚あたり20円）</td>
                    </tr>
                  </table>

                <h4 class="c-heading-4">イ）ごみ処理券</h4>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong">大型ごみ用</th>
                      <td>1枚160円</td>
                    </tr>
                  </table>

                <h4 class="c-heading-4">ウ）取扱店一覧</h4>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong">滝之町</th>
                      <td>ショップインふじかわ、フレンドショップトミタ、壮瞥郵便局、セイコーマートふじさわ、
                      Ａコープ壮瞥店、増井電機商会、ドリームハウスカトウ、ほそかわ、ゆーあいの家</td>
                    </tr>
                    <tr>
                      <th class="__strong">久保内</th>
                      <td>久保内郵便局、久保内ふれあいセンター</td>
                    </tr>
                    <tr>
                      <th class="__strong">壮瞥温泉</th>
                      <td>セイコ－マート壮瞥温泉店</td>
                    </tr>
                    <tr>
                      <th class="__strong">仲洞爺</th>
                      <td>渡辺豆腐店、来夢人の家</td>
                    </tr>
                    <tr>
                      <th class="__strong">蟠渓</th>
                      <td>蟠渓ふれあいセンター</td>
                    </tr>
                  </table>
           	</section>

            <section>
            	<h3 class="c-heading-3">(3) 引っ越しなどで一度に多くのごみが出る場合</h3>
                <p>次のどちらかの方法で処分してください。この場合、指定袋、ごみ処理券は必要ありません。</p>
                <h4 class="c-heading-4">ア）許可業者に依頼</h4>
                <p>次の一般廃棄物収集運搬業許可業者に直接お問い合わせください。<br>別途収集運搬処理料金が必要になります。</p>
                <ul class=" c-list">
	           		<li>（株）出田建設 <a href="tel:0142-66-6011">0142-66-6011</a></li>
                    <li>壮建興業（株）<a href="tel:0142-66-2412">0142-66-2412</a></li>
                    <li>（株）中山工務店 <a href="tel:0142-66-2038">0142-66-2038</a></li>
                    <li>不二工営（株）<a href="tel:0142-23-2629">0142-23-2629</a></li>
                    <li>（株）ダテックス <a href="tel:0142-22-0071">0142-22-0071</a></li> 
                </ul>
                
                <h4 class="c-heading-4">イ）自分で運ぶ自己搬入</h4>
                <p>ご自分で室蘭のメルトタワー21へ運ぶ場合、入場時にごみ量を計算し、処理料金を直接お支払いいただきます。
                処理料金は、ごみ100kgまで500円、100kgを超えたときは10kg毎に50円加算されます。</p>
                <p><a href="http://www.union.nishi-iburi.lg.jp/melt.html" class="c-link__blank">メルトタワー21</a></p>
            </section>
        </div>
      </div>


      
      


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
