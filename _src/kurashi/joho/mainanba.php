<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">社会保障・税番号（マイナンバー）制度</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">社会保障・税番号（マイナンバー）制度</h1>

     <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/mainanba/img01.gif" width="" height="" alt="マイナちゃん">
        </div>
        <div class="u-grid__col-8">
          <p>平成27年10月5日から社会保障・税番号（マイナンバー）制度が始まり、順次、日本国内の全住民（住民票をもつ外国人を含みます。）に12桁の個人番号が記載された「通知カード」が世帯ごとに簡易書留で送付されています。</p>
          <p>また、法人に対しては、13桁の法人番号が付番されます。郵便局に郵便物の転送届をしている人は、転送先に届きません。住所地と異なる住所に居住している人は、住所地の市区町村役場の戸籍・住民窓口にご連絡ください。
送付された通知カード（個人番号）は生涯にわたり、社会保障・税の手続き時に使用しますので、大切に保管してください。</p>
<p>封筒の中に、「個人番号カード」の発行申請書が入っていますので、希望される方は申請書に必要事項を記入し顔写真を同封して返信用封筒で申請してください。<br>
スマートフォンなどからも発行申請が行えます。</p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■通知カード・個人番号カード </h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 通知カードとは</h3>
                    <p>紙製のクレジットカードサイズのカードで、透かしなどの最新の偽造防止技術を使って作られています。<br>通知カードは平成27年10月から順次発送されています。</p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 個人番号カードとは</h3>
                    <p>マイナンバーが記載された、顔写真付きの公的身分証明書として利用できるプラスチック製のＩＣカードで、発行にかかる手数料は無料です。e-Taxなどの電子申請で利用できる、公的個人認証による電子証明書が最初から格納されています。<br>「住民基本台帳カード」をお持ちの方は、「個人番号カード」発行の際、「住民基本台帳カード」の返却が必要になります。<br>
                    「個人番号カード」が発行されると、「通知カード」の返却が必要になります。<br>
                    有効期間は満20歳以上の人は発行日から10回目の誕生日で、満20歳未満の人は容姿の変化を考慮して、発行日から5回目の誕生日までです。</p>
                 <div class="u-grid__row">
                    <div class="u-grid__col-6">
                      <img src="images/mainanba/omote.jpg" width="" height="" alt="表面">
                    </div>
                    <div class="u-grid__col-6">
                      <img src="images/mainanba/ura.jpg" width="" height="" alt="裏面">
                    </div>
                  </div>
                  <p><span class=" u-bold">通知カード・個人番号カードのお問い合わせは</span><br>
                  【ナビダイヤル　平日8:30～22:00 (H28.3.31まで)、～17:30 (H28.4.1から)、土日祝9:30～17:30】<br>
                  個人番号カードコールセンター<br>
                  TEL <a href="tel:0570-783-578">0570-783-578 </a>(日本語)<br>
                  TEL <a href="tel:0570-064-738">0570-064-738</a>（英語、中国語、韓国語、スペイン語、ポルトガル語）<br>
                   <a href="https://www.kojinbango-card.go.jp/" class="c-link">個人番号カード総合サイト（地方公共団体情報システム機構）</a>	</p>
                </li>
            </ol>
        </div>
      </div>
      
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■平成28年1月1日以降の各手続について</h2>
            <p>窓口での本人確認が厳格化され、マイナンバーを利用する手続きにおいて、<br>
            「通知カード」＋「運転免許証、パスポート、健康保険証、年金手帳など」の本人確認書類の提示が必要になります。<br>
            「個人番号カード」を持っていると、窓口での本人確認が「個人番号カード」1枚で済むため手続きがスムーズになりますので、<br>「個人番号カード」の申請をお勧めします。
            また、戸籍の届出、転入、町内転居の手続きの際は、通知カード券面の書き換え、個人番号カード格納情報と券面の書き換えが必要になりますので、
            世帯全員分の各カードを窓口に必ず持参してください。</p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■マイナンバーに関するお問合せ先</h2>
            <p>【全国共通ナビダイヤル】9:30から17:30（土日祝日・年末年始を除く）<br>
            <span class=" u-bold">コールセンター</span> <a href="tel:0570-20-0178">0570-20-0178</a><br>
            </p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■さらに詳しいことは</h2>
            <ul class=" c-list p-list__mb10">
            	<li><a href="http://www.cas.go.jp/jp/seisaku/bangoseido/" class="c-link__blank">内閣官房マイナンバーホームページ</a></li>
                <li><a href="http://www.gov-online.go.jp/" class="c-link__blank">政府広報オンライン</a></li>
                <li><a href="http://www.mhlw.go.jp/stf/seisakunitsuite/bunya/0000062603.html" class="c-link__blank">厚生労働省マイナンバーホームページ</a></li>
                <li><a href="http://www.nta.go.jp/mynumberinfo/" class="c-link__blank">国税庁マイナンバーホームページ</a></li>
                <li><a href="http://www.ppc.go.jp/" class="c-link__blank">特定個人情報保護委員会K</a>            
            </ul>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■特定個人情報保護評価</h2>
            <p>番号法では、マイナンバーを含む個人情報を特定個人情報として、国の行政機関や地方公共団体等が、特定個人情報を含むファイルを保有しようとするときには、個人のプライバシー等の権利利益に与える影響を予測し、特定個人情報の漏えい等の事態を発生させるリスクを分析して、そのようなリスクを軽減するための適切な措置を講ずることを宣言することとされています。<br>
壮瞥町が作成した特定個人情報保護評価書について、以下のとおり公表します。</p>

            <ul class=" c-list p-list__mb10">
            <li><a href="" class="c-link__pdf">01_特定個人情報保護評価書（住民基本台帳）DL/PDF</a></li>
            <li><a href="" class="c-link__pdf">02_特定個人情報保護評価書（健康増進）DL/PDF</a></li>
            <li><a href="" class="c-link__pdf">03_特定個人情報保護評価書（予防接種）DL/PDF</a></li>
            <li><a href="" class="c-link__pdf">04_特定個人情報保護評価書（個人住民税）DL/PDF</a></li>
            <li><a href="" class="c-link__pdf">05_特定個人情報保護評価書（固定資産税）DL/PDF</a></li>
            <li><a href="" class="c-link__pdf">06_特定個人情報保護評価書（被災者台帳）DL/PDF</a></li>
            <li><a href="" class="c-link__pdf">07_特定個人情報保護評価書（介護保険）DL/PDF</a></li>
            </ul>
        </div>
      </div>





      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
