<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">引っ越し手続き</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">引っ越し手続き</h1>
      

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■転入・転出されるみなさんへ</h2>
            <p>転入、転出の際にはいろいろな手続きが必要になります。代表的な手続きをまとめていますので、お忘れのないようご注意ください。</p>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 転入届・転出届</h3>
                    <p>住民票は、みなさんの居住関係を証明するほか、選挙人名簿の登録、小中学校への就学、国民年金・国民健康保険などの基礎となる大切なものです。
                    住所や世帯の変更などがあったときには、速やかに届出を行ってください。</p>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">転入</th>
                          <td>新しい住所地に転入されましたら、その日から14日以内に<br>
                          ・転出証明書（または住民基本台帳カード）<br>
                          ・届出人の印鑑<br>
                          ・本人確認書類（運転免許証など）<br>
                          ・個人番号通知カード<br>
                          ・個人番号カード（お持ちの方）<br>
                          をご持参のうえ、転入届の手続を行ってください。</td>
                        </tr>
                        <tr>
                          <th class="__strong">転出</th>
                          <td>転出先住所・転出予定日が決まりましたら、その日までに転出の届出が必要です。<br>手続後に転出証明書を交付します。<br>
                          手続には<br>・届出人の印鑑<br>・本人確認書類（運転免許証など）<br>・住民基本台帳カード（お持ちの方）<br>・印鑑登録証（お持ちの方）<br>をご持参ください。</td>
                        </tr>
                      </table>
                      <p><a href="" class="c-link">「住民登録・戸籍・印鑑登録・パスポート」を見る</a></p> 
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 国民健康保険</h3>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">転入</th>
                          <td>印鑑、本人確認書類（運転免許証など）をご持参ください。<br>
                          また、国民年金に加入されている方は、基礎年金番号が分かるもの（年金手帳など）もご持参ください。</td>
                        </tr>
                        <tr>
                          <th class="__strong">転出</th>
                          <td>転出されると、国民健康保険被保険者の資格がなくなります。<br>
                          転出予定日までに転出される方全員の国民健康保険の被保険者証と印鑑をご持参の上、
                          手続きを行ってください。</td>
                        </tr>
                      </table>
                      <p><a href="" class="c-link">「年金・国民健康保険・後期高齢者医療制度」を見る</a></p> 
                </li>
            	<li>
                	<h3 class="c-heading-3">(3) 後期高齢者医療制度</h3>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">転入</th>
                          <td>道外からの転入の場合は、前住所地で交付された負担区分証明書を持参のうえ、役場までお越しください。<br>
                          道内からの転入の場合は、手続きは不要です。後日保険証を送付します。</td>
                        </tr>
                        <tr>
                          <th class="__strong">転出</th>
                          <td>道外への転出の場合、負担区分証明書を発行しますので窓口までお越しください。<br>
                          また道外、道内どちらの場合も保険証は転出予定日までに返還してください。</td>
                        </tr>
                      </table>
                      <p><a href="" class="c-link">「年金・国民健康保険・後期高齢者医療制度」を見る</a></p> 
                </li>
            	<li>
                	<h3 class="c-heading-3">(4) 介護保険</h3>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">転入</th>
                          <td>前住所地で介護認定を受けていた人は受給資格証明書をご持参ください。<br>
                          前の市町村の認定内容でサービスが受けられます。
                          介護認定を受けていない人は、手続き不要です。<br>後日、介護保険被保険者証を送付します。</td>
                        </tr>
                        <tr>
                          <th class="__strong">転出</th>
                          <td>介護認定を受けている人には受給資格証明書をお渡ししますので、転出先の市町村の介護保険担当窓口に提出してください。</td>
                        </tr>
                      </table>
                      <p><a href="" class="c-link">「介護保険・高齢者福祉」を見る</a></p> 
                </li>
            	<li>
                	<h3 class="c-heading-3">(5) 医療費助成</h3>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">転入</th>
                          <td>障がいのある人、母と子（父と子）で生活している人、乳幼児などのいる人に医療費の助成を行っています。
                          健康保険証、（身体障害者手帳）、印鑑、世帯全員分の市町村民税課税証明書などをご持参ください。</td>
                        </tr>
                        <tr>
                          <th class="__strong">転出</th>
                          <td>乳幼児等・障がい者・母子家庭等の医療費受給者証の交付を受けている人は役場で返還してください。<br>
                          転出後は、壮瞥町の各種医療費受給者証は使用することができません。<br>また、転出先で課税証明書が必要になりますので、税務財政課で交付を受けてください。</td>
                        </tr>
                      </table>
                      <p>
                      	<a href="" class="c-link">「障がい福祉」を見る</a><br>
                        <a href="" class="c-link">「子育て支援」を見る</a><br>
                        <a href="" class="c-link">「お母さんと子どもの保健」を見る</a><br>
                      </p> 
                </li>
            	<li>
                	<h3 class="c-heading-3">(6) 児童手当・児童扶養手当</h3>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">転入</th>
                          <td>壮瞥町への転出予定日から15日以内に、届出人の印鑑、健康保険証の写し、受給者名義の預金通帳の写し、<br>所得証明書（後日提出も可）をご持参ください。</td>
                        </tr>
                        <tr>
                          <th class="__strong">転出</th>
                          <td>児童手当・児童扶養手当ともに手続きが必要です。なお、児童手当を受給している公務員の人は勤務先へ届出をしてください。
                          また、転出先で所得証明書が必要になりますので、税務財政課で交付を受けてください。</td>
                        </tr>
                      </table>
                      <p>
                        <a href="" class="c-link">「子育て支援」を見る</a><br>
                        <a href="" class="c-link">「お母さんと子どもの保健」を見る</a><br>
                      </p> 
                </li>
            	<li>
                	<h3 class="c-heading-3">(7) 犬の登録事項変更届</h3>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">転入</th>
                          <td>犬を飼っている人は犬の登録事項変更届が必要です。</td>
                        </tr>
                        <tr>
                          <th class="__strong">転出</th>
                          <td>転入先の市区町村で犬の所在地の変更を行ってください。壮瞥町での届出の必要はありません。</td>
                        </tr>
                      </table>
                      <p>
                        <a href="" class="c-link">「ペット・害虫駆除」を見る</a><br>
                      </p> 
                </li>
            	<li>
                	<h3 class="c-heading-3">(8) 水道の開始・休止の届出</h3>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">転入</th>
                          <td rowspan="2">役場までお越しください。</td>
                        </tr>
                        <tr>
                          <th class="__strong">転出</th>
                        </tr>
                      </table>
                      <p>
                        <a href="" class="c-link">「水道・下水道・温泉水」を見る</a><br>
                      </p> 
                </li>
            	<li>
                	<h3 class="c-heading-3">(9) ごみの出し方について</h3>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">転入</th>
                          <td rowspan="2">各地域でごみ収集日が違います。<br>お住まいの地域の収集の日程を確認して、最寄りのごみステーションに出してください。</td>
                        </tr>
                        <tr>
                          <th class="__strong">転出</th>
                        </tr>
                      </table>
                      <p>
                        <a href="" class="c-link">「ごみ・リサイクル」を見る</a><br>
                      </p> 
                </li>
            </ol>
        </div>
      </div>
      
      


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
