<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">持家・空き家・民間住宅（住宅制度）</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">持家・空き家・民間住宅（住宅制度）</h1>
      

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■住宅関係の助成制度</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 持ち家住宅取得奨励金</h3>
                    <p>壮瞥町内で持ち家を取得（新築・中古）される方への助成制度です。</p>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">対象者</th>
                          <td>町税滞納がなく、5年以上居住することを確約する方</td>
                        </tr>
                        <tr>
                          <th class="__strong">要件</th>
                          <td>50㎡以上の独立した住宅の新築、購入（中古含）※ただし、移転補償・保険適用は除く</td>
                        </tr>
                        <tr>
                          <th class="__strong">助成額</th>
                          <td>取得費の1/10以内<br>新築・建替 上限70万円以内（町内業者施工の場合は30万円分の商品券追加）<br>中古 上限50万円以内（3親等以内からの購入は対象外）</td>
                        </tr>
                        <tr>
                          <th class="__strong">手続き</th>
                          <td>・契約前に利用申込書提出（添付書類：納税調査同意書、見積書、平面図、定住誓約書）<br>
                          ・転居又は登記後1カ月以内に交付申請書提出（添付書類：住民票、登記簿謄本、契約書・領収書の写し）</td>
                        </tr>
                        <tr>
                          <th class="__strong">問合せ</th>
                          <td>役場企画調整課　[TEL] <a href="tel:0142-66-2121">0142-66-2121</a></td>
                        </tr>
                        <tr>
                          <th class="__strong">様式等</th>
                          <td><a href="" class="c-link__pdf">・持家取得奨励実施要綱DL/PDF</a><br>
                          ・「持ち家住宅取得奨励事業利用申込書」は「様式ダウンロード」ページからダウンロードしてください。
	                      <p><a href="" class="c-link">「様式ダウンロード」ページはこちら</a></p> 
                          </td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 住宅等リフォーム・住環境整備支援</h3>
                    <p>現在、壮瞥町内にお住まいで持ち家をリフォームされる方への助成制度です。</p>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">対象者</th>
                          <td>町内に住民票を有し、所有かつ自ら居住されている方で、リフォーム後も3年以上居住する方</td>
                        </tr>
                        <tr>
                          <th class="__strong u-w20">対象事業</th>
                          <td>・増改築、バリアフリー化、壁の張替、屋根・壁の塗装、床暖房工事、トイレ・洗面所<br>
                          ・浴槽の改修、サッシの取替、給湯機の設置、アスファルト舗装など<br>
                          ・本町に本店所在地を有する壮瞥町商工会員又は工事施工業者として登録している業者への発注が条件となります</td>
                        </tr>
                        <tr>
                          <th class="__strong">助成額</th>
                          <td>
                          <p class=" u-bold u-mb__small u-circle__blue">工事費用額と助成額</p>
                              <table class="c-table-1 u-mt__0">
                                <tr>
                                  <td class="__weak">30万円以上40万円未満</td>
                                  <td>6万円分の商品券を交付</td>
                                </tr>
                                <tr>
                                  <td class="__weak">40万円以上50万円未満</td>
                                  <td>8万円分の商品券を交付</td>
                                </tr>
                                <tr>
                                  <td class="__weak">50万円以上</td>
                                  <td>10万円分の商品券を交付</td>
                                </tr>
                              </table>
                        </tr>
                        <tr>
                          <th class="__strong">手続き</th>
                          <td>商工会または施工業者に備え付けの申請書に必要事項を記載し、商工会へ申請してください<br>
                          （申請は1申請者、1年1回まで）<br>
                          （添付書類：工事見積書、施工前写真、納税証明書（町税の滞納がないこと））</td>
                        </tr>
                        <tr>
                          <th class="__strong">問合せ</th>
                          <td>壮瞥町商工会　[TEL] <a href="tel:0142-66-2121">0142-66-2121</a></td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(3) 空き家改修・整理補助金</h3>
                    <p>現在、壮瞥町内に空き家をお持ちで、第三者への売却・賃貸のため、空き家の廃棄家具等の整理や居住するために必要最低限の改修をしようとする方への助成制度です。</p>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">対象者</th>
                          <td>壮瞥町内に所有する空き家を第三者へ売却・賃貸しようとする方</td>
                        </tr>
                        <tr>
                          <th class="__strong u-w20">対象事業</th>
                          <td>不動産事業者との媒介委任契約が成立または、壮瞥町不動産情報提供事業により賃貸又は売買契約が成立した空き家の整理又は改修<br>
                          ア）家財家具等の運搬及び廃棄<br>
                          イ）台所、浴室、便所、洗面所、床、内装等の生活するために必要な改修<br>
                          本町に本店所在地を有する壮瞥町商工会員又は工事施工業者として登録している業者への発注が条件となります</td>
                        </tr>
                        <tr>
                          <th class="__strong">助成額</th>
                          <td>整理・改修費の2/3以内で30万円以内（下限額3万円）</td>
                        </tr>
                        <tr>
                          <th class="__strong">手続き</th>
                          <td>・手続きの流れはこちらをご覧ください：<a href="" class="c-link__pdf">「空き家整理改修事業流れ」DL/PDF</a><br>
                          ・事前に利用申込書提出が必要です（添付書類：納税調査同意書、見積書など）</td>
                        </tr>
                        <tr>
                          <th class="__strong">問合せ</th>
                          <td>役場企画調整課　[TEL] <a href="tel:0142-66-2121">0142-66-2121</a></td>
                        </tr>
                        <tr>
                          <th class="__strong">様式</th>
                          <td><a href="" class="c-link__pdf">空き家整理改修事業要綱 DL/PDF</a><br>
                          「空き家整理改修事業利用申込書」は「様式ダウンロード」ページからダウンロードしてください。<br>
                          <a href="" class="c-link">「様式ダウンロード」ページはこちら</a></td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(4) 民間賃貸住宅建設助成事業</h3>
                    <p>壮瞥町内に賃貸住宅を建設される方への助成制度です。</p>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">対象者</th>
                          <td>町内に賃貸共同住宅を建設する個人、法人</td>
                        </tr>
                        <tr>
                          <th class="__strong">要件</th>
                          <td>・建築基準法の基準に適合するものであること。<br>
                          ・各戸に独立した玄関、台所、居間、浴室、便所等の設備が設けられていること。<br>
                          ・建設する1棟につき2以上の戸数を有すること。</td>
                        </tr>
                        <tr>
                          <th class="__strong">助成額</th>
                          <td>建設費の1/10以内で1戸あたり上限額70万円（町内業者施工の場合は30万円追加）</td>
                        </tr>
                        <tr>
                          <th class="__strong">手続き</th>
                          <td>・着工前に、事業認定申請書を提出（認定後2年以内の完成が条件）<br>
                          （添付書類：建物平面図･立面図、積算書類、住宅管理に関する書類など）<br>
                          ・完了検定後に交付申請書提出（添付書類：助成金算出書、登記簿謄本など）</td>
                        </tr>
                        <tr>
                          <th class="__strong">問合せ</th>
                          <td>役場企画調整課　[TEL] <a href="tel:0142-66-2121">0142-66-2121</a></td>
                        </tr>
                        <tr>
                          <th class="__strong">様式</th>
                          <td>「民間賃貸住宅建設助成事業認定申請書」は「様式ダウンロード」ページからダウンロードしてください。<br>
                          <a href="" class="c-link">「様式ダウンロード」ページはこちら</a></td>
                        </tr>
                      </table>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■町営住宅</h2>
            <p>町営住宅についての情報は「町営住宅」ページをご覧ください</p>
             <a href="" class="c-link">「町営住宅」を見る</a>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■不動産情報提供事業（空き家バンク）</h2>
            <p>壮瞥町では、町民や町内に空き家あるいは空き地を所有している方、不動産業者等からの情報をもとに、町内の空き家、空き地情報を提供しています。</p>
             <a href="" class="c-link">「空き家バンク」を見る</a>
             
             <section>
             	<h3 class="c-heading-3">お問い合わせ先が不動産業者名の物件について</h3>
                <p>掲載している空き家、空き地については、既に売却されていることがあります。直接不動産業者にご確認ください。</p>             
             </section>

             <section>
             	<h3 class="c-heading-3">お問い合わせ先が「役場企画調整課」の物件について</h3>
                <img src="images/jutaku/images01.jpg" width="" height="" alt="">
                <ul class=" c-list__num p-list__mb10">
                	<li>所有者等が役場に空き家・土地の情報を提供。役場へ登録申込書を提出。</li>
                    <li>担当職員が申込書の内容を確認。</li>
                    <li>不備がなければ町ホームページにて情報公開。</li>
                    <li>町ホームページを見て希望者より連絡を受ける。役場へ登録申込書を提出。</li>
                    <li>担当職員が登録申込書の内容を確認。</li>
                    <li>不備がなければ、所有者等に希望者情報を連絡し、その後所有者の連絡先を希望者に伝えるか、所有者より連絡が入る。</li>
                    <li>交渉・契約は当事者間で行う。<br>
                    <span class="c-annotation__no">※交渉や契約については、当事者間で行ってください。
                    町は、空き家や空き地などの紹介に必要な連絡調整は行いますが、売買または賃貸などに関する仲介はいたしません。
                    また、物件に関するトラブルなどについても、当事者間で解決していただきます。</span></li>
                </ul>         
             </section>

             <section>
             	<h3 class="c-heading-3">お持ちの物件の登録方法について</h3>
                <ul class=" c-list">
                	<li>空き家・土地の情報、登録申込書を役場に提出。</li>
                    <li>町ホームページにて情報公開。</li>
                    <li>希望者から連絡があった際に希望者の情報をお伝えし、所有者の連絡先を希望者に伝えるかどうか確認します。</li>
                    <li>交渉・契約は当事者間で行っていただきます。<br>
                    <span class="c-annotation__no">※町は、空き家や空き地などの紹介に必要な連絡調整は行いますが、売買または賃貸などに関する仲介はいたしません。<br>
                    また、物件に関するトラブルなどについても、当事者間で解決していただきますので予めご了承ください。</span></li>
                </ul>         
             </section>

             <section>
             	<h3 class="c-heading-3">制度内容</h3>
                <p>制度についての詳しい情報はこちらをご覧ください：<a href="" class="c-link__pdf">「不動産情報実施要領」DL/PDF</a></p>
             </section>             

             <section>
             	<h3 class="c-heading-3">登録申込書の種類について</h3>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong u-w30">売りたい、貸したい方の様式</th>
                      <td>不動産情報提供事業登録申込書（売主・貸主）</td>
                    </tr>
                    <tr>
                      <th class="__strong">空き家を買いたい、<br>借りたい方の様式</th>
                      <td>不動産情報提供事業登録申込書（買主・借主）</td>
                    </tr>
                    <tr>
                      <th class="__strong">物件を掲載したい<br>不動産業者様の様式</th>
                      <td>不動産情報登録申請書（事業者）</td>
                    </tr>
                  </table>
                  <p>上記の申込書様式のダウンロードは「様式ダウンロード」ページからダウンロードしてください。</p>
                  <a href="" class="c-link">「様式ダウンロード」ページはこちら</a>
             </section>              
             <p><span class=" u-bold">【お問い合わせ先】</span><br>壮瞥町役場企画調整課　TEL <a href="tel:0142-66-2121">0142-66-2121</a></p>  
        </div>
      </div>
      
      


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
