<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">し尿処理・浄化槽</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">し尿処理・浄化槽</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■し尿のくみ取り</h2>
			<table class="c-table-1">
            	<tr>
                	<th class="__strong u-w30">申込先</th>
                    <td>壮瞥町役場経済環境課環境衛生係<a href="tel:0142-66-2121">（0142-66-2121）</a></td>
				</tr>
            	<tr>
                	<th class="__strong">収集料金</th>
                    <td>100Ｌ迄840円、100Ｌ以上は50Ｌ毎に420円加算</td>
				</tr>
			</table>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■浄化槽を設置したいとき</h2>
			<table class="c-table-1">
            	<tr>
                	<th class="__strong u-w30">申込先</th>
                    <td>役場建設課までお問い合わせください</td>
				</tr>
            	<tr>
                	<th class="__strong">対象区域</th>
                    <td>下水道対象区域（滝之町、久保内2･3･4･5、南久保内の一部、仲洞爺区域）を除く町内全域</td>
				</tr>
			</table>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■浄化槽設置整備事業補助金</h2>
            <p>合併処理浄化槽は、し尿だけでなく、台所、風呂、洗濯などの生活雑排水を併せて処理し放流しますので、水質の保全に寄与します。<br>
            またトイレの水洗化により、害虫の発生の減少や悪臭の軽減など、生活環境の向上に役立ちます。
            本事業は、合併処理浄化槽の設置に要する費用の一部を補助することにより、生活排水による公共用水域の水質汚濁を防止することを目的としています。</p>
			<table class="c-table-1 c-td__left">
            	<tr>
                	<th class="__strong u-w30">制度</th>
                    <td>壮瞥町浄化槽整備事業補助金について <a href="" class="c-link__pdf">DL/PDF</a></td>
				</tr>
            	<tr>
                	<th class="__strong">提出書類</th>
                    <td>・補助金交付申請書<br>・浄化槽工事チェックリスト<br>上記のPDFは「様式ダウンロード」ページからダウンロードしてください。<br>
                    <a href="" class="c-link">「様式ダウンロード」はこちら</a></td>
				</tr>
			</table>
        </div>
      </div>


      
      


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
