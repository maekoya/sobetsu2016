<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">ペット・害虫駆除</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">ペット・害虫駆除</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■犬の登録と狂犬病予防注射</h2>
            <p>生後91日以上の飼い犬は、登録と年1回の狂犬病予防注射が義務付けられています。
            すでに登録されている飼い主の方には、年1回狂犬病予防注射の定期集合注射の通知を送付しています。
            新たに犬を飼う場合や、集合注射を受けられなかった場合は、最寄りの動物病院で受けてください。 <br>
            <span class="c-annotation__no">※新たに犬を飼った場合、飼い犬が死亡した場合、引越しした場合(転入・転出)なども、手続きが必要です。</span></p>
			<table class="c-table-1">
            	<tr>
                	<th class="__strong u-w30">犬の登録手数料</th>
                    <td>1件3,000円（生涯に1回）</td>
				</tr>
            	<tr>
                	<th class="__strong">狂犬病予防注射料</th>
                    <td>1回3,110円</td>
				</tr>
			</table>
            
          <p class="u-mb__small"><span class=" u-bold">平成27年度の狂犬病予防注射実施日程（平成28年土のスケジュール作成中）</span></p>
          <table class="c-table-1 u-mt__0">
            <tr>
              <th class="__weak u-w20" rowspan="17">5月19日(火)</th>
              <th class="__strong">時間</th>
              <th class="__strong">場所</th>
            </tr>
            <tr>
              <td>9:00～9:20</td>
              <td>仲洞爺公民館</td>
            </tr>
            <tr>
              <td>9:35～9:45</td>
              <td>東湖畔１集会所</td>
            </tr>
            <tr>
              <td>10:00～10:10</td>
              <td>東湖畔２集会所</td>
            </tr>
            <tr>
              <td>10:40～10:50</td>
              <td>壮瞥温泉（旧八景苑跡地前）</td>
            </tr>
            <tr>
              <td>11:05～11:25</td>
              <td>壮瞥温泉（サウスポー横空き地）</td>
            </tr>
            <tr>
              <td>11:40～11:50</td>
              <td>壮瞥温泉（戸田観光前）</td>
            </tr>
            <tr>
              <td>13:00～13:10</td>
              <td>昭和新山熊牧場前</td>
            </tr>
            <tr>
              <td>13:30～13:50</td>
              <td>建部団地浴場前</td>
            </tr>
            <tr>
              <td>14:05～14:15</td>
              <td>とうや湖農協壮瞥支所前</td>
            </tr>
            <tr>
              <td>14:30～14:40</td>
              <td>古川（雅樹）果樹園前</td>
            </tr>
            <tr>
              <td>14:50～15:00</td>
              <td>滝4（矢野果樹園駐車場）</td>
            </tr>
            <tr>
              <td>15:15～15:25</td>
              <td>立香2集会所</td>
            </tr>
            <tr>
              <td>15:40～15:50</td>
              <td>立香ふれあいセンター</td>
            </tr>
            <tr>
              <td>16:05～16:10</td>
              <td>立香（高畑武様宅前）</td>
            </tr>
            <tr>
              <td>16:20～16:40</td>
              <td>紫明苑広場</td>
            </tr>
            <tr>
              <td>16:50～17:00</td>
              <td>橋口2集会所</td>
            </tr>
            <tr>
              <th class="__weak" rowspan="10">5月20日(水)</th>
              <td>9:00～9:10</td>
              <td>久保内1集会所</td>
            </tr>
            <tr>
              <td>9:25～9:45</td>
              <td>森下興産前</td>
            </tr>
            <tr>
              <td>10:00～10:10</td>
              <td>弁景2集会所</td>
            </tr>
            <tr>
              <td>10:25～10:45</td>
              <td>幸内1集会所</td>
            </tr>
            <tr>
              <td>11:00～11:10</td>
              <td>幸内2集会所</td>
            </tr>
            <tr>
              <td>11:25～11:35</td>
              <td>蟠渓（旧学校前）</td>
            </tr>
            <tr>
              <td>11:50～12:00</td>
              <td>蟠渓（ひかり温泉前）</td>
            </tr>
            <tr>
              <td>13:30～13:40</td>
              <td>上久保内集会所</td>
            </tr>
            <tr>
              <td>14:00～14:30</td>
              <td>久保内出張所</td>
            </tr>
            <tr>
              <td>15:00～16:00</td>
              <td>旧壮瞥町役場前</td>
            </tr>
          </table>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■害虫駆除</h2>
            <p>毒蛾の幼虫（毛虫）や蜂の巣の駆除は、下記の業者に委託していますので直接、ご連絡下さい。<br>
            なお、個人の庭先などにいる毒蛾や毒蛾以外の毛虫の駆除は対象となりません。<br>また、土・日曜日。祝日は受付できませんのでご了承ください。</p>
            <p><span class=" u-bold">委託業者</span><br>（株）出田建設　Tel <a href="tel:0142-66-6011">0142-66-6011</a></p>
        </div>
      </div>



      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
