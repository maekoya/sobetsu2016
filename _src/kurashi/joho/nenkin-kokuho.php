<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">年金・国民健康保険・後期高齢者医療制度</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">年金・国民健康保険・後期高齢者医療制度</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■国民年金</h2>
            <p>国民年金は、外国人を含む日本に住む20歳以上60歳未満のすべての方が加入しなければならない「公的年金」制度です。
            国民年金は、会社員や公務員の方が加入する厚生年金を含む共通の「基礎年金」で、実際の年金給付は、国民年金を土台部分として厚生年金分を上乗せする2階建ての仕組みになっています。詳しくはこちらでご確認ください。<br>
            <a href="http://www.nenkin.go.jp/" class="c-link__blank">日本年金機構</a><br>
            <a hre="http://www.hokkaido-kikin.or.jp/" class="c-link__blank">北海道国民年金基金</a>
            </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■後期高齢者医療制度</h2>
            <p>後期高齢者医療に関してはこちらをご覧ください。<br>
            <a href="http://iryokouiki-hokkaido.jp/" class="c-link__blank">北海道後期高齢者医療広域連合</a></p>
            <p>後期高齢者医療制度に関するご不明な点、ご相談は<br>住民福祉課住民係まで<a href="tel:0142-66-2121">TEL　0142-66-2121</a></p>
        </div>
      </div>
      
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■国民健康保険</h2>
            <p>国民健康保険（国保）制度は、病気やけがをしたときに、安心して医療を受けることができるようにお互いが助け合って医療費を負担しあう最も身近な医療保険制度です。
            国保制度は、職場の健康保険（社会保険や共済組合等）や後期高齢者医療制度に加入している人、生活保護を受けている人を除くすべての人が加入する制度で、
            市町村等が運営しています。</p>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 一部負担金の割合</h3>
                    <p>生まれた日から14日以内に手続きしてください。</p>
                      <table class="c-table-1">
                        <tr>
                          <th class="__strong">&nbsp;</th>
                          <th class="__strong">一部負担金の割合（自己負担金）</th>
                          <th class="__strong">国保負担割合</th>
                        </tr>
                        <tr>
                          <td class="__weak">義務教育就学前の乳幼児</td>
                          <td>2割<span class="c-annotation__no">（※1）</span></td>
                          <td>8割</td>
                        </tr>
                        <tr>
                          <td class="__weak">義務教育就学以上70歳未満の方</td>
                          <td>3割<span class="c-annotation__no">（※1）</span></td>
                          <td>7割</td>
                        </tr>
                        <tr>
                          <td class="__weak">70歳以上75歳未満の方</td>
                          <td>2割<span class="c-annotation__no">（※2）</span></td>
                          <td>8割</td>
                        </tr>
                        <tr>
                          <td class="__weak">70歳以上75歳未満で現役並み所得の方</td>
                          <td>3割</td>
                          <td>7割</td>
                        </tr>
                      </table>
                      <p class="c-annotation__no">※1　壮瞥町では、中学生までの入院・通院に係る保険適用の医療費の全額助成を行っているため、実質的には負担がありません。<br>
                      ※2　平成26年4月1日以降に70歳に達する方（誕生日が昭和19年4月2日以降の方）は2割負担。<br>ただし、平成26年3月31日以前に70歳に達した方（誕生日が昭和19年4月1日までの方）は1割負担。</p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 主な給付制度</h3>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">高額療養費</th>
                            <td>月の１日から末日までの１か月間で医療費の一部負担金が高額になり、<br>
                            さらに一定条件を満たした世帯に自己負担限度額を超えた部分があとから支給される制度です。</td>
                        </tr>
                        <tr>
                            <th class="__strong">高額介護合算療養費</th>
                            <td>医療保険と介護保険の自己負担の合算額が著しく高額になる場合に負担が軽減される制度です。</td>
                        </tr>
                        <tr>
                            <th class="__strong">療養費</th>
                            <td>医師が必要と認めたコルセットなどの治療用装具やはり・きゅう・マッサージなどの施術費用の一部が支給される制度です。</td>
                        </tr>
                        <tr>
                            <th class="__strong">出産育児一時金</th>
                            <td rowspan="2">出産した国保の加入者や、国保加入者が死亡した時に葬祭を行った方に支払われます。</td>
                        </tr>
                        <tr>
                            <th class="__strong">葬祭費</th>
                        </tr>
                   </table>
                   <p class="c-annotation__no">※上記制度の詳細・手続き等については、住民福祉課国保係までお問い合わせください。</p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(3) 加入・喪失等の手続き</h3>
                    <p>職場の健康保険（社会保険等）から国保に加入する場合や、国保から社会保険等に加入した場合は、できるだけ早く（14日以内に）届出をしてください。
                    社会保険等から国保に加入する場合、届出が遅れた場合でも、国民健康保険税（国保税）は社会保険等の資格喪失日までさかのぼって（最長3年）
                    国保税を納めていただくことになりますので、ご注意ください。また、社会保険等への加入後に国保の保険証を使用し、医療機関にかかると、
                    国保で負担した費用を全額返還していただくことになります。みなさまの社会保険料の二重払い等を防ぐためにも、
                    壮瞥町の国保の適正な運営のためにも必ず手続きをしてください。</p>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w15">&nbsp;</th>
                          <th class="__strong">こんなとき</th>
                          <th class="__strong">届出に必要なもの</th>
                        </tr>
                        <tr>
                          <th class="__weak" rowspan="5">加入するとき</th>
                          <td>他の市区町村から転入してきたとき</td>
                          <td>他の市区町村の転出証明書・印鑑</td>
                        </tr>
                        <tr>
                          <td>職場の健康保険をやめたとき</td>
                          <td>職場の健康保険をやめた証明書・印鑑</td>
                        </tr>
                        <tr>
                          <td>職場の健康保険の被扶養者からはずれたとき</td>
                          <td>被扶養者になれない理由の証明書（保険離脱証明書等）・印鑑</td>
                        </tr>
                        <tr>
                          <td>子どもが生まれたとき</td>
                          <td>母子健康手帳・保険証・印鑑</td>
                        </tr>
                        <tr>
                          <td>生活保護を受けなくなったとき</td>
                          <td>保護廃止決定通知書・印鑑</td>
                        </tr>
                        <tr>
                          <th class="__weak" rowspan="5">やめるとき</th>
                          <td>他の市区町村に転出するとき</td>
                          <td>保険証・印鑑</td>
                        </tr>
                        <tr>
                          <td>職場の健康保険に加入したとき</td>
                          <td rowspan="2">国保と職場の健康保険の両方の保険証（後者が未交付の場合は加入証明書）・印鑑</td>
                        </tr>
                        <tr>
                          <td>職場の健康保険の被扶養者になったとき</td>
                        </tr>
                        <tr>
                          <td>国保の被保険者が死亡したとき</td>
                          <td>保険証・死亡証明書・印鑑</td>
                        </tr>
                        <tr>
                          <td>生活保護を受け始めたとき</td>
                          <td>保護開始決定通知書・保険証・印鑑</td>
                        </tr>
                        <tr>
                          <th class="__weak" rowspan="5">その他</th>
                          <td>壮瞥町内で住所が変わったとき</td>
                          <td rowspan="3">保険証・印鑑</td>
                        </tr>
                        <tr>
                          <td>世帯主や氏名が変わったとき</td>
                        </tr>
                        <tr>
                          <td>世帯が分かれたり、一緒になったとき</td>
                        </tr>
                        <tr>
                          <td>修学のため、別の住所を定めるとき</td>
                          <td>在学証明書・保険証・印鑑</td>
                        </tr>
                        <tr>
                          <td>保険証をなくしたときや、汚れて使えなくなったとき</td>
                          <td>身分を証明するもの（使えなくなった保険証など）･印鑑</td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(4) 交通事故などの被害者になってしまったら・・・</h3>
                    <p>交通事故に限らず、第三者から傷害を受けた場合、医療費は加害者が負担するのが原則です。
                    また、業務上または通勤途上の傷害は労災保険が適用となります。
                    したがって国保で治療を受けたときの医療費は後日、国保が被害者に代わって加害者に請求することとなります。
                    <ul class="c-list__num">
                    	<li>交通事故にあった場合はすみやかに警察に届け出てください。</li>
                    	<li>必ず住民福祉課国保係に連絡し、被害届を提出してください。</li>
                    </ul>
                    【必要なもの】印鑑・保険証・交通事故証明書など<br>
                    <span class="c-annotation__no">※ 加害者から治療費を受け取り、示談が成立してしまうと国保が立て替えた医療費を返還していただくことがあります。</span></p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(5) 国保税</h3>
                    <p>壮瞥町の国民健康保険税については「税金」のページをご覧ください。</p>
                    <p><a href="" class="c-link">「税金」ページを見る</a></p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(6) 特定健康診査等実施計画</h3>
                    <p>町は、生活習慣病を中心とした疾病を予防するため、「壮瞥町第2期特定健康診査等実施計画」を策定しました。<br>
                    本計画は、高齢者の医療の確保に関する法律第19条第1項の規定に基づき、40歳から74歳までの国民健康保険加入者に対して、<br>
                    平成25年度～29年度における特定健康診査及び特定保健指導の実施に関する具体的な内容を定めたものです。<br>
                    特定健康診査等実施計画について、詳しくは「政策・計画／特定健康診査等実施計画」をご覧ください。</p>
                    <p><a href="" class="c-link">「政策・計画」を見る</a></p>
                </li>
            </ol>
        </div>
      </div>



      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
