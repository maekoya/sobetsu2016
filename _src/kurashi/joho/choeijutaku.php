<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">町営住宅</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">町営住宅</h1>

      <ul>
      	<li><a href="">現在入居者を募集している住宅</a></li>
        <li><a href="">壮瞥町の町営住宅のご紹介</a></li>
        <li><a href="">入居資格・申込方法</a></li>
        <li><a href="">申込から入居までの流れ</a></li>
      </ul>
      
      
      

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■現在入居者を募集している住宅</h2>
            <p><a href="" class=" c-link">「空き家バンク」を見る</a></p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■壮瞥町の町営住宅のご紹介</h2>
            <p>壮瞥町の各町営住宅は下記のご説明をご覧ください。</p>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1)	町単独住宅 ママと考えた子育て応援住宅「コティ」</h3>
                    <section>
                    	<h4 class="c-heading-4">ママと考えた子育て応援住宅とは？</h4>
                        <p>壮瞥町に居住しているお母さん達の意見等を参考にすることで、子育て世帯の方が住みやすく、より子育てに専念できるように建設した住宅です。
                        「コティ」という言葉は、壮瞥町の友好都市であるフィンランドの言葉で、「家」を意味しています。</p>                    
                    </section>
                    <section>
                    	<h4 class="c-heading-4">住宅の特色</h4>
                        <p>居住空間が1・2階で構成されるメゾネットタイプを採用していることや、子供の成長に合わせて移動できる間仕切り収納棚・サンルーム・対面キッチンなどを
                        完備しています。また、家賃は月額41,000円で子供の人数に応じて減額になります。</p>                    
                    </section>
                    <section>
                    	<h4 class="c-heading-4">入居資格</h4>
                        <ol class="c-list__num">
                        	<li>中学生以下の同居親族が１人以上いる世帯であること。</li>
                            <li>収入（給与所得控除後の金額÷12）が154,000円以上であること。</li>
                            <li>市町村民税等の公共料金を滞納していないこと。</li>
                            <li>入居者及び同居者に暴力団員がいないこと。</li>
                        </ol>
                    </section>
                    <section>
                    	<h4 class="c-heading-4">入居期間</h4>
                        <p>子育て応援住宅“コティ”には入居期間が設定されています。<br>
                        <span class="c-annotation__no">※入居期間が終了するまでに退去していただきます。</span><br>
                        入居期間は、同居親族の誕生日の到来により18歳未満の同居親族がいなくなる日に属する年度の末日までです。<br>
                        <span class="c-annotation__no">※通常の年齢で、高校を卒業する3月末日まで。詳しくはお問い合わせ下さい。</span>
                        </p>                    
                    </section>
                    <section>
                    	<h4 class="c-heading-4">その他</h4>
                        <p>・応募者多数の場合は、書類選考その他公正な方法により入居者を決定します。<br>
                        移住定住促進のために建設された住宅ですので、ご理解の程よろしくお願い致します。</p>
                        <p>・観賞用魚類以外の動物の飼育は禁止します。</p>
                        <p>【写真・間取図】</p>                        
                    </section>
                </li>

            	<li>
                	<h3 class="c-heading-3">(2)	公営住宅・改良住宅</h3>
                    <section>
                    	<h4 class="c-heading-4">公営住宅・改良住宅とは？</h4>
                        <p>公営住宅・改良住宅とは、公営住宅法・住宅地区改良法に基づき建設し、住宅に困窮していて所得の低い方に低額な家賃で賃貸する住宅のことです。毎年収入申告をしてもらい、所得や世帯状況に応じて家賃が変わります。現在、壮瞥町には11団地218戸の公営住宅と1団地64戸の改良住宅があります。</p>                    
                    </section>
                    <section>
                    	<h4 class="c-heading-4">壮瞥町の公営住宅・改良住宅名</h4>
                        <p>壮瞥温泉団地・建部公営住宅・建部B団地・星野団地・しらかば団地・ほくと団地・久保内団地・南久保内団地・第２南久保内団地・仲洞爺団地
                        ・蟠渓団地・建部改良住宅</p>
                        <p>【写真】</p>
                    </section>
                </li>
                
            	<li>
                	<h3 class="c-heading-3">(3)	特定公共賃貸住宅・地域優良賃貸住宅</h3>
                    <section>
                    	<h4 class="c-heading-4">特定公共賃貸住宅・地域優良賃貸住宅とは？</h4>
                        <p>特定公共賃貸住宅・地域優良賃貸住宅とは、特定優良賃貸住宅の供給の促進に関する法律等に基づき建設し、
                        公営住宅に入居できない中堅所得者層に賃貸する住宅のことです。毎年、減額申請をしてもらうことで、家賃の減額が可能です（※所得や世帯状況による）。
                        現在、壮瞥町には4団地54戸の特公賃住宅と1団地8戸の地優賃住宅があります。</p>                    
                    </section>
                    <section>
                    	<h4 class="c-heading-4">壮瞥町の特定公共賃貸住宅・地域優良賃貸住宅名</h4>
                        <p>壮瞥温泉団地（単身者向け）、星野団地（単身者用）、しらかば団地（世帯用・単身者用）、ほくと団地（世帯用）、ふれあい団地（世帯用）</p>
                        <p>【写真】</p>
                        <p class=" u-circle__blue">住宅一覧表はこちらをご覧ください：<a href="" class="c-link__pdf">「住宅一覧表H28」DL/PDF</a></p>
                        <p class=" u-circle__blue">町営住宅位置図</p>
                    </section>
                    <section>
                    	<h4 class="c-heading-4">入居資格・申込方法</h4>
                        <dl class="">
                        	<dt>(1) 入居資格</dt>
                            <dd>
                              <table class="c-table-1 c-td__left">
                                <tr>
                                  <th class="__strong u-w20">全共通</th>
                                  <td>
                                  	<ul>
                                    	<li>・市町村民税等の滞納していない者</li>
                                        <li>・所得要件を満たす者</li>
                                        <li>・入居者及び同居者に暴力団員がいない者</li>
                                        <li>・町長が定める基準に該当する者</li>
                                        <li>・壮瞥町に住所がある方、もしくは入居後必ず壮瞥町の住民になることが見込める方。</li>
                                        <li>・観賞用魚類以外の動物の飼育は禁止します。</li>                                    
                                    </ul>
                                  </td>
                                </tr>
                                <tr>
                                  <th class="__strong">町単独住宅<br>（子育て応援住宅<br>「コティ」）</th>
                                  <td>
                                  	<ul>
                                    	<li>・中学生以下の同居親族が１人以上いる世帯であること。</li>
                                        <li>・収入（給与所得控除後の金額÷12）が154,000円以上であること。</li>
                                        <li>・入居期間は同居親族の誕生日の到来により18歳未満の同居親族がいなくなる日に属する年度の末日までで、
                                        入居期間が終了するまでに退去していただきます（通常の年齢で高校を卒業する3月末日まで。詳しくはお問い合わせください）</li>
                                    </ul>
                                  </td>
                                </tr>
                                <tr>
                                  <th class="__strong">公営住宅改良住宅</th>
                                  <td>
                                  	<ul>
                                    	<li>・現に住宅に困窮していることが明らかな者であること。</li>
                                        <li>・算定する１ヶ月当たりの政令月収が15万8千円以下であること。<br>
                                        <span class="c-annotation__no">※政令月収が15万8千円を超える方は入居申込みができません。<br>
                                        （小学生以下のお子さんのいる世帯や新婚世帯などは所得要件が緩和されますので詳しくはお問い合わせ下さい。）</span></li>
                                        <li>・<a href="" class="c-link__pdf">「政令月収表」DL/PDF</a></li>
                                        <li>・<a href="" class="c-link__">「政令月収自動計算表」DL/XLS</a></li>
                                        <li>・<a href="" class="c-link__pdf">「裁量階層説明」DL/PDF</a></li>
                                    </ul>
                                  </td>
                                </tr>
                                <tr>
                                  <th class="__strong">特公賃住宅<br>地優賃住宅</th>
                                  <td>
                                  	<ul>
                                    	<li>・政令月収が15万8千円以上の中堅所得者の方（15万8千円未満の場合は、所得の上昇が見込まれる方）</li>
                                        <li>・単身者向け住宅は、同居者のいない方（年齢制限なし）</li>
                                        <li>・単身者用住宅は、33歳未満で同居者のいない方</li>
                                        <li>・単身者用住宅の入居期間は、35歳に達する日の前日まで。
                                        （ただし、協議により、35歳を超え40歳に達する日の前日までを入居期間とする更新をすることができます。）</li>
                                    </ul>
                                  </td>
                                </tr>                                
                              </table>
                              <p>※それぞれ入居資格等が異なりますので入居申し込みの際はご注意下さい。</p>
                            </dd>
                        </dl>
                        <dl>
                        	<dt>(2) 申込方法</dt>
                            <dd>
                            	<ul>
                                	<li>・入居申請書（役場建設課住宅係にあります。）</li>
                                    <li>・世帯構成を証明する書類（住民票等）</li>
                                    <li>・所得を証明する書類等（源泉徴収票の写し等）</li>
                                    <li>・市町村民税等の滞納がないことを証する書類（完納証明や納税証明等）</li>                                
                                </ul>
                                <p>以上を揃えてお申込み下さい。また、その他に必要である場合は書類を求めることがあります。</p>
                            </dd>
                        </dl>
                    </section>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■申込から入居までの流れ</h2>
            <ol class=" p-list__mb10">
            	<li>(1) 毎月1日頃（月初めが休日の場合は、週明けから）より町営住宅入居者募集開始します。<br>
                <a href="" class="c-link">「空き家バンク」を見る</a>                
                </li>
                <li>(2) 申込締切。（入居申込期間は15日間ほど）</li>
                <li>(3)	申込締切から1～3日ほどで入居者決定します。入居決定者に入居書類を送付します。</li>
                <li>(4) 入居の手続き
                	<p class=" u-circle__blue">入居書類に記入していただき、各証明書類を添付して提出していただきます。</p>
                    <dl>
                    	<dt>○入居請書（連帯保証人2名）</dt>
                    	<dd>・連帯保証人の印鑑証明書<br>
                    ・連帯保証人の所得を証明する書類（源泉徴収票の写し又は所得証明書）<br>
                    ※連帯保証人になれる方<br>
                    ・日本国内に住民票を有する2親等内の親族（親、兄弟、祖父母、子、孫）<br>
                     ・上記親族がいない場合は、近隣市町（室蘭市・登別市・伊達市・洞爺湖町・豊浦町）に居住する者</dd>
                     	<dt class="u-mt__20">○同意書</dt>
                        <dd>団地内及び建物ごとの共益費（共用部の電気料、団地内草刈・除雪費など）を納めること。入居したときには、氏名及び家族数等を自治会長へ情報提供すること。</dd>
                        <dt class="u-mt__20">○水道開始届け</dt>
                        <dt class="u-mt__20">○その他必要な書類</dt>
                    </dl>
                    <p class=" u-circle__blue">敷金納入を納入していただきます。敷金の額は、各住宅により異なります。</p>                
                </li>
                <li>(5) 入居の手続きが完了すれば入居許可がでます。
                	<p>引っ越しをしていただきます。<br>
                    ・転入の際にはいろいろな手続きが必要になります（転入届など）。代表的な手続きをこちらでまとめています<br>
                    <a href="" class="c-link">「引っ越し手続き」を見る</a><br>
                    ・電気の申込・ガスの申込などお忘れのないようご注意ください。</p>
                </li>
            </ol>
            <p>【問い合わせ先】<br>壮瞥町役場建設課住宅係まで TEL: <a href="tel:0142-66-2121">0142-66-2121</a></p>
        </div>
      </div>
      
      


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
