<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">住民登録・戸籍・印鑑登録・パスポート</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">住民登録・戸籍・印鑑登録・パスポート</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■各種証明手数料</h2>
            <p>各種証明書等の交付手数料は以下のとおりです。</p>
            <table class="c-table-1">
            	<tr>
                	<th class="__strong">住民票（除票）、不在住証明</th>
                    <td>1通 200円</td>
                </tr>
            	<tr>
                	<th class="__strong">戸籍謄本（全部事項証明）・戸籍抄本（個人事項証明）</th>
                    <td>1通 450円</td>
                </tr>
            	<tr>
                	<th class="__strong">除籍謄本・除籍抄本・改製原戸籍謄本・改製原戸籍抄本</th>
                    <td>1通 750円</td>
                </tr>
            	<tr>
                	<th class="__strong">戸籍の附票・除籍の附票・身分証明書・独身証明書</th>
                    <td>1通 200円</td>
                </tr>
            	<tr>
                	<th class="__strong">印鑑登録</th>
                    <td>1通 200円</td>
                </tr>
            	<tr>
                	<th class="__strong">印鑑登録証明</th>
                    <td>1通 200円</td>
                </tr>
           </table>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■住民登録</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 転入届（壮瞥町に引越ししてきたとき）</h3>
                    <p>転入した日から14日以内に手続きしてください。</p>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">必要なもの</th>
                            <td>・前住所地の市区町村役場が発行した転出証明書<br>
                            ・印鑑<br>
                            ・国民年金手帳（加入している方のみ）<br>
                            ・身分確認資料（運転免許証など）<br>
                            ・住民基本台帳カード（持っている方のみ。暗証番号が必要です。）<br>
                            ・個人番号通知カード<br>
                            ・個人番号カード（持っている方のみ。暗証番号が必要です。）
                            </td>
                        </tr>
                        <tr>
                            <th class="__strong">その他</th>
                            <td>小中学校の児童・生徒がいる方は、在学証明書をご持参ください。<br>（転校の手続きは、教育委員会学校教育係まで）</td>
                        </tr>
                   </table>                    
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 転出届（壮瞥町から引越しするとき、海外に転居するとき）</h3>
                    <p>転出する日までに手続きしてください。</p>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">必要なもの</th>
                            <td>・印鑑<br>
                            ・身分確認資料（運転免許証など）<br>
                            ・住民基本台帳カード（持っている方のみ。）</td>
                        </tr>
                        <tr>
                            <th class="__strong">その他</th>
                            <td>・壮瞥町の国民健康保険に加入している方、後期高齢者医療に加入されている方は保険証をお返しください。<br>
                            ・印鑑登録をされている方は、登録証をお返しください。</td>
                        </tr>
                   </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(3) 転居届（壮瞥町内で引越ししたとき）</h3>
                    <p>引越しした日から14日以内に手続きしてください。</p>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">必要なもの</th>
                            <td>・印鑑<br>
                            ・壮瞥町の国民健康保険証（加入している方のみ）<br>
                            ・身分確認資料（運転免許証など）<br>
                            ・住民基本台帳カード（持っている方のみ。暗証番号が必要です。）<br>
                            ・個人番号通知カード<br>
                            ・個人番号カード（持っている方のみ。暗証番号が必要です。）
                            </td>
                        </tr>
                   </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(4) 世帯変更届（属する世帯または世帯主に変更があったとき）</h3>
                    <p>変更した日から14以内に手続きしてください。</p>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">必要なもの</th>
                            <td>・印鑑<br>
                            ・壮瞥町の国民健康保険証（加入している方のみ）<br>
                            ・身分確認資料（運転免許証など）
                           	</td>
                        </tr>
                   </table>
                </li>
            </ol>
        </div>
      </div>
      
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■戸籍</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 出生届（子どもが生まれたとき）</h3>
                    <p>生まれた日から14日以内に手続きしてください。</p>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">届出する人</th>
                            <td>父または母</td>
                        </tr>
                        <tr>
                            <th class="__strong">届出する場所</th>
                            <td>届出人の住所、本籍地、出生地などのいずれかの市区町村</td>
                        </tr>
                        <tr>
                            <th class="__strong">必要なもの</th>
                            <td>・出生届出書（医師または助産師の記入が必要です）<br>
                            ・印鑑<br>
                            ・母子健康手帳<br>
                            ・お子さんが加入する健康保険証
                            </td>
                        </tr>
                        <tr>
                            <th class="__strong">その他</th>
                            <td>乳幼児医療・児童手当などの手続きもあります。</td>
                        </tr>
                     </table>                    
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 死亡届（お亡くなりになったとき）</h3>
                    <p>死亡の事実を知った日から7日以内に手続きしてください。</p>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">届出する人</th>
                            <td>親族、同居人など</td>
                        </tr>
                        <tr>
                            <th class="__strong">届出する場所</th>
                            <td>死亡地もしくは死亡者の本籍地、住所地などの市区町村</td>
                        </tr>
                        <tr>
                            <th class="__strong">必要なもの</th>
                            <td>・死亡届出書<br>
                            ・印鑑</td>
                        </tr>
                        <tr>
                            <th class="__strong">その他</th>
                            <td>亡くなられた方の壮瞥町の国民健康保険証や後期高齢者医療保険証、<br>介護保険証などの証も回収いたしますので、一緒にご持参ください。</td>
                        </tr>
                   </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(3) 婚姻届（ご結婚されたとき）</h3>
                    <p>届出された日から法律の効力が発生します。</p>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">届出する人</th>
                            <td>夫および妻</td>
                        </tr>
                        <tr>
                            <th class="__strong">届出する場所</th>
                            <td>届出人の本籍地または住所地などの市区町村</td>
                        </tr>
                        <tr>
                            <th class="__strong">必要なもの</th>
                            <td>・婚姻届書<br>
                            ・夫と妻（旧姓）の印鑑<br>
                            ・届出人の身分確認資料（運転免許証など）<br>
                            ・戸籍謄本（本籍地が壮瞥町以外のとき）
                            </td>
                        </tr>
                        <tr>
                            <th class="__strong">その他</th>
                            <td>・未成年者は父母の同意が必要です。<br>
                            ・婚姻により壮瞥町に転入される方は前住所地の転出証明書が必要です。<br>
                            ・町内転居の場合も婚姻届とは別に手続きがあります。</td>
                        </tr>
                   </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(4) 離婚届（離婚されたとき）</h3>
                    <p>届出された日から法律の効力が発生します。裁判離婚の場合は、調停成立・審判確定・判決決定の日から10日以内に手続きしてください。</p>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">届出する人</th>
                            <td>夫および妻（調停・審判・裁判離婚の場合は申立人）</td>
                        </tr>
                        <tr>
                            <th class="__strong">届出する場所</th>
                            <td>夫婦の本籍地あるいは住所地の市区町村</td>
                        </tr>
                        <tr>
                            <th class="__strong">必要なもの</th>
                            <td>・離婚届書（協議離婚の場合は成人の証人2名による署名・押印が必要）<br>
                            ・夫と妻の印鑑<br>
                            ・戸籍謄本（本籍が壮瞥町以外のとき）<br>
                            ・調停離婚の場合、調停調書の謄本<br>
                            ・審判・判決離婚の場合、審判書または判決書の謄本および確定証明書<br>
                            ・届出人の身分確認資料（運転免許証など）
                            </td>
                        </tr>
                   </table>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■戸籍等証明書・住民票の郵送請求</h2>
            <p>遠方にお住まいの方や日中都合が合わず窓口に来られない方は、郵便で請求することができます。</p>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 請求できる方</h3>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">戸籍等証明書</th>
                            <td>
                            	<ul class="">
                                	<li>(1) 戸籍に記載されている本人、又はその配偶者（夫又は妻）、その直系尊属（父母、祖父母等）もしくは直系卑属（子、孫等）</li>
                                    <li>(2) 自己の権利の行使又は義務の履行のために必要な方</li>
                                    <li>(3) 国又は地方公共団体機関に提出する必要がある方</li>
                                    <li>(4) その他戸籍に記載された事項を利用する正当な理由がある方</li>                                
                                </ul>
                                <p class="c-annotation__no">※(1)以外の方が請求される場合は、請求に応じられない場合があります。その場合は、郵送請求される前に電話でお問い合わせください。
                                また、請求者が戸籍に記載されていない場合は、その関係がわかる戸籍のコピーなどの添付をお願いしています。</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="__strong">住民票</th>
                            <td>本人及び同一世帯員もしくは請求する正当な理由（上記の請求することができる方(2)(3)(4)等の理由）がある方以外の代理人が請求されるときは、
                            委任状（本人署名捺印のもの）が必要となります。</td>
                        </tr>
                     </table>                    
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 請求書の様式</h3>
                    <p>「戸籍証明請求書」・「住民票郵送交付申請書」「委任状様式」は「様式ダウンロード」ページからダウンロードしてください。</p>
                    <a href="" class="c-link">「様式ダウンロード」に行く</a>
                    <p class="c-annotation__no">※他の市区町村で使われている様式を代用していただいても結構です。<br>
                    ※内容の確認が必要となることがありますので、日中連絡のとれる電話番号を必ずご記入ください。</p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(3) 本人確認書類の写し</h3>
                    <p>運転免許証等の写真付きの公的証明書の写しを同封してください。本人確認書類となるものについての確認や、本人確認書類をお持ちでない場合はお問い合わせください。</p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(4) 手数料</h3>
                    <p>上記「各種証明手数料」を参考に、必要金額分の定額小為替証書をゆうちょ銀行にてお求めください。<br>なお、証書の表面には何も書かずに同封してください。</p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(5) 返信用封筒・切手</h3>
                    <p>必要分の切手を貼った返信用封筒の宛名に請求者の住所氏名を記入してください。（返送先は請求者の住民登録地となります）<br>
                    証明書等はＡ４サイズとなりますので、極端に小さい封筒はお避けください。通数が多い場合は、多めに準備いただいた切手を封筒に貼らずに送っていただくか（不要分はお返しいたします）、
                    返信用封筒に「不足料金受取人払」と朱書きで明記ください</p>
                    <p>(2)～(5)の書類を封筒に入れ、下記まで送付してください。<br>
                    <span class=" u-bold">【送付先】</span><br>
                    〒052-0101<br>
                    北海道有珠郡壮瞥町字滝之町287番地7<br>
                    壮瞥町役場住民福祉課住民係
                    </p>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■印鑑登録</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 印鑑登録</h3>
                    <p>初めて登録するときは、本人による申請が必要です。
                    本人が都合により直接申請できない場合は、本人が自署した委任状を提出し、代理人が申請することができます（あらかじめお問い合わせください）。<br>
                    <span class="c-annotation__no">※代理人が申請する場合は、郵送で委任状を送付するため登録までに日数がかかります。同一世帯で同じ印鑑を登録することはできません。</span></p>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">必要なもの</th>
                            <td>・登録する印鑑<br>・身分確認資料（運転免許証など）</td>
                        </tr>
                     </table>                    
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 印鑑証明書</h3>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">請求する人</th>
                            <td>本人または代理人（委任状不要）</td>
                        </tr>
                        <tr>
                            <th class="__strong">必要なもの</th>
                            <td>印鑑登録証</td>
                        </tr>
                     </table>                    
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■パスポート（一般旅券）</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 申請書類</h3>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">新しくパスポートをつくるとき<br>期限切れのパスポートがあるとき</th>
                            <td>・一般旅券発給申請書（5年用・10年用）<span class="c-annotation__no">※1</span><br>
                            ・戸籍謄本または戸籍抄本 <span class="c-annotation__no">※2</span><br>
                            ・写真 <span class="c-annotation__no">※3</span><br>
                            ・本人確認書類（運転免許証や健康保険証など。申請のごあんないでご確認ください。）<br>
                            ・印鑑（認印）<br>
                            ・期限切れのパスポート（持っている方）
                            </td>
                        </tr>
                        <tr>
                            <th class="__strong">有効期限１年未満のパスポートがあるとき（氏名・本籍地に変更がないとき）</th>
                            <td>・一般旅券発給申請書（5年用・10年用）<span class="c-annotation__no">※1</span><br>
                            ・写真 <span class="c-annotation__no">※3</span><br>
                            ・パスポート<br>
                            ・印鑑（認印）
                            </td>
                        </tr>
                        <tr>
                            <th class="__strong">氏名・本籍地が変わったとき</th>
                            <td>・一般旅券発給申請書（5年用・10年用）<span class="c-annotation__no">※1</span><br>
                            ・変更履歴が確認できる戸籍謄本または戸籍抄本 <span class="c-annotation__no">※2</span><br>
                            ・写真 <span class="c-annotation__no">※3</span><br>
                            ・パスポート<br>
                            ・印鑑（認印）
                            </td>
                        </tr>
                     </table>
                     <p class="c-annotation__no"><span class=" u-bold">※1　一般旅券発給申請書（5年用・10年用）</span><br>
                     ・役場住民福祉課窓口に用意してあります。<br>
                     <span class=" u-bold">※2　戸籍謄本または戸籍抄本</span><br>
                     ・6ヵ月以内に発行されたもの<br>・戸籍が同一のご家族が同時に申請するときは、戸籍謄本1通で受付可能です。<br>
                     <span class=" u-bold">※3　写真</span><br>
                     ・6ヵ月以内に撮影したもの<br>・正面向き、無帽、無背景、目元・輪郭が隠れていないもの<br>
                     ・縦45ミリ×横35ミリで、頭頂からあごまでが34±2ミリ、顔の中心から写真の左右の端までが17±2ミリ、頭頂から写真の上端までが4±2ミリのもの。<br>
                     ・規格外の写真は受付できません。
                     </p>                    
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 申請・受取手続き</h3>
                    <table class="c-table-1 c-td__left">
                        <tr>
                            <th class="__strong u-w30">申請・受取場所</th>
                            <td>役場住民福祉課窓口</td>
                        </tr>
                        <tr>
                            <th class="__strong">申請・受取時間</th>
                            <td>月曜～金曜日（祝日・年末年始を除く）8:45～17:30</td>
                        </tr>
                        <tr>
                            <th class="__strong">申請できる方</th>
                            <td>・壮瞥町に住民登録をされている方<br>
                            <span class="c-annotation__no">※代理提出は可能ですが、「一般旅券発給申請書」に申請者本人が自署しなければならない内容がありますので、
                            あらかじめ申請書を取り寄せて必要事項を記入したうえ、代理の方が窓口で手続きしてください。<br>
                            ※代理提出の場合にも、申請者の本人確認書類の原本が必要です。また、引受人の方も本人確認書類（原本）と印鑑をお持ちください。</span></td>
                        </tr>
                        <tr>
                            <th class="__strong">受取時期</th>
                            <td>申請から2週間後（休日をはさむときはその日数分を加算）</td>
                        </tr>
                        <tr>
                            <th class="__strong">受取できる方</th>
                            <td>パスポートを発給される本人のみ</td>
                        </tr>
                     </table>
                     <p>・期間に余裕をもって申請の手続をしましょう。<br>
                     ・パスポートは国籍や身分を証明する大切なものです。受け取ったら大切に保管し、取扱いには十分注意してください。</p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(3) 手数料</h3>
                    <p>手数料はパスポート受取の際に必要です。</p>
                      <table class="c-table-1">
                        <tr>
                          <th class="__strong" colspan="2">パスポートの種類</th>
                          <th class="__strong">収入印紙</th>
                          <th class="__strong">北海道収入証紙</th>
                          <th class="__strong">合計金額</th>
                        </tr>
                        <tr>
                          <td class="__weak" colspan="2">10年用</td>
                          <td>14,000円</td>
                          <td>2,000円</td>
                          <td>16,000円</td>
                        </tr>
                        <tr>
                          <td class="__weak" rowspan="2">5年用</td>
                          <td>12歳以上</td>
                          <td>9,000円</td>
                          <td>2,000円</td>
                          <td>11,000円</td>
                        </tr>
                        <tr>
                          <td>12歳未満</td>
                          <td>4,000円</td>
                          <td>2,000円</td>
                          <td>6,000円</td>
                        </tr>
                        <tr>
                          <td class="__weak" colspan="2">記載事項変更</td>
                          <td>4,000円</td>
                          <td>2,000円</td>
                          <td>6,000円</td>
                        </tr>
                      </table>
                      <p><h4 class="c-heading-4">収入印紙・北海道収入証紙の購入場所（壮瞥町内）</h4>
                      <span class=" u-circle__blue">北海道収入証紙</span><br>
                      ・とうや湖農協壮瞥支所金融窓口<br>
                      月曜～金曜日（祝祭日除く）8:45～16:30
                      TEL <a href="0142-66-2111">0142-66-2111</a></p>
                      <p>
                      <span class=" u-circle__blue">収入印紙</span><br>
                      ・とうや湖農協壮瞥支所金融窓口（上記と同様）<br>
                      ・壮瞥郵便局・久保内郵便局<br>
                      月曜～金曜日（祝祭日除く）9:00～17:00<br>
                      壮瞥 TEL <a href="tel:0142-66-236">0142-66-236</a><br>久保内 TEL <a href="0142-65-2200">0142-65-2200</a></p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(4) その他</h3>
                    パスポート申請に関する詳しい内容は、北海道庁・外務省のホームページをご覧ください。<br>
                    <a href="http://www.pref.hokkaido.lg.jp/ss/tsk/passport/passport_0.htm" class="c-link__blank">北海道庁</a><br>
                    <a href="http://www.mofa.go.jp/mofaj/toko/passport/" class="c-link__blank">外務省</a>
               </li>

            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■取扱場所・時間</h2>
              <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong">取扱場所</th>
                  <th class="__strong">取扱時間</th>
                  <th class="__strong">取扱業務</th>
                </tr>
                <tr>
                  <td class="__weak">壮瞥町役場住民福祉課窓口</td>
                  <td>月曜～金曜日（祝日除く）8:45~17:30</td>
                  <td>上記事務全般</td>
                </tr>
                <tr>
                  <td class="__weak">久保内出張所</td>
                  <td>月曜～金曜日（祝日除く）8:45~17:30<br>※平成28年4月1日からは8:45～15:30へ変更になります。</td>
                  <td>・住民票の写しの発行<br>・印鑑証明書の発行<br>・税及び税外収入金の収入</td>
                </tr>
              </table>
              <p>【お問い合わせ】<br>壮瞥町役場住民福祉課 TEL <a href="tel:0142-66-2121">0142-66-2121</a></p>
        </div>
      </div>




      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
