<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">公共交通</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">公共交通</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■コミュニティタクシー</h2>
            <p>コミュニティタクシーは、路線バスとは異なり、概ねの運行時間帯とコースに沿って、ドア・ツー・ドア（利用者の自宅などの出発地から目的地間の輸送を行います。）で
            運行するタクシーです（事前予約制）。通常は予約のあった複数の利用者を乗せるため、乗り合わせでの利用となります。</p>
        </div>
         <div class="u-clearboth">
            <div class="u-grid__col-6">
              <img src="images/kotsukikan/img01.jpg" width="" height="" alt="">
            </div>
            <div class="u-grid__col-6">
			  <img src="images/kotsukikan/img02.jpg" width="" height="" alt="">
            </div>
          </div>
          
         <section class="u-grid__col-12">
         	<h3 class="c-heading-3">(1) 制度の概要</h3>
              <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">利用対象</th>
                  <td>町民全員が対象です（年齢制限はありません）</td>
                </tr>
                <tr>
                  <th class="__strong">乗降場所</th>
                  <td>ご自宅まで迎えに行って目的地までお連れします（お帰りもご自宅まで）</td>
                </tr>
                <tr>
                  <th class="__strong">種別</th>
                  <td><span class=" u-bold">【町内便】</span><br>町内全域を運行します（利用目的は問いません）<br>
                  <span class=" u-bold">【通院便】</span><br>伊達市内・洞爺湖町内の医療機関への通院のみ</td>
                </tr>
                <tr>
                  <th class="__strong">利用料金</th>
                  <td>1乗降につき、町内便は一人100円、通院便は一人500円です。<br>（通院目的の利用に限り、1月あたり上限額が3,000円になります）</td>
                </tr>
                <tr>
                  <th class="__strong">運行曜日</th>
                  <td>平日のみの運行になります</td>
                </tr>
                <tr>
                  <th class="__strong">運行会社</th>
                  <td>町内便 毛利ハイヤー、通院便 道南ハイヤー</td>
                </tr>
              </table>         
         </section>

         <section class="u-grid__col-12">
         	<h3 class="c-heading-3">(2) 運行ダイヤ</h3>
            <p class=" u-circle__blue">運行ダイヤPDF　<a href="" class="c-link__pdf">DL/PDF</a></p>
            <img src="images/kotsukikan/jikoku01.jpg" width="" height="" alt="通院便運行時刻表"><br>
            <img src="images/kotsukikan/jikoku02.jpg" width="" height="" alt="町内便運行時刻表" class="u-mt__small">
         </section>

         <section class="u-grid__col-12">
         	<h3 class="c-heading-3">(3) 利用方法</h3>
              <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w30">事前登録</th>
                  <td>初めてご利用いただく際には事前登録が必要です（電話）。<br>登録先　壮瞥町役場企画調整課 TEL <a href="tel:0142-66-2121">0142-66-2121</a></td>
                </tr>
                <tr>
                  <th class="__strong">電話予約</th>
                  <td>利用の都度、直接、運行幹事会社へ電話して予約してください。<br>予約先　毛利ハイヤー　TEL <a href="tel:0142-66-2366">0142-66-2366</a></td>
                </tr>
                <tr>
                  <th class="__strong">当日の流れ</th>
                  <td>・タクシーがご自宅まで迎えに行きます<br>
                  ・途中で他のお客様も同乗します<br>
                  ・目的地に着いたら利用料をお支払いください
                  </td>
                </tr>
              </table>         
         </section>

         <section class="u-grid__col-12">
         	<h3 class="c-heading-3">(4) あらかじめご了承いただきたい事項</h3>
            <ul class=" c-list">
            	<li>運行は乗合制で、一定の時間帯で決められた方向にのみ運行しているため、すべての利用者のご希望どおりには運行できない場合があります。</li>
                <li>乗車数には上限があるため、予約をお断りする場合もあります。</li>
                <li>予約したにも関わらず、無連絡でキャンセルする方もいらっしゃるようです。<br>
                他のお客様にもご迷惑をおかけしますので、キャンセルの際も必ず事前にお電話をお願いします。</li>
            </ul>
         </section>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■路線バス</h2>
            <p>現在、壮瞥町内には次の通り路線バスが運行しています。</p>
              <table class="c-table-1">
                <tr>
                  <th class="__strong u-w10">①</th>
                  <td>東町（サンパレス前）～洞爺湖温泉～定山渓～札幌駅</td>
                </tr>
                <tr>
                  <th class="__strong">②</th>
                  <td>東町（サンパレス前）～洞爺湖温泉～洞爺駅、豊浦</td>
                </tr>
                <tr>
                  <th class="__strong">③</th>
                  <td>洞爺湖温泉（洞爺水の駅）～壮瞥役場前～伊達市内（緑丘高校）～室蘭市内</td>
                </tr>
                <tr>
                  <th class="__strong">④</th>
                  <td>洞爺湖温泉～壮瞥役場前～洞爺水の駅</td>
                </tr>
                <tr>
                  <th class="__strong">⑤</th>
                  <td>伊達市内～壮瞥役場前～大滝東団地、倶知安町</td>
                </tr>
                <tr>
                  <th class="__strong">⑥</th>
                  <td>洞爺湖温泉～昭和新山　※季節運行</td>
                </tr>
              </table>
             <p>運行時刻、停留所等は道南バスでご確認ください<br>
             <a href="http://donanbus.co.jp/" class="c-link__blank">道南バスサイトはこちら</a></p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■EV（電気自動車）充電器</h2>
            <p>道の駅そうべつ情報館に電気自動車急速充電スタンドが整備されています。<br>
            24時間運用可能ですが、電気自動車充電のご利用には精算に必要な充電カードまたはモバイル登録が必要となります。<br>
            詳しくは商工観光課までお問い合わせください（Tel <a href="tel:0142-66-4200">0142-66-4200</a>）</p>
        </div>
         <div class="u-clearboth">
            <div class="u-grid__col-6">
              <img src="images/kotsukikan/img03.jpg" width="" height="" alt="">
            </div>
            <div class="u-grid__col-6">
			  <img src="images/kotsukikan/img04.jpg" width="" height="" alt="">
            </div>
          </div>
      </div>





      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
