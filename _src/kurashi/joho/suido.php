<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">暮らしの情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">暮らし</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">暮らしの情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">水道・下水道（集落排水）・温泉水</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">水道・下水道（集落排水）・温泉水</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■水道・下水道（集落排水）の利用申し込み</h2>
            <section>
            	<h3 class="c-heading-3">(1) 給水等の工事などは法律や条例に基づき行われなければならない工事ですので、<br>必ず壮瞥町が指定した事業者に申し込んでください。</h3>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong u-w30">指定業者</th>
                      <td><a href="" class="c-link__pdf">「壮瞥町指定給水装置工事事業者及び壮瞥町排水設備工事指定業者一覧」 DL/PDF</a></td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">(2) 水道の給水開始・中止等の各種届け出</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w30">手続きに必要なもの</th>
                      <td>
                      	印鑑、「給水開始届」、「給水中止・休止・廃止届」「給水開始届」、<br>「給水中止・休止・廃止届」は<br>
                        「様式ダウンロード」ページからダウンロードしてください。
                        <p class="u-mb__0"><a href="" class="c-link">「様式ダウンロード」はこちら</a></p>
                      </td>
                    </tr>
                  </table>
           	</section>
            <section>
            	<h3 class="c-heading-3">(3) 下水道（集落排水）の加入申し込み</h3>
                <p>直接、上記の排水設備工事指定業者にお申込みください。</p>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong u-w30">対象区域</th>
                      <td>滝之町、久保内2,3,4,5、南久保内の一部、仲洞爺</td>
                    </tr>
                  </table>
            </section>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■水道・集落排水・浄化槽使用料</h2>
            <p>水道・集落排水・浄化槽使用料は、毎月水道メーターの検針を行い、使用された水道水量に応じて使用料をいただいています。水道メーターの検針は、毎月概ね25日頃に行っており、
            前月の検針日から当月の検針日までの1ヶ月間の使用料を翌月に請求しています。たとえば、4月25日から5月25日の使用料は5月分使用料として6月初めに請求をしています。</p>
            <section>
            	<h3 class="c-heading-3">(1) 水道使用料表（月額/消費税込）</h3>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong">用途</th>
                      <th class="__strong">基本水量</th>
                      <th class="__strong">基本料金</th>
                      <th class="__strong">1&#13221;あたり超過料金</th>
                    </tr>
                    <tr>
                      <td class="__weak">一般家庭用、営業用、官公署その他の団体用</td>
                      <td>10&#13221;</td>
                      <td>1,130円</td>
                      <td>113円</td>
                    </tr>
                    <tr>
                      <td class="__weak">大口用</td>
                      <td>500㎥</td>
                      <td>56,500円</td>
                      <td>72円</td>
                    </tr>
                    <tr>
                      <td class="__weak">臨時用</td>
                      <td>10㎥</td>
                      <td>2,260円</td>
                      <td>113円</td>
                    </tr>
                  </table>
                  <p class="u-mb__0">計算例）一般家庭で25m3使用の場合</p>
                  <p class=" u-box_border u-mt__0">
                  基本料金　・・・　1,130円<br>
                  超過料金　・・・（25-10）&#13221; × 113円 ＝ 1,695円<br>
                  合計金額　・・・　2,825円</p>
            </section>

            <section>
            	<h3 class="c-heading-3">(2) 集落排水・浄化槽使用料表（月額/消費税込）</h3>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong">用途</th>
                      <th class="__strong">基本水量</th>
                      <th class="__strong">基本料金</th>
                      <th class="__strong">1&#13221;あたり超過料金</th>
                    </tr>
                    <tr>
                      <th class="__strong">一般用</th>
                      <td>10&#13221;</td>
                      <td>1,440円</td>
                      <td>144円</td>
                    </tr>
                    <tr>
                      <th class="__strong">大口用</th>
                      <td>100&#13221;</td>
                      <td>14,440円</td>
                      <td>82円</td>
                    </tr>
                  </table>                  
                  <p class="u-mb__0">計算例）一般家庭で25m3使用の場合</p>
                  <p class=" u-box_border u-mt__0">
                  基本料金　・・・　1,440円<br>
                  超過料金　・・・（25-10）&#13221; × 144円 ＝ 2,160円<br>
                  合計金額　・・・　3,600円</p>
           	</section>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■お知らせ表の見方</h2>
             <div class="u-clearboth">
                <div class="u-grid__col-4">
                  <img src="images/suido/img01.jpg" width="" height="" alt="">
                </div>
                <dl class="u-grid__col-8">
                 <dt class=" u-bold">①	お客様の情報</dt>
                 <dd>お客様のお名前やお客様番号が記載されています。お問い合わせの際には、お客様番号をお知らせください。</dd>
                 <dt class=" u-bold u-mt__small">②	使用水量</dt>
                 <dd>今回の指針から前回の指針を差し引いて今回分の使用水量を算定しています。</dd>
                 <dt class=" u-bold u-mt__small">③	水道使用料口座振替請求書</dt>
                 <dd>口座振替のお客様へ、今月ご請求する水道使用料の金額をお知らせしています。</dd>
                 <dt class=" u-bold u-mt__small">④水道使用料口座振替済のお知らせ</dt>
                 <dd>口座振替のお客様へ、先月の振替済み水道使用料の金額と振替日をお知らせしています。</dd>
                 <dt class=" u-bold u-mt__small">⑤集落排水使用料口座振替請求書</dt>
                 <dd>口座振替のお客様へ、今月ご請求する集落排水・浄化槽使用料の金額をお知らせしています。</dd>
                 <dt class=" u-bold u-mt__small">⑥集落排水使用料口座振替済みのお知らせ</dt>
                 <dd>口座振替のお客様へ、先月の振替済み集落排水・浄化槽使用料の金額と振替日をお知らせしています。</dd>
                 <dd class="c-annotation__no">※口座振替をご利用でないお客様へは、別途、納入通知書にてご請求金額などをお知らせしています。</dd>
                </dl>
              </div>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■使用料のお支払いについて</h2>
            <p>水道・集落排水・浄化槽使用料のお支払いは、以下の場所でお支払いください。</p>
            <ul class="c-list">
            	<li>壮瞥町役場内金融機関窓口</li>
                <li>伊達信用金庫本店・各支店</li>
                <li>とうや湖農協本所・各支所</li>
                <li>北海道内の郵便局</li>
            </ul>
            <p>また、お支払いには便利な口座振替をおすすめしています。ご利用の場合は、預金通帳と通帳に使用している印鑑を持参して、下記の金融機関窓口にてお手続きください。</p>
              <table class="c-table-1">
                <tr>
                  <th class="__strong u-w30">口座振替手続場所</th>
                  <td>壮瞥町役場内金融機関窓口・郵便局・伊達信用金庫本店・各支店<br>とうや湖農協本所・各支所</td>
                </tr>
              </table>
             <p class="c-annotation__no">※集落排水・浄化槽を使用されているお客様は、水道料金と合わせて口座から引落としとなります。</p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■水洗便所改造等資金貸付金</h2>
            <section>
            	<h3 class="c-heading-3 u-circle__blue">対象者</h3>
                <dl>
                	<dt class=" u-bold">【集落排水（下水道）事業区域の方】</dt>
                    <dd>集落排水（下水道）事業区域（滝之町、久保内2,3,4,5、南久保内の一部、仲洞爺）にお住まいの住宅所有者または、
                    その所有者の同意を受けた使用者（法人、団体所有の建物は該当しません。ただし、住宅部分については対象とします。）
                    また、継続的にお住まいのならない別荘等は該当しません。</dd>
                </dl>
                <dl>
                	<dt class=" u-bold">【管理型浄化槽事業区域の方】</dt>
                    <dd>集落排水（下水道）事業区域外（滝之町、久保内2、3、4、5、南久保内の一部、仲洞爺以外）にお住まいの住宅の所有者または、
                    その所有者の同意を受けた使用者（法人、団体所有の建物及び別荘は該当しません。ただし、住宅部分については対象とします。）。
                    処理対象人員が10人以下の場合。</dd>
                </dl>
            </section>
            <section>
            	<h3 class="c-heading-3 u-circle__blue">貸付内容</h3>
                <p>集落排水（下水道）加入及び浄化槽設置を促進するため、トイレの水洗化や排水設備工事を行う際の、
                みなさんの経済負担をなるべく少なくする水洗便所改造等資金貸付制度を設けています。制度の内容は、既設のトイレを水洗トイレに改造したり、
                排水設備工事をしようとする方に必要な工事資金を無利子で貸付けるものです。</p>
                <dl>
                	<dt>借入申請書（「集落排水」・「管理型浄化槽」）</dt>
                    <dd>「集落排水」・「管理型浄化槽」は「様式ダウンロード」ページからダウンロードしてください。
                    <p><a href="" class="c-link">「様式ダウンロード」はこちら</a></p></dd>
                </dl>
            </section>
            <section>
            	<h3 class="c-heading-3 u-circle__blue">貸付額</h3>
                <p>1件につき50万円以内です。排水設備のみについては1件につき20万円以内です。</p>
            </section>
            <section>
            	<h3 class="c-heading-3 u-circle__blue">償還方法</h3>
                <p>貸付けした月の翌月から毎月元金均等で60カ月以内、排水設備資金のみの場合は24カ月以内。</p>
            </section>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■その他</h2>
            <ul class=" p-list__mb10">
            	<li>ア）水道の使用開始や休止、中止などは届出をしてください</li>
                <li>イ）水道の使用を開始される際や、長期間の留守などによる水道の使用を休止する際、引越しなどにより水道の使用を中止する際などや、使用の内容が変更となる場合には、必ず事前に届出をしてください。届出がない場合、引越し後などにも引き続き料金をいただくこととなったり、水が使用できなくなることもありますので、ご注意ください。</li>
                <li>ウ）漏水にご注意ください<br>
                使用した覚えがないのに、いつもよりも水道の使用水量が多くなっている。または、徐々に使用水量が増えているといった場合は漏水の可能性があります。
                お心当たりの場合には、町の指定給水装置工事事業者もしくは、建設課上下水道係へご相談ください。</li>
                <li>エ）水質検査結果はこちらをご覧ください　<a href="" class="c-link__pdf">DL/PDF</a></li>
                <li>オ）その他、ご不明な点がありましたら、建設課上下水道係までお問合せください。</li>
            </ul>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■温泉水</h2>
            <p>壮瞥町の蟠渓地区では、豊富な温泉を活用し、事業用や一般家庭用の浴用、暖房用に温泉水を供給しています。</p>
            <section>
            	<h3 class="c-heading-3 u-circle__blue">利用区分</h3>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong u-w30">業務用</th>
                      <td>公衆浴場、旅館、ホテル、保養所、民宿等で主として営業目的で使用するもの</td>
                    </tr>
                    <tr>
                      <th class="__strong">一般用</th>
                      <td>一般住宅、アパート等で、入居世帯の浴用等に供するもの</td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3 u-circle__blue">利用料金</h3>
                <p class=" u-mb__0 u-text__right">単位:円</p>
                  <table class="c-table-1 u-mt__0">
                    <tr>
                      <th class="__strong" rowspan="3" width="10%">&nbsp;</th>
                      <th class="__strong" colspan="6" width="50%">温泉許可量（毎分）</th>
                      <th class="__strong" rowspan="3">備考</th>
                    </tr>
                    <tr>
                      <td colspan="2">20ﾘｯﾄﾙまで</td>
                      <td colspan="2">50ﾘｯﾄﾙまで</td>
                      <td colspan="2">50ﾘｯﾄﾙ超</td>
                    </tr>
                    <tr>
                      <td>浴用</td>
                      <td>その他</td>
                      <td>浴用</td>
                      <td>その他</td>
                      <td>浴用</td>
                      <td>その他</td>
                    </tr>
                    <tr>
                      <th class="__weak">業務用</th>
                      <td>35</td>
                      <td>38</td>
                      <td>31</td>
                      <td>35</td>
                      <td>28</td>
                      <td>31</td>
                      <td>「浴用」使用者で暖房等のため温泉を使う者は<br>11月から3月までの間「その他」の料金を適用する。</td>
                    </tr>
                    <tr>
                      <th class="__weak">一般用</th>
                      <td>42</td>
                      <td>45</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <p>基本料金（毎分1ﾘｯﾄﾙ当り600円を許可量に乗じた額）、温泉メーター貸与料（1ケ月2,000円）が上記使用料のほかに毎月かかります。</p>
            </section>
            <p class=" u-circle__blue">「温泉供給申請書」は「様式ダウンロード」ページからダウンロードしてください。<br>
            <a href="" class="c-link">「様式ダウンロード」はこちら</a></p>
        </div>
      </div>


      
      


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
