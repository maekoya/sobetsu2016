<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">健康・福祉</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">健康・福祉</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">教育行政</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">健康・福祉</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        
        <section>        
          <h2 class="c-heading-2">■社会福祉協議会</h2>
          <p>社会福祉協議会は、誰もが住み慣れた場所でともに支えあい、安心して暮らせるまちづくりを住民と行政と協働で進めています。<br>
          地域のいろいろな方々のご意見をお聞きしながら、皆様のご協力のもと運営しています。</p>
         </section>
         
         <ol>
         	<li>
            	<h3 class="c-heading-3">(1) 社会福祉協議会の組織・業務</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w25">地域福祉係</th>
                      <td>・配食、入浴等の在宅サービスに関すること<br>
                      ・小地域ネットワーク、ふれあい敬老昼食会、あっぷるひろば等の地域福祉サービスに関すること<br>
                      ・共同募金委員会、老人クラブ、身障壮瞥支部、ボランティアセンター等の団体運営に関すること<br>
                      ・経理、広報、会議運営等に関すること</td>
                    </tr>
                    <tr>
                      <th class="__strong">地域包括支援センター<br>(包括的支援事業の実施)</th>
                      <td>・介護予防ケアマネジメント業務<br>
                      ・権利擁護業務<br>
                      ・総合相談支援業務<br>
                      ・包括的、継続的ケアマネジメント支援業務<br>
                      ・地域ケア会議の開催<br>
                      ・在宅医療、介護連携の推進<br>
                      ・認知症施策の推進<br>
                      ・生活支援サービスの体制整備
                      </td>
                    </tr>
                    <tr>
                      <th class="__strong">居宅介護支援事業所<br>ほほえみ</th>
                      <td><span class=" u-bold">介護保険法指定居宅介護保険に準じて</span><br>
                      ・在宅で介護サービスを利用する際に必要なケアプランの作成（要介護1～5）、円滑に利用できるよう調整<br>
                      ・在宅生活で介護が必要になったが、どんなサービスがあるか分からない、利用をどうしようか等の相談や要介護認定の申請代行</td>
                    </tr>
                  </table>
            </li>
         	<li>
            	<h3 class="c-heading-3">(2) 社会福祉協議会の主な事業</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w25">あっぷるひろば</th>
                      <td>定期的に高齢者が気軽に集える時間と場所を提供しています</td>
                    </tr>
                    <tr>
                      <th class="__strong">小地域ネットワーク事業</th>
                      <td>隣近所で互いに関わりあう関係づくりを目的とした自治会活動を支援するための助成制度</td>
                    </tr>
                    <tr>
                      <th class="__strong">ふれあい敬老昼食会</th>
                      <td>閉じこもり防止と同世代の交流を目的とした昼食会</td>
                    </tr>
                    <tr>
                      <th class="__strong">生活一時資金貸付</th>
                      <td>一時的に生活が困窮している世帯に対し、生活費を貸付ます。貸付限度額5万円。<br>町内在住の連帯保証人が必要。</td>
                    </tr>
                    <tr>
                      <th class="__strong">ふれあい交流会</th>
                      <td>高齢者が障がい者で介助ボランティアが同行する日帰りバス旅行</td>
                    </tr>
                    <tr>
                      <th class="__strong">児童生徒福祉啓発事業</th>
                      <td>福祉に対する意識向上を目的とした講演会や体験学習会</td>
                    </tr>
                    <tr>
                      <th class="__strong">日常生活自立支援事業</th>
                      <td>判断力の低下などにより独り暮らしが難しくなった方の金銭や貴重品を管理し、在宅生活を支援</td>
                    </tr>
                  </table>
                  <p>それぞれの事業のスケジュール、詳細は、広報をご覧になるか下記までお問合せください。</p>
                  <p><span class=" u-bold">壮瞥町社会福祉協議会</span><br>
                  壮瞥町字滝之町284-2保健センター内<br>
                  TEL <a href="tel:0142-66-2511">0142-66-2511</a><br>
                  <a hrfe="http://www.662511.net/" class="c-link__blank">http://www.662511.net/</a>
                  </p>
            </li>
         </ol>

        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
            <section>        
              <h2 class="c-heading-2">■特定利用券</h2>
              <h3 class="c-heading-4">特定利用券（平成27年度分）交付のご案内</h3>
              <p class="u-mt__0">下記の交付対象者に該当される方は、町営温泉（ゆーあいの家・久保内ふれあいセンター・蟠渓ふれあいセンター・仲洞爺来夢人の家）を利用できる
              特定利用券（1人30枚）を交付いたしますので、役場住民福祉課にて申請願います。なお、平成 27 年4月1日以前より町内に住まわれている方が交付の対象となります。<br>
              <strong>お持ちいただくもの／印鑑</strong><br>
              <span class="c-annotation">65歳以上の町内在住の方には、誕生日の前月に通知しております特定利用券及び敬老福祉証の対象となりますので、本件の交付対象にはなりません。</span>
              </p>
             </section>
             <table class="c-table-1 c-td__left">
                <tr>
                    <th class="__strong u-w25">内容</th>
                    <td>町営温泉施設のお風呂が大人1回130円、子ども1回70円、幼児1回40円で入浴できます</td>
                 </tr>
                <tr>
                    <th class="__strong">対象者</th>
                    <td>・生活保護法に規定する扶助等を受けている満65歳未満の町内在住の方。<br>
                    ・母子及び寡婦福祉法に規定する母子家庭満65歳未満の町内在住の方。<br>
                    ・障害者基本法に基づく障害者手帳等の交付を受けている町内在住の方。<br>
                    <span class="c-annotation">上記全共通で3歳未満の方は除きます（3歳未満無料）。</span>
                    </td>
                 </tr>
                <tr>
                    <th class="__strong">費用等</th>
                    <td>入浴1回大人130円、子ども70円、幼児40円<br>
                    <span class="c-annotation">特定利用券の発行は無料です</span>
                    </td>
                 </tr>
                <tr>
                    <th class="__strong">申込先</th>
                    <td>役場住民福祉課　TEL <a href="tel:0142-66-2121">0142-66-2121</a><br>（印鑑を持参してください）</td>
                 </tr>
              </table>
        </div>
      </div>




      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
