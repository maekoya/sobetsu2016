<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">健康・福祉</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">健康・福祉</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">医療機関</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">医療機関</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        
        <section>        
          <h2 class="c-heading-2">■壮瞥町内の医療機関</h2>
        </section>
         
         <ol>
         	<li>
            	<h3 class="c-heading-3">(1) 壮瞥歯科診療所</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w25">住所</th>
                      <td>壮瞥町字滝之町384-2　壮瞥町保健センター内</td>
                    </tr>
                    <tr>
                      <th class="__strong">電話</th>
                      <td><a href="tel:014-66-3191">014-66-3191</a></td>
                    </tr>
                    <tr>
                      <th class="__strong">受付</th>
                      <td>平日 9:00-12:00 / 14:00-19:00<br>土曜日 9:00-12:00（予約制）</td>
                    </tr>
                    <tr>
                      <th class="__strong">休診</th>
                      <td>日曜日・祝日</td>
                    </tr>
                  </table>
            </li>
         	<li>
            	<h3 class="c-heading-3">(2) 民間の医療機関</h3>
                <ul class="c-list p-list__mt20">
                	<li>
                    	<span class=" u-bold">医療法人交雄会そうべつ温泉病院（内科・リハビリテーション科）</span><br>
                        壮瞥町字南久保内146番地12<br>
                        TEL <a href="tel:0142-65-2221">0142-65-2221</a><br>
                        <a href="http://www.koyukai-g.jp/soh/" class="c-link__blank">http://www.koyukai-g.jp/soh/</a>
                     </li>
                     <li>
                     <span class=" u-bold">医療法人倭会三恵病院（精神科・神経科・内科）</span><br>
                     壮瞥町字仲洞爺69番地<br>
                     TEL <a href="tel:0142-66-3232">0142-66-3232</a><br>
                     <a href="http://www.minerva.gr.jp/8.html#id1" class="c-link__blank">http://www.minerva.gr.jp/8.html#id1</a>
                     </li>
                </ul>
            </li>
         </ol>

        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
             <h2 class="c-heading-2">■救急診療案内</h2>
             <ul class="c-list p-list__mt20">
             	<li>
                	<span class=" u-bold">伊達赤十字病院救急センター／胆振西部救急センター</span><br>
                        壮瞥町字南久保内146番地12<br>
                        <a href="http://date.jrc.or.jp/outpatient/holiday.html" class="c-link__blank">http://date.jrc.or.jp/outpatient/holiday.html</a>
                </li>
                <li>
                	<span class=" u-bold">休日歯科当番医</span><br>
                    室蘭歯科医師会<br>
                     <a href="http://www.muro-shi.com/emergency/" class="c-link__blank">http://www.muro-shi.com/emergency/</a>
                 </li>
                </ul>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
             <h2 class="c-heading-2">■近隣市町の医療機関</h2>
             <p>北海道医療機能情報システム（<strong>伊達市</strong>で検索してください）</p>
             <a href="http://www.mi.pref.hokkaido.lg.jp/hokkaido/ap/qq/men/pwtpmenult01.aspx" class="c-link__blank">http://www.mi.pref.hokkaido.lg.jp/hokkaido/ap/qq/men/pwtpmenult01.aspx</a>
        </div>
      </div>




      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
