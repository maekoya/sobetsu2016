<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">健康・福祉</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">健康・福祉</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">大人の保健・健康づくり</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">大人の保健・健康づくり</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        <a href="" class="c-link__pdf u-circle__blue">平成28年度保健センター年間行事予定 DL/PDF（作成中）</a>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■健康相談・健康教育</h2>
            <p>健康相談、栄養相談は、電話・面接など随時行っています。<br>お気軽に保健センターへお越しください。<br>
            TEL <a href="tel:0142-66-2340">0142-66-2340</a><br>曜日･時間　平日の9:00-17:00<br>
            また、自治会や老人クラブ、婦人会、企業などの団体やグループなど、少人数でも依頼があれば、その場に出向いて健康づくりや栄養、運動などのお話をいたします。
            同時に健康相談も行いますのでどうぞお気軽に声をかけてください。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■家庭訪問</h2>
            <p>保健師や管理栄養士より保健指導や栄養指導等が必要と思われる方の自宅を訪問しています。</p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■検診・健診</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 特定健診／生活習慣病健診</h3>
                    <p>壮瞥町の生活習慣病健診（特定健診）は、病気の早期発見や現在の健康状態を確認するため、札幌医科大学のご協力のもと昭和53年から続いている事業です。20歳以上の町民であれば誰でも気軽に受けることができ、特に、普段、病院ではしないような検査も無料で受けられるお得な健診です。</p>
                    <p class=" u-bold u-mb__small">平成28年度事業</p>
                      <table class="c-table-1 u-mt__0 c-td__left">
                        <tr>
                          <th class="__strong u-w20">日程</th>
                          <td>夏　7月29日（金）～8月6日（土）、<br>冬　12月10日（土）<br>
                          各日程･会場はこちら<br><a href="" class="c-link__pdf">夏の健診DL/PDF</a><br><a href="" class="c-link__pdf">冬の健診DL/PDF （作成中）</a></td>
                        </tr>
                        <tr>
                          <th class="__strong">対象</th>
                          <td>20歳以上の町民全員</td>
                        </tr>
                        <tr>
                          <th class="__strong">場所</th>
                          <td>保健センターほか町内各地</td>
                        </tr>
                        <tr>
                          <th class="__strong">料金</th>
                          <td>国保・後期・生保は無料<br>社保で受診券のない方　2,000円（受診券のある方は料金が変わります）</td>
                        </tr>
                        <tr>
                          <th class="__strong">特徴</th>
                          <td>・健診を受けた方のみ、無料のオプション検査が受けられます（希望者）<br>
                          ・男性は内臓脂肪スキャン検査で内臓脂肪の状態が、女性は脳波検査で血管年齢がその場で判ります（希望者）。<br>
                          ・自分が塩分をどのくらい摂取しているのかが検査で判ります。<br>
                          ・病気の治療の有無に関係なく、今の健康状態を知ることができ、健康管理ができます。
                          </td>
                        </tr>
                        <tr>
                          <th class="__strong">備考</th>
                          <td>コミュニティタクシーも臨時運行します（要予約）。</td>
                        </tr>
                      </table>
                      <p>また、健康づくりの専門家が、皆さんが健康でいられるよう、よい生活習慣についてアドバイスする特定保健指導も合わせて行っています。
                      ご自宅に案内が届いた方はぜひご利用ください。</p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) がん検診・その他の検診</h3>
                    <p>早期発見・早期治療を目的として、肺・胃・大腸・子宮・乳・前立腺がんの検診や、結核検診、骨検診、脳ドック検診などを行っています。</p>
                      <table class="c-table-1">
                        <tr>
                          <th class="__strong u-w20">胃がん<br>大腸がん</th>
                          <td>30歳以上の方対象</td>
                          <td>4月19日（火）保健センター<br>4月20日（水）農村環境改善センター<br>4月21日（木）保健センター<br>10月25日（火）保健センター</td>
                        </tr>
                        <tr>
                          <th class="__strong">結核<br>肺がん</th>
                          <td>20歳以上の方対象</td>
                          <td>5月下旬<br>7月上旬※会場は町内各地</td>
                        </tr>
                        <tr>
                          <th class="__strong">子宮がん</th>
                          <td>子宮がんは20歳以上の女性対象</td>
                          <td>1月28日（土）保健センター</td>
                        </tr>
                        <tr>
                          <th class="__strong">乳がん</th>
                          <td>乳がんは40歳以上の女性対象</td>
                          <td>6月8日（水）、１月28日（土）保健センター</td>
                        </tr>
                        <tr>
                          <th class="__strong">骨</th>
                          <td>20歳以上の方対象</td>
                          <td>10月25日（火）保健センター<br>1月28日（土）保健センター</td>
                        </tr>
                        <tr>
                          <th class="__strong">前立腺がん</th>
                          <td>40歳以上の男性対象</td>
                          <td>1月26日（木）保健センター</td>
                        </tr>
                        <tr>
                          <th class="__strong">口腔がん</th>
                          <td>今年度も実施予定</td>
                          <td>10月頃予定</td>
                        </tr>
                        <tr>
                          <th class="__strong">脳ドック検診</th>
                          <td>40歳以上の方対象</td>
                          <td>6月～2月 伊達赤十字病院健診センター</td>
                        </tr>
                      </table>
                      <p>年々増えてきているガン。しかし、ガン検診を受ける方はまだまだ少ないのが現状です。そこで、がんが発生しやすい年代の方が気軽に検診を受けられるように、一定の年齢に達した方々に対して大腸がん・乳がん・子宮がん検診の無料クーポンの配布を今年もおこないます。<br>詳細が決まりましたら、お知らせそうべつに掲載します。また対象となった方には、 5月の中ごろまでには、個別に無料クーポンを郵送しますので、ぜひこの機会にガン検診を受けましょう。</p>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■予防接種</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 高齢者肺炎球菌予防接種助成</h3>
                    <p>対象者は毎年異なるため、接種の機会を逃さないように注意しましょう。</p>
                      <table class="c-table-1 u-mt__0 c-td__left">
                        <tr>
                          <th class="__strong u-w20">対象者</th>
                          <td>壮瞥町に住所があり下記の(1)、(2)のいずれかに該当する人<br>
                          <span class="c-annotation__no">※既に高齢者肺炎球菌ワクチン（ニューモバックス NP）を接種したことがある人は対象になりません。</span>
                          <p class="u-mb__small ">（1）平成28年度の対象となる方は、下記のとおりです。</p>
                          <table class="c-table-1 u-mt__0 u-mb__small ">
                            <tr>
                              <th class="__weak">65歳</th>
                              <td>昭和26年4月2日生～昭和27年4月1日生</td>
                            </tr>
                            <tr>
                              <th class="__weak">70歳</th>
                              <td>昭和21年4月2日生～昭和22年4月1日生</td>
                            </tr>
                            <tr>
                              <th class="__weak">75歳</th>
                              <td>昭和16年4月2日生～昭和17 年4月1日生</td>
                            </tr>
                            <tr>
                              <th class="__weak">80歳</th>
                              <td>昭和11年４月2日生～昭和12年4月1日生</td>
                            </tr>
                            <tr>
                              <th class="__weak">85歳</th>
                              <td>昭和6年4月2日生～昭和7年4月1日生</td>
                            </tr>
                            <tr>
                              <th class="__weak">90歳</th>
                              <td>大正15年4月2日生～昭和元年4月1日生</td>
                            </tr>
                            <tr>
                              <th class="__weak">95歳</th>
                              <td>大正10年4月2日生～大正11 年4月1日生</td>
                            </tr>
                            <tr>
                              <th class="__weak">70歳</th>
                              <td>昭和21年4月2日生～昭和22年4月1日生</td>
                            </tr>
                            <tr>
                              <th class="__weak">100歳</th>
                              <td>大正5年4月2日生～大正6年4月1日生</td>
                            </tr>
                          </table>
						<span class="c-annotation__no">※対象となる方には、後日、案内ハガキを郵送します。</span>
                        <p>（2）60～64歳の方で、心臓・腎臓・呼吸器の機能またはヒト免疫不全ウイルスによる免疫の機能に障害があり、日常生活が極端に制限される人</p>
                        </td>
                      </tr>
                        <tr>
                          <th class="__strong">自己負担額</th>
                          <td>2,300円（※生活保護受給者、町民税非課税世帯の人には、個人負担免除の制度もあります。）</td>
                        </tr>
                        <tr>
                          <th class="__strong">備　考</th>
                          <td>接種期間／平成28年4月1日～平成29年3月31日<br>接種できる医療機関／そうべつ温泉病院（要予約）</td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) インフルエンザ予防接種助成</h3>
                    <p>早期発見・早期治療を目的として、肺・胃・大腸・子宮・乳・前立腺がんの検診や、結核検診、骨検診、脳ドック検診などを行っています。</p>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">対象者</th>
                          <td>壮瞥町に住所があり下記の(1)、(2)のいずれかに該当する人<br>
                          （1）65歳以上の高齢者<br>
                          （2）60～64歳の方で、心臓・腎臓・呼吸器の機能またはヒト免疫不全ウイルスによる免疫の機能に障害があり、日常生活が極端に制限される人</td>
                        </tr>
                        <tr>
                          <th class="__strong">自己負担額</th>
                          <td>2,500円（※生活保護受給者、町民税非課税世帯の人には、個人負担免除の制度もあります。）</td>
                        </tr>
                        <tr>
                          <th class="__strong">備考</th>
                          <td>接種期間／平成28年10月 中旬～平成28年 12月31日<br>接種できる医療機関／そうべつ温泉病院・三恵病院</td>
                        </tr>
                      </table>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■各種教室</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 介護予防教室「転ばん塾」</h3>
                    <p>最近ちょっとした段差で足元がつまづきやすくなってきた、足腰が痛い、足元がおぼつかないなど感じることはありませんか？<br>
                    今年も10月から3月までの6ヶ月間、リハビリの先生や運動指導士が簡単に身に付く「転ばない秘訣」を教えてくれます。<br>
                    みんなで集まって楽しく学んでみませんか？ちょっとした健康になるためのミニ講話もあります。皆様の参加をお待ちしています。</p>
                      <table class="c-table-1 u-mt__0 c-td__left">
                        <tr>
                          <th class="__strong u-w20">実施日</th>
                          <td><span class=" u-bold">全9回コース</span><br>
                          <span class=" u-circle__blue">平成28年</span><br>
                          10月6日（木）、10月20日（木）、11月10日（木）、<br>
                          11月24日（木）、12月8日（木）、12月22日（木）<br>
                          <span class=" u-circle__blue">平成29年</span><br>
                          1月19日（木）、2月16日（木）、3月16日（木）                          
                          </td>
                      </tr>
                        <tr>
                          <th class="__strong">会場</th>
                          <td>壮瞥町保健センター 集団検診室</td>
                        </tr>
                        <tr>
                          <th class="__strong">時間</th>
                          <td>午後1時30分～3時30分</td>
                        </tr>
                        <tr>
                          <th class="__strong">内容</th>
                          <td>リハビリの先生や運動指導士による転倒予防の運動、そのほか健康のまめ知識など</td>
                        </tr>
                        <tr>
                          <th class="__strong">対象者</th>
                          <td>足腰に不安のある65歳以上の町民（要支援1～2の方も含む）</td>
                        </tr>
                        <tr>
                          <th class="__strong">募集人数</th>
                          <td>30名程度（参加費無料、希望者には送迎あり）</td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 健康づくり教室「かろやかクラブ」</h3>
                    <p>生活習慣病健診の結果、このまま放っておくと、生活習慣病になるおそれのある方を対象に、講義と運動を組み合わせた教室を2月頃に5日間、開催しています。</p>
                      <table class="c-table-1 c-td__left">
                        <tr>
                          <th class="__strong u-w20">実施日</th>
                          <td><span class=" u-bold">全5回コース</span><br>
                          <span class=" u-circle__blue">平成29年</span><br>2月3日（金）、2月10日（金）、2月17日（金）、2月24日（金）、3月3日（金）
                          </td>
                        </tr>
                        <tr>
                          <th class="__strong">会場</th>
                          <td>山美湖会議室1・2、山美湖ホール、保健センター（調理実習のみ）</td>
                        </tr>
                        <tr>
                          <th class="__strong">時間</th>
                          <td>午後1時30分～3時30分</td>
                        </tr>
                        <tr>
                          <th class="__strong">内容</th>
                          <td>医師、保健師、管理栄養士による健康講話、調理実習、インストラクターによる運動など</td>
                        </tr>
                        <tr>
                          <th class="__strong">対象者</th>
                          <td>生活習慣病健診の結果、対象となった者（対象となった方には、ご案内を差し上げています）</td>
                        </tr>
                        <tr>
                          <th class="__strong">募集人数</th>
                          <td>25名程度（参加費無料、希望者には送迎あり）</td>
                        </tr>
                      </table>
                </li>
            </ol>
        </div>
             <div class="u-clearboth">
                <div class="u-grid__col-6">
                  <img src="images/kenkozukuri/img01.jpg" width="" height="" alt="健康づくり教室">
                </div>
                <div class="u-grid__col-6">
                  <img src="images/kenkozukuri/img02.jpg" width="" height="" alt="かろやかクラブ">
                </div>
              </div>

      </div>
      




      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
