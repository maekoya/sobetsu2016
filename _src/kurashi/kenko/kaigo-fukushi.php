<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">健康・福祉</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">健康・福祉</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">介護保険・高齢者福祉</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">介護保険・高齢者福祉</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■地域包括支援センター</h2>
            <p>地域包括支援センターは、住み慣れた地域で尊厳あるその人らしい生活を継続することができるよう、さまざまな方面から高齢者のみなさんを支える機関です。
            介護に関する相談や悩み以外にも健康や福祉、医療や生活に関することなど、どのようなご相談にも対応します。「どこに相談すればよいのかわからない」といった悩みも、
            まずはご相談ください。必要なサービスや制度、関係機関の情報提供やご紹介をします。高齢者本人からだけではなく、家族、近隣に暮らす方などからもご相談を受け付けています。
            電話での相談もできます。</p>
            <p><span class=" u-bold">【地域包括支援センター】</span><br>壮瞥町字滝之町284-2保健センター内　TEL  <a href="tel:0142-66-4165">0142-66-4165</a></p>
            <h3 class=" c-heading-3 u-mb__small u-mt__0">サポート・ご相談の事例</h3>
            <div class=" u-box_border u-mt__0">
            	<ul class="c-list p-list__mb10">
                	<li>引っ越してきたばかりで友人がいないので地域のサークルを教えてほしい</li>
                    <li>認知症の母親は徘徊がひどくて困っているが何か良いサービスはないだろうか</li>
                    <li>元気で生活できているので、いまの状態をなるべく維持したい</li>
                    <li>足の筋力が衰えているので、介護予防のプログラムを受けるよう勧められた</li>
                    <li>要介護認定の申請を頼みたい</li>
                    <li>介護保険のサービスを利用したい</li>
                    <li>要支援１と認定されたので介護予防プランを作りたい</li>
                    <li>介護保険では非該当と認定されたが体力に自信がない</li>
                    <li>利用していない介護サービスの金額を請求されているようだ</li>
                </ul>            
            </div>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■高齢者福祉サービス</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 特定利用証</h3>
                    <table class="c-table-1">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>町営温泉施設のお風呂が1回130円で入浴できます</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>壮瞥町内に住む65歳から69歳までの方<br>（70歳以上の方は (2)敬老福祉証に代わります）</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>入浴1回130円<br>※特定利用券の発行は無料です</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>役場住民福祉課　TEL <a href="tel:0142-66-2121">0142-66-2121</a></td>
                        </tr>
                    </table>                    
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 敬老福祉証</h3>
                    <table class="c-table-1">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>町営温泉施設のお風呂が1回130円で入浴できます<br>町内の区間に限り、路線バス（道南バス）が無料で利用できます</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>壮瞥町内に住む70歳以上の方（5年ごとに更新が必要です）</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>入浴1回130円<br>※敬老福祉証の発行は無料です</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>役場住民福祉課　TEL <a href="tel:0142-66-2121">0142-66-2121</a></td>
                        </tr>
                    </table>                    
                </li>
            	<li>
                	<h3 class="c-heading-3">(3) 緊急通報システムの設置</h3>
                    <table class="c-table-1 c-td__left">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>携帯電話型端末の通報機を押すと、管理センターに24時間いつでもつながり、<br>状況を確認するための会話もできます。<br>
                            必要に応じて救急車の手配や近所の協力者に安否確認を依頼します。</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>壮瞥町内に住む独り暮らし、又は高齢者世帯で、<br>健康状態や身体状況等から日常生活に支障を感じている方</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>利用者負担額　年額3,000円（月額250円）<br>
                            ア）受信センターへの通話に要する電話使用料<br>
                            イ）利用者の事由による場合<br>
                            ウ）急病、事故等緊急事態が発生した際の経費（当初の端末機の設置・撤去、利用に係る経費は町が負担します）</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>役場住民福祉課　TEL <a href="tel:0142-66-2121">0142-66-2121</a></td>
                        </tr>
                    </table>                    
                </li>
            	<li>
                	<h3 class="c-heading-3">(4) 配食サービス</h3>
                    <table class="c-table-1">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>高齢者や障害者世帯に週1～2回、長日園で調理された栄養バランスのとれた夕食を<br>ボランティアが配達することにより、健康状態の維持と増進に努め、在宅生活を支援します。</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>体が不自由で買い物や調理ができず、栄養状態に不安のある、<br>概ね65歳以上のお独り又は夫婦の高齢者世帯</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>1食300円</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>社会福祉協議会　TEL <a href="tel:0142-66-2121">0142-66-2121</a> <span class="c-annotation__no">※注1</span></td>
                        </tr>
                    </table>                    
                </li>
            	<li>
                	<h3 class="c-heading-3">(5) 除雪サービス</h3>
                    <table class="c-table-1">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>高齢者や障害者世帯を対象に、現在住んでいる自宅母屋の屋根の雪下ろしを行い、<br>
                            冬期間の生活の安全確保に努めます。<br>
                            (家の周りや倉庫は対象外など除雪できる範囲は限られます)。</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>体が不自由で除雪ができない、概ね65歳以上のお独り又は夫婦の高齢者世帯</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>無料<br>（ただし、事務局で現地を確認し、要不要及び実施時期を判断します）</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>社会福祉協議会　TEL <a href="tel:0142-66-2121">0142-66-2121</a> <span class="c-annotation__no">※注1</span></td>
                        </tr>
                    </table>                    
                </li>
            	<li>
                	<h3 class="c-heading-3">(6) 入浴送迎サービス</h3>
                    <table class="c-table-1">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>高齢者、障害者世帯を対象に、自宅で入浴できない方を特別養護老人ホーム第2長日園へ移送し、お風呂と介護職員の介助により安心して入浴できます。<br>
                            （週1回、毎週火曜日）</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>体が不自由で自宅でお風呂に入れない、概ね65歳以上のお独り又は夫婦の高齢者世帯</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>年間1,000円（送迎サービス補償制度保険料）</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>社会福祉協議会　TEL <a href="tel:0142-66-2121">0142-66-2121</a> <span class="c-annotation__no">※注1</span></td>
                        </tr>
                    </table>
                    <p class="c-annotation__no">注1<br>
                    ・ご自宅に訪問して体の状態や病気の有無、その他生活上の不便な点などをお聞きします。<br>
                    ・必要に応じて申請書等に記名、押印をいただきます。<br>
                    ・後日、社協又は役場から利用できるかどうかご連絡します。<br>
                    ・生活の状況や支援者（親戚、友人等）の有無によっては、ご利用できない場合がありますのでまずはご相談ください。
                    </p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(7) ふれあい友愛訪問</h3>
                    <table class="c-table-1">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>80歳以上の一人暮らしの高齢者世帯に、月に2回、ボランティアが自宅にヤクルトをお届けし、安否確認や日常の相談、話し相手になります。</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>80歳以上で、自宅でお独りで暮らしている方</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>無料</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>社会福祉協議会　TEL <a href="tel:0142-66-2121">0142-66-2121</a><br>（該当する方はいつでも始められますのでお電話ください）</td>
                        </tr>
                    </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(8) 外出支援サービス（自立支援デイサービス）</h3>
                    <table class="c-table-1">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>概ね65歳以上の高齢者で、閉じこもりが心配される方に、<br>ほかの方とゲームを楽しむ時間や食事と入浴、必要に応じて運動等を提供します。</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>閉じこもりがちな概ね65歳以上で自宅で生活している方</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>・週1回　入浴あり2,099円、入浴なし1,899円（月額）<br>
                            ・週2回　入浴あり4,205円、入浴なし3,805円（月額）<br>
                            ※別途、食事費がかかります</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>役場住民福祉課　TEL <a href="tel:0142-66-2121">0142-66-2121</a> <span class="c-annotation__no">※注2</span></td>
                        </tr>
                    </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(9) 訪問支援事業（自立支援ホームヘルプ）</h3>
                    <table class="c-table-1">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>概ね65歳以上の高齢者で近くに支援者がいない方を対象に、<br>利用者ができない生活動作（調理、掃除等）を<br>ホームヘルパーが訪問して自立した生活を支援します。
                            </td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>生活の一部に助けが必要な概ね65歳以上で自宅で生活している方</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>・当初6か月の月額　週1回1,220円、週2回2,440円<br>
                            ・7か月目以降の月額　週1回2,196円、週2回4,392円</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>役場住民福祉課　TEL <a href="tel:0142-66-2121">0142-66-2121</a> <span class="c-annotation__no">※注2</span></td>
                        </tr>
                    </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(10) 短期入所生活支援事業（ショートステイ）</h3>
                    <table class="c-table-1">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>普段介護しているご家族が、外出等で介護ができない期間、介護を必要とされるご家族（介護保険未申請者）が、
                            町内の特別養護老人ホームで宿泊することができます。</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>家族の介護が必要な概ね65歳以上の方</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>食事を食べた場合は別に料金が必要となります。<br>
                            <span class=" u-bold">自立・要支援1</span> 517円/日<br>
                            <span class=" u-bold">要支援2</span> 635円/日<br>
                            <span class=" u-bold">要介護1</span> 705円/日<br>
                            <span class=" u-bold">要介護2</span> 775円/日</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>役場住民福祉課　TEL <a href="tel:0142-66-2121">0142-66-2121</a> <span class="c-annotation__no">※注2</span></td>
                        </tr>
                    </table>
                    <p class="c-annotation__no">注2<br>
                    ・要介護度が自立判定となった方も状態を勘案して利用できます<br>
                    ・ご本人やご家族の状況を確認したうえで利用できるかどうか判断されます。<br>
                    ・生活の状況や支援者（親戚、友人等）の有無によっては、ご利用できない場合がありますのでまずはご相談ください。</p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(12) 長寿祝金</h3>
                    <p>満88歳、99歳、100歳になる高齢者に対し、長寿祝い金をお渡しし、ご長寿を祝福するとともに多年にわたる社会貢献への敬意を表します。</p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(13) 家族介護用品の支給</h3>
                    <table class="c-table-1">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>在宅で要介護認定を受けた高齢者等を介護する家族に対し経済的負担の軽減を図るために<br>介護用品の支給に関し助成する。</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>本町に住所を有し尚かつ在宅者で要介護2から要介護5に認定された高齢者</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>1ヶ月8,000円まで</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>役場住民福祉課 介護保険係<br>TEL <a href="tel:0142-66-2121">0142-66-2121</a> <span class="c-annotation__no">※注2</span></td>
                        </tr>
                    </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(14) 住宅改修拡大措置事業</h3>
                    <table class="c-table-1">
                    	<tr>
                        	<th class="__strong u-w30">内容</th>
                            <td>介護保険法に規定されている住宅改修及びそれに付帯して必要となる住宅改修又は<br>介護給付額の対象とならない改修の工事軽費を助成する。</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">対象者</th>
                            <td>本町に住所を有し尚かつ在宅者で要支援又は要介護認定を受けた高齢者</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">費用等</th>
                            <td>対象工事軽費の半額を補助。<br>（対象工事軽費は80万円まで）</td>
                        </tr>
                    	<tr>
                        	<th class="__strong">申込先</th>
                            <td>役場住民福祉課 介護保険係<br>TEL <a href="tel:0142-66-2121">0142-66-2121</a> <span class="c-annotation__no">※注2</span></td>
                        </tr>
                    </table>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■介護保険</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 介護保険サービス</h3>
                    <p>介護保険事業は、介護が必要な人や介護する家族負担を社会全体で支えることを目的としています。
                    介護保険サービスを利用するためには、「介護や支援が必要である」と任されることが必要です。介護認定の申請等は役場住民福祉課介護保険係までご相談ください。<br>
                    <span class=" u-bold">【相談先】</span><br>壮瞥町役場住民福祉課 介護保険係　TEL <a href="tel:0142-66-2121">0142-66-2121</a></p>
                    <img src="images/kaigo/img01.jpg" width="" height="" alt="">
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 介護保険料</h3>
                    <p>介護保険料は介護保険事業計画の改訂に合わせて3年に一度見直しされます。現計画（第6期介護保険事業計画/平成27-29年度）に定める介護保険料は次のとおりです。<br>
                    <a href="" class="c-link">第6期高齢者保健福祉計画・介護保険事業計画</a>
                    </p>
                    <h4 class="c-heading-4 u-mb__0">対象（65歳以上の方）</h4>
                      <table class="c-table-1 u-mt__0">
                        <tr>
                          <th class="__strong u-w15">保険料段階</th>
                          <th class="__strong u-w15">世帯</th>
                          <th class="__strong">本人所得等</th>
                          <th class="__strong u-w10">割合</th>
                          <th class="__strong u-w10">保険料<br>月額</th>
                          <th class="__strong u-w10">保険料<br>年額</th>
                        </tr>
                        <tr>
                          <td class="__weak" rowspan="2">第1段階</td>
                          <td rowspan="4">住民税非課税<br>世帯</td>
                          <td>生活保護受給者又は老齢福祉年金受給者</td>
                          <td rowspan="2">基準額×0.45</td>
                          <td rowspan="2">2,560円</td>
                          <td rowspan="2">30,700円</td>
                        </tr>
                        <tr>
                          <td>本人の前年の課税年金収入額と合計所得金額の合計が80万円以下</td>
                        </tr>

                        <tr>
                          <td class="__weak">第2段階</td>
                          <td>本人の前年の課税年金収入額と合計所得金額の合計が80万円を超え120万円以下</td>
                          <td>基準額×0.63</td>
                          <td>3,590円</td>
                          <td>43,000円</td>
                        </tr>
                        <tr>
                          <td class="__weak">第3段階</td>
                          <td>本人の前年の課税年金収入額と合計所得金額の合計が120万円を超える</td>
                          <td>基準額×0.75</td>
                          <td>4,270円</td>
                          <td>51,200円</td>
                        </tr>

                        <tr>
                          <td class="__weak">第4段階</td>
                          <td rowspan="6">住民税課税<br>世帯</td>
                          <td><span class=" u-bold">[本人住民税非課税]</span><br>本人の前年の課税年金収入額と合計所得金額が<br>80万円以下</td>
                          <td>基準額×0.83</td>
                          <td>4,730円</td>
                          <td>56,700円</td>
                        </tr>
                        <tr>
                          <td class="__weak">第5段階</td>
                          <td><span class=" u-bold">[本人住民税非課税]</span><br>本人の前年の課税年金収入額と合計所得金額が<br>80万円を超える</td>
                          <td>基準額</td>
                          <td>5,700円</td>
                          <td>68,400円</td>
                        </tr>
                        <tr>
                          <td class="__weak">第6段階</td>
                          <td>本人の前年の合計所得金額が120万円未満</td>
                          <td>基準額×1.25</td>
                          <td>7,120円</td>
                          <td>85,400円</td>
                        </tr>
                        <tr>
                          <td class="__weak">第7段階</td>
                          <td>本人の前年の合計所得金額が<br>120万円以上190万円未満</td>
                          <td>基準額×1.30</td>
                          <td>7,410円</td>
                          <td>88,900円</td>
                        </tr>
                        <tr>
                          <td class="__weak">第8段階</td>
                          <td>本人の前年の合計所得金額が<br>190万円以上290万円未満</td>
                          <td>基準額×1.60</td>
                          <td>9,120円</td>
                          <td>109,400円</td>
                        </tr>
                        <tr>
                          <td class="__weak">第9段階</td>
                          <td>本人の前年の合計所得金額が290万円以上</td>
                          <td>基準額×1.75</td>
                          <td>9,970円</td>
                          <td>119,600円</td>
                        </tr>
                      </table>
                      <p>
                      	<span class=" u-circle__blue">第6期計画の基準額は5,700円です。平成27年度の介護保険料納入のご案内は6月以降になります。</span><br>
                        <span class=" u-circle__blue">40歳から64歳の方（第２号被保険者）の介護保険料は加入されている医療保険と併せて納めていただいております。</span>
                      </p>
                      <p>
                    <span class="c-annotation__no">※1「合計所得金額」とは収入額から必要経費に相当する額を控除した金額で、医療費控除などの各種所得控除を行う前の金額です。</span><br>
                    <span class="c-annotation__no">※2「老齢福祉年金」とは明治44年４月１日以前に生まれた方で、所得が低く、他の年金を受給できない人に支給される年金です。</span>
                    </p>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■介護保険制度改正<span class=" u-font__s">(平成27年8月からの改正点)</span>についてお知らせします。</h2>
            <ol>
            	<li>
                	<h4 class="c-heading-3">(1) 一定以上の所得がある方は自己負担が「2割」になります。</h4>                    
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong" colspan="3">第1号被保険者</th>
                    </tr>
                    <tr>
                      <td class="__weak" rowspan="2">本人の合計所得が<br>160万円以上</td>
                      <td class="">合計所得金額<span class="c-annotation__no">※1</span> ＋ 年金収入が<br>65歳以上で単身者280万円未満、<br>2人以上いる世帯で346万円未満の場合</td>
                      <td class=" u-w10">1割負担</td>
                    </tr>
                    <tr>
                      <td>上記以外の場合</td>
                      <td>2割負担</td>
                    </tr>
                    <tr>
                      <td class="__weak">本人の合計所得が<br>160万円未満</td>
                      <td colspan="2">1割負担</td>
                    </tr>
                  </table>
                    <p class="c-annotation__no">※1　収入金額から必要経費に相当する金額を控除した金額です。</p>                
                </li>
            	<li>
                	<h4 class="c-heading-3">(2) 介護保険負担割合証が発行されます。</h4>
                    <p>要支援・要介護の認定を受けた方全員に利用者負担の割合（1割または2割）が記載された「介護保険負担割合証」を送付します。<br>
                    介護サービスを利用する際に事業所又は施設の窓口に保険証と一緒に提出することになります。</p>                
                </li>
            	<li>
                	<h4 class="c-heading-3">(3) 高額介護サービス費に新しく上限額が設定されます。</h4>
                    <p>同じ月に利用した介護サービスの利用者負担合計が一定額を超えたときに支給される「高額介護サービス費」の利用者負担段階区分で「現役並の所得者」が新設されます。</p>
                    <p class="c-heading-3 u-mb__0"><span class=" u-circle__blue">利用者負担の上限（1ヶ月）</span></p>
                      <table class="c-table-1 u-mt__0 u-mb__small">
                        <tr>
                          <th class="__strong">利用者負担段階区分</th>
                          <th class="__strong">平成27年7月まで上限額</th>
                          <th class="__strong">平成27年8月から上限額</th>
                        </tr>
                        <tr>
                          <td>現役並み所得者（新設）<span class="c-annotation__no">※2</span></td>
                          <td>&nbsp;</td>
                          <td>世帯：44,400円</td>
                        </tr>
                        <tr>
                          <td>一般世帯</td>
                          <td>世帯：37,200円</td>
                          <td>世帯：37,200円</td>
                        </tr>
                        <tr>
                          <td rowspan="2">住民税世帯非課税<br>
                          <span class="c-txt__left">
                          ◎合計所得金額及び課税年金収入額の合計が80万円以下の人<br>
                          ◎老齢福祉年金の受給者
                          </span>
                          </td>
                          <td>世帯：24,600円</td>
                          <td>世帯：24,600円</td>
                        </tr>
                        <tr>
                          <td>個人：15,000円</td>
                          <td>個人：15,000円</td>
                        </tr>
                        <tr>
                          <td>◎生活保護の受給者<br>
                          ◎利用者負担を15,000円に減額することで生活保護の受給者とならない場合
                          </td>
                          <td>個人：15,000円<br>世帯：15,000円</td>
                          <td>個人：15,000円<br>世帯：15,000円</td>
                        </tr>
                      </table>
                      <p class="c-annotation__no u-mt__0">※2 現役並み所得者・・・同一世帯に課税所得145万円以上の第1号被保険者がいて、収入が単身383万円以上、2人以上520万円以上の方</p>
                </li>
            	<li>
                	<h4 class="c-heading-3">(4) 低所得の施設利用者への食費・居住費の軽減（負担限度額）について、適用要件が変更されます。</h4>
                    <p>施設サービスの居住費と食費は所得の低い方に対しては自己負担の上限額が設けられており、これを超えた分は「特定入所者介護サービス費」として、
                    介護保険から施設等に支払われますが、その対象者となる要件が変更されます。</p>
                    <p class="u-mb__small">① 住民票上世帯が異なる（世帯分離している）場合の配偶者の所得も判断材料になります。</p>
                    <p class="u-mt__0"><span class=" u-bold">【配偶者の範囲】</span><br>
                    婚姻届を提出していない事実婚も含みます。<br>
                    DV防止法における配偶者からの暴力を受けた場合や行方不明の場合等は対象外となります。</p>
                    <p>② 預貯金等が単身者1,000万円以下、夫婦2,000万円以下であることが要件に加わります。<br>
                          <table class="c-table-1">
                            <tr>
                              <th class="__strong">預貯金等に含まれるもの<br><span class="u-normal">（資産性があり、換金性が高く、価格評価が容易なものが対象）</span></th>
                              <th class="__strong">確認方法<br><span class="u-normal">（価格評価を確認できる書類の入手が容易なものは添付を求めます。）</span></th>
                            </tr>
                            <tr>
                              <td>預貯金（普通・定期）</td>
                              <td>通帳の写し<br>（インターネットバンクであれば口座残高ページの写し）</td>
                            </tr>
                            <tr>
                              <td>有価証券（株式・国債・地方債・社債など）</td>
                              <td>証券会社や銀行の口座残高の写し<br>（ウエブサイトの写しも可）</td>
                            </tr>
                            <tr>
                              <td>金・銀（積立購入を含む）など、<br>購入先の口座残高によって時価評価額が容易に把握できる貴金属</td>
                              <td>購入先の銀行等の口座残高の写し<br>（ウエブサイトの写しも可）</td>
                            </tr>
                            <tr>
                              <td>投資信託</td>
                              <td>銀行、信託銀行、証券会社等の口座残高の写し<br>（ウエブサイトの写しも可）</td>
                            </tr>
                            <tr>
                              <td>タンス預金（現金）</td>
                              <td>自己申告</td>
                            </tr>
                          </table>
                          <span class=" u-circle__blue">預貯金等には生命保険、自動車、腕時計・宝石などの時価評価額の把握が難しい金属、絵画・骨董品・家財は含まれません。</span><br>
                          <span class=" u-circle__blue">負債（借入金・住宅ローンなど）は、預貯金等から差し引いて計算します。（借用証書などで確認）また、価格評価は申請日の直近2ヶ月前までの写し等により行います。</span>
                    </p>
                    <p>③ 非課税年金（遺族年金・障害年金）を収入として算定します。（平成28年8月から）</p>
                </li>
            	<li>
                	<h4 class="c-heading-3">(5) 70歳未満の方の高額医療・高額介護合算制度の限度額が変更されます。</h4>
                    <p>70歳未満の方の所得区分が平成27年1月から細分化され、「高額医療・高額介護合算制度」の限度額が下記のように変わりました。<br>
                    計算期間は毎年8月から翌年7月（12ヶ月）となります。</p>
                    <p class="c-heading-3 u-mb__small">70歳未満の方の医療と介護の自己負担合算後の限度額（年額）</p>
                      <table class="c-table-1 u-mt__0">
                        <tr>
                          <th class="__strong">所得区分</th>
                          <th class="__strong">平成26年7月まで</th>
                        </tr>
                        <tr>
                          <td class="__weak">上位所得者</td>
                          <td>126万円</td>
                        </tr>
                        <tr>
                          <td class="__weak">一般</td>
                          <td>67万円</td>
                        </tr>
                        <tr>
                          <td class="__weak">町民税非課税世帯</td>
                          <td>34万円</td>
                        </tr>
                      </table>

                      <p class="arrow_b">&nbsp;</p>

                      <table class="c-table-1">
                        <tr>
                          <th class="__strong" colspan="2">所得区分</th>
                          <th class="__strong">平成27年8月から ※3</th>
                        </tr>
                        <tr>
                          <td class="__weak" rowspan="4">得基額準総所 ※4</td>
                          <td>901万円超</td>
                          <td>212万円（176万円）</td>
                        </tr>
                        <tr>
                          <td>600万円超～901万円以下</td>
                          <td>141万円（135万円）</td>
                        </tr>
                        <tr>
                          <td>210万円超～600万円以下</td>
                          <td>67万円（67万円）</td>
                        </tr>
                        <tr>
                          <td>210万円以下</td>
                          <td>60万円（63万円）</td>
                        </tr>
                        <tr>
                          <td class="__weak" colspan="2">町民税非課税世帯</td>
                          <td>34万円（34万円）</td>
                        </tr>
                      </table>
                      <p class="c-annotation__no">※3 平成26年8月～平成27年7月の限度額は、経過措置として( )内の金額となります。<br>
                      ※4 基準総所得額＝前年の総所得金額等－基礎控除額33万円</p>
                </li>
            </ol>
        </div>
      </div>



      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
