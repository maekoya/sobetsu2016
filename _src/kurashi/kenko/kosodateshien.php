<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">健康・福祉</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">健康・福祉</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">子育て支援</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">子育て支援</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■そうべつ子どもセンター・くぼない児童クラブ</h2>
            <p>そうべつ子どもセンターは、「保育所（認定こども園）」、「子育て支援センター」、「児童館・児童クラブ」という3つの機能を持つ子育て支援総合施設で、
            平成22年度にオープンしました。</p>

         <ul class="u-clearboth">
            <li class="u-grid__col-4">
              <img src="/images/shien/img01.jpg" width="" height="" alt="">
            </li>
            <li class="u-grid__col-4">
              <img src="/images/shien/img02.jpg" width="" height="" alt="">
            </li>
            <li class="u-grid__col-4">
              <img src="/images/shien/img03.jpg" width="" height="" alt="">
            </li>
          </ul>
          
          <ol>
            	<li>
                	<h3 class="c-heading-3">(1) そうべつ保育所</h3>
                    <p>家庭内で保育ができない小学校就学前の児童（長時間保育）や、保護者の状況に関わらず給食終了後までお子さんを預かる（短時間保育）認定こども園です。</p>
                      <table class="c-table-1">
                        <tr>
                          <th class="__strong"></th>
                          <th class="__strong">長時間保育</th>
                          <th class="__strong">短時間保育</th>
                        </tr>
                        <tr>
                          <td class="__weak">開所時間</td>
                          <td>7:30-18:30</td>
                          <td>8:30-13:00</td>
                        </tr>
                        <tr>
                          <td class="__weak">対象年齢</td>
                          <td>生後6か月（0歳児）～5歳児</td>
                          <td>3歳～5歳児</td>
                        </tr>
                        <tr>
                          <td class="__weak" rowspan="2">入所定員</td>
                          <td>75名</td>
                          <td>10名</td>
                        </tr>
                        <tr>
                          <td colspan="2">受入体制の都合上、原則、0歳児は定員3名、<br>1歳児クラスは定員6名、2歳児クラスは定員12名です</td>
                        </tr>
                      </table>
                     <p><span class=" u-bold">ダウンロード</span><br><a href="" class="c-link__pdf">平成28年度入所のしおりPDF/DL</a></p> 
                     <p>「入所申込書」、「就労証明書」、「就労申告書」、「求職活動申立書」のダウンロードは「様式ダウンロード」ページから<br>ダウンロードしてください。<br>
                     	<a href="" class="c-link">「様式ダウンロード」に行く</a>
                     </p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 子育て支援センターげんき</h3>
                    <p>保育所に入所されていない保護者、児童が利用できます。
                    主に週2回の子育て支援サークルの開催や育児等の相談等を行っています。
                    子育て中のお母さん、お父さんが安心して楽しく子育てができるような場所、又、子育ての輪がひろがり仲間が広がるような場所です。
                    </p>
                    <ul class="c-list p-list__mt20">
                    	<li>
                        	<span class=" u-bold">子育て支援センター開館時間</span><br>
                            月～金曜日：9時～17 時<br>
                            休館日：土日祝日、12月31日～1月5日まで<br>
                            お気軽にご利用ください。
                        </li>
                    	<li>
                        	<span class=" u-bold">子育て支援センター開放</span><br>
                            毎週火曜日・金曜日：10時～12時<br>
                            子どもと保護者（父母・祖父母）が、おもちゃで遊んだり絵を描いたりお友達を作って楽しく過ごす場です。<br>お子さんの生活リズムに合わせてご利用ください。
                        </li>
                    	<li>
                        	<span class=" u-bold">育児サークル</span><br>
                            月に1～2回、木曜日：10時～12時<br>
                            親子で遊んだり交流したり情報交換をする場です。製作遊び・公園遊び・子どもの発達に関するお話・体をつかった楽しい遊び・消防署の方のお話・栄養士のお話などを
                            予定しています。又、身体測定も行います。
                        </li>
                    	<li>
                        	<span class=" u-bold">育児相談</span><br>
                            子育て支援センターでは 子育てに関する様々な相談をお受けしています。子育てを支援させていただきますのでお気軽にご相談ください。
                        </li>
                    </ul>
                </li>
                <li>
                	<h3 class="c-heading-3">(3) 壮瞥町児童館</h3>
                     <p>学校等下校後、自由に遊ぶことができる児童館です。就学前児童も利用できますが、その際は保護者の同伴が必要です。<br>その他、スクールバスは閉館時間に合わせて子どもセンター前から出発しています。また、小学生を対象に書道教室を毎週開催しています。</p>
                  <table class="c-table-1">
                    <tr>
                      <th class="__strong u-w20" rowspan="2">開所時間</th>
                      <td>夏季（4月～9月まで）下校時間後～17:00まで</td>
                    </tr>
                    <tr>
                      <td>冬季（10月～3月まで）下校時間後～16:30まで<br><span class="c-annotation__no">※冬季は小学校の帰宅指導時間に間に合うように退館指導しています。</span></td>
                    </tr>
                  </table>
                </li>

                <li>
                	<h3 class="c-heading-3">(4) そうべつ児童クラブ</h3>
                     <p>小学校下校後や、学校休校日（土曜日・振替休日等）に保護者の就労等により家庭内保育ができない児童を預かる児童クラブです。<br>
                     くぼない児童クラブは、久保内小学校区の児童クラブで青少年会館内の専用スペースで運営しています。<br>いずれも小学校6年生まで入所受け入れをしています。</p>
                      <table class="c-table-1">
                        <tr>
                          <th class="__strong">開所時間</th>
                          <th class="__strong">そうべつ児童クラブ</th>
                          <th class="__strong">くぼない児童クラブ</th>
                        </tr>
                        <tr>
                          <td class="__weak">学校授業日</td>
                          <td>学校下校時～18:30</td>
                          <td>学校下校時～18:00</td>
                        </tr>
                        <tr>
                          <td class="__weak">学校授業日以外の日<br>（日曜・祝日除く）</td>
                          <td>7:30～18:30</td>
                          <td>8:00～18:00</td>
                        </tr>
                      </table>
                     <p><span class=" u-bold">ダウンロード</span><br><a href="" class="c-link__pdf">平成28年度児童クラブしおりPDF/DL</a></p> 
                     <p>「そうべつ児童クラブ申込書」、「くぼない児童クラブ申込書」、「就労証明書」、「就労申告書」、「求職活動申立書」のダウンロードは「様式ダウンロード」ページから
                     ダウンロードしてください。<br>
                     	<a href="" class="c-link">「様式ダウンロード」に行く</a>
                     </p>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■子どもの医療費助成制度</h2>
            <p>壮瞥町では乳幼児医療の助成対象を拡大し、中学生まで入院・通院に係る保険適用の医療費を全額助成します。</p>
          <table class="c-table-1 c-td__left">
            <tr>
              <th class="__strong u-w25">対象者</th>
              <td>町内に住所を有する乳幼児、小学生、中学生（15歳に達する日以後最初の3月31日までの子ども）</td>
            </tr>
            <tr>
              <th class="__strong">対象医療費</th>
              <td>平成25年8月1日以降に診療を受けた医療費で保険適用のもの</td>
            </tr>
            <tr>
              <th class="__strong">助成方法</th>
              <td>「乳幼児等医療費受給者証」を役場窓口で発行します。<br>この受給者証を医療機関の窓口で提示することによって、保険診療の自己負担額を助成します。
              <p>※胆振西部管内（室蘭市・登別市・伊達市・壮瞥町・洞爺湖町・豊浦町）以外
              の医療機関に受診された際は、町からの償還払いとなりますので、医療機関窓口で医療費をお支払いいただき、領収書をひと月分まとめて役場窓口までご提示下さい。
              </p>
              <p>※胆振西部管内の医療機関でも、一部医療機関が対応できない場合がありますので、その際は上記同様、償還払いとなります。</p>
              <p>※健康保険の適用とならないもの（予防接種・健康審査・薬の容器代等）は助成対象ではありません。</p>
              </td>
            </tr>
            <tr>
              <th class="__strong">助成を受けるために</th>
              <td>現在0歳～15歳のお子さんがいらっしゃる保護者の方で、「乳幼児等医療受給者証」をお持ちでない方は、役場窓口までお申し出ください。</td>
            </tr>
            <tr>
              <th class="__strong">「ひとり親家庭等医療費制度」の受給者について</th>
              <td>「ひとり親家庭等医療制度」の対象者も、中学生以下の方は助成対象となりますので、<br>医療機関窓口で医療費をお支払いいただき、
              領収書をひと月分まとめて役場窓口に提示してください。</td>
            </tr>
            <tr>
              <th class="__strong">申請様式等</th>
              <td>「受給者証交付申請書」は「様式ダウンロード」ページからダウンロードしてください。<br>
              <a href="" class="c-link">「様式ダウンロード」に行く</a>
              </td>
            </tr>
          </table>
          
          <section>
          	<h3 class="c-heading-3">児童手当</h3>
              <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w25">支給対象</th>
                  <td>中学校卒業まで（15歳の誕生日後の最初の3月31日まで）の児童を養育している方</td>
                </tr>
                <tr>
                  <th class="__strong">支給額</th>
                  <td>3歳未満　一律15,000円<br>
                  3歳以上小学校修了前　10,000円（第3子以降は15,000円）<br>
                  中学生　一律10,000円<br>
                  特例給付　一律5,000円</td>
                </tr>
                <tr>
                  <th class="__strong">支給時期</th>
                  <td>原則として6月、10月、2月にそれぞれの前月分までの手当を支給</td>
                </tr>
              </table>
          </section>

          <section>
          	<h3 class="c-heading-3">通学定期補助</h3>
            <p>JRやバスの定期券を購入して通学する本町在住の高校、大学、各種学生の保護者を対象とした助成制度です。</p>
              <table class="c-table-1 c-td__left">
                <tr>
                  <th class="__strong u-w25">対象区間</th>
                  <td>道南バス・JR北海道の通学定期で利用する全区間（パスカード、回数券は対象になりません）</td>
                </tr>
                <tr>
                  <th class="__strong">補助率</th>
                  <td>通学定期代の1/2</td>
                </tr>
                <tr>
                  <th class="__strong">手続き</th>
                  <td>・学校長の在学証明印と通学定期の写しのついた交付申請書を提出（4月）<br>
                  ・定期券代の領収書写しをつけた実績報告書、請求書を提出（9月･3月）
                  </td>
                </tr>
                <tr>
                  <th class="__strong">様式等</th>
                  <td>「交付申請書」は「様式ダウンロード」ページからダウンロードしてください。<br><a href="" class="c-link">「様式ダウンロード」に行く</a></td>
                </tr>
              </table>
          </section>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■その他の支援制度</h2>
          <ol>
            	<li>
                	<h3 class="c-heading-3">(1) チャイルドシート等の無料貸し出し</h3>
                    <p>子どもが生まれてから一定の年齢に達するまでの必需品ですが、成長とともに使えなくなってしまうのがチャイルドシートです。<br>
                    壮瞥町ではお子さんの年齢に合わせて、ベビーシート、チャイルドシート、ジュニアシートの無料貸し出しをしています。</p>
                      <table class="c-table-1">
                        <tr>
                          <th class="__strong">種類</th>
                          <th class="__strong">対象年齢の目安</th>
                          <th class="__strong">備考</th>
                        </tr>
                        <tr>
                          <td class="__weak">ベビーシート</td>
                          <td>0歳～1歳</td>
                          <td rowspan="3">対象年齢の目安は<br>お子さんの体型によって変わります</td>
                        </tr>
                        <tr>
                          <td class="__weak">チャイルドシート</td>
                          <td>2歳～4歳</td>
                        </tr>
                        <tr>
                          <td class="__weak">ジュニアシート</td>
                          <td>5歳～6歳</td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) ブックスタート事業</h3>
                    <p>0歳の赤ちゃんから本を読んであげることによる「ことばかけ」の行為から親子のきずなとコミュニケーションを深め、絵本を通して赤ちゃんと保護者が楽しい時間を分かち合うことを応援するため、壮瞥町で絵本や読み聞かせアドバイス集などの入ったブックスタートパックを<br>プレゼントしています。
                    </p>
                </li>
                <li>
                	<h3 class="c-heading-3">(3) 子育て世帯臨時特例給付金</h3>
                     <p>平成26年4月から消費税が8％に引き上げられたことに伴い、子育て世帯への影響を緩和するための、暫定的・臨時的な給付措置です。</p>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w25">給付対象者</th>
                      <td>平成27年6月分の児童手当の受給者であり、<br>平成27年5月31日時点で中学校修了前の児童が対象です（特例給付は除く）。</td>
                    </tr>
                    <tr>
                      <th class="__strong">給付額</th>
                      <td>対象児童一人につき3,000円</td>
                    </tr>
                    <tr>
                      <th class="__strong">申請期間</th>
                      <td>平成27年6月2日～10月30日<br><span class="c-annotation__no">※期間経過後の申請はご相談ください</span></td>
                    </tr>
                  </table>
                </li>
                <li>
                	<h3 class="c-heading-3">(4) 出産育児一時金</h3>
                     <p>妊娠4ヵ月（85日）以上の方が出産したときは、一児につき42万円（産科医療補償制度の対象外となる出産の場合は39万円（平成27年1月1日以降の出産は40.4万円））
                     出産育児一時金が支給されます。<br>手続きにつきましては、ご加入の医療保険者の窓口、または出産される病院などにご確認ください<br>
                     <span class="c-annotation__no">※産科医療補償制度とは、分娩に関連して重度脳性麻痺となった赤ちゃんが速やかに補償を受けられる制度で、
                     分娩を取り扱う医療機関等が加入する制度です。</span>
                     </p>
                </li>
                <li>
                	<h3 class="c-heading-3">(5) 児童扶養手当</h3>
                     <p>母子・父子家庭等で、18歳以下（18歳に達した年度末まで）の児童（一定の障害がある時は20歳未満）を養育されている方に支給されます。</p>
                </li>
                <li>
                	<h3 class="c-heading-3">(6) 特別児童扶養手当</h3>
                     <p>特別児童扶養手当は、児童の健やかな成長を願って、身体や精神に中程度以上の障がいのある児童を監護している父もしくは母、<br>
                     または父母に代わってその児童を養育している方に対して支給される手当です。</p>
                </li>
            </ol>
        </div>
      </div>
      




      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
