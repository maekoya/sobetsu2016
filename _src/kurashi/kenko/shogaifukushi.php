<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">健康・福祉</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">健康・福祉</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">障害者福祉</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">障害者福祉</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■手帳の交付</h2>
            <p>身体に障害のある人には｢身体障害者手帳｣、知的障害のある人には｢療育手帳｣がそれぞれ交付されます。 これらの手帳を持っていると障害の程度によってさまざまな支援制度を受けることができます。手帳の交付を受けるには、主治医と相談のうえ、役場窓口にて申請が必要です。</p>
            <table class="c-table-1">
            	<tr>
                	<th class="__strong u-w30">申請に必要なもの</th>
                    <td>医師意見書、印鑑、写真</td>
                </tr>
           </table>
            <ol>
            	<li>
                	<h3 class="c-heading-4">(1) 身体障害者手帳</h3>
                    <p>身体障害者手帳とは、身体に一定の障害を持つ人に対して、身体障害者福祉法に基づき、その自立を援護するために交付されるものです。<br>
                    この身体障害者手帳を所持することにより、各種のサｰビスを受けることができるようになります。
                    交付の対象となる障害は、視覚、聴覚、平衡機能、音声・言語機能・そしゃく機能、肢体、心臓機能、じん臓機能、呼吸器機能、ぼうこう・直腸機能、小腸機能、免疫機能、肝臓機能の障害で、程度により1級から7級まであります。(7級は手帳交付されません。)</p>
                </li>
            	<li>
                	<h3 class="c-heading-4">(2) 療育手帳</h3>
                    <p>療育手帳とは、知的障害児・者に対して一貫した指導・相談を行うとともに、各種の援助制度を受けやすくするために交付される手帳です。 
                    障害の程度は、｢A｣または｢B｣で記載され、｢A｣は重度、｢B｣は中・軽度の障害に該当します。</p>
                </li>
            	<li>
                	<h3 class="c-heading-4">(3) 精神障害者保健福祉手帳</h3>
                    <p>精神障害者保健福祉手帳とは、精神障害のため長期にわたり日常生活または社会生活への制約がある方に、その自立と社会参加の促進を図るために交付されるものです。 
                    この精神障害者保健福祉手帳を所持することにより、各種のサｰビスを受けることができます。<br>障害の等級は、1級から3級までです。</p>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■障害福祉サービス</h2>
            <p>障害福祉は、障がいのある方が日常生活に必要な支援が受けられる「介護給付」と、自立した生活に必要な知識や技術を身につける「訓練等給付」の2つに大きく分けられます。
            サービスの利用については役場住民福祉課福祉係までお問い合わせください。<br>
            <span class=" u-bold">【問い合わせ先】</span><br>壮瞥町役場住民福祉課福祉係　TEL  <a href="tel:0142-66-2121">0142-66-2121</a></p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■地域生活支援事業</h2>
            <p>障害者総合支援法に基づくサｰビスで、地域生活支援事業は、市町村が自らサｰビスの内容や基準を定めています。<br>詳細は役場住民福祉課福祉係までお問い合わせください。</p>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 地域活動支援センター・ノンノ</h3>
                     <div class="u-clearboth">
                        <div class="u-grid__col-4">
                          <img src="images/shogaifukushi/img01.jpg" width="" height="" alt="">
                        </div>
                        <div class="u-grid__col-8">
                          <p>地域活動支援センターは、障がいを抱かえている方や社会参加に壁を感じている方が、地域の中で安心して自立した生活を送っていただけるように、
                          様々な活動を通して応援している施設で、創作的活動や生産活動の機会の提供のほか、社会との交流の促進などの事業を行っています。
                          壮瞥町ではNPO法人サポートセンターたつかーむに運営を委託しています。<br>
                          <a href="http://www.tatukam.org/" class="c-link__blank">地域活動支援センター・ノンノ</a></p>
                        </div>
                          <table class="c-table-1">
                            <tr>
                              <th class="__strong u-w30">住所</th>
                              <td>壮瞥町滝之町242</td>
                            </tr>
                            <tr>
                              <th class="__strong">電話番号 / FAX</th>
                              <td><a href="tel:0142-66-2588">0142-66-2588</a></td>
                            </tr>
                            <tr>
                              <th class="__strong">開館日時</th>
                              <td>月曜日～金曜日（土・日及び国民の祝日は休み）の9:30-16:00</td>
                            </tr>
                          </table>
                      </div>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) コミュニケｰション支援</h3>
                    <p>聴覚障害等で意思疎通を図ることに支障がある方に、意思疎通を仲介する手話通訳者等の派遣等を行います。</p>
                    <table class="c-table-1">
                        <tr>
                            <th class="__strong u-w30">利用者負担</th>
                            <td>無料</td>
                        </tr>
                   </table>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■重度心身障害者医療費助成</h2>
            <p>身体や精神に障害のある人は、一割または初診時一部負担金のみで受診できます。</p>
            <table class="c-table-1 c-td__left">
            	<tr>
                	<th class="__strong u-w30">対象となる人</th>
                    <td>・身体障害の認定者で、1級、2級又は3級で心臓、腎臓もしくは呼吸器または膀胱もしくは直腸、小腸、
                    ヒト免疫不全ウイルスによる免疫もしくは肝臓の機能の障害に該当している人<br>
                    ・医療機関などにおいて重度の知的障害と判定、診断された人<br>
                    ・精神障害者保健福祉手帳１級の交付を受けた人（通院のみ）</td>
                </tr>
           </table>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■自立支援医療費（更生医療・育成医療）</h2>
            <p>身体障害児・者の機能障害を除去したり軽減することで、職業能力を増進して日常生活を容易にするなどの更生がはかられるよう、
            医学的処理、薬剤、治療材料などの更生医療の給付を行います。</p>
            <table class="c-table-1">
            	<tr>
                	<th class="__strong u-w30">申請に必要なもの</th>
                    <td>自立支援医療費意見書、印鑑、障害者手帳</td>
                </tr>
            	<tr>
                	<th class="__strong">利用者負担</th>
                    <td>1割（所得に応じ、利用者負担上限月額を設けています。）</td>
                </tr>
           </table>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■じん臓機能障害者通院福祉手当</h2>
            <p>人工透析を受けている人の通院に要する費用の一部を助成します。</p>
            <table class="c-table-1">
            	<tr>
                	<th class="__strong u-w30">申請に必要なもの</th>
                    <td>通院証明、印鑑</td>
                </tr>
           </table>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■NHK料金割引申請の受付</h2>
            <p>各種障害者手帳をお持ちの方がいる世帯で世帯員全員が町民税非課税の場合や、障害等級が重度の方が世帯主で受信契約している場合はNHK料金の割引を受けることができます。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■有料道路における障害者割引申請の受付</h2>
            <p>身体障害者手帳の交付を受けている方本人が運転する場合、重度の身体障害者手帳又は療育手帳の交付を受けている方が乗車する車を運転する場合は、
            有料道路料金の割引を受けることが出来ます。割引を受けるためには、事前の登録が必要です。</p>
            <table class="c-table-1">
            	<tr>
                	<th class="__strong u-w30">申請に必要なもの</th>
                    <td>障害者手帳、印鑑、自動車検査証、運転免許証、<br>ETCを利用される場合は障害者本人名義のETCカード、<br>
                    ETC車載器セットアップ申込み書など車載器管理番号が確認できるもの</td>
                </tr>
           </table>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■壮瞥町障がい者就労施設等からの物品等の調達方針</h2>
            <p>障がいのある方が就労によって地域において経済的に自立し、安定した生活を送るため、国や市町村等が物品等を調達する際に、障害者就労施設等から優先的に購入することを推進するために制定された、「国等による障害者就労施設等からの物品等の調達の推進等に関する法律」に基づき、壮瞥町の障害者就労施設等からの物品等の調達の推進を図るための方針を策定しましたので公表します。</p>
            <p><span class=" u-bold">ダウンロード</span><br>
            <a href="" class="c-link__pdf">平成27年度方針DL/PDF</a></p>
        </div>
      </div>



      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
