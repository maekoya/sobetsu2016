<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">健康・福祉</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">健康・福祉</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">お母さんと子どもの保健</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">お母さんと子どもの保健</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
            <div class="u-box_border">
              <p class="u-mt__0 u-mb__small">子育て支援センター・育児サークル、保育所、児童クラブ、児童手当、医療費助成制度などについては
              「子育て支援」のページを<br>ご覧ください。</p>
              <p class="u-mb__0 u-mt__0"><a href="/kurashi/kenko/kosodateshien.php" class="c-link">「子育て支援」ページを見る</a></p>
             </div>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■妊娠・出産</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 母子健康手帳</h3>
                    <p>妊娠がわかったら保健センターにお越しください。<br>
                    母子保健手帳と妊婦健診の受診券をお渡ししながら、お母さんの体調やこれからの生活についてお話を伺い、サポートします。</p>
                    <p><strong>必要なもの</strong><br>
                    病院等が発行した妊娠届出書、印鑑<br>
                    <span class="c-annotation__no">※準備の都合がありますので来所前にお電話をいただけると助かります。</span>
                    </p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) 妊婦の健康診査</h3>
                    <p>母子健康手帳の交付申請の際に、妊婦さん全員（壮瞥町に住所を有する方）に定期健康診査14回分と超音波検査11回分の受診票もお渡ししています。
                    この受診票を使用すると無料で健康診査を受けることができます。<br>
                    ただし、医療機関で独自で実施している検査については自己負担となります。<br>
                    <span class="c-annotation__no">※受診票は、道内の医療機関のみ有効です。里帰りなどで道外の医療機関での妊婦健診・出産を予定されている方は里帰り前にご相談ください。<br>既に里帰りされている場合も、ご連絡ください。</span>
                    </p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(3) 赤ちゃん訪問</h3>
                    <p>赤ちゃんが生まれたら、保健師が訪問し、赤ちゃんが元気に成長しているか、お母さんが育児で悩んでいないかお話を聞きながら、<br>
                    これからの生活についてアドバイスします。</p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(4) 離乳食訪問・電話</h3>
                    <p>生後5か月から始まる離乳食について、管理栄養士が訪問または電話をし、進め方についてアドバイスします。</p>
                </li>
            </ol>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■健診・予防接種</h2>
            <ol>
            	<li>
                	<h3 class="c-heading-3">(1) 乳幼児健診</h3>
                    <p>4カ月、7か月、12カ月、1歳6か月、2歳、3歳の節目に健康診査を実施しています。年6回、奇数月の月曜日に実施しています。<br>
                    対象のお子さんには案内を送ります。</p>
                    <p class=" u-bold u-mb__small">平成28年度のスケジュール</p>
                      <table class="c-table-1 u-mt__0">
                        <tr>
                          <th class="__strong u-w20">日程</th>
                          <td>5月16日（月）、7月4日（月）、9月26日（月）、11月7日（月）、1月30日（月）、3月6日（月）</td>
                        </tr>
                        <tr>
                          <th class="__strong">時間・場所</th>
                          <td>いずれも受付時間は12:30-、会場は壮瞥町保健センター</td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(2) フッ素塗布・洗口</h3>
                    <p>お子さんの歯を強くし、むし歯を予防するため、1歳6か月～3歳11ヵ月のお子さんに年3回、フッ素塗布を実施しています。<br>
                    4歳からは自宅で簡単にむし歯予防ができるフッ素洗口をお勧めしています。</p>
                    <p class=" u-bold u-mb__small">平成28年度のスケジュール</p>
                      <table class="c-table-1 u-mt__0">
                        <tr>
                          <th class="__strong u-w20">日程</th>
                          <td>6月22日（水）、10月19日（水）、2月22日（水）</td>
                        </tr>
                        <tr>
                          <th class="__strong">時間・場所</th>
                          <td>いずれも受付時間は9:30-11:00、会場は壮瞥町保健センター</td>
                        </tr>
                      </table>
                </li>
            	<li>
                	<h3 class="c-heading-3">(3) 法定予防接種</h3>
                    <p>お子さんを病気から守るため、定期予防接種の時期が近づきましたら案内文と問診票を送付しています。</p>
                      <table class="c-table-1">
                        <tr>
                          <th class="__weak">麻疹・風疹混合、ヒブ・肺炎球菌、四種混合・水痘・日本脳炎</th>
                          <td><span class=" u-bold">会場</span><br>そうべつ温泉病院</td>
                        </tr>
                        <tr>
                          <th class="__weak">BCG</th>
                          <td><span class=" u-bold">会場</span><br>インター通り小児科</td>
                        </tr>
                      </table>
                     <p>日程はこちらをご覧ください　<a href="" class="c-link__pdf">平成28年度保健センター年間行事予定DL/PDF（作成中）</a></p>
                </li>
            	<li>
                	<h3 class="c-heading-3">(4) 法定外予防接種</h3>
                    <p>法定外予防接種については、町独自の助成制度があります。詳しくは保健センターまでお問合せください。</p>
                      <table class="c-table-1">
                        <tr>
                          <th class="__strong">インフルエンザ</th>
                          <td>対象は生後6か月から就学前まで。<br>全額を町が負担します</td>
                          <td rowspan="2"><strong>申請時に必要なもの</strong><br>・予防接種を受けた領収書<br>・振込先の通帳<br>・印鑑</td>
                        </tr>
                        <tr>
                          <th class="__strong">おたふくかぜ<br>みずぼうそう</th>
                          <td>対象は生後12か月から就学前まで。<br>半額を町が負担します</td>
                        </tr>
                      </table>
                </li>
            </ol>
        </div>
      </div>
      




      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
