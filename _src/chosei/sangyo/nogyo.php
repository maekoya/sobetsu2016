<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">農林業</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">農林業</h1>

     <div class="u-grid__row">
        <div class="u-grid__col-4">
          <img src="images/nogyo_img01.jpg" width="" height="" alt="物産展">
        </div>
        <div class="u-grid__col-8">
          <p>太陽と地熱。壮瞥町は地球のエネルギーにあふれています。壮瞥町の農業は温暖な気候や火山の恵みである肥沃な土地を生かし、水稲、畑作、施設栽培、果樹園、畜産など多種多様な農業が行われているのが特徴です。</p>
          <p>肥沃な大地のもとで手間を惜しまずに、主要道路の沿道を包むように広がる果樹園では、サクランボ、いちご、ぶどう、りんご・・・春から秋まで収穫の実りを絶やしません。壮瞥町では、シーズンを通して何らかの果物を味わうことができ、果物狩り体験もできる観光果樹園に多くの人が訪れています。</p>
        </div>
      </div>

     <div class="u-grid__row">
            <div class="u-grid__col-6">
              <p>壮瞥町では恵まれた地熱エネルギーを活かした施設農業が盛んで、<br>トマトなどの野菜のほか、花卉栽培も季節を問わず、「旬」を収穫しています。</p>
              <p>安全・安心でおいしい低タンパク米づくりに挑戦する「壮瞥町こだわり米を作ろう会」では、独自の厳しい品質基準（タンパク含有率等）を設け、
              基準を満たしたお米だけを「そうのみのり」としてお届けしています。減農薬・化学肥料抑制による、コメ本来の旨みとねばりが特徴です。</p>
                <p>道の駅そうべつ情報館にある農産物直売所サムズには、丹精込めて育てられた新鮮で安心、安全な壮瞥産の農産品が常時陳列され、熱心なファンに喜ばれ、確実に支持者を広げています。</p>
            </div>
            <div class="u-grid__col-6">
              <img src="images/nogyo_img02.jpg" width="" height="" alt="地熱ハウス">
            </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■新規就農者・農業後継者支援制度</h2>
          <h3 class="c-heading-3">壮瞥町における就農者への支援について</h3>
          <p>壮瞥町では、農家戸数の減少や農業経営者の高齢化の進行など、担い手不足が深刻な状況にある中、壮瞥町内で新たに農業を営む者及び親の農業経営を継承する者に対して積極的に支援を行うことにより、就農のさらなる推進を図り、本町農業の安定的な発展と活力ある農村社会の構築を目指すため、就農者に対して次のとおり支援を行います。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__pdf" target="_blank">就農者支援 DL/PDF</a><br>
          </p>

          <table class="c-table-1 u-mb__0">
            <tr>
              <th class="__strong">助成金等名</th>
              <th class="__strong">助成金等交付の基準</th>
              <th class="__strong">助成金等交付の対象経費</th>
              <th class="__strong">助成金等交付の期間</th>
              <th class="__strong">対象者</th>
            </tr>
            <tr>
              <td class="__weak" rowspan="3">就農助成金</td>
              <td>A.農用地の取得に対する助成</td>
              <td>年50万円を限度 </td>
              <td>就農開始時から1年以内</td>
              <td>就農研修を修了した<br>新規就農者</td>
            </tr>
            <tr>
              <td>B. 農用地の賃借料に対する助成</td>
              <td>1/2以内で<br>年10万円を限度</td>
              <td>就農開始時から5年間</td>
              <td>就農研修を修了した新規就農者</td>
            </tr>
            <tr>
              <td>c. 農業用施設及び機械等の取得に対する助成</td>
              <td>年200万円を限度</td>
              <td>就農開始時から1年以内</td>
              <td>就農研修を修了した新規就農者及び就農後継者</td>
            </tr>
            <tr>
              <td class="__weak">受入農家指導謝金</td>
              <td>就農研修の受入指導農家に対する謝金</td>
              <td>月額1万円以内</td>
              <td>就農研修開始時から<br>1年以内</td>
              <td>就農研修者(就農後継者)を受入れ、指導する農家</td>
            </tr>
          </table>
          <p class="c-annotation__no">※就農開始から5年以上営農を継続しなかった場合は、 就農支援助成金を返還しなければなりません。<br>
          ※就農助成金のうちA・Bは選択制。</p>

          <table class="c-table-1">
            <tr>
              <th class="__strong">資金名</th>
              <th class="__strong">資金の用途</th>
              <th class="__strong">資金貸付の基準</th>
              <th class="__strong">資金貸付の期間</th>
              <th class="__strong">対象者</th>
            </tr>
            <tr>
              <td class="__weak" rowspan="2">就農研修資金</td>
              <td rowspan="2">就農研修経費に対する資金貸付</td>
              <td>月額8万円</td>
              <td>就農研修開始時から1年以内</td>
              <td>就農研修者(新規就農者)</td>
            </tr>
            <tr>
              <td>月額6万5千円以内 </td>
              <td>就農研修開始時から1年以内</td>
              <td>就農研修者(就農後継者)</td>
            </tr>
          </table>
          
          <table class="c-table-1">
            <tr>
              <th class="__strong u-w20">様式等</th>
              <td>「認定申請書」は「様式ダウンロード」からダウンロードしてください。<br>
	              <a href="" class="c-link">「様式ダウンロード」に行く</a>
              </td>
            </tr>
          </table>
          
          <section>
              <h3 class="c-heading-3">就農者の定義</h3>
              <ul class="p-list__mb10">
              	<li><strong>新規就農者とは</strong><br>
                農業以外の産業に従事(学生を含む)し、町内に就農を希望する年齢が18歳以上56歳未満で、配偶者または18歳以上60歳未満の同居の親族を有している方。</li>
                <li><strong>就農後継者とは</strong><br>農業以外の産業に従事(学生を合む)し、親が町内で農業を営み、その経営を継承することが確実と見込まれる年齢が18歳以上46歳未満の方。</li>
                <li><strong>就農研修者とは</strong><br>農業者資格を得るため一定期間農業研修を受ける新規就農者及び就農後継者。</li>
              </ul>
              <p class="c-annotation">各項目すべて壮瞥町に住所を有していることが条件となります。</p>
          </section>
        </div>
      </div>




      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■農林業関係政策・計画</h2>
          <p>農林業関係政策・計画について、詳しくは「政策・計画」をご覧ください。<br>
          <a href="pdf/" class="c-link">「政策・計画」に行く</a></p>
        </div>
      </div>



      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■農業委員会の組織と活動</h2>
          <section>
              <h3 class="c-heading-3">(1) 沿革</h3>
              <p>農業委員会は、地方自治法第180条の5第3	項によって市町村に設置が義務づけられている行政機関で、<br>
              公職選挙法を準用した選挙によって選ばれた農業委員を中心に構成される行政委員会です。</p>
          </section>
          <section>
              <h3 class="c-heading-3">(2) 構成（委員の構成）</h3>
              <table class="c-table-1 u-mb__0">
                <tr>
                  <th class="__strong">区分</th>
                  <th class="__strong">委員数</th>
                  <th class="__strong">合計</th>
                </tr>
                <tr>
                  <td>公選（選挙による委員）</td>
                  <td>7名</td>
                  <td rowspan="2">10名</td>
                </tr>
                <tr>
                  <td>選任<br>1号委員（農業協同組合選任委員）<br>1号委員（農業共済組合選任委員）<br>2号委員（議会選任委員）</td>
                  <td>各1名</td>
                </tr>
              </table>
              <p class="u-mt__small">会長 南 和孝（平成26年7月25日互選）<br>
              会長職務代理者 岩倉 隆（平成26年7月25日互選）<br>
              北海道農業会議会議員 南 和孝（平成26年7月25日互選）
              </p>
          </section>
          <section>
              <h3 class="c-heading-3">(3) 名簿</h3>
              <table class="c-table-1">
                <tr>
                  <th class="__strong">議席番号</th>
                  <th class="__strong">選出区分</th>
                  <th class="__strong">氏名</th>
                  <th class="__strong">年齢</th>
                  <th class="__strong">当選・選任回数</th>
                  <th class="__strong">認定農業者</th>
                </tr>
                <tr>
                  <td>1</td>
                  <td>選挙</td>
                  <td><ruby>堀口 英男<rt>ほりぐち ひでお</rt></ruby></td>
                  <td>50</td>
                  <td>2</td>
                  <td>○</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>選挙</td>
                  <td><ruby>杉村 和男<rt>すぎむら かずお</rt></ruby></td>
                  <td>64</td>
                  <td>3</td>
                  <td>○</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>選挙</td>
                  <td><ruby>伊貸 史吉<rt> いかし ふみよし</rt></ruby></td>
                  <td>41</td>
                  <td>3</td>
                  <td>○</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>選任共済</td>
                  <td><ruby>松本 敏春<rt>まつもと としはる</rt></ruby></td>
                  <td>51</td>
                  <td>2</td>
                  <td>○</td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>選挙</td>
                  <td><ruby>清水 藤男<rt>しみず ふじお</rt></ruby></td>
                  <td>77</td>
                  <td>6</td>
                  <td>○</td>
                </tr>
                <tr>
                  <td>6</td>
                  <td>選任農協</td>
                  <td><ruby>清水 俊一<rt>しみず しゅんいち</rt></ruby></td>
                  <td>60</td>
                  <td>2</td>
                  <td>○</td>
                </tr>
                <tr>
                  <td>7</td>
                  <td>選任議会</td>
                  <td><ruby>関 昭博<rt>せき あきひろ</rt></ruby></td>
                  <td>54</td>
                  <td>2</td>
                  <td>○</td>
                </tr>
                <tr>
                  <td>8</td>
                  <td>選挙</td>
                  <td><ruby>毛利 文康<rt> もうり のりやす</rt></ruby></td>
                  <td>60</td>
                  <td>3</td>
                  <td>○</td>
                </tr>
                <tr>
                  <td>9</td>
                  <td>選挙</td>
                  <td><ruby>岩倉 隆<rt>いわくら たかし</rt></ruby><br>（会長職務代理者）</td>
                  <td>63</td>
                  <td>3</td>
                  <td>○</td>
                </tr>
                <tr>
                  <td>10</td>
                  <td>選挙</td>
                  <td><ruby>南 和孝<rt>みなみ かずたか</rt></ruby><br>（会長）</td>
                  <td>48</td>
                  <td>5</td>
                  <td>○</td>
                </tr>
              </table>
          </section>

          <section>
              <h3 class="c-heading-3">(4) 仕事・役割</h3>
              <ul>
              	<li>ア）農地を農地以外の用途にするための届出の受理や許可</li>
                <li>イ）農地のままの売買、貸し借り許可</li>
                <li>ウ）農業者の声を政策に反映するための建議</li>
                <li>エ）その他農地法、納税猶予制度、国有農地、年金等の事務農業委員は農業者の代表として、農業の普及推進を図るほか、各地域の農業者の立場に立って要望や悩みに応えていく役割も担っています。</li>
              </ul>
          </section>

          <section>
              <h3 class="c-heading-3">(5) 農地法第3条許可事務</h3>
              <p class=" u-mb__small">農業委員会では、農地法第3条許可事務（農地の売買、贈与、貸借等の許可）について、下記のとおり役場事務室に備え付けていますので、<br>
              ご利用の方は担当者までお問い合わせください。なお、申請書受付締切日は、毎月10日（当該日が休日の場合は前日）までとなっております。
              農業委員会では、この事務に係る処理について、申請書受付から許可決定までの標準処理期間を15日と定め、迅速な事務処理による行政サービスの向上に努めています。</p>
              <ul>
              	<li>ア）「農地の売買、贈与、貸借等の許可（農地法第3条）」（許可のポイント、申請から許可までの流れ）</li>
                <li>イ）「申請書記入マニュアル」</li>
                <li>ウ）「必要書類一覧」</li>
                <li>エ）「必要書類チェックリスト」</li>
                <li>オ）「申請書受付のお知らせ」</li>
              </ul>
          </section>

          <section>
              <h3 class="c-heading-3">(6) 定例総会開催予定</h3>
              <p>申請内容を審議する総会の開催予定日については、役場事務室に備え付けておりますので、担当者までお問い合わせください。</p>
          </section>

          <section>
              <h3 class="c-heading-3">(7) 別段の面積（下限面積）の設定</h3>
              <p>農業委員会では、管内の別段の面積（下限面積）を次のように定めています。<br>
              ・地域 全域<br>・下限面積 2ヘクタール
              </p>
              <p><h4 class="u-mb__0 u-bold">別段の面積（下限面積）設定理由</h4>
              別段の面積（下限面積）を基準（2ヘクタール）より小さくするための基準である「別段の面積（下限面積）以下の農家が全農家の40％を下回らない」とする項目について
              該当しないため。</p>
          </section>

          <section>
              <h3 class="c-heading-3">(8) 農地パトロール（農地利用状況調査含む）実施のお知らせ</h3>
              <p>農業委員会では、監視による無断転用禁止、農地法許可案件の履行状況確認、遊休農地の把握に努めるため、<br>
              農地法第30条に基づく農地の利用状況調査も併せた農地パトロールを11月に行います。ご協力をお願いします。</p>
          </section>          
        </div>
      </div>


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
