<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">商工業・企業誘致</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">商工業・企業誘致</h1>

     <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">商工業・特産品等開発支援</h2>
            <p>壮瞥町では以下のような支援制度を行っています。ただし、応募受付は毎年定める時期のみです。<br>広報等でお知らせしますのでご確認ください。</p>
            
            <section>
            	<h3 class="c-heading-3">(1) 起業化支援</h3>
                <p>壮瞥町において新たに事業活動を行う方や新規分野での事業活動を行う方が対象です</p>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">対象者</th>
                      <td>町長が認定した起業化計画を実行する個人、団体及び中小企業者ただし、新規の事業活動開始後３年を経過していない方<br>
                      <span class="c-annotation">本奨励金の交付を受けるにあたり、起業化計画の作成、提出をいただき審査委員会で審査を行います。</span></td>
                    </tr>
                    <tr>
                      <th class="__strong">内容</th>
                      <td>・工事請負費：建物等の建築、改修（造成、整備は除く）<br>・財産購入費：1品20万円以上の機械、器具、※事業用車両購入費<br>・委 託 料：工事設計費<br>
                      <span class="c-annotation">事業専用車両とは商工業専用の設備を有した特殊な車両とし、専ら日常生活用と思われる車両は対象外です。</span></td>
                    </tr>
                    <tr>
                      <th class="__strong">補助額</th>
                      <td>対象経費の2分の1以内、上限100万円</td>
                    </tr>
                    <tr>
                      <th class="__strong">問合せ</th>
                      <td>役場商工観光課 <a href="tel:0142664200">TEL 0142(66)4200</a></td>
                    </tr>
                    <tr>
                      <th class="__strong">様式等</th>
                      <td>「起業化計画書」は「様式ダウンロード」からダウンロードしてください。<br>
                      <a href="#" class="c-link">「様式ダウンロード」に行く</a>
                      </td>
                    </tr>
                  </table>
            </section>

            <section>
            	<h3 class="c-heading-3">(2) 特産品開発助成</h3>
                <p>地域の資源等を活かした特産品開発及びその販売を促進し、地域の活性化を図ることを目的としています。</p>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">対象者</th>
                      <td>壮瞥町内に事業所の所在地を有する法人、<br>その他団体又は壮瞥町内に住所を有する個人で以下のすべてに該当する方<br>
                      ①壮瞥町で農林水産業又は商工業の事業を行っている方又は新規に行う方<br>②同一年度内にこの補助制度を使用していない方等町内に事業所を有する法人の方々等</td>
                    </tr>
                    <tr>
                      <th class="__strong">内容</th>
                      <td>以下の事業に要する経費に対して補助します。<br>
                      ①特産品開発推進事業<br>
                      ・特産品開発に向けた人材育成に要する経費（例：研修会講師謝金、研修会参加負担金、資料購入費等）<br>
                      ・コンクール及び試食会、各種イベントへの参加に要する経費<br>（例：原材料費、副資材購入費、イベント参加費等）<br>
                      ②特産品開発実施事業<br>
                      ・特産品及びそのデザインの開発、改良に要する経費（例：デザイン開発料、広告宣伝費、成分分析費等）<br>
                      ・特産品の流通及び販路開拓に要する経費（例：機器のレンタルリース料、技術コンサルタント委託料等）<br>
                      ・地域資源を活かした地特産品開発を行うための人材育成やデザインの開発等、販路開発に要する経費に対して補助</td>
                    </tr>
                    <tr>
                      <th class="__strong">補助額</th>
                      <td>対象経費の2分の1以内、上限100万円</td>
                    </tr>
                    <tr>
                      <th class="__strong">問合せ</th>
                      <td>役場商工観光課 <a href="tel:0142664200">TEL 0142(66)4200</a></td>
                    </tr>
                    <tr>
                      <th class="__strong">様式等</th>
                      <td>「事業計画書」、「収支予算書」は「様式ダウンロード」からダウンロードしてください。<br>
                      <a href="#" class="c-link">「様式ダウンロード」に行く</a>
                      </td>
                    </tr>
                  </table>
            </section>


            <section>
            	<h3 class="c-heading-3">(3) 農商工連携</h3>
                <p>本町の基幹産業である農業と商工業及び観光業が連携・協力して取り組む、本町の資源や特性を活かした特産品の開発及びその販売を支援する補助事業です。</p>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">対象者</th>
                      <td>農業者及び商工業者それぞれ各2名(団体)以上(計4名(団体)以上)をもって組織する団体</td>
                    </tr>
                    <tr>
                      <th class="__strong">内容</th>
                      <td>特産品の開発に向けた人材育成、試作、イベント等への参加、試食会、発表会、普及拡大、広告宣伝、デザインの開発、流通及び販路開拓、
                      先進地の視察研修に要する経費、事業に関連する備品購入費(汎用性のないもの) </td>
                    </tr>
                    <tr>
                      <th class="__strong">補助額</th>
                      <td>対象経費の4分の5以内　上限額200万円、下限額50万円</td>
                    </tr>
                    <tr>
                      <th class="__strong">問合せ</th>
                      <td>役場商工観光課 <a href="tel:0142664200">TEL 0142(66)4200</a></td>
                    </tr>
                    <tr>
                      <th class="__strong">様式等</th>
                      <td>「認定申請書」、「事業計画書」、「収支予算書」は「様式ダウンロード」からダウンロードしてください。<br>
                      <a href="#" class="c-link">「様式ダウンロード」に行く</a>
                      </td>
                    </tr>
                  </table>
            </section>


            <section>
            	<h3 class="c-heading-3">(4) 商工業活性化</h3>
                <p>壮瞥町内で３年以上事業所をおいて商工業を営んでいる方を対象としています。<br>事業所の改修等をご検討されている方は本制度をぜひご活用ください。</p>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">対象者</th>
                      <td>町内で3年以上事業所をおいて商工業を営んでいる方</td>
                    </tr>
                    <tr>
                      <th class="__strong">内容</th>
                      <td>・工事請負費：建物等の建築、改修（造成・整備は除く）<br>
                      ・財産購入費：建物等購入費（用地取得費は除く）<br>1品20万円以上の機械・器具・事業専用、車両購入費<br>
                      <span class="c-annotation">事業専用車両は専ら日常生活用と思われる車両は対象外です。</span><br>
                      ・委託料：工事設計費</td>
                    </tr>
                    <tr>
                      <th class="__strong">補助額</th>
                      <td>対象経費の2分の1（3分の1）以内、上限200万円（100万円）<br><span class="c-annotation">カッコ内は以前に採択を受けた事業者の方</span></td>
                    </tr>
                    <tr>
                      <th class="__strong">問合せ</th>
                      <td>役場商工観光課 <a href="tel:0142664200">TEL 0142(66)4200</a><br>壮瞥町商工会 <a href="tel:0142662151">TEL 0142(66)2151</a></td>
                    </tr>
                    <tr>
                      <th class="__strong">様式等</th>
                      <td>申請書類等は商工会にご相談ください</td>
                    </tr>
                  </table>
            </section>

            <section>
              <h3 class="c-heading-3">(5) その他</h3>
              <p>その他の経営支援・融資制度等については壮瞥町商工会までご相談ください。<br>
              <a href="http://www.sobetsu.net/" target="_blank" class="c-link__blank">壮瞥町商工会</a></p>
            </section>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">立地企業の優遇措置</h2>
          <p>壮瞥町では、企業立地の促進を目的として、次のような助成を行っています。</p>
          <p class="c-heading-3">(1) 助成措置：壮瞥町企業立地促進条例に基づく助成</p>

          <section>
              <p class="c-heading-3">(2) 対象事業者</p>
              <table class="c-table-1">
                <tr>
                  <th class="__strong">区 分</th>
                  <th class="__strong">対象要件</th>
                </tr>
                <tr>
                  <td>リゾート施設<br>（テーマパーク、ホテル、スキー場等）</td>
                  <td>新設に要する固定資産の取得価格が3億円以上<br>増設に要する固定資産の取得価格が3千万円以上</td>
                </tr>
                <tr>
                  <td>福祉等施設<br>（社会福祉法人、医療法人が設置する施設で町長が定める施設）</td>
                  <td rowspan="6">新設に要する固定資産の取得価格が5千万円以上<br>増設に要する固定資産の取得価格が2千万円以上</td>
                </tr>
                <tr>
                  <td>工場（物の製造又は加工を行う施設）</td>
                </tr>
                <tr>
                  <td>事業所<br>（電子計算機のプログラム又はシステムの作成を行う施設）</td>
                </tr>
                <tr>
                  <td>試験研究施設<br>（工業製品の開発のための試験若しくは研究又は開発を行う施設）</td>
                </tr>
                <tr>
                  <td>その他町長が特に必要と認めた施設<br>（議会と協議して決定した施設）</td>
                </tr>
              </table>
              </table>
          </section>

          <section>
              <p class="c-heading-3">(3) 助成措置内容</p>
              <table class="c-table-1">
                <tr>
                  <th class="__strong u-w20">区 分</th>
                  <th class="__strong">内 容</th>
                  <th class="__strong">備 考</th>
                </tr>
                <tr>
                  <td class="__weak">施設設置助成</td>
                  <td class="u-text__left">固定資産に対して町が課する固定資産税額相当額<br>（上限2千万円）<br>助成期間は、最初に課する年度から5年間</td>
                  <td class="u-text__left">当該年度の7月31日までに申請書提出が必要です</td>
                </tr>
                <tr>
                  <td class="__weak">用地取得助成</td>
                  <td class="u-text__left">新設のために必要な土地の無償貸付<br>５年間業績が安定していると認めたときは、土地の無償譲渡</td>
                  <td></td>
                </tr>
                <tr>
                  <td class="__weak">その他</td>
                  <td class="u-text__left">町長が必要と認める助成</td>
                  <td></td>
                </tr>
                <tr>
                  <td class="__weak">様式等</td>
                  <td class="u-text__left">「指定申請書」、「事業計画書」は<br>「様式ダウンロード」からダウンロードしてください。<br>
                  <a href="#" class="c-link">「様式ダウンロード」に行く</a></td>
                  <td></td>
                </tr>
              </table>
          </section>

          <section>
              <p class="c-heading-3">(4) 便宜供与</p>
              <table class="c-table-1">
                <tr>
                  <th class="__strong u-w20">便宜供与内容</th>
                  <td class="u-text__left">1．上水道に関する協力　2．関連道路に関する協力　3．温泉水の供給に関する協力<br>4．建物、物品等の貸与に関する協力　5．経営安定資金貸付に関する協力 6．その他町長が必要と認める協力</td>
                </tr>
              </table>
          </section>
        </div>
      </div>



      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
