<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">観光</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">観光</h1>



        <div class="u-grid__row">
            <div class="u-grid__col-6">
              <img src="images/kanko/img01.jpg" width="" height="" alt="">
            </div>
            <div class="u-grid__col-6">
               <img src="images/kanko/img02.jpg" width="" height="" alt="環境景観保全">
            </div>
          </div>          

         <div class="u-grid__row">
            <div class="u-grid__col-4">
              <img src="images/kanko/img03.jpg" width="" height="" alt="昭和新山">
            </div>
            <div class="u-grid__col-8">
              <p>壮瞥町は、支笏洞爺国立公園内にあり、昭和新山、洞爺湖、有珠山など世界的にも誇れるダイナミックかつ優美な自然景観、火山の恵みである洞爺湖温泉、壮瞥温泉、蟠渓温泉の温泉地や町内各地にある日帰り温泉施設、町内を縦貫する国道453号沿いには果物畑が広がり、観光シーズンを通して果物狩りが楽しめるほか、洞爺湖畔沿いには300本の梅林からなる壮瞥公園、洞爺湖を一周する彫刻公園、美しい夕日を楽しめる仲洞爺キャンプ場などもあります</p>
            </div>
          </div>

         <div class="u-grid__row">
            <div class="u-grid__col-8">
              <p>火山のまち・壮瞥町は、火山御エネルギー壮瞥町を含む洞爺湖有珠山ジオパークは、平成21年に世界ジオパークにも認定され、火山エネルギーにより形成された奇跡の山・昭和新山を筆頭とする数々の火山遺構のほか、昭和新山山麓には、昭和新山の生成過程を克明に記録したミマツダイヤグラムなどの貴重な資料を展示する三松正夫記念館、有珠山山頂まで結ぶロープウェイ、熊牧場やガラス館、土産店街など、それだけでも1日満喫できます。</p>
            </div>
            <div class="u-grid__col-4">
              <img src="images/kanko/img04.jpg" width="" height="" alt="">
            </div>
          </div>


         <div class="u-grid__row">
         	<ul class="u-clearboth">
            	<li class="u-grid__col-4"><img src="images/kanko/img05.jpg" width="" height="" alt=""></li>
                <li class="u-grid__col-4"><img src="images/kanko/img06.jpg" width="" height="" alt=""></li>
                <li class="u-grid__col-4"><img src="images/kanko/img07.jpg" width="" height="" alt=""><img src="images/kanko/img08.jpg" width="" height="" alt="" class="u-mt__20"></li>
    </ul>
            <div class="u-grid__col-12">
            <p>その他にも、ファミリーゲレンデが中心のオロフレスキー場、清流・壮瞥滝、横綱北の湖の偉業をたたえる横綱北の湖記念館とパークゴルフ場、洞爺湖の眺めを楽しみながらバーベキューや宿泊を楽しめる森と木の里センター、壮瞥町の旬の野菜や果物の販売、有珠山噴火史の貴重な資料や観光情報発信施設のある道の駅そうべつ情報館ｉ、町民が考案し今や世界に広がる冬のニュースポーツ昭和新山国際雪合戦、大地の実りを大放出するそうべつりんごまつり、春から秋まで毎晩、洞爺湖の夜空を彩る洞爺湖ロングラン花火大会など、壮瞥町は町ごとまるまる観光スポットなのです。</p>
            <p>観光情報の詳細はこちら：<br><a href="(www.sobetsu-kanko.com" class="c-link__blank" target="_blank">そうべつ観光協会ウェブサイトに行く</a></p>
            </div>
            
          </div>



      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
