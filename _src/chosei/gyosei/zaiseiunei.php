<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">行政情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">行財政運営</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">行財政運営</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<p>行政の執行方針、予算・決算、財政状況などに関する情報の一覧です。</p>
        </div>
        
        <section class="u-grid__col-4">
          <h2 class="c-heading-3">予算･主要事業の概要</h2>
          <ul class=" p-list">
          	<li>
            	<h4 class=" u-bold u-mb__0">平成27年度</h4>
                <a href="pdf/" class="c-link__pdf" target="_blank">予算の概要 DL/PDF</a><br>
                <a href="pdf/" class="c-link__pdf" target="_blank">主要事業紹介 DL/PDF</a>
            </li>
          	<li>
            	<h4 class=" u-bold u-mb__0">平成26年度</h4>
                <a href="pdf/" class="c-link__pdf" target="_blank">予算の概要 DL/PDF</a><br>
                <a href="pdf/" class="c-link__pdf" target="_blank">主要事業紹介 DL/PDF</a>
            </li>
          	<li>
            	<h4 class=" u-bold u-mb__0">平成25年度</h4>
                <a href="pdf/" class="c-link__pdf" target="_blank">予算の概要 DL/PDF</a><br>
            </li>
           </ul>
         </section>

		<section class="u-grid__col-4">
          <h2 class="c-heading-3">決算の概要</h2>
          <ul class=" p-list">
          	<li><h4 class=" u-bold u-mb__0">平成26年度</h4>
                <a href="pdf/" class="c-link__pdf" target="_blank">決算の概要DL/PDF</a><br>
            </li>
          	<li><h4 class=" u-bold u-mb__0">平成25年度</h4>
                <a href="pdf/" class="c-link__pdf" target="_blank">平成25年度決算の概要DL/PDF</a><br>
            </li>
          	<li><h4 class=" u-bold u-mb__0">平成24年度</h4>
                <a href="pdf/" class="c-link__pdf" target="_blank">平成24年度決算の概要DL/PDF</a><br>
            </li>
           </ul>
        </section>
        
        <section class="u-grid__col-4">
          <h2 class="c-heading-3">財政状況</h2>
          <ul class=" p-list">
          	<li>
            	<h4 class=" u-bold u-mb__0">平成26年度</h4>
                <a href="pdf/" class="c-link__pdf" target="_blank">財政状況資料集 DL/PDF（作成中）</a><br>
                <a href="pdf/" class="c-link__pdf" target="_blank">決算に基づく健全化判断比率等 DL/PDF</a>
            </li>
          	<li>
            	<h4 class=" u-bold u-mb__0">平成25年度</h4>
                <a href="pdf/" class="c-link__pdf" target="_blank">財政状況資料集 DL/PDF（作成中）</a><br>
                <a href="pdf/" class="c-link__pdf" target="_blank">決算に基づく健全化判断比率等 DL/PDF</a>
            </li>
          	<li>
            	<h4 class=" u-bold u-mb__0">平成24年度</h4>
                <a href="pdf/" class="c-link__pdf" target="_blank">財政状況資料集 DL/PDF（作成中）</a><br>
                <a href="pdf/" class="c-link__pdf" target="_blank">決算に基づく健全化判断比率等 DL/PDF</a>
            </li>
           </ul>
         </section>
      </div>

            






      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
