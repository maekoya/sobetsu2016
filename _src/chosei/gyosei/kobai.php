<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">行政情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">政策・計画</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">インターネット公売</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <p>壮瞥町では、平成24年度から町税の滞納対策を強化し、預金・給与・年金・国税還付金等の債権差押だけではなく、動産や不動産の差押も行っています。差し押さえた動産や不動産をインターネット公売（ヤフー株式会社の「官公庁オークション」を利用したせり売り・入札）を実施して公売しており、換価された代金は滞納町税にあてられます。</p>
          <p class="c-annotation"><span class=" u-bold">インターネット公売に参加するには</span><br>
          Yahoo!JAPAN IDを持っていない方は、まずYahoo!JAPAN ID を取得して、メールアドレスの認証を受けることが必要です。<br>
          さらに、参加申込期間中に参加申込をしないと入札に参加できませんのでご注意ください。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-3">現在インターネット公売をしている財産</h2>
          <p class=" u-bold">こちらでご確認ください<br>
              <a href="http://koubai.auctions.yahoo.co.jp/auction/middle?pid=hok_sobetsu_town&oid=1449018762648900" class="c-link__blank" target="_blank">Yahoo官公庁オークション（壮瞥町）</a><br>
          </p>
        </div>
      </div>
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">インターネット公売に関する様式</h3>
          <p>様式ダウンロード集をご覧ください。</p>
          <p>
              <a href="" class="c-link">「様式ダウンロード」に行く</a>
          </p>
        </div>
      </div>


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
