<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">行政情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">職員採用</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">職員採用</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
      <table class="c-table-1 c-td__left">
        <tr>
          <th class="__strong u-w30">採用職種等</th>
          <td>一般事務若干名（平成28年4月1日採用）</td>
        </tr>
        <tr>
          <th class="__strong">受付期間</th>
          <td>平成27年12月1日（火）から平成27年12月21日（月）までの期間で、<br>午前8時45分から午後5時30分まで受け付けます。（土曜、日曜、祝日は休みです）<br>
          なお、郵送の場合は平成27年12月21日（月）までの消印があるものに限り受け付けます。</td>
        </tr>
        <tr>
          <th class="__strong">採用試験（第1次試験）</th>
          <td>平成28年1月17日（日）午前9時10分（午前8時40分までに集合）<br>
          会場：壮瞥町役場（有珠郡壮瞥町字滝之町287番地7： TEL <a href="tel:0142-66-2121">0142-66-2121</a>）</td>
        </tr>
        <tr>
          <th class="__strong">様式等</th>
          <td><a href="" class="c-link__pdf">採用試験実施要領 DL/PDF</a><br>
          <a href="" class="c-link__pdf">受験申込書 DL/PDF</a></td>
        </tr>
        <tr>
          <th class="__strong">問い合わせ、資料請求<br>応募書類提出先</th>
          <td>壮瞥町役場総務課職員係<br>〒052-0101 北海道有珠郡壮瞥町字滝之町287-7<br>
          電話　<a href="tel:0142662121">0142(66)2121</a><br>
          電子メール <a href="mailto:somu@town.sobetsu.lg.jp">somu@town.sobetsu.lg.jp</a></td>
        </tr>
      </table>
        </div>
     </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	 <h2 class="c-heading-2">壮瞥町の給与・定員管理等について</h2>
             <p>壮瞥町では、職員給与・定員管理の適正化をはじめ行政改革の推進に積極的に取り組んでいるところであり、<br>職員の給与等の状況については、毎年12月の広報に掲載、
             独自に公表してまいりました。<br>総務省では、給与・定員管理等の給与情報について、各地方公共団体間の比較分析を可能とする「公表システム」を平成18年3月に構築しており、
             それに合わせ、壮瞥町では総務省の統一様式による「壮瞥町の給与・定員管理等について」を平成17年度から公表しています。</p>
             <ul class="c-list">
             	<li><a href="" class="c-link__pdf">壮瞥町の給与・定員管理等について（平成26年度）DL/PDF</a></li>
                <li><a href="" class="c-link__pdf">壮瞥町の給与・定員管理等について（平成25年度）DL/PDF</a></li>
                <li><a href="" class="c-link__pdf">壮瞥町の給与・定員管理等について（平成24年度）DL/PDF</a></li>
             </ul>
        </div>
     </div>

            






      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
