<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">行政情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">広報アーカイブ</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">広報</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">広報</h2>
          <p>広報そうべつが平成27年2月号から電子書籍でご覧になれるようになりました。検索エリアを「壮瞥町」を選ぶと電子書籍が検索されます。<br>ぜひこちらもご覧になってください。</p>
          <p><a href="http://www.hokkaido-ebooks.jp/" class="c-link__blank" target="_blank">ホッカイドウ・イーブックス</a></p>
        </div>
      </div>

      <div class="u-grid__row">
       <div class="u-grid__col-12">
       	<h2 class="c-heading-2">広報そうべつ・おしらせそうべつ</h2>
        <h3 class="c-heading-3">今月の広報そうべつ・おしらせそうべつ</h3>
        <p><a href="#" class="c-link__pdf">広報そうべつ2月 2016年No.626＋おしらせそうべつ DL/PDF</a></p>
       </div>
      </div>

     <div class="u-grid__row">
     	<div class="u-grid__col-12">
       		<h3 class="c-heading-3">広報そうべつ・おしらせそうべつのバックナンバー</h3>
        </div>
       	<div class="u-grid__col-6">
         <dl>
         <dt><strong>2016年広報</strong></dt>
         <dd>
          <ul class="c-list u-mt__small">
           <li><a href="/pdf/koho/2016/Kouhou_Sobetsu_2016_1_No_625+Oshirase.pdf" class="c-link__pdf">広報そうべつ1月<br>No.625＋おしらせそうべつ DL/PDF</a></li>
          </ul>
         </dd>
        </dl>
        </div>
        <div class="u-grid__col-6">
            <dl>
             <dt><strong>2015年広報</strong></dt>
             <dd>
              <ul class="c-list u-mt__small">
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_12_No_624+Oshirase.pdf" class="c-link__pdf">広報そうべつ12月<br>No.624＋おしらせそうべつ DL/PDF</a></li>
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_11_No_623+Oshirase.pdf" class="c-link__pdf">広報そうべつ11月<br>No.623＋おしらせそうべつ DL/PDF</a></li>
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_10_No_622+Oshirase.pdf" class="c-link__pdf">広報そうべつ10月<br>No.622＋おしらせそうべつ DL/PDF</a></li>
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_9_No_621+Oshirase.pdf" class="c-link__pdf">広報そうべつ9月<br>No.621＋おしらせそうべつ DL/PDF</a></li>
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_8_No_620+Oshirase.pdf" class="c-link__pdf">広報そうべつ8月<br>No.620＋おしらせそうべつ DL/PDF</a></li>
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_7_No_619+Oshirase.pdf" class="c-link__pdf">広報そうべつ7月<br>No.619＋おしらせそうべつ DL/PDF</a></li>
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_6_No_618+Oshirase.pdf" class="c-link__pdf">広報そうべつ6月<br>No.618＋おしらせそうべつ DL/PDF</a></li>
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_5_No_617+Oshirase.pdf" class="c-link__pdf">広報そうべつ5月<br>No.617＋おしらせそうべつ DL/PDF</a></li>
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_4_No_616+Oshirase.pdf" class="c-link__pdf">広報そうべつ4月<br>No.616＋おしらせそうべつ DL/PDF</a></li>
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_3_No_615+Oshirase.pdf" class="c-link__pdf">広報そうべつ3月<br>No.615＋おしらせそうべつ DL/PDF</a></li>
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_2_No_614+Oshirase.pdf" class="c-link__pdf">広報そうべつ2月<br>No.614＋おしらせそうべつ DL/PDF</a></li>
               <li><a href="/pdf/koho/2015/Kouhou_Sobetsu_2015_1_No_613+Oshirase.pdf" class="c-link__pdf">広報そうべつ1月<br>No.613＋おしらせそうべつ DL/PDF</a></li>
              </ul>
             </dd>
            </dl>
        </div>
      </div>
     <div class="u-grid__row">
       <div class="u-grid__col-6">
        <dl>
         <dt><strong>2014年広報</strong></dt>
         <dd>
          <ul class="c-list u-mt__small">
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_612+Oshirase.pdf" class="c-link__pdf">広報そうべつ12月<br>No.612＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_611+Oshirase.pdf" class="c-link__pdf">広報そうべつ11月<br>No.611＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_610+Oshirase.pdf" class="c-link__pdf">広報そうべつ10月<br>No.610＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_609+Oshirase.pdf" class="c-link__pdf">広報そうべつ9月<br>No.609＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_608+Oshirase.pdf" class="c-link__pdf">広報そうべつ8月<br>No.608＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_607+Oshirase.pdf" class="c-link__pdf">広報そうべつ7月<br>No.607＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_606+Oshirase.pdf" class="c-link__pdf">広報そうべつ6月<br>No.606＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_605+Oshirase.pdf" class="c-link__pdf">広報そうべつ5月<br>No.605＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_604+Oshirase.pdf" class="c-link__pdf">広報そうべつ4月<br>No.604＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_603+Oshirase.pdf" class="c-link__pdf">広報そうべつ3月<br>No.603＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_602+Oshirase.pdf" class="c-link__pdf">広報そうべつ2月<br>No.602＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2014/Kouhou_Sobetsu_2014_12_No_601+Oshirase.pdf" class="c-link__pdf">広報そうべつ1月<br>No.601＋おしらせそうべつ DL/PDF</a></li>
          </ul>
         </dd>
        </dl>
        </div>
        <div class="u-grid__col-6">
        <dl>
         <dt><strong>2013年広報</strong></dt>
         <dd>
          <ul class="c-list u-mt__small">
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_12_No_600+Oshirase.pdf" class="c-link__pdf">広報そうべつ12月<br>No.600＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_11_No_599+Oshirase.pdf" class="c-link__pdf">広報そうべつ11月<br>No.599＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_10_No_598+Oshirase.pdf" class="c-link__pdf">広報そうべつ10月<br>No.598＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_9_No_597+Oshirase.pdf" class="c-link__pdf">広報そうべつ9月<br>No.597＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_8_No_596+Oshirase.pdf" class="c-link__pdf">広報そうべつ8月<br>No.596＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_7_No_595+Oshirase.pdf" class="c-link__pdf">広報そうべつ7月<br>No.595＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_6_No_594+Oshirase.pdf" class="c-link__pdf">広報そうべつ6月<br>No.594＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_5_No_593+Oshirase.pdf" class="c-link__pdf">広報そうべつ5月<br>No.593＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_4_No_592+Oshirase.pdf" class="c-link__pdf">広報そうべつ4月<br>No.592＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_3_No_591+Oshirase.pdf" class="c-link__pdf">広報そうべつ3月<br>No.591＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_2_No_590+Oshirase.pdf" class="c-link__pdf">広報そうべつ2月<br>No.590＋おしらせそうべつ DL/PDF</a></li>
           <li><a href="/pdf/koho/2013/Kouhou_Sobetsu_2013_1_No_589+Oshirase.pdf" class="c-link__pdf">広報そうべつ1月<br>No.589＋おしらせそうべつ DL/PDF</a></li>
          </ul>
         </dd>
        </dl>
        </div>
      </div>

      </div>


      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
