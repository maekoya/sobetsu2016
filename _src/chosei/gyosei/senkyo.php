<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">行政情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">選挙</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">選挙</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■町長・町議会議員選挙</h2>
          <p>平成27年4月21日に告示された、任期満了に伴う壮瞥町長選挙は、現職の佐藤秀敏氏以外の立候補の届け出がなく、無投票での2選が決まりました。
          また、同日に告示された任期満了に伴う壮瞥町議会議員選挙は定数9に対し、立候補者9人の届け出となり、こちらも無投票当選が決まりました。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■選挙人名簿の登録</h2>
          <h3 class="c-heading-3">転入・転出等異動の時には必ず届出をしましょう</h3>
          <p>選挙人名簿は選挙権のある人をあらかじめ登録しておく公簿です。選挙権のある人でもこの選挙人名簿に登録されていなければ、投票することはできません。
          選挙人名簿の登録は、住民基本台帳に基づいて行われていますので、転出、転入等異動があった場合は、必ず市町村役場に届け出てください。
          </p>

          <h3 class="c-heading-3">登録の資格要件</h3>
          <p>選挙人名簿の登録は、市町村の選挙管理委員会が行いますが、次の要件を満たしている人が登録されます。<br>
          ・日本国民で年齢が満20歳以上であること。（H28.7月の参院選挙から18歳に引き下げ）<br>
          ・登録基準日現在、住民票が作成された日（転入届をした日）から引き続き３ヵ月以上住民基本台帳に記録されていること。
          </p>
          <p><span class=" u-bold u-mb__small">※学生等の選挙権</span><br>
            修学のために町外に居住する学生は、学生生活がもっとも通常な状態にあり、その住所は、修学地の市町村にあると判例があります。
            壮瞥町に住民登録をしたまま、修学地に居住する学生の方は、壮瞥町の名簿に登録する要件を満たさない者として取り扱われます。
            修学等で壮瞥町を離れる際は、忘れずに住民登録を移す手続を行ってください。
          </p>


          <h3 class="c-heading-3">登録の時期</h3>
          <p><span class=" u-bold">定時登録</span><br>毎年3月、6月、9月、12月の1日を基準日として、上記登録資格のある人を各月の2日に登録します。</p>
          <p><span class=" u-bold">選挙時登録</span><br>選挙のつど登録の基準日、登録日を定めて登録します。</p>          


          <h3 class="c-heading-3">登録の抹消</h3>
          <p>次の場合には登録の抹消が行われます。<br>
          ・死亡又は日本国籍を失ったとき<br>・他の市町村に住所を移して4ヵ月を経過したとき</p>          
          <p>【お問い合わせ先】<br>壮瞥町選挙管理委員会（TEL <a href="tel:0142-66-2121">0142-66-2121</a>）</p>
        </div>
      </div>
      




      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
