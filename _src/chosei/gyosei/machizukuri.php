<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">行政情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">住民参画・まちづくり</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">住民参画・まちづくり</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">委員募集・パブリックコメント（意見）募集</h2>
          <div class="u-box_border"><p>現在、募集しているものはありません</p></div>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-2">住民参画の取り組み</h3>
          <p>町民の皆様の「声」をまちづくりに反映させるため、壮瞥町では次のような取り組みをしています。</p>
          
          <h4 class="c-heading-3">(1) 町政懇談会</h4>
          <p>毎年、秋から冬にかけて、開催をご希望される自治会に出向いて、町民の皆さんと町長が膝を交えて話し合う場として、自治会単位で開催しています。開催自治会やスケジュールは広報又はお知らせでご確認ください。</p>

          <h4 class="c-heading-3">(2) みんなのひろば</h4>
          <p>広報に年4回挟み込んでいる用紙のことで、町へのご意見、ご要望をお書きいただき投函することができます。お寄せいただいたご意見は、広報の紙面上又はご意見をいただいた方に直接ご回答しています（内容により広報誌に掲載できない場合や回答できない場合もありますのでご注意ください）</p>

          <h4 class="c-heading-3">(3) 住民活動支援担当職員制度</h4>
          <p>地域と行政のパイプ役として職員を原則１年以内で配置し、地域の会議や打合せへの参加、行政上必要な手続きや町政などを担う制度です。
          町内の自治会単位や5名以上の住民で組織された団体等が、・NPO法人を立ち上げ、在宅介護の支援事業をしたい
          　・自治会で周年行事を行いたい
          など、地域の課題解決、自治会や団体活動の活性化に取り組む際に活用できます。
          自治会や団体等の通常業務や政治性、宗教性のある活動などの場合はお断りする場合がありますので詳しくは役場企画調整課までお問い合わせください。
          </p>

          <h4 class="c-heading-3">(4) まちづくり出前講座</h4>
          <p>町民の皆さんとの情報共有を図るため、担当職員がご指定の場所へ行って、26の講座の中からお好きなテーマで講座を行う「まちづくり出前講座」の制度を設けていますので
          ご活用ください。</p>
          <table class="c-table-1 c-td__left">
            <tr>
              <th class="__strong u-w30">対象</th>
              <td>5名以上の町民で組織された団体やサークル等</td>
            </tr>
            <tr>
              <th class="__strong">開催時間</th>
              <td>平日9時～20時までのうち2時間</td>
            </tr>
            <tr>
              <th class="__strong">開催場所</th>
              <td>町内にある公共施設等</td>
            </tr>
            <tr>
              <th class="__strong">申込方法</th>
              <td>講座を開催しようとする2週間前までに、電話・FAX・Emailでお申し込みください。</td>
            </tr>
          </table>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-2">行政評価と総合戦略</h3>
          
          <h4 class="c-heading-3">(1) 行政評価とは？</h4>
          <p class="">行政評価とは、行政の行っているさまざまな仕事を有効性・妥当性などといった視点から見直し、今後の進め方を改善していく取り組みです。本町では、試行期間を経て平成15年度から、まちづくり総合計画に定める施策ごとに検証、評価を行ってきました。平成22年度からは一時的に休止していましたが、現在の第4次まちづくり総合計画がスタートして3年が経過し、また基本計画の見直し時期もきているため、平成24年度分から再開したものです。
　この行政評価を行うため、町では住民の皆様で構成する行政評価委員会を設置しています。</p>

          <h4 class="c-heading-3">(2) 総合戦略の評価</h4>
          <p>本町では、今後急激に進む人口減少に歯止めをかけ、活気のある地域づくりに取り組むための方策をまとめた「壮瞥町総合戦略（計画期間 平成27年度～31年度）」を、さまざまな町民の皆様、関係機関の協力のもと策定しました。
この総合戦略は、その実効性を高めていくために、毎年度、個々の事業を評価し、必要に応じて見直しをしていくこととしています。そのため、本町では、平成27年度からは外部有識者を加えた行政評価委員会でその評価を行っていく予定です。
評価の結果等については、このページの中で随時ご報告をしていきます。</p>
        </div>
      </div>









      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
