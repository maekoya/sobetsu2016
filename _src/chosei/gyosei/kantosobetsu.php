<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">行政情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">選挙</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">関東そうべつ会</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
            <p class="u-text__center"><img src="images/kanto_sobetsu_kai.jpg" alt="関東そうべつ会"></p>
        	<p>関東そうべつ会は、平成9年に結成したふるさと会です。現在、会員は78名で、親睦と情報交換を図ることを目的として、年1回東京都内で開催する総会を中心に様々な事業を行っております。壮瞥町出身の方、壮瞥町にゆかりのある方など入会希望の方は、役場総務課までご連絡願います。</p>
            <p>平成27年度関東そうべつ会総会及び懇親交流会は、10月24日（土）、会員が経営する東京都内の飲食店で開催されました。今年度は、18名の会員の皆さんと壮瞥町から佐藤町長、松本議長など11名が出席し懇親を深めるとともに、ふるさと壮瞥について語り合いました。</p>
            <p>総会では、平成26年度事業報告・会計報告、平成27年度事業計画・予算案、役員選任が審議され、全議案、原案どおり承認されました。役員選任では、新たに毛利シゲ子さんを幹事に選任されました。懇親交流会では、お酒を飲みながら和やかに懇親を繰り広げ、ふるさと壮瞥のことに思いを馳せ語るなど、楽しいひとときとなりました。</p>
        </div>
      </div>
      




      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
