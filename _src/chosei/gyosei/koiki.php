<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">行政情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">広域行政</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">広域行政</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■西いぶり定住自立圏</h2>
          <h3 class="c-heading-3">(1) 定住自立圏構想とは</h3>
          <p>「定住自立圏構想」とは、人口5万人、昼夜間人口比1以上などの要件を満たす中心市が周辺市町と協定を結び、各自治体の強みを補完することで必要な都市機能を維持し、圏域として一つの生活圏を形成するものです。</p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(2) 定住自立圏形成協定・共生ビジョン</h3>
          <p>室蘭市と登別市、伊達市、豊浦町、壮瞥町、洞爺湖町は、平成22年9月30日に、「生活機能の強化」、「結びつきやネットワークの強化」、「圏域マネジメント能力の強化」の3分野8項目について、それぞれ1対1の協定を締結し、西いぶり定住自立圏が形成されました。<br>
また、室蘭市（中心市）では、関係市町との協議や、ビジョン懇談会、パブリックコメントなどによる提言や意見を取り入れ、平成23年3月23日に「西いぶり定住自立圏共生ビジョン」を策定しています。共生ビジョンでは、5カ年で21の具体事業が予定されており、今後計画的に推進するとともに、毎年必要な見直しを行うこととされております。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__pdf" target="_blank">定住自立圏の形成に関する協定書（室蘭市・壮瞥町）DL/PDF</a><br>
              <a href="pdf/" class="c-link__pdf" target="_blank">西いぶり定住自立圏共生ビジョン DL/PDF</a>
          </p>
          <p class=" u-bold">関連リンク<br>
              <a href="http://www.city.muroran.lg.jp/main/org2200/kouiki.html" class="c-link__blank" target="_blank">室蘭市ホームページ</a>
          </p>
        </div>
      </div>
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(3) 西いぶり生活情報メール配信サービス</h3>
          <p>西いぶり定住自立圏連携事業として、生活に密着した情報を電子メールで配信しています。</p>
          <p><span class=" u-bold">登録方法</span><br>
          それぞれの登録用アドレスに携帯電話やパソコンからメールを送信してください。（件名、本文ともに何も入力する必要はありません）送信後、登録確認メールが返信されますので、メールに記載されているURLをクリックして下さい。その後、追加情報を登録して終了です。（追加情報の登録が不要な場合もあります）迷惑メールの拒否設定をしている方は「@ml.nishi-iburi.jp」からのメールを受信できるよう、あらかじめ設定して下さい。なお、迷惑メールに関する設定については、ご利用の通信事業者へお問い合わせ下さい。</p>
          <ul class="c-list">
             <li>西いぶりおでかけガイド (odekake@ml.nishi-iburi.jp)<br>西胆振のイベント情報や催し物などの情報（月1回配信）</li>
             <li>ぼうさい西いぶり情報メール(bousai@ml.nishi-iburi.jp)<br>災害時の避難の呼びかけや防災情報（月1回配信）</li>
             <li>にしいぶり子育てサポート情報(kosodate@ml.nishi-iburi.jp)<br>子育てに関するお役立ち情報（月1回配信）</li>
             <li>消費生活サポート情報 (syouhi@ml.nishi-iburi.jp)<br>振込め詐欺や訪問販売の注意など消費生活情報（月1回配信）</li>
          </ul>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■西いぶり広域連合</h2>
          <p>広域連合制度は、多様化した広域行政需要に適切かつ効率的に対応するために創設された制度です。<br>
          本町は、室蘭市、登別市、伊達市、豊浦町、洞爺湖町とともに「西いぶり広域連合」を構成し、<br>
          ・ごみ処理施設及び粗大ごみ処理施設の設置、管理及び運営<br>・共同電算センターの設置、管理及び運営などを広域連携の下で行っています。</p>
          <p class=" u-bold">関連リンク<br>
              <a href="http://www.union.nishi-iburi.lg.jp/" class="c-link__blank" target="_blank">西いぶり広域連合ホームページ</a>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■北海道新幹線×nittan地域戦略会議</h2>
          <p>北海道新幹線×nittan地域戦略会議は北海道新幹線開業に向けて官民一体となって設立した広域連携組織で、北海道の南に位置する胆振と日高という２つの地域で構成しています。
nittan地域戦略会議では、2016年度の新函館北斗駅開業までの約２年間は「内外への情報発信」と「魅力の再発見」に力を入れ、日胆地域の優れた魅力を様々な形で発信しています。</p>
          <p class=" u-bold">関連リンク<br>
              <a href="http://nittanweb.jp/" class="c-link__blank" target="_blank">北海道新幹線×nittan地域戦略会議ホームページ</a>
          </p>
        </div>
      </div>
            






      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
