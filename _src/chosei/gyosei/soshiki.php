<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">行政情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">役場案内・組織機構</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">役場案内・組織機構</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■壮瞥町役場の組織機構・各課連絡先</h2>
            
            <section>
            	<h3 class="c-heading-3">総務課</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2121">0142-66-2121</a> / FAX 0142-66-7001</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:somu@town.sobetsu.lg.jp">somu@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">条例・規則、町役場の組織・機構、表彰、職員の定数・採用・給与に関すること</span><br>
                      <span class="u-circle__blue">町の財産・役場庁舎の管理、防犯・防災・交通安全・街路灯、ジオパークに関すること</span>
                      </td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">企画調整課</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2121">0142-66-2121</a> / FAX 0142-66-7001</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:kikaku@town.sobetsu.lg.jp">kikaku@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">総合計画、まちづくり、定住・過疎対策自治会、町に対する意見・要望に関すること</span><br>
                      <span class="u-circle__blue">地域交通、広報そうべつ・ホームページの運用管理、情報化、統計に関すること</span>
                      </td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">税務財政課</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2121">0142-66-2121</a> / FAX 0142-66-7001</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:zaisei@town.sobetsu.lg.jp">zaisei@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">税金、町の財政・予算・決算、地籍に関すること</span><br>
                      </td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">住民福祉課</h3>
                  <table class="c-table-1 c-td__left u-mb__small">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2121">0142-66-2121</a> / FAX 0142-66-7001</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:jumin@town.sobetsu.lg.jp">jumin@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">民生委員、生活保護、保育所・子育て支援・児童相談、障がい者福祉に関すること</span><br>
                      <span class="u-circle__blue">戸籍・年金、住民登録・印鑑登録、パスポート、ＤＶ相談に関すること</span><br>
                      <span class="u-circle__blue">高齢者福祉・介護保険、国民健康保険、後期高齢者医療、医療費助成に関すること</span>
                      </td>
                    </tr>
                  </table>
            	<h4 class="c-heading-4 u-mb__0 u-mt__0">（保健センター）</h4>
                  <table class="c-table-1 c-td__left u-mt__0">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2340"> 0142-66-2340</a> / FAX 0142-66-2678</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:hoken@town.sobetsu.lg.jp">hoken@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">健診・健診、予防接種、感染症予防、母子保健、健康相談・健康づくりに関すること</span>
                      </td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">経済環境課</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2121">0142-66-2121</a> / FAX 0142-66-7001</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:keizai@town.sobetsu.lg.jp">keizai@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">農林漁業、有害鳥獣、環境・エネルギー・温泉に関すること</span><br>
                      <span class="u-circle__blue">墓地・火葬場、ペット、ごみ・リサイクル、し尿処理に関すること</span>
                      </td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">商工観光課</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2121">0142-66-2121</a> / FAX 0142-66-7001</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:kanko@town.sobetsu.lg.jp">kanko@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">商工業、観光、企業誘致、消費者保護、雪合戦に関すること</span>
                      </td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">建設課</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2121">0142-66-2121</a> / FAX 0142-66-7001</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:kensetsu@town.sobetsu.lg.jp">kensetsu@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">道路・河川の維持管理、町営住宅・上下水道、建築基準法、都市計画に関すること</span>
                      </td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">会計課</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2121">0142-66-2121</a> / FAX 0142-66-7001</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:kaikei@town.sobetsu.lg.jp">kaikei@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">会計に関すること</span>
                      </td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">議会･監査委員事務局</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2121">0142-66-2121</a> / FAX 0142-66-7001</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:gikai@town.sobetsu.lg.jp">gikai@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">議会に関すること、監査に関すること</span>
                      </td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">農業委員会</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2121">0142-66-2121</a> / FAX 0142-66-7001</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:nogyo@town.sobetsu.lg.jp">nogyo@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">農業者年金、農地の利用に関すること</span>
                      </td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">選挙管理委員会</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2121">0142-66-2121</a> / FAX 0142-66-7001</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:senkyo@town.sobetsu.lg.jp">senkyo@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">選挙に関すること</span>
                      </td>
                    </tr>
                  </table>
            </section>
            <section>
            	<h3 class="c-heading-3">生涯学習課</h3>
                  <table class="c-table-1 c-td__left">
                    <tr>
                      <th class="__strong u-w20">TEL / FAX</th>
                      <td>TEL <a href="tel:0142-66-2131">0142-66-2131</a> / FAX 0142-66-2132</td>
                    </tr>
                    <tr>
                    	<th class="__strong">Email</th>
                      <td><a href="mailto:gakko@town.sobetsu.lg.jp">gakko@town.sobetsu.lg.jp</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                      <span class="u-circle__blue">教育委員会の運営、学校教育、生涯学習、青少年の健全育成、スポーツの振興に関すること</span><br>
                      <span class="u-circle__blue">教育施設の維持管理、地域交流センター・図書館の運営、文化財に関すること</span>
                      </td>
                    </tr>
                  </table>
            </section>
        </div>
        
      </div>
      
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
        	<h2 class="c-heading-2">■職員採用</h2>
            <p>職員採用に関する情報は「行政情報」をご覧ください。</p>
            <p><a href="#" class="c-link">「行政情報」に行く</a></p>
        </div>        
      </div>

            






      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
