<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="../../css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">行政・産業情報</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">行政情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">政策・計画</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">
      <h1 class="c-heading-1">政策・計画</h1>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■まちづくり総合</h2>
          <h3 class="c-heading-3">(1) 第４次壮瞥町まちづくり総合計画</h3>
          <p>総合計画の基本的な役割と機能</p>
          <ul>
          	<li><h4 class=" u-bold u-mb__0">ア）地域づくりの最上位計画</h4>
            壮瞥町の様々な分野の計画等の策定に対して整合性を保ち、方向性を示すものです。</li>
            <li><h4 class=" u-bold u-mb__0">イ）総合的、計画的行政運営の指針</h4>
            地域づくりの総合分野を守備範囲とするもので、長期展望に立った計画的なものです。</li>
            <li><h4 class="u-bold u-mb__0">ウ）国・道など関係機関が尊重すべき計画</h4>
            地方自治体の総意として地域づくりの意志を表現するものであり、地方自治の精神からも、国・道など関係機関も重視、尊重すべきものです。</li>
            <li><h4 class="u-bold u-mb__0">エ）住民・民間活動の指針</h4>
            行政と住民が一体となった地域づくりが課題となっている今日、総合計画は行政のみなず住民や民間活動の指針としての機能も重視されます。そのため住民の参画の方策を明らかにし、積極的に住民の参画を促進することが肝要です。</li>
          </ul>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">第4次壮瞥町まちづくり総合計画（本編）DL/PDF</a><br>
              <a href="pdf/" class="c-link__blank" target="_blank">第4次壮瞥町まちづくり総合計画（概要版）DL/PDF</a>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(2) 壮瞥町過疎地域自立促進市町村計画</h3>
          <p>壮瞥町では、過疎地域自立促進特別措置法の規定に基づき、壮瞥町過疎地域自立促進市町村計画を策定しています。この法律に定められた財政上の優遇措置等を活用しながら、地域資源を最大限活用して地域の自給力を高めるとともに、生活にかかわる公益的機能を十分に発揮し、住民が誇りと愛着を持つことのできる活力に満ちた地域社会を実現させることとしています。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町過疎地域自立促進市町村計画 DL/PDF</a><br>
          </p>
        </div>
      </div>
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(3) 壮瞥町総合戦略</h3>
          <p>2014年11月に成立した「まち・ひと・しごと創生法」を踏まえ、壮瞥町では今後急激に進む人口減少に歯止めをかけ、活気のある地域づくりに取り組むため、様々な町民の皆様、関係機関の協力のもと、壮瞥町総合戦略を策定しました。<br>今後、本戦略に位置づけた施策等を中心に「移住・定住」、「仕事づくり」、「結婚・出産・子育て」、「まちづくり」等に取り組んでいきます。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町総合戦略（概要版）DL/PDF</a><br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町総合戦略（全文）DL/PDF</a><br>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">（４）壮瞥町定住促進・公共施設有効活用計画</h3>
          <p>壮瞥町ではこれまで、定住人口の減少が続いており、今後さらに加速化が予測されるなど地域の経済や住民生活への影響が懸念されます。その一方で、いわゆる「ハコモノ」と呼ばれる公共施設については、行政改革実施計画に基づき、母と子の家などの老朽施設の廃止や民間委託んどを推進してきましたが、現在ある公共施設をこのまま維持し続けることは今後の町政運営にとって大きな負担となり、真に必要となる行政サービスにまで悪影響を及ぼしてしまうことが懸念されます。</p>
          <p>このような背景を踏まえ、本町が抱える「定住人口の減少抑制」と「公共施設の有効活用」という2つの大きな、かつ喫緊の課題について、<br>一体的・集中的に取り組んでいくため、本計画を策定し、今後も健全な行財政運営に努めながら施策を展開していきます。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町定住促進・公共施設有効活用計画DL/PDF</a><br>
          </p>
        </div>
      </div>



      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■保健福祉関係計画</h2>
          <h3 class="c-heading-3">(1) 生活の質向上計画（第２期壮瞥町地域福祉計画）</h3>
          <p>「地域福祉計画」は社会福祉法に基づくもので、地域福祉のあり方や推進に向けての基本的な方向性を定めるものです。本町では誰もが生きがいや社会的役割を意識しながら福祉に参加することが、より「豊かな生活につながるものと期待されることから、「福祉」を「生活の質の向上」という視点で捉え、ひとり暮らし高齢者の増加や地域住民の関係の希薄化などの社会情勢の変化やアンケート調査結果を反映した「生活の質向上計画（第２期壮瞥町地域福祉計画）」を策定いたしました。
　　　町は、本計画に基づき、住み慣れた家庭や地域で、誰もが安心していきいきと暮らせるよう福祉への理解と意識の向上に向けた取り組みを進めるとともに、地域で支え合う福祉活動の促進に取り組んで参ります。
</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">生活の質向上計画 DL/PDF</a><br>
              <a href="pdf/" class="c-link__blank" target="_blank">生活の質向上計画概要版 DL/PDF</a>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(2) 第6期壮瞥町高齢者保健福祉計画・介護保険事業計画</h3>
          <p>「高齢者保健福祉計画」は、第4次壮瞥町まちづくり総合計画に定めた「健やかな暮らしのまちづくり」を実践するため、町の高齢者福祉施策全般を総合的に推進するための計画です。
　　　また、「介護保険事業計画」は介護保険法に基づくもので、介護サービスや介護予防サービスなどの見込み量を定めるなど、介護保険事業の円滑な運営に必要な事項を定めるものです。
　　　計画の策定にあたっては、アンケートによる高齢者の実態調査や町内関係者で構成する策定委員会において、現状や課題分析、事業内容について審議、検討を行いました。
　　　町では、本計画に基づき、住み慣れた地域で安心して暮らせるまちづくりの実現と、地域の実情に応じた高齢者福祉及び介護保険事業の適切な運営に今後も取り組んで参ります。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">第6期高齢者保健福祉計画・介護保険事業計画 DL/PDF</a><br>
          </p>
        </div>
      </div>
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(3) 第2期障がい者計画・第4期障がい福祉計画</h3>
          <p>「障がい者計画」は障害者基本法に基づくもので、障がい者がその年齢や障がい種別等に関わりなく、身近なところで必要なサービスを受け安心して暮らせるよう、町の障がい福祉施策全般を総合的に推進するための計画です。また、「障がい福祉計画」は障害者自立支援法に基づくもので、障がい福祉サービス等の確保に関し、具体的な数値目標を掲げた計画です。
計画の策定にあたっては、アンケート調査や関係者との懇談会、関係団体の代表者で構成する策定委員会において、現状や課題分析、計画内容について審議、検討を行いました。「障がい者と共に生きるまちづくり」を推進するためには、障がいや障がいのある人についての理解を高めていくことが必要です。そのため、行政だけでなく、障がいのある人、福祉団体、ボランティア等がそれぞれの役割を果たし、相互に連携しながら一体となって取り組みます。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">第2期障がい者計画・第4期障がい福祉計画 DL/PDF</a><br>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(4) 壮瞥町子ども・子育て支援事業計画</h3>
          <p>町では、子ども子育て支援法に基づき、本町における教育・保育及び子ども・子育て支援サービスの質を高めるため、各サービスの需給量の見込みや提供方策等をきめ細かく計画するとともに、住民や教育・保育従事者、地域、行政が協働で取り組んでいく施策、事業の方向を明らかにすることを目的として「壮瞥町子ども・子育て支援事業計画」を策定しました。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町子ども・子育て支援事業計画 DL/PDF</a><br>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(4) 壮瞥町子ども・子育て支援事業計画</h3>
          <p>町では、子ども子育て支援法に基づき、本町における教育・保育及び子ども・子育て支援サービスの質を高めるため、各サービスの需給量の見込みや提供方策等をきめ細かく計画するとともに、住民や教育・保育従事者、地域、行政が協働で取り組んでいく施策、事業の方向を明らかにすることを目的として「壮瞥町子ども・子育て支援事業計画」を策定しました。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町子ども・子育て支援事業計画 DL/PDF</a><br>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(5) 第2期特定健康診査等実施計画</h3>
          <p>町は、生活習慣病を中心とした疾病を予防するため、「壮瞥町第2期特定健康診査等実施計画」を策定しました。本計画は、高齢者の医療の確保に関する法律第19条第1項の規定に基づき、40歳から74歳までの国民健康保険加入者に対して、平成25年度～29年度における特定健康診査及び特定保健指導の実施に関する具体的な内容を定めたものです。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町第2期特定健康診査等実施計画 DL/PDF</a><br>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(6) 壮瞥町新型インフルエンザ等対策行動計画</h3>
          <p>町では、新型インフルエンザ等が全国各地で急速にまん延した場合における、感染の拡大を防止するための様々な対策を定めるために、「壮瞥町新型インフルエンザ等対策行動計画」を策定しました。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町新型インフルエンザ等対策行動計画 DL/PDF</a><br>
          </p>
        </div>
      </div>



      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■教育・生涯学習関係計画</h2>
          <h3 class="c-heading-3">(1) 壮瞥町教育大綱</h3>
          <p>壮瞥町・壮瞥町教育委員会では、平成27年4月1日施行「地方教育行政の組織及び運営
に関する法律の一部を改正する法律」に基づき、首長・教育委員会で構成する「総合教育会議」を設置し、本町の教育の目標や施策の根本的な方針である「壮瞥町教育大綱」を策定しています。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町教育大綱 DL/PDF</a><br>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(2) 壮瞥町立小中学校適正配置方針</h3>
          <p>過疎化・少子化の進行による児童生徒数の減少を踏まえ、将来を担う子どもたちに、より望ましい教育環境を整えるため、中・長期的な展望に立ち、望ましい学校の在り方についての基本的な考え方と進め方をまとめた「壮瞥町立小中学校適正配置方針」を策定しています。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町立小中学校適正配置方針DL/PDF</a><br>
          </p>
        </div>
      </div>
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(3) 壮瞥町第7次社会教育中期計画</h3>
          <p>現在の生涯学習を取り巻く環境は、少子高齢化や高度情報化、国際化など激しく変化する中で価値観が多様化し、地域社会や家庭の在り方も大きく変化してきています。こうした環境の変化に対応していくためには、町民が生涯にわたって自主的・主体的に学習を継続することが大切であり、その効果は壮瞥町の将来を担う「まちづくり・ひとづくり」に確実につながるものです。本計画は、壮瞥町の社会教育のあるべき姿を展望し、「第4次壮瞥町まちづくり総合計画」の基本方針で示された「地域を支える人づくり」の実現のために、また町民憲章、教育目標、社会教育目標等の理念を具現化するために策定しています。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町第7次社会教育中期計画 DL/PDF</a><br>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(4) 壮瞥町スポーツ推進計画</h3>
          <p>平成23年に制定されたスポーツ基本法の理念の実現には、国や地方公共団体、学校、スポーツ団体など多様な主体が連携・協働し、総合的かつ計画的な取り組みが重要であり、国・道における関係計画策定の動きを踏まえ、本町においても策定するものです。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町スポーツ推進計画 DL/PDF</a><br>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(5) 壮瞥町子ども読書活動推進計画「より深く豊かな人生を育むために（第二次計画）</h3>
          <p>壮瞥町は北海道の計画を受けて平成20年度に壮瞥町読書推進計画を策定し概ね6年間の期間で壮瞥町の読書推進に取り組んできました。平成25年度で計画期間の最終年度であることから、国、北海道の計画に合わせて平成26年度からの第2期計画を策定しました。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">第2次壮瞥町子ども読書活動推進計画・添付資料 DL/PDF</a><br>
          </p>
        </div>
      </div>



      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■産業関係計画</h2>
          <h3 class="c-heading-3">(1) 農業経営基盤強化促進基本構想</h3>
          <p>農業経営基盤強化促進基本構想が、平成26年9月19日に北海道知事の同意を受け、平成26年9月22日変更しました。この基本構想は、「農業経営基盤強化促進法」に基づき、壮瞥町の農業者等の経営規模、生産方式、経営管理の方法、農業従事の態様等に関する営農の類型ごとの効率的かつ安定的な農業経営の指標、農業経営基盤の強化の促進に関する目標を示した構想です。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">農業経営基盤強化促進基本構想・添付資料 DL/PDF</a><br>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(2) 水田フル活用ビジョン</h3>
          <p>壮瞥町地域農業再生協議会では、水田を有効活用し、地域作物振興の推進に係る取り組みを行うため「水田フル活用ビジョン」を策定しました。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">水田フル活用ビジョン DL/PDF</a><br>
          </p>
        </div>
      </div>
      
      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(3) 鳥獣被害防止計画・緊急捕獲等計画</h3>
          <p>町は、拡大する野生鳥獣による農林業被害を防止するため、平成２２年度に壮瞥町鳥獣被害防止計画を樹立しましましたが、計画期間の終期を迎えたことから、平成25年度に計画の変更を行いました。この計画では、エゾシカ、ヒグマ、アライグマ、カラス（ハシブトガラス、ハシボソガラス）、スズメ（スズメ、ニュウナイスズメ）を対象に、捕獲計画頭数や取組内容について記載しております。
また、国では、鳥獣による農林水産業への被害が深刻化していることを受け、捕獲活動の更なる強化のための緊急捕獲活動、地域の実情に応じたきめ細かな鳥獣被害防止施設の機能向上の取組への支援を行うこととなりました。
　　　町は、捕獲活動の更なる強化を図り、緊急捕獲活動を実施するため、平成24年度に壮瞥町緊急捕獲等計画を策定しました。町は、今後もこれらの計画や関係法令等に基づき、関係機関の協力を得て有害鳥獣の捕獲等を推進しますので、ご理解とご協力をお願いします。
</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町鳥獣被害防止計画 DL/PDF</a><br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町緊急捕獲等計画 DL/PDF</a>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(4) 壮瞥町森林整備計画</h3>
          <p>市町村森林整備計画は、北海道が作成する地域森林計画の対象となる民有林が所在する市町村が５年ごとに作成する10年を一期とする計画であり、市町村における森林関連施策の方向や森林所有者が行う伐採や造林等の森林施業に関する指針等を定めるもので、地域の実情に応じた適切な森林整備を推進することを目的とするものです。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町森林整備計画書・図面 DL/PDF</a>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(5) 壮瞥町農商工連携推進基本戦略</h3>
          <p>観光客入り込み数や農業粗生産額の減少など、本町の産業は徐々に衰退しつつあります。昨年、地方創生の大号令が発せられ地方再生に向けて知恵と工夫が問われる局面に入ってきました。本町の基幹産業である農業と商工観光業がしっかりと手を組み、今何ができるか、何を目指すのかを定め、行動するために「壮瞥町農商工連携推進基本戦略」を取りまとめました。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町農商工連携推進基本戦略 DL/PDF</a>
          </p>
        </div>
      </div>



      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■住宅・橋梁関係計画</h2>
          <h3 class="c-heading-3">(1) 壮瞥町住生活基本計画・壮瞥町公営住宅等長寿命化計画</h3>
          <p>本町では平成16年度に「壮瞥町住宅マスタープラン」を策定し、計画期間を平成17年度から平成26年度までの10年間とし、壮瞥町の住宅施策を推進しています。また、平成21年度には「壮瞥町公営住宅等長寿命化計画」を策定し、平成22年度から計画に基づいて公営住宅等の建設及び改善事業を推進しています。
　　　住宅マスタープランの終了に伴い、今後の壮瞥町における住宅施策の目標、推進方法を定め、具体的な住宅施策を推進することを目的として壮瞥町住生活基本計画し、同時に、長寿命化に係る全計画も策定後5年を経過し、見直し時期にきていることから合わせて公営住宅等長寿命化計画を策定しました。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町住生活基本計画 DL/PDF</a><br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町公営住宅等長寿命化計画 DL/PDF</a>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(2) 壮瞥町橋梁長寿命化修繕計画</h3>
          <p>壮瞥町が管理する道路橋は29橋（分離歩道橋を含めて30橋）であり、30年後には全体の約80％にあたる橋梁が建設後50年を経過する高齢化橋梁となります。管理橋の維持管理について、従来の事後保全的な対応を継続した場合、維持管理に要する費用が膨大となり、安全性・信頼性を確保するための適切な維持管理を続けることが困難となる恐れがあります。
このため、限られた財源の中で効率的に維持管理していくためには、適切な時期に修繕を行うなどの維持管理計画の取り組みが不可欠であり、管理橋の高齢化に対応するため、従来の事後保全的な対応から予防保全的な対応に転換を図り、地域の道路ネットワークの安全性・信頼性を向上させ、計画的な取り組みにより修繕・架替えに係わるコスト縮減を図ることを目的とし、橋梁長寿命化修繕計画を策定しました。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町橋梁長寿命化修繕計画 DL/PDF</a><br>
          </p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■交通関係計画</h2>
          <h3 class="c-heading-3">(1) 壮瞥町地域公共交通総合連携計画</h3>
          <p>壮瞥町における地域課題及び交通課題を踏まえ、公共交通の維持・確保を図るとともに、地域の活性化に資する地域公共交通総合連携計画が平成24年5月に策定されました。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町地域公共交通総合連携計画 DL/PDF</a>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(2) 生活交通ネットワーク計画（地域内フィーダー系統確保維持計画）</h3>
          <p>平成25年10月から国庫補助を受けての本格運行を実施するため生活交通ネットワーク計画（地域内フィーダー系統確保維持計画）が平成25年6月に策定されました。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">地域内フィーダー系統確保維持計画 DL/PDF</a>
          </p>
        </div>
      </div>


      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h2 class="c-heading-2">■防災・国民保護関係計画</h2>
          <h3 class="c-heading-3">(1) 壮瞥町地域防災計画</h3>
          <p>この計画は、地域の災害予防、災害応急対策及び災害復旧対策について行う事項を定め、町民や地域団体、事業所と、町及び防災関係機関が協働してこれらの防災活動を円滑に行うことにより、災害の被害軽減と拡大防止に努め、町民の生命、身体及び財産を災害から守ることを目的として定めています。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町地域防災計画</a>
          </p>
        </div>
      </div>

      <div class="u-grid__row">
        <div class="u-grid__col-12">
          <h3 class="c-heading-3">(2) 壮瞥町国民保護計画</h3>
          <p>平成16年6月に制定された「武力攻撃事態等における国民の保護のための措置に関する法律（国民保護法、同年9月施行）」の規定により、壮瞥町国民保護計画を策定しました。</p>
          <p class=" u-bold">ダウンロード<br>
              <a href="pdf/" class="c-link__blank" target="_blank">壮瞥町国民保護計画 DL/PDF</a>
          </p>
        </div>
      </div>
            






      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
