<?php
// Include general settings.
require($_SERVER["DOCUMENT_ROOT"].'/sobetsu/resources/etc/config.php');

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">リンク</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">リンク</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">

      <h2 class="c-heading-1">壮瞥町内の団体など</h2>

        <section class="u-grid__row u-grid__col-12">
          <h3 class="c-heading-2">観光・農業</h3>
         <ul class="c-list">
            <li><a href="http://www.sobetsu-kanko.com" class="c-link__blank" target="_blank">特非）そうべつ観光協会</a></li>
            <li><a href="http://sobetsu-fp.co.jp/" class="c-link__blank" target="_blank">そうべつ農産物直売所サムズ</a></li>
            <li><a href="http://www.sobetsu.net/" class="c-link__blank" target="_blank">壮瞥町商工会</a></li>
            <li><a href="http://okutoya.com/" class="c-link__blank" target="_blank">奥洞爺温泉郷</a></li>
            <li><a href="http://www.sobetsu-onsen.net/" class="c-link__blank" target="_blank">壮瞥町公共日帰り温泉</a></li>
            <li><a href="http://orofure.web.fc2.com/" class="c-link__blank" target="_blank">オロフレスキー場</a></li>
            <li><a href="http://www.yukigassen.jp/" class="c-link__blank" target="_blank">昭和新山国際雪合戦</a></li>
            <li><a href="http://www.kudamonomura.com/" class="c-link__blank" target="_blank">そうべつくだもの村</a></li>
            <li><a href="http://ff-fujimori.com/" class="c-link__blank" target="_blank">フジモリ果樹園</a></li>
            <li><a href="http://www.hamada-en.com/index.html" class="c-link__blank" target="_blank">くだもの農家浜田園（じゃむ工房杏の樹）</a></li>
            <li><a href="http://www.phoenix-c.or.jp/~shin33/" class="c-link__blank" target="_blank">ほくほくファーム（農業生産法人（有）ファームアグリエイト）</a></li>
            <li><a href="http://www.tatukam.jp/" class="c-link__blank" target="_blank">同）農場たつかーむ</a></li>
            <li><a href="http://www.pippara.que.jp/" class="c-link__blank" target="_blank">ザ・ラーメン　ピッパラの森</a></li>
            <li><a href="http://www.29kambe.com/" class="c-link__blank" target="_blank">焼肉ハウスサウスポー</a></li>
          </ul>          
        </section>

        <section class="u-grid__row u-grid__col-12">
          <h3 class="c-heading-2">医療・福祉</h3>
         <ul class="c-list">
            <li><a href="http://www.662511.net/" class="c-link__blank" target="_blank">福）壮瞥町社会福祉協議会</a></li>
            <li><a href="http://www.minerva.gr.jp/8.html#id1" class="c-link__blank" target="_blank">医）倭会三恵病院</a></li>
            <li><a href="http://www.koyukai-g.jp/soh/" class="c-link__blank" target="_blank">医）交雄会そうべつ温泉病院</a></li>
            <li><a href="http://www.koyukai-g.jp/plime-s/" class="c-link__blank" target="_blank">医）交雄会介護老人保健施設プライムそうべつ</a></li>
            <li><a href="http://www.tyoujituen.jp/" class="c-link__blank" target="_blank">社）長日会（特別養護老人ホーム第２長日園ほか）</a></li>
            <li><a href="http://www.tatukam.org/" class="c-link__blank" target="_blank">特非）サポートセンターたつかーむ（壮瞥町地域活動支援センター・ノンノ）</a></li>
            <li><a href="http://sky.geocities.jp/sararasoubetsu/" class="c-link__blank" target="_blank">特非）さらら壮瞥</a></li>
          </ul>          
        </section>

        <section class="u-grid__row u-grid__col-12">
          <h3 class="c-heading-2">火山・教育・文化</h3>
         <ul class="c-list">
            <li><a href="http://uvo4.sci.hokudai.ac.jp/~www/" class="c-link__blank" target="_blank">北海道大学有珠火山観測所</a></li>
            <li><a href="http://www005.upp.so-net.ne.jp/usuvolcano/index.htm" class="c-link__blank" target="_blank">特非）有珠山周辺地域ジオパーク友の会</a></li>
            <li><a href="http://www9.plala.or.jp/usuvolcano/" class="c-link__blank" target="_blank">有珠山火山活動と壮瞥町の火山防災</a></li>
            <li><a href="http://www.sobetsu.jp/soukou/" class="c-link__blank" target="_blank">北海道壮瞥高等学校</a></li>
            <li><a href="http://www14.plala.or.jp/tossy230/" class="c-link__blank" target="_blank">洞爺湖のtossy（壮瞥町天文同好会便りetc.）</a></li>
          </ul>          
        </section>

        <section class="u-grid__row u-grid__col-12">
          <h3 class="c-heading-2">その他</h3>
          <h4 class="c-heading-3">ジオパーク・火山関係</h4>          
         <ul class="c-list">
            <li><a href="http://toya.lohasin.net/index.htm" class="c-link__blank" target="_blank">洞爺湖宣隊ジオレンジャー</a></li>
            <li><a href="http://www.toya-usu-geopark.org/" class="c-link__blank" target="_blank">洞爺湖有珠山ジオパーク</a></li>
            <li><a href="http://www.geopark.jp/" class="c-link__blank" target="_blank">特非）日本ジオパークネットワーク</a></li>
            <li><a href="http://www.kazan-g.sakura.ne.jp/J/index.html" class="c-link__blank" target="_blank">特非）日本火山学会</a></li>
          </ul>
        </section>
        
        <div class="u-grid__col-12">
        	<h4 class="c-heading-3">行政関係</h4>
        </div>
        
        <div class=" u-clearboth">
        <section class="u-grid__col-6">
          <h4 class="c-heading-4">近隣市町･行政、広域連携</h4>          
         <ul class="c-list u-mb__0 u-mt__0">
            <li><a href="http://www.city.muroran.lg.jp/" class="c-link__blank" target="_blank">室蘭市</a></li>
            <li><a href="http://www.city.noboribetsu.lg.jp/index.html" class="c-link__blank" target="_blank">登別市</a></li>
            <li><a href="http://www.city.date.hokkaido.jp/" class="c-link__blank" target="_blank">伊達市</a></li>
            <li><a href="http://www.town.toyako.hokkaido.jp/top.jsp" class="c-link__blank" target="_blank">洞爺湖町</a></li>
            <li><a href="http://www.town.toyoura.hokkaido.jp/" class="c-link__blank" target="_blank">豊浦町</a></li>
            <li><a href="http://nfd119.sakura.ne.jp/" class="c-link__blank" target="_blank">西胆振消防組合</a></li>
            <li><a href="http://www.date-syo.police.pref.hokkaido.lg.jp/" class="c-link__blank" target="_blank">伊達警察署</a></li>
            <li><a href="http://www.union.nishi-iburi.lg.jp/" class="c-link__blank" target="_blank">西いぶり広域連合</a></li>
            <li><a href="http://westiburi.jp/" class="c-link__blank" target="_blank">北海道登別洞爺広域観光圏協議会</a></li>
            <li><a href="http://nittanweb.jp/" class="c-link__blank" target="_blank">北海道新幹線ｘnittan地域戦略会議</a></li>
          </ul>
         </section>
         <section class="u-grid__col-6">
          <h4 class="c-heading-4">北海道・国関係</h4>          
         <ul class="c-list u-mb__0 u-mt__0">
            <li><a href="http://www.pref.hokkaido.lg.jp/ / http://www.iburi.pref.hokkaido.lg.jp/index.htm" class="c-link__blank" target="_blank">北海道 / 胆振総合振興局</a></li>
            <li><a href="http://www.mr.hkd.mlit.go.jp/" class="c-link__blank" target="_blank">北海道開発局室蘭開発建設部</a></li>
            <li><a href="http://www.nta.go.jp/sapporo/" class="c-link__blank" target="_blank">札幌国税局</a></li>
            <li><a href="http://www.e-tax.nta.go.jp/" class="c-link__blank" target="_blank">e-Taxホームページ</a></li>
            <li><a href="http://www.soumu.go.jp/soutsu/hokkaido/" class="c-link__blank" target="_blank">北海道総合通信局</a></li>
            <li><a href="http://www.rinya.maff.go.jp/hokkaido/" class="c-link__blank" target="_blank">北海道森林管理局</a></li>
            <li><a href="http://www.mod.go.jp/pco/sapporo/" class="c-link__blank" target="_blank">自衛隊札幌地方協力本部</a></li>
            <li><a href="http://www.hokkaido-kikin.or.jp/" class="c-link__blank" target="_blank">北海道国民年金基金</a></li>
            <li><a href="http://www.moj.go.jp/jinkennet/sapporo/sapporo_index.html" class="c-link__blank" target="_blank">道央人権啓発活動ネットワーク協議会</a></li>
          </ul>
         </section>
        </div>

        <section class="u-grid__col-12 u-mt__30">
         <h4 class="c-heading-3">その他団体など</h4>
         <ul class="c-list">
            <li><a href="http://www.hsc.or.jp/index.cgi" class="c-link__blank" target="_blank">公財）北海道中小企業総合支援センター</a></li>
            <li><a href="http://www.houterasu.or.jp/index.html" class="c-link__blank" target="_blank">法テラス（日本司法支援センター）</a></li>
            <li><a href="http://www.nhk.or.jp/muroran/" class="c-link__blank" target="_blank">NHK室蘭放送局</a></li>
            <li><a href="http://noukanotomo.com/" class="c-link__blank" target="_blank">農家の友（公社）北海道農業改良普及協会）</a></li>
            <li><a href="http://northern-road.jp/navi/" class="c-link__blank" target="_blank">北の道ナビ（国研）土木研究所寒地土木研究所）</a></li>
            <li><a href="http://www.scenicbyway.jp/" class="c-link__blank" target="_blank">シーニックバイウェイ北海道</a></li>
            <li><a href="http://www.hamanasu.or.jp/" class="c-link__blank" target="_blank">公財）はまなす財団</a></li>
            <li><a href="http://bunkashisan.ne.jp/" class="c-link__blank" target="_blank">地域文化資産（一財）地域創造）</a></li>
            <li><a href="http://www.bfh.jp/" class="c-link__blank" target="_blank">BEST!from北海道</a></li>
            <li><a href="http://www.autoswap.co.jp/" class="c-link__blank" target="_blank">AUTOSWAP HOKKAIDO</a></li>
          </ul>          
        </section>
        
       <section class="u-grid__row u-grid__col-12">
          <h3 class="c-heading-2">ダウンロード</h3>
          <p><a href="/images/link/link_shinseisyo.doc" class="c-link__pdf">リンク設定に関する申請書 DL / PDF</a></p>
        </section>





      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
