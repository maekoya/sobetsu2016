<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">町からのお知らせ</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $page->base; ?>/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $page->base; ?>/shinchaku/" itemprop="url"><span itemprop="title">町からのお知らせ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $page->base; ?>/shinchaku/" itemprop="url"><span itemprop="title">行政・産業情報</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">タイトルが入ります</span></li>
    </ul>

    <div class="p-container">
      <div id="base-content__main">
        <div class="p-container__auto-margin-paragraph">
          <article class="p-entry">
            <div class="p-entry__header">
              <h1 class="p-entry__header__heading">タイトルが入ります</h1>
              <div class="p-entry__header__data">
                <ul class="p-entry__header__data__tag-list">
                  <li class="p-entry__header__data__tag-list__item"><a href="#" class="__is-gyosei">行政・産業情報</a></li>
                </ul>
                <time class="p-entry__header__data__date" datetime="2016-00-00" pubdate>2016年00月00日</time>
              </div>
            </div>
            <div class="p-entry__body">
              <p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。<a href="#">テキスト入ります。</a>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。</p>
              <p>テキスト入りま入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。<span class="u-text__orange">テキスト入ります。</span>テキスト入ります。テキスト入ります。</p>
              <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-1.png" width="" height="" alt="">
            </div>
            <div class="p-entry__footer">
              <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="300" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
            </div>
          </article><!-- /.p-entry -->

          <div class="entries-recently">
            <div class="entries-recently__header">
              <div class="entries-recently__header__heading"><span class="__tag">新着</span>行政・産業情報のお知らせ</div>
            </div>
            <ul class="entries-recently__list">
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
            </ul>
          </div>

        </div><!-- /.p-container -->
      </div><!-- /#base-content__main -->

      <div id="base-content__side">
        <div class="p-entries-side">
          <div class="p-entries-side__item">
            <p class="p-entries-side__item__heading">カテゴリー</p>
            <ul class="p-entries-side__item__list__category">
              <li><a href="#">暮らしガイド</a></li>
              <li><a href="#">行政・産業情報</a></li>
              <li><a href="#">防災・安全情報</a></li>
              <li><a href="#">イベント</a></li>
              <li><a href="#">広報そうべつ</a></li>
              <li><a href="#">議会情報</a></li>
              <li><a href="#">採用情報</a></li>
              <li><a href="#">その他</a></li>
            </ul>
          </div>
        </div>
      </div><!-- /#base-content__side -->

    </div><!-- /.p-container -->
    <div class="c-pagetop"><a href="#base-page">TOP</a></div>
  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
