<?php
// Include general settings.
require($_SERVER['CONFIG_PATH']);

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">町からのお知らせ</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $page->base; ?>/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $page->base; ?>/shinchaku/" itemprop="url"><span itemprop="title">町からのお知らせ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">行政・産業情報</span></li>
    </ul>

    <div class="p-container">
      <div id="base-content__main">
        <div class="p-container">

          <div class="entries-recently">
            <div class="entries-recently__header">
              <div class="entries-recently__header__heading">行政・産業情報のトピックス</div>
            </div>
            <ul class="entries-recently__list">
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
            </ul>
          </div>

          <div class="entries-recently">
            <div class="entries-recently__header">
              <div class="entries-recently__header__heading"><span class="__tag">新着</span>行政・産業情報のお知らせ</div>
            </div>
            <ul class="entries-recently__list">
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
              <li class="entries-recently__list__item">
                <span class="__tag"><a href="/sinchaku/" class="__is-gyosei">行政・産業情報</a></span>
                <time class="__date" datetime="2016-00-00" pubdate>2015.11.25</time>
                <a class="__title" href="/sinchaku/">広報そうべつ4月号（企画調整課）</a>
              </li>
            </ul>
          </div>

        </div><!-- /.p-container -->
      </div><!-- /#base-content__main -->

      <div id="base-content__side">
        <div class="p-entries-side">
          <div class="p-entries-side__item">
            <p class="p-entries-side__item__heading">カテゴリー</p>
            <ul class="p-entries-side__item__list__category">
              <li><a href="#">暮らしガイド</a></li>
              <li><a href="#">行政・産業情報</a></li>
              <li><a href="#">防災・安全情報</a></li>
              <li><a href="#">イベント</a></li>
              <li><a href="#">広報そうべつ</a></li>
              <li><a href="#">議会情報</a></li>
              <li><a href="#">採用情報</a></li>
              <li><a href="#">その他</a></li>
            </ul>
          </div>
        </div>
      </div><!-- /#base-content__side -->

    </div><!-- /.p-container -->
    <div class="c-pagetop"><a href="#base-page">TOP</a></div>
  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
