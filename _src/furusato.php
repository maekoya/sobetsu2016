<?php
// Include general settings.
require($_SERVER["DOCUMENT_ROOT"].'/sobetsu/resources/etc/config.php');

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">ふるさと納税</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">ふるさと納税</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">

      <h2 class="c-heading-1">ふるさと応援寄附金（ふるさと納税）</h2>

        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <p>壮瞥町は北海道の南西部に位置し、人口はおよそ2,700人、観光と農業を中心とする小さな農村です。しかし、町内の多くが支笏洞爺国立公園内にあり、20世紀4回の噴火を繰り返した有珠山、火山活動により麦畑が隆起してできた奇跡の山・昭和新山、2008G8サミットの舞台となった優美な洞爺湖、火山の恵みである温泉や肥沃な大地を活用した多種多彩な農業、住民パワーの結晶・昭和新山国際雪合戦など、多くの資源に満ちあふれ、世界中から訪れる観光客を魅了しています。
壮瞥町では今後も、大自然と共生しながら産業を伸ばし、活力あふれる郷土づくりに取り組んでいきます。<br>皆様には、こうした考えにご賛同をいただき「ふるさと寄附金」のかたちでまちづくりをご支援くださいますようお願いいたします。</p>
            </section>
        </div>

        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">ふるさと応援寄附金（ふるさと納税）とは</h3>
              <p>ふるさと寄附金（ふるさと納税）とは、生まれ育ったまちや愛着のある地域に寄附をすると税金が軽減される制度です。<br>
               応援したいと思う市区町村や都道府県を自由に選べて、自治体によっては寄附金の使い道を選択できたり、寄附することでそのまちの記念品や特産品をもらうことができます。</p>
            </section>
        </div>

        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">感謝特典（お礼の品）について</h3>
              <p>壮瞥町では、平成27年12月からふるさと納税制度をリニューアルし、感謝特典（お礼の品）のメニューの拡大やインターネット申込み・クレジット決済を導入しました。
1回に10,000円以上のご寄附をいただいた町外在住の個人のかたに、まちの特産品をお礼の品としてお贈りしております。ふるさと納税を機に、ぜひ、壮瞥町の味覚を堪能してください。お礼の品は寄附金額に応じて選択することができます。<br>また、回数制限を設けておりませんので、何度でもお申し込みいただけます。<br>
<a href="http://www.furusato-tax.jp/japan/prefecture/01575" class="c-link__blank" target="_blank">感謝特典（お礼の品）の一覧</a>
            </section>
        </div>

        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">お申し込み方法について</h3>
              <p><span class="c-heading-3">WEB申込み</span><br>
              ふるさと納税ポータルサイト「ふるさとチョイス」の「寄附を申し込む」ボタンからお申し込みいただけます。<br>
              <a href="http://www.furusato-tax.jp/japan/prefecture/01575" class="c-link__blank" target="_blank">ふるさと納税ポータルサイト</a></p>
              <p>
                    <span class=" u-bold">● クレジット決済（Yahoo!公金支払い）でさらに便利になりました。</span><br>
                    お申込みから決済までの手続きをそのまま行うことができます。ぜひご利用ください。<br>
                    <span class="c-annotation">クレジット決済のご利用につきましては、5,000円以上からとなりますので、ご了承願います。</span><br>
                    <span class="c-annotation">寄附申込者と決済するかた（クレジットカードの名義人）は同一である必要がありますのでご注意ください。</span>
               </p>				
               <p><span class=" u-bold">● 郵送又はFAXで申込み郵送又はFAXをご希望の場合</span><br>
                申込書をダウンロードしてご活用ください。<br>
                <span class="c-annotation">「寄附金申込書」は「様式ダウンロード」からダウンロードください。<br><a href="#" class="c-link">「様式ダウンロード」に行く</a></span>
               </p>
               <p><span class=" u-bold">●申込書郵送先</span><br>
                〒052-0101 北海道有珠郡壮瞥町字滝之町287番地7 壮瞥町役場企画調整課あて<br>
                Tel <a href="tel:0142-66-2121">0142-66-2121</a> / Fax  0142-66-7001<br>Email <a href="mailto:kikaku@town.sobetsu.lg.jp">kikaku@town.sobetsu.lg.jp</a>
               </p>
            </section>
        </div>
        
        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">ふるさと応援寄附金（ふるさと納税）とは</h3>
              <p>ふるさと寄附金（ふるさと納税）とは、生まれ育ったまちや愛着のある地域に寄附をすると税金が軽減される制度です。
               応援したいと思う市区町村や都道府県を自由に選べて、自治体によっては寄附金の使い道を選択できたり、寄附することでそのまちの記念品や特産品をもらうことができます。</p>
            </section>
        </div>


        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">寄附金の納付方法について</h3>
              <p><span class="c-heading-3">クレジット決済の場合</span><br>
              ふるさとチョイスでのお申し込み後、ヤフー公金支払サイトでクレジット情報などをご入力ください。手数料はかかりません。<br>
              <a href="http://www.furusato-tax.jp/japan/prefecture/0157" class="c-link__blank" target="_blank">ふるさとチョイス</a>
              </p>
              
              <p><span class="c-heading-3">郵便局からの払い込みの場合</span><br>
              申込書の受領後、当町から振込用紙を送付しますので最寄のゆうちょ銀行（郵便局）で寄付金を払い込み願います。<br>
              手数料はかかりません。</p>
              
              <p><span class="c-heading-3">現金書留にて納入の場合</span><br>郵送料については、寄附をされる方のご負担となります。ご了承ください。
              寄付金のご入金を確認後、当町から「受領証明書」を郵送させていただきます。寄付控除の際に使用しますので大切に保管願います。</p>
            </section>
        </div>

        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">選べる使い道について</h3>
              <p>皆さんからいただく大切な寄附金は、以下の７つの特色ある事業へ活用させていただきます</p>
            </section>
            <div class="u-clearboth">
                <div class="u-grid__col-4">
                  <img src="images/furusato/img01.jpg" width="" height="" alt="">
                </div>
                <dl class="u-grid__col-8">
                  <dt class="c-heading-3">フィンランド・ケミヤルヴィ市との交流</dt>
                  <dd>壮瞥町では友好都市である同市に、毎年、町内の中学2年生全員を全額公費負担で研修派遣しています。町内のすべての子どもたちが差異なく、
                  若いうちに交流を通じて国際感覚を身につけてもらうことを目的としています。</dd>
                </dl>
            </div>
            <div class="u-clearboth">
                <div class="u-grid__col-4">
                  <img src="images/furusato/img02.jpg" width="" height="" alt="">
                </div>
                <dl class="u-grid__col-8">
                  <dt class="c-heading-3">おいし農作物づくり・特産品開発</dt>
                  <dd>壮瞥町の農業は、火山がもたらす肥沃な大地を活用した果樹・野菜・お米、温泉熱利用によるトマトの冬季ハウス栽培などバラエティに富んでいるのが特徴で、近年はさらに高付加価値化を図るため、農商工連携による特産品開発も進んでいます。</dd>
                </dl>
            </div>
            <div class="u-clearboth">
                <div class="u-grid__col-4">
                  <img src="images/furusato/img03.jpg" width="" height="" alt="">
                </div>
                <dl class="u-grid__col-8">
                  <dt class="c-heading-3">子育て支援施策</dt>
                  <dd>壮瞥町では、子育て世代を応援するため、子どもの医療費無料化や「ママと考えた子育て応援住宅」の建設、予防接種への助成、ブックスタート事業、子育て支援総合施設「子どもセンター」の運営などに独自に取り組んでいます。</dd>
                </dl>
            </div>
            <div class="u-clearboth">
                <div class="u-grid__col-4">
                  <img src="images/furusato/img04.jpg" width="" height="" alt="">
                </div>
                <dl class="u-grid__col-8">
                  <dt class="c-heading-3">横綱北の湖記念館</dt>
                  <dd>壮瞥町は、日本相撲協会理事長であり、昭和の大横綱･北の湖の生誕の地です。町でその偉業をたたえるため、平成4年に名誉町民章を贈呈したほか、町内に横綱北の湖記念館を建設し、その運営を行っています。
平成27年11月20日、北の湖理事長が逝去されました。相撲中継などを通し壮瞥町の名を全国に知らしめ、二度の有珠山噴火の際にも町の復興に大変なご支援をいただきました。生前のご功績に対し、敬意と感謝の念を表すとともに心よりご冥福をお祈り申し上げます。
					<p class="u-mt__small"><a href="http://www.sumo-museum.net/" class="c-link__blank" target="_blank">横綱北の湖記念館</a></p>
                  </dd>
                </dl>
            </div>
            <div class="u-clearboth">
                <div class="u-grid__col-4">
                  <img src="images/furusato/img05.jpg" width="" height="" alt="">
                </div>
                <dl class="u-grid__col-8">
                  <dt class="c-heading-3">環境･景観の保全活動</dt>
                  <dd>壮瞥町には優美な洞爺湖、噴火活動でできた奇跡の山･昭和新山、全国で３番目にきれいな星空など多様な自然･観光資源があり、それらを後世に残していくため、様々な保全活動を行っています。</dd>
                </dl>
            </div>
            <div class="u-clearboth">
                <div class="u-grid__col-4">
                  <img src="images/furusato/img06.jpg" width="" height="" alt="">
                </div>
                <dl class="u-grid__col-8">
                  <dt class="c-heading-3">火山との共生の取り組み</dt>
                  <dd>有珠山は周期的に噴火を繰り返す（20世紀４回）一方、温泉や肥沃な大地などの恵みをもたらし、平成21年には国内初の世界ジオパークに認定されました。壮瞥町では平時から町民･観光客等の安全管理の強化や普及啓発に取り組んでいます。
                  <p class="u-mt__small"><a href="http://www.toya-usu-geopark.org/" class="c-link__blank" target="_blank">洞爺湖有珠山ジオパーク</a></p>
                  </dd>
                </dl>
            </div>
            <div class="u-clearboth">
                <div class="u-grid__col-4">
                  <img src="images/furusato/img07.jpg" width="" height="" alt="">
                </div>
                <dl class="u-grid__col-8">
                  <dt class="c-heading-3">昭和新山国際雪合戦の運営･普及</dt>
                  <dd>やっかいものの雪を逆に活用し、雪国の暗い冬の生活を元気にしようと壮瞥町民が考案した冬のニュースポーツ･雪合戦。今では海外10か国でも大会が行われ、毎年2月に世界チャンピオンを決める昭和新山国際雪合戦が開催されています。
                  <p class="u-mt__small"><a href="http://www.yukigassen.jp/" class="c-link__blank" target="_blank">雪合戦実行委員会</a></p></dd>
                </dl>
            </div>
        </div>

        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">寄附金控除について</h3>
              <p>個人が2,000円を超える寄附を行ったときに住民税のおよそ2割程度が還付、控除されます。また、2015年4月1日より、確定申告が不要な給与所得者等に限り、確定申告の代わりとなる「寄附金税額控除に係る申告特例申請書」を寄附先自治体へ寄附する都度、提出（郵送）することで住民税から控除されます。<br>
              <span class="c-annotation">控除対象寄付金、控除の概要、詳細については総務省のサイトをご覧ください。</span><br>
              <a href="http://www.soumu.go.jp/main_sosiki/jichi_zeisei/czaisei/czaisei_seido/080430_2_kojin.html" class="c-link__blank" target="_blank">
              総務省ふるさと納税ポータルサイト</a>
              </p>
            </section>
        </div>

        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">くれぐれもご注意ください</h3>
              <p>①壮瞥町が寄附を強要することは一切ありません。ふるさと寄附金をかたった詐欺行為には十分注意してください。</p>
              <p>②お礼状・寄附金受領証明書の発送につきましては、お礼の品とは別にえりも町役場より発送しております。ご入金を確認してから発送するため、１か月程度お時間をいただいております。40日以上経ってもお手元に届かない　場合は、お手数ですがご連絡ください。</p>
              <p>③お礼の品につきましては、各企業から直接発送されます。希望されたお礼の品の種類によっては、発送前にメールまたは電話にてご連絡させていただく場合がございますので、日中ご連絡がつきやすい連絡先をご入力ください。</p>
              <p class=" u-mb__0">④あらかじめご了承ください
                <ul class="c-list u-mt__small u-mb__0">
                <li>お届けの日時指定はお受けしておりません。</li>
                <li>長期不在のご予定や配送曜日希望等があれば要望欄にご記入ください。<br>なお、長期不在等によりお礼の品をお受取りできなかった場合、再発送はできません。</li>
                <li>のし・包装・名入れのご希望はお受けしておりません。</li>
                <li>お申込み後のお礼の品の変更は受けかねますので、ご了承ください。</li>
                <li>ご注文の状況によっては、一時的に品切れが発生する場合があります。</li>
                <li>メーカーの都合により仕様などが変更される場合があります。</li>
                <li>色調が実物と異なる場合があります。</li>
                <li>写真は全てイメージです。小物類は商品に含まれません。</li>
                </ul>
               </p>
            </section>
        </div>
        
        <div class="u-grid__row">
            <section class="u-grid__col-12">
              <h3 class="c-heading-2">お問い合わせ先</h3>
              <p>〒052-0101　北海道有珠郡壮瞥町字滝之町287番地7壮瞥町役場企画調整課<br>Tel <a href="tel:0142-66-2121">0142-66-2121</a> / Fax 0142-66-7001<br>
              Email <a href="mailto:kikaku@town.sobetsu.lg.jp">kikaku@town.sobetsu.lg.jp</a></p>
            </section>
        </div>




      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
