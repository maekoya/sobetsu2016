<?php
// Include general settings.
require($_SERVER["DOCUMENT_ROOT"].'/sobetsu/resources/etc/config.php');

// Setting Meta data.
$page->title = 'タイトルが入ります';
$page->description = 'ディスクリプションが入ります';

// Include <head>.
include($page->root.'/resources/tpl/head.tpl');
?>
<link rel="stylesheet" media="screen,print" href="css/sub.css">
</head>




<body>
<div id="base-page">
  <?php include($page->root.'/resources/tpl/base-header.tpl'); ?>
  <div id="base-container">

    <div class="p-content-header">
      <div class="p-content-header__heading">
        <h1 class="__text">プライバシーポリシー</h1>
      </div>
      <img src="<?php echo $page->base; ?>/resources/img/_develop/dummy-5.jpg" width="1600" height="160" alt="">
    </div><!-- /.p-content-header -->

    <ul class="p-breadcrumb">
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">トップ</span></a></li>
      <li class="p-breadcrumb__item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">リンク</span></li>
    </ul>

    <div class="p-container__full__auto-margin-paragraph">

      <h2 class="c-heading-1">個人情報／プライバシーポリシー</h2>

        <div class="u-grid__row">
            <section class="u-grid__row u-grid__col-12">
              <h3 class="c-heading-2">■個人情報の取り扱い</h3>
              <p>壮瞥町ウェブサイトは、個人情報の収集・利用・管理について以下のとおり適切に取り扱います。</p>
            </section>
        </div>

        <div class="u-grid__row">
            <section class="u-grid__row u-grid__col-12">
              <h3 class="c-heading-2">■個人情報とは</h3>
              <p>壮瞥町ウェブサイトを通じて提供を受けた住所、氏名、電話番号、メールアドレスなどの個人に関する情報であり、特定の個人が識別され得るものをいいます。</p>
              <p class="c-heading-3 u-mb__0">収集する個人情報の範囲</p>
              <ul class="c-list u-mt__small">
                  <li>壮瞥町ウェブサイトを通じて個人情報を収集するときは、利用者ご本人の意思による情報の提供（登録）を原則とします。</li>
                  <li>個人情報の収集に当たっては、その収集目的を明示し、当該目的の達成のために必要な範囲内でこれを行います。</li>
                  <li>思想、信条または信教に関する個人情報及び社会的差別の原因となるおそれのある個人情報は、原則として収集いたしません。</li>
               </ul>              
            </section>
        </div>

        <div class="u-grid__row">
            <section class="u-grid__row u-grid__col-12">
              <h3 class="c-heading-2">■個人情報の管理</h3>
              <p>収集しました個人情報については厳重に管理し、漏洩、不正流用、改ざん等の防止に適切な対策を講じます。また、保有する必要のなくなった個人情報は、確実かつ速やかに消去します。</p>
            </section>
        </div>

        <div class="u-grid__row">
            <section class="u-grid__row u-grid__col-12">
              <h3 class="c-heading-2">■リンク</h3>
              <ul class="c-list">
                  <li>壮瞥町ウェブサイトのトップへはご自由にリンクしていただけます。事前の連絡は必要ありませんが、リンクを行った場合は、
                  壮瞥町役場企画調整課までリンク元のURLをご連絡ください。</li>
                  <li>リンク元のウェブサイトの内容が、法令や公序良俗に反する場合などには、リンクをお断りする場合もありますので、あらかじめご了承ください。</li>
                  <li>リンクを設定する際に、壮瞥町ウェブサイト内のページをフレーム内に表示させるリンク設定は行わないでください。なお、壮瞥町ウェブサイト内の文書等各ファイル及び
                  内容は、予告なしに変更または削除する場合がありますので、あらかじめご了承ください。</li>
               </ul>
            </section>
        </div>
        
        <div class="u-grid__row">
            <section class="u-grid__row u-grid__col-12">
              <h3 class="c-heading-2">■著作権</h3>
              <p>壮瞥町で提供するすべての情報（文章・画像・イラスト等）について、「私的私用のための複製」や「引用」等の著作権法上認められた場合を除き、壮瞥町の許可なく複製・転用・販売することはできません。</p>
            </section>
        </div>

        <div class="u-grid__row">
            <section class="u-grid__row u-grid__col-12">
              <h3 class="c-heading-2">■免責事項</h3>
              <p>壮瞥町ウェブサイトに掲載されている情報の内容に関しましては万全を期していますが、壮瞥町は利用者が当ウェブサイトの情報を用いて行う一切の行為につい て、いかなる責任も負いません。また、いかなる場合でも壮瞥町は、利用者が壮瞥町ウェブサイトにアクセスしたために被った損害、損失に対しても一切の責任 を負いません。</p>
            </section>
        </div>

        <div class="u-grid__row">
            <section class="u-grid__row u-grid__col-12">
              <h3 class="c-heading-2">■お問い合わせ</h3>
              <p>掲載内容に関することについては、担当課に直接お問い合わせください。</p>
              <p>ウェブサイト全体に関するお問い合わせは壮瞥町役場企画調整課までご連絡ください<br>（電話：<a href="tel:0142-66-2121">0142-66-2121／E-Mail：<a href="mailto:kikaku@town.sobetsu.lg.jp">kikaku@town.sobetsu.lg.jp</a>）</p>
            </section>
        </div>





      <div class="c-pagetop"><a href="#base-page">TOP</a></div>
    </div><!-- /.p-container -->

  </div><!-- /#base-container -->
  <?php include($page->root.'/resources/tpl/base-footer.tpl'); ?>
</div><!-- /#base-page -->
<?php include($page->root.'/resources/tpl/foot.tpl'); ?>
</body>
</html>
