# Install gulp globally
Install 'node-js' and 'gulp' to global.
$ npm install --global gulp

# Install module to project
$ npm install

# Run gulp task
## Develop command
$ gulp watch
or
$ gulp

## Release command
$ gulp release
